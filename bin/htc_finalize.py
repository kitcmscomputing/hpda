#!/usr/bin/python
#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
from __future__ import print_function
import sys

try:
	import hpda
except ImportError:
	# monkey patch pythonpath to default repository location
	import os
	sys.path[0] = os.path.join(os.path.dirname(os.path.realpath(__file__)),"..")
	import hpda
from hpda.utility.exceptions import ExceptionFrame
from hpda.interfaces.htcondor.router_hooks import HTCFinalizeConfigLoader

if __name__ == "__main__":
	with ExceptionFrame():
		hook = HTCFinalizeConfigLoader.configure("/etc/hpda/htcondor.cfg")[0]
		success = hook.execute()
		if not success:
			sys.exit(1)