#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
from glob import glob
from math import floor, log, sqrt, pow
from random import random
import threading
import time
import cjson
import json
from hpda.client.locator import Locator
from hpda.client.coordinator import Coordinator
from hpda.client.cacheproxy import CacheProxy
from hpda.common.database.base import database

coordinatorAPI = Coordinator("127.0.0.1:8082")
locatorAPI = Locator("http://127.0.0.1:8082")
cacheAPI = CacheProxy("http://127.0.0.1:8080")

_db = database("mysql://hpda:hpda@localhost/hpda", echo=True)
with _db.begin() as db:
	#print db.file_add("/foo/bar.root")
	print db.get_file_id(unicode("/foo/bar.root"))
	#timeout = 60 * 60 * 24 * 30 * 6
	#db.jobs_cleanup(timeout)
	#ts = time.time();
	#result = db.get_job_stats(['creation_time', 'cpu_efficiency', 'locality_rate', 'exit_code', 'memory_usage'])
	#results = [entry.values() for entry in result]
	#print cjson.encode(results)
	#print "%d" % (time.time() - ts)

"""files_per_job = 5
def send_job_requests(thread_number = 0, datasets_per_thread = 1, dataset_start = 300):

	for i in xrange(thread_number*datasets_per_thread, thread_number*datasets_per_thread+datasets_per_thread):
		dataset_files = glob('/storage/dataset' + str(dataset_start+i) + '/*.root')
		file_groups = [dataset_files[i:i+files_per_job] for i in xrange(0, len(dataset_files), files_per_job)]

		for file_group in file_groups:
			locality_rate = random()
			memory_usage = 0.5 + random() * 0.5
			min_cpu_efficiency =  0.1 + pow(locality_rate, 2) * 0.8 if (locality_rate > 0 ) else 0.1
			max_cpu_efficiency = 0.2 + sqrt(locality_rate) * 0.8
			wall_time = random() * 1000
			cpu_time = min(min_cpu_efficiency + (max_cpu_efficiency - min_cpu_efficiency) * random(), max_cpu_efficiency) * wall_time

			exit_code_rnd = random()
			exit_code = 0
			if exit_code_rnd > 0.99:
				exit_code = 1
			elif exit_code_rnd > 0.98:
				exit_code = 2


			job_id = "job_" + str(thread_number) + "_" + str(time.time())
			print "Add job %s" % (job_id)
			coordinatorAPI.job_add(job_id, "weltensurfer", file_group)
			coordinatorAPI.job_report(job_id, cpu_time, memory_usage, wall_time, exit_code, "127.0.0.1:8080", locality_rate, locality_rate)

job_threads = []
for j in xrange(1):
	job_thread = threading.Thread(target=send_job_requests, kwargs={"thread_number":j, "datasets_per_thread": 50, "dataset_start": j*50})
	job_thread.daemon = True
	job_thread.start()
	job_threads.append(job_thread)

# wait for outstanding updates to finish
for thread in job_threads:
	thread.join()
"""
file_lists = []
for i in xrange(20):
	file_list = ["/storage/dataset" + str(i+1) + "/file1.root",
			 "/storage/dataset" + str(i+1) + "/file2.root",
			 "/storage/dataset" + str(i+1) + "/file3.root",
			 "/storage/dataset" + str(i+1) + "/file4.root",
			 "/storage/dataset" + str(i+1) + "/file5.root"]
	file_list2 = ["/storage/dataset" + str(i+1) + "/file6.root",
			 "/storage/dataset" + str(i+1) + "/file7.root",
			 "/storage/dataset" + str(i+1) + "/file8.root",
			 "/storage/dataset" + str(i+1) + "/file9.root",
			 "/storage/dataset" + str(i+1) + "/file10.root"]
	file_lists.append(file_list)
	#coordinatorAPI.stage_request("weltensurfer", file_list)
	#coordinatorAPI.stage_request("weltensurfer2", file_list)
	#coordinatorAPI.stage_request("weltensurfer3", file_list)
	#coordinatorAPI.stage_request("weltensurfer", file_list2)
	#coordinatorAPI.stage_request("weltensurfer2", file_list2)

#locatorAPI.add_file_location('/home/weltensurfer/hpda/external/file6.root', '623C09DC')
#time.sleep(1)
#locatorAPI.remove_file_location('/home/weltensurfer/hpda/external/file6.root', '623C09DC', 600)

#cacheAPI.add_files([{'source_uri': "/home/weltensurfer/hpda/external/file6.root", 'score': 0}])


#print cacheAPI.get_file_info("/home/weltensurfer/hpda/external/file1.root")
#print cacheAPI.get_file_list()

file_list = ["/storage/dataset1/file1.root",
			 "/storage/dataset1/file2.root",
			 "/storage/dataset1/file3.root",
			 "/storage/dataset1/file4.root",
			 "/storage/dataset1/file5.root"]

"""for i in xrange(50):
	locality_rate = random()
	min_cpu_efficiency =  0.1 + pow(locality_rate, 2) * 0.8 if (locality_rate > 0 ) else 0.1
	max_cpu_efficiency = 0.2 + sqrt(locality_rate) * 0.8
	wall_time = random() * 1000
	cpu_time = min(min_cpu_efficiency + (max_cpu_efficiency - min_cpu_efficiency) * random(), max_cpu_efficiency) * wall_time


	print "(x, y) => (%.2f, %.2f)" %  (1.0 * cpu_time/wall_time,  locality_rate)
	for j in xrange(int(random()*20)):
		job_id = "job" + str(time.time()) + str(j)
		coordinatorAPI.job_add(job_id, "weltensurfer", file_list)
		coordinatorAPI.job_report(job_id, cpu_time, 1.0, wall_time, 0, "127.0.0.1:8080", locality_rate)

"""

#coordinatorAPI.job_add("job" + str(ts), "weltensurfer", file_list)
#coordinatorAPI.job_report("job" + str(ts), 0.5, 0.2, 0.3, 2, "localhost2", 1.0)