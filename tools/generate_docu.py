#!/usr/bin/python
#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
from __future__ import print_function
import os
import subprocess
import shutil
import sys
import argparse
import random

CLI = argparse.ArgumentParser("Create the most up-to-date HPDA sphinx HTML documentation")
CLI.add_argument(
	"-t",
	"--tmp-folder",
	default="/tmp/hpda_%X" % random.getrandbits(16),
	help="Folder in which to build documentation [%(default)s]"
)
CLI.add_argument(
	"-g",
	"--git-source",
	default="git@bitbucket.org:kitcmscomputing/hpda.git",
	help="git repository to clone [%(default)s]",
)
CLI.add_argument(
	"-p",
	"--public-html",
	default="~/public_html/public/sphinx/hpda/",
	help="folder to transfer HTML output to for publishing [%(default)s]",
)
CLI.add_argument(
    "-v",
    "--verbosity",
    action="count",
    default=0,
    help="Verbosity level [%(default)s/1]",
)

args = CLI.parse_args()
if args.verbosity:
	out_file = sys.stderr
else:
	out_file = open("/dev/null")

# check for dependencies
for package in ("cherrypy","pycurl","sqlalchemy","crontab"):
	try:
		__import__(package)
	except ImportError:
		print("Package '%s' not available! Some pages will not be built correctly!" % package, file=sys.stderr)

print("Cloning repository...", file=sys.stderr)
subprocess.check_output(["git","clone", args.git_source, args.tmp_folder, "-b", "devel"], stdin=out_file, stderr=subprocess.STDOUT)
os.chdir(args.tmp_folder)
print("Cloning dependencies...", file=sys.stderr)
subprocess.check_output(["bash","README.me"], stdin=out_file, stderr=subprocess.STDOUT)
os.chdir(os.path.join(args.tmp_folder, "sphinx"))
print("Creating API doc...", file=sys.stderr)
subprocess.check_output(["./apidoc.sh"], stdin=out_file, stderr=subprocess.STDOUT)
print("Building documentation...", file=sys.stderr)
subprocess.check_output(["make","html"], stdin=out_file, stderr=subprocess.STDOUT)
if args.public_html:
	print("Publishing documentation...", file=sys.stderr)
	subprocess.check_output(["rsync","-ap",os.path.join(args.tmp_folder, "sphinx/_build/html/"), os.path.abspath(os.path.expanduser(args.public_html))], stdin=out_file, stderr=subprocess.STDOUT)
print("Cleaning up...", file=sys.stderr)
shutil.rmtree(args.tmp_folder)