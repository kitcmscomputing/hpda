#!/usr/bin/python
#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
import ROOT
import argparse
from datetime import datetime
from time import time
import json
import urllib2
import numpy


stats_branches = {
	'cache_status': {
		'time': [int, 'time', None],
		'node_id': [int, 'node_id', None],
		'max_size': [int, 'max_size', None],
		'score_min': [float, 'score_min', None],
		'score_max': [float, 'score_max', None],
		'score_total': [float, 'score_total', None],
		'allocated': [int, 'allocated', None],
		'files_total': [int, 'files_total', None],
	},
	'jobs': {
		'creation_time': [int, 'creation_time', None],
		'owner_id': [int, 'owner_id', None],
		'node_id': [int, 'node_id', None],
		'files_amount': [int, 'files_amount', None],
		'file_size': [int, 'file_size', None],
		'wall_time': [float, 'wall_time', None],
		'cpu_time': [float, 'cpu_time', None],
		'locality_rate': [float, 'locality_rate', None],
		'cachehit_rate': [float, 'cachehit_rate', None],
		'memory_usage': [float, 'memory_usage', None],
		'exit_code': [int, 'exit_code', None],
	},
	'file_groups': {
		'score': [float, 'score', None],
		'files_amount': [int, 'files_amount', None],
		'size': [int, 'size', None],
	},
	'coordinator_timing': {
		'time': [int, 'time', None],
		'file_amount': [int, 'file_amount', None],
		'file_group_amount': [int, 'file_group_amount', None],
		'file_size_update': [int, 'file_size_update', None],
		'file_group_scores': [int, 'file_group_scores', None],
		'update_file_scores': [int, 'update_file_scores', None],
		'update_node_scores': [int, 'update_node_scores', None],
		'fetch_available_space': [int, 'fetch_available_space', None],
		'stage_requests': [int, 'stage_requests', None],
	}
}

core_parser = argparse.ArgumentParser(description="Gather stats from the Statistics API and store it into a Root TTree")
core_parser.add_argument("path", help="Path to store the ROOT file")
core_parser.add_argument("--coordinator", default="http://ekpsg03:8082", help="Coordinator URL")
cli_args = core_parser.parse_args()

api_url = cli_args.coordinator + "/coordinator/stats/"

f = ROOT.TFile(cli_args.path, "RECREATE")
branch_entries = {}
branches = {}
for branch_name, entries in stats_branches.items():
	branches[branch_name] = ROOT.TTree(branch_name, branch_name.title())
	request_params = branch_name + "?fields=" + "&fields=".join(entries.keys())
	start_time = time()
	request_handle = urllib2.urlopen(api_url + request_params, None, 600)
	print "Request %s required %.2f seconds." % ( api_url + request_params, (time() - start_time) )
	stats = json.load(request_handle)

	branch_entries[branch_name] = {}
	for entry, entry_options in entries.items():
		branch_entry = numpy.zeros(1, dtype=entry_options[0])

		root_type = 'D'
		if entry_options[0] is int:
			root_type = 'I'

		branches[branch_name].Branch(entry_options[1], branch_entry, entry_options[1]+'/' + root_type)
		branch_entries[branch_name][entry] = branch_entry

	for row in stats:

		for entry, entry_options, i in zip(entries.keys(), entries.values(), xrange(len(row))):
			value = row[i]
			if entry_options[2] is not None:
				value = entry_options[2](value)
			if value is None:
				value = -999
			branch_entries[branch_name][entry][0] = value

		branches[branch_name].Fill()

f.Write()
f.Close()