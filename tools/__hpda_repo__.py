#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
Ensure import'ability of main repository
"""
try:
	import hpda
except ImportError:
	import os, sys
	sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)),".."))
	import hpda

# create basic logging config
from hpda.utility.report.configuration import logger_factory, stream_handler_factory

logger_factory(logger='',minimum_level="DEBUG",propagate=True)
stream_handler_factory(handler='stderr', logger='',minimum_level="DEBUG",destination='stderr')