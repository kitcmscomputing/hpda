#!/usr/bin/python
#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
import argparse
from getpass import getuser
from glob import glob
from time import sleep

import __hpda_repo__
from hpda.client.coordinator import Coordinator
from hpda.client.locator import Locator

core_parser = argparse.ArgumentParser(description="Handles Stage Requests")
core_parser.add_argument("path", help="Path of the Dataset")
core_parser.add_argument("--status", action='store_true', default=False, help="Only print Stage Status")
core_parser.add_argument("--files_per_job", default=1, help="Amount of files per job")
core_parser.add_argument("--coordinator", default="http://ekpsg03:8082", help="Coordinator URL")
core_parser.add_argument("--locator", default="http://ekpsg03:8081", help="Locator URL")
cli_args = core_parser.parse_args()

dataset_files = glob(cli_args.path)
files_per_job = int(cli_args.files_per_job)
file_groups = [dataset_files[i:i+files_per_job] for i in xrange(0, len(dataset_files), files_per_job)]

if not cli_args.status:
	coordinator_client = Coordinator(cli_args.coordinator)
	for file_group in file_groups:
		print "Send Stage Request for File Group:"
		print file_group
		coordinator_client.stage_request(getuser() + "@physik.uni-karlsruhe.de", file_group)
else:
	try:
		file_groups_staged = 0
		locator_client = Locator(cli_args.locator)
		for file_group in file_groups:
			for node_file_count in locator_client.get_file_count_on_node(file_group).values():
				if node_file_count == len(file_group):
					file_groups_staged += 1

		print "File Groups Staged: %d%%" % (100.0*file_groups_staged/len(file_groups)),
	except (KeyboardInterrupt, SystemExit):
		pass