
function generateBinning(data, options)
{
    var settings = $.extend({
        x_bins: 100,
        x_min: 0.0,
        x_max: 1.0,
        y_bins: 100,
        y_min: 0.0,
        y_max: 1.0
    }, options);

    var x_step = (settings.x_max - settings.x_min) / settings.x_bins;
    var y_step = (settings.y_max - settings.y_min) / settings.y_bins;

    var bins = [];
    for(var i=0; i < settings.x_bins; i++)
    {
        bins[i] = [];
        for (var j=0; j < settings.y_bins; j ++)
        {
            bins[i][j] = 0;
        }
    }

    var max_value = 0;
    for(var k=0; k < data.length; k++)
    {
        if (data[k][0] < settings.x_min || data[k][0] > settings.x_max) continue;
        if (data[k][1] < settings.x_min || data[k][1] > settings.x_max) continue;

        var x_bin = Math.max(Math.ceil((data[k][0] - settings.x_min) / x_step), 1);
        var y_bin = Math.max(Math.ceil((data[k][1] - settings.y_min) / y_step), 1);

        bins[x_bin-1][y_bin-1] += 1;
        if (bins[x_bin-1][y_bin-1] > max_value)
        {
            max_value = bins[x_bin-1][y_bin-1];
        }
    }

    var result = [];
    for (var l=0; l<=max_value; l++)
    {
        result[l] = [];
    }

    for (var n=0; n<bins.length; n++)
    {
        for (var m=0; m<bins[n].length; m++)
        {
            if (bins[n][m] > 0)
            {
                result[bins[n][m]].push([n*x_step, m*y_step]);
            }
        }
    }

    return result;
}


function Interpolate(start, end, steps, count) {
    var s = start,
        e = end,
        final = s + (((e - s) / steps) * count);
    return Math.floor(final);
}

function Color(_r, _g, _b) {
    var r, g, b;
    var setColors = function(_r, _g, _b) {
        r = _r;
        g = _g;
        b = _b;
    };

    setColors(_r, _g, _b);
    this.getColors = function() {
        var colors = {
            r: r,
            g: g,
            b: b
        };
        return colors;
    };
}

var options = {
        chart: {
            type: 'scatter',
            zoomType: "xy"
        },
        title: {
            text: 'Locality-CPU-Efficiency-Plot '
        },
        plotOptions: {
            scatter: {
                marker: {
                    radius: 2,
                    symbol: 'circle'
                }
            }
        },
        series: [{}],
        legend: {
            enabled: false
        },
        xAxis: {
            title: {
                text: 'CPU Efficiency'
            },
            min: 0.0,
            max: 1.0
        },
        yAxis: {
            title: {
                text: 'Locality Rate'
            },
            min: 0.0,
            max: 1.0
        }
    };
var startColors = new Color(6, 170, 60).getColors();
var endColors = new Color(232, 9, 26).getColors();

function loadPlot()
{
    $.getJSON('coordinator/stats/jobs?fields=cpu_efficiency&fields=locality_rate', function(data) {

        binnedData = generateBinning(data, {x_bins: 100, y_binx: 100});
        var series = [];
        for (var i=0; i < binnedData.length; i++)
        {
            var r = Interpolate(startColors.r, endColors.r, binnedData.length, i);
            var g = Interpolate(startColors.g, endColors.g, binnedData.length, i);
            var b = Interpolate(startColors.b, endColors.b, binnedData.length, i);

            series.push({
                color: 'rgba(' + r + ',' + g + ',' + b + ',1.0)',
                data: binnedData[i]
            });
        }

        options.series = series;
        $('#container').highcharts(options);
    });
}

$(document).ready(function() {

    $('#requestPlot').on('click', function () {
        loadPlot();
    });
    loadPlot();
});
