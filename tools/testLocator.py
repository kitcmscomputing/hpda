#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
import random

from hpda.client import locator
from hpda.cache.rest_api import FileListSchema


locatorAPI = locator.Locator("127.0.0.1:8081")

print "Get all nodes from the locator:"
print locatorAPI.get_nodes()
print "."
print "Get the fileList for node \"623C09DC\" from the locator:"
fileList = locatorAPI.get_node("623C09DC")
print  fileList
print "."
if FileListSchema.validate(fileList):

	print "Generate a random list of files to request from the locator:"
	requestFiles = []
	while len(requestFiles) < 5 and len(fileList['files']) > 0:
		chosenFile = random.choice(fileList['files'])
		fileList['files'].remove(chosenFile)
		requestFiles.append(chosenFile)

	print requestFiles
	print "."
	print "Request nodes which have the requested files cached:"
	print locatorAPI.get_file_count_on_node(requestFiles)

print "Add file locations"
locatorAPI.add_file_location('/home/bla/blub1', "623C09DC")
locatorAPI.add_file_location('/home/bla/blub2', "623C09DC")
locatorAPI.add_file_location('/home/bla/blub3', "623C09DC")
locatorAPI.add_file_location('/home/bla/blub4', "623C09DC")
locatorAPI.add_file_location('/home/bla/blub5', "623C09DC")

print "Remove file locations"
locatorAPI.remove_file_location('/home/bla/blub5', "623C09DC")

#print "get File count"
#print locatorAPI.get_file_count_on_node(requestFiles)

