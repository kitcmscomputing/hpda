#!/usr/bin/python
#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
from __future__ import print_function
import sys
import __hpda_repo__
# standard library imports
import argparse

# third party imports

# application/library imports
from hpda.utility.configuration.container import Configuration
from hpda.utility.extensions import ExtendAction

main_parser = argparse.ArgumentParser(
	"Check, concatenate and pretty-print a set of configurations",
)
main_parser.add_argument(
	"config",
	help="File(s) containing a configuration",
	action  = ExtendAction,
	nargs   = "+",
)
main_parser.add_argument(
	"-o","--output-file",
	help="A file to print new config to",
	default = sys.stdout
)

def pretty_print(*config_sources, **kwargs):
	cfg = Configuration(*config_sources)
	print(cfg.dumps(), file=kwargs.get("destination",sys.stdout))

if __name__ == "__main__":
	args = main_parser.parse_args()
	pretty_print(*args.config, destination=args.output_file)