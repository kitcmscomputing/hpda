#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
from glob import glob
from random import random
import time
import ROOT
import numpy
from hpda.client.cacheproxy import CacheProxy
from hpda.utility.rest_client import ServerError

cacheAPI = CacheProxy("http://127.0.0.1:8080")
files_per_job = 5

f = ROOT.TFile('cache_test.root', "RECREATE")
tree = ROOT.TTree('cache', 'cache')
files_in_cache = numpy.zeros(1, dtype=int)
required_time = numpy.zeros(1, dtype=float)

tree.Branch('files_in_cache', files_in_cache, 'files_in_cache/I')
tree.Branch('required_time', required_time, 'required_time/D')

try:
	files_updated = 0
	for j in xrange(1, 1000):
		dataset_files = glob('/storage/dataset' + str(j) + '/*.root')
		for i in xrange(0, len(dataset_files), files_per_job):
			file_groups = []
			file_group_score = random() * 1000
			file_group_entries = [{"source_uri": path, "score": file_group_score} for path in dataset_files[i:i+files_per_job]]
			file_groups.append(file_group_entries)

			start_time = time.time()
			try:
				print "Send request"
				cacheAPI.stage_file_groups(file_groups)
			except ServerError:
				print "Server Error -> break"
				break
			duration = time.time() - start_time

			print "Stage request with %d  cached files required %f seconds." % (files_updated, duration)

			files_in_cache[0] = files_updated
			required_time[0] = duration
			tree.Fill()

			files_updated += files_per_job
except Exception:
	pass


f.Write()
f.Close()