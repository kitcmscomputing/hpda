#!/usr/bin/python
#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.

import subprocess
import uuid
import itertools
import time
import multiprocessing

import os
import sys

hpda_base = os.path.dirname(os.path.realpath(sys.argv[0])) + "/../"
hpda_base = os.path.abspath(hpda_base)
rest_client = hpda_base + "/RestClient/src/python/"

sys.path = sys.path + [ rest_client ]

# Benchmark Settings
fileSize = 10
fileCount = 20
maxChunk = 20

from hpda.RestClient.RestApi import RestApi
from hpda.client.cacheproxy import CacheProxy

def genRandomFile( (filename, sizeInMB) ):
	subprocess.call( "dd if=/dev/urandom of=" + filename + " bs=1M count=" + str(sizeInMB), shell = True )
	print "File " + filename + " done"

def generatingRandomFiles ( count, sizeInMB ):

	genPool = multiprocessing.Pool( 4 )
	filenames = map ( lambda x : os.getcwd() + "/testfile_" + str( uuid.uuid4() ) + ".bin", range( count))
	genPool.map( genRandomFile, zip(filenames, itertools.repeat( sizeInMB) ))

	return filenames

def chunks(l, n):
    """ Yield successive n-sized chunks from l.
    """
    for i in xrange(0, len(l), n):
        yield l[i:i+n]

def postStageFile( callData ):
	url = "http://127.0.0.1:8080"
	cacheApi = CacheProxy(url)
	restApi = RestApi()
	api = "catalogue/"

	print "Staging files: "
	print cacheApi.add_files(callData)
	sys.exit(0)

def getStagedFiles ():
	restApi = RestApi()
	url = "http://127.0.0.1:8080"
	cacheApi = CacheProxy(url)

	return cacheApi.get_file_list()

	respo = restApi.get(url,api )
	sys.exit(0)

def main():

	# generate random file
	print "Generating " + str(fileCount) + " random files ..."
	testfiles = generatingRandomFiles( fileCount, fileSize )
	print "... done"

	print "Staging Test files"

	apiCallPool = multiprocessing.Pool( 4 )

	callData = chunks(testfiles, maxChunk)
	apiCallPool.map( postStageFile, callData )

	print "Staged", len(testfiles), "in", len(callData), "chunks,", str(fileSize*fileCount), "MB in total"

	while True:
		cachedFiles = getStagedFiles()
		if not cachedFiles == None:
			isect = set(cachedFiles).intersection(testfiles)
		else:
			isect = set()

		print "Files in cache: [ " + str(len(isect)) + " / " + str(fileCount) + " ]"

		if len(isect) == fileCount:
			print " > All files staged"
			break

		time.sleep(5)

	#print "removing test files ..."
	#for tf in testfiles:
	#	os.remove( tf)
	#print "... done"

if __name__ == "__main__":
    main()
