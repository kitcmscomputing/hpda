#!/usr/bin/python
#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
Helper script for creating an HPDA cache mount using a union filesystem
"""
from __future__ import print_function
# standard library imports
import os, sys
import argparse
import subprocess
import time

# third party imports

# application/library imports
import __hpda_repo__
from hpda.utility.exceptions import ExceptionFrame
from hpda.utility.extensions import ExtendAction
from hpda.utility.caching import hashkey

# Helpers
############
class Process(subprocess.Popen):
	def __init__(self, *cmds):
		subprocess.Popen.__init__(
			self,
			cmds,
			stdout=subprocess.PIPE,
			stderr=subprocess.PIPE,
			universal_newlines = True,
			)
		self._proc = cmds[0]
		self.start = time.time()

	def wait(self, timeout = -1, kill = True):
		"""Get the process status, waiting for termination"""
		if not timeout > 0:
			return subprocess.Popen.wait(self)
		while timeout > ( time.time() - self.start ) and self.poll() is None :
			time.sleep(1)
		if kill and timeout < ( time.time() - self.start ):
			self.kill()
		return self.poll()

	def report(self):
		if self.poll() != 0:
			return "Process '%s' did not succeed\n== stdout ==\n%s\n\n== stderr ==\n%s"%(self._proc, "\n".join(self.stdout.readlines()), "\n".join(self.stderr.readlines()))
		return "Process '%s' succeeded\n== stdout ==\n%s\n\n== stderr ==\n%s"%(self._proc, "\n".join(self.stdout.readlines()), "\n".join(self.stderr.readlines()))

class PrintProcess(object):
	pass

# union types
class UnionMount(object):
	mount_type = "<no name set>"
	def __init__(self, mount_dir, source_dir, cache_dirs, dry_run=False):
		self.mount_dir  = mount_dir
		self.source_dir = source_dir
		self.cache_dirs = cache_dirs
		self.dry_run = dry_run

	@classmethod
	def available(cls):
		# check mount support
		for type_source in ("/etc/filesystems", "/proc/filesystems"):
			if os.path.exists(type_source):
				with open(type_source,'r') as fslist:
					for line in fslist:
						if line.rpartition(" ")[2].strip() == cls.mount_type:
							return True
						if line.rpartition("\t")[2].strip() == cls.mount_type:
							return True
		return False

	def get_cache_path(self, cache_base):
		source_base = os.path.splitdrive(self.source_dir)[1][1:]
		return os.path.join(cache_base, source_base)

	def mount(self):
		"""
		Mount the union cache
		"""
		command = self._get_mount()
		if self.dry_run:
			print(" ".join(command))
		else:
			mnt_proc = Process(*command)
			if mnt_proc.wait(20) != 0:
				raise OSError(mnt_proc.report())

	def unmount(self):
		"""
		Mount the union cache
		"""
		command = self._get_unmount()
		if self.dry_run:
			print(" ".join(command))
		else:
			mnt_proc = Process(*command)
			if mnt_proc.wait(20) != 0:
				raise OSError(mnt_proc.report())

	def fstab(self):
		"""
		Create mount directive for fstab
		"""
		print(self._get_fstab())

	def mkdir(self):
		"""
		Create mount directive for fstab
		"""
		command = self._get_mkdir()
		if self.dry_run:
			print(" ".join(command))
		else:
			proc = Process(*command)
			if proc.wait(20) != 0:
				raise OSError(proc.report())

	def _get_mount(self):
		raise NotImplementedError
	def _get_unmount(self):
		return "umount", self.mount_dir
	def _get_fstab(self):
		raise NotImplementedError
	def _get_mkdir(self):
		return ["mkdir", "-p", self.source_dir, self.mount_dir] + [self.get_cache_path(cdir) for cdir in self.cache_dirs]

	def __repr__(self):
		return "%s(mount_dir=%r, source_dir=%r, cache_dirs=%r)" % (self.__class__.__name__, self.mount_dir, self.source_dir, self.cache_dirs)

class AufsMount(UnionMount):
	mount_type = "aufs"
	def _branch_opt_str(self):
		"""
		Format directories as branch mount option

		:return: branch mount options
		:rtype: str
		"""
		return "br=%s:%s"%(
			":".join("%s=ro"% self.get_cache_path(cdir) for cdir in self.cache_dirs),
			"%s=rw"%self.source_dir,
		)
	def _xino_opt_str(self):
		"""
		Create an E**x**ternal **Ino**de file path; no file is actually created

		:return: path to xino file
		"""
		return "xino=/tmp/.hpda_%s_%s_%s.aufs.xino"%(hashkey(str(self.mount_dir)), hashkey(str(self.source_dir)), hashkey(str(self.cache_dirs)))

	def _get_mount(self):
		return (
			"mount",
			"-t", self.mount_type,
			"-o", "noatime",
			"-o", self._branch_opt_str(),
			"-o", self._xino_opt_str(),
			"none", # aufs has no attached device
			self.mount_dir
		)

	def _get_fstab(self):
		return "%(device)s\t%(mount_dir)s\t%(fs_type)s\t%(options)s\t%(dump)s\t%(check)s" %{
			"device" : "none",
			"mount_dir" : self.mount_dir.replace(" ", "\\040"),
			"fs_type" : self.mount_type,
			"options" : ",".join(("noatime",self._branch_opt_str(), self._xino_opt_str())),
			"dump" : "0",
			"check" : "0",
			}

_arg_mounts = {
	None : UnionMount,
	AufsMount.mount_type : AufsMount,
}
_def_mount = None

for ufs in (AufsMount,):
	if ufs.available():
		_def_mount = ufs.mount_type
		break

# Config Templates
##################

CacheTemplate = """
[cache.%(name)s]
sequence   = %(cache_order)d
uri prefix = %(cache_dir)s
size limit = 0.95
"""

StorageTemplate = """
[storage.%(name)s]
sequence   = %(source_order)d
uri prefix = %(target_dir)s
raw mount  = %(source_dir)s
"""

# CLI parser
############

main_parser = argparse.ArgumentParser(
	description="Create an HPDA cache union mount",
	epilog="See 'man hpda_tools' or the hpda documentation for details",
	formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)

action_parser = main_parser.add_argument_group("Output Options")
action_parser.add_argument(
	"--action",
	action=ExtendAction,
	nargs   = "+",
	choices=("mount","umount","fstab","config", "mkdir"),
	default=["fstab"],
	help="action to perform"
)
action_parser.add_argument(
	"-d","--dry-run",
	action = "store_true",
	help = "Do not perform any operation; print corresponding commands to stdout"
)

mount_parser = main_parser.add_argument_group("Directory Options")
mount_parser.add_argument(
	"-c","--cache-dir",
	action  = ExtendAction,
	nargs   = "+",
	default =["/dev/shm/hpda_test/cache"],
	help = "a directory to use as a cache"
)
mount_parser.add_argument(
	"-s","--source-dir",
	action  = ExtendAction,
	nargs   = "+",
	default =["/tmp/hpda_test/source"],
	help = "a directory to use as a source"
)
mount_parser.add_argument(
	"-m","--mount-dir",
	default = r"%(prefix)s/%(sourcebase)s",
	help = "path segments at which to provide the cache"
)
mount_parser.add_argument(
	"-p","--mount-prefix",
	default ="",
	help = "path for use in the mount dir segments",
)
mount_parser.add_argument(
	"-t","--mount-type",
	default =_def_mount,
	choices = _arg_mounts.keys(),
	help = "type of union mount to use"
)

if __name__ == "__main__":
	with ExceptionFrame():
		args = main_parser.parse_args()
		if args.mount_type is None and args.action in ["mount", "fstab"]:
			print("Creating a cache mount requires a mount type", file=sys.stderr)
			sys.exit(1)
		mounts = []
		for source_dir in args.source_dir:
			mount_dir = args.mount_dir%{"prefix":args.mount_prefix,"sourcebase":os.path.basename(source_dir),"sourcedir":os.path.dirname(source_dir)}
			mounts.append(_arg_mounts[args.mount_type](
				mount_dir=mount_dir,
				source_dir=source_dir,
				cache_dirs=args.cache_dir,
				dry_run=args.dry_run
			))
		if "mkdir" in args.action:
			print(" >>> mkdir", file=sys.stderr)
			for union_mount in mounts:
				union_mount.mkdir()
		if "mount" in args.action:
			print(" >>> mount", file=sys.stderr)
			for union_mount in mounts:
				union_mount.mount()
		if "umount" in args.action:
			print(" >>> umount", file=sys.stderr)
			for union_mount in mounts:
				union_mount.unmount()
		if "fstab" in args.action:
			print(" >>> fstab", file=sys.stderr)
			for union_mount in mounts:
				union_mount.fstab()
		if "config" in args.action:
			print(" >>> config", file=sys.stderr)
			for order, cache_dir in enumerate(args.cache_dir):
				print(CacheTemplate%{"name":cache_dir.replace(".","-dot-"), "cache_dir":cache_dir, "cache_order":(len(args.cache_dir)-order)*1})
			for order, source_dir in enumerate(args.source_dir):
				mount_dir = args.mount_dir%{"prefix":args.mount_prefix,"sourcebase":os.path.basename(source_dir),"sourcedir":os.path.dirname(source_dir)}
				print(StorageTemplate%{"name":source_dir.replace(".","-dot-"),"source_dir":source_dir, "source_order":(len(args.source_dir)-order)*1, "target_dir":mount_dir})

