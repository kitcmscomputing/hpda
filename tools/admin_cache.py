#!/usr/bin/python
#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
Interface to an HPDA cache
"""
from __future__ import print_function
# standard library imports
import argparse
from collections import Counter, deque
import multiprocessing.pool
import time

# third party imports

# application/library imports
import __hpda_repo__
from hpda.utility.exceptions import ExceptionFrame
from hpda.utility.utils import Progress
from hpda.client.cacheproxy import CacheProxy
from hpda.client.node import NodeProxy

main_parser = argparse.ArgumentParser(description="Administrate an HPDA cache")

node_parser = main_parser.add_argument_group("Node Options")
node_parser.add_argument(
	"-a","--address",
	help="address of the node we connect to",
	default="http://localhost:8080"
)
node_parser.add_argument(
	"-p","--proxy",
	help="query node at ADDRESS for known nodes of our type",
	action="store_true"
)

cache_parser = main_parser.add_argument_group("Cache Options")
cache_parser.add_argument(
	"--stage",
	nargs="*",
	help="stage files",
)
cache_parser.add_argument(
	"--delete",
	nargs="*",
	help="delete files",
)
cache_parser.add_argument(
	"--list",
	help="list all staged files",
	action="store_true"
)
cache_parser.add_argument(
	"--info",
	nargs="*",
	help="show information on specific files; use '*' to show all",
)
cache_parser.add_argument(
	"--short",
	help="Modifies all options to only show a summary for each section",
	action="store_true"
)
tune_parser = main_parser.add_argument_group("Tuning Options")
tune_parser.add_argument(
	"--query-threads",
	help="maximum threads to use for querying",
	default=multiprocessing.cpu_count(),
	type=int,
)


def generate_async(func, allargs, threads=1, buffer=0):
	"""
	Asnychronously generate the results from calling ``func`` with ``allargs``

	:param func: the function to call
	:param allargs: iterable of tuples of ``(*args, **kwargs)``
	:param threads: maximum number of threads to use
	:param buffer: maximum number of results to accumulate before they are consumed
	"""
	if buffer < threads:
		buffer = int(threads*1.5)
	pool = multiprocessing.pool.ThreadPool(processes=threads)
	results = deque()
	def get_result():
		"""Loop over outstanding results, returning the first to be available"""
		while results:
			result = results.popleft()
			try:
				return result.get(0.001)
			except multiprocessing.TimeoutError:
				results.append(result)
	for args, kwargs in allargs:
		results.append(pool.apply_async(func, args=args, kwds=kwargs))
		time.sleep(0.01)
		if len(results) < buffer:
			continue
		yield get_result()
	while results:
		yield get_result()


def main(address="http://localhost:8080", proxy=False, stage=None, delete=None, list=False, info=None, short=False, query_threads=1):
	with ExceptionFrame():
		if proxy:
			caches = [ CacheProxy(node.url) for node in NodeProxy(address).get_nodes("CACHE") ]
			print("%r:\tknows %r"%(address, caches))
		else:
			caches = [CacheProxy(address)]
		if stage:
			for cache in caches:
				reply = cache.insert_files([{'source_uri': source_uri} for source_uri in stage])
				if short:
					print("%r:\tstage %d/%d"%(cache, len(reply), len(stage)))
				else:
					print("%r:\tstage %r"%(cache, reply))
		if delete:
			for cache in caches:
				delete_list = delete
				if "*" in delete_list:
					delete_list = cache.get_file_list()
				if delete_list:
					reply = cache.unlink_files(*delete_list)
					unlinked = set(delete_list) - set(reply)
				else:
					reply, unlinked = [], []
				if short:
					print("%r:\tunlink %d/%d"%(cache, len(delete_list)-len(reply), len(delete_list)))
				else:
					print("%r:\tunlink %r"%(cache, unlinked))
		if list:
			for cache in caches:
				reply = cache.get_file_list()
				if short:
					print("%r:\thosts %d files"%(cache, len(reply)))
				else:
					print("%r:\thosts %r"%(cache, reply))
		if info:
			for cache in caches:
				info_list = info
				if "*" in info_list:
					info_list = cache.get_file_list()
				file_infos = generate_async(cache.get_file_info, allargs=(((uri,), {}) for uri in info_list), threads=query_threads)
				got_list = set()
				if not short:
					for file_info in file_infos:
						print("%r:\thosts %r"%(cache, file_info))
				else:
					names = ("assigned [#]", "assigned [B]", "cache [#]", "cache [B]", "store [#]", "store [B]")
					links_counts = Counter()
					links_size = Counter()
					cache_counts = Counter()
					store_counts = Counter()
					cache_size = Counter()
					store_size = Counter()
					progress = Progress(name="Querrying", maximum=len(info_list), bar_length=50)
					for file_info in file_infos:
						progress.step()
						reply = file_info
						got_list.add(reply["source_uri"])
						link_key = "%s=>%s"%(reply["storage"],reply["cache"])
						links_counts[link_key] += 1
						links_size[link_key] += reply["size"]
						cache_counts[reply["cache"]] += 1
						cache_size[reply["cache"]] += reply["size"]
						store_counts[reply["storage"]] += 1
						store_size[reply["storage"]] += reply["size"]
					print()
					print("Total: %d"%len(info_list))
					for idx, summary in enumerate((links_counts, links_size, cache_counts, cache_size, store_counts, store_size)):
						print(names[idx])
						for key in summary:
							print("%r:\thosts %s : %s"%(cache, key, summary[key]))

if __name__ == "__main__":
	args = main_parser.parse_args()
	main(**args.__dict__)
