#!/bin/bash

### Debian package information
# ----------------------------
### BEGIN INIT INFO
# Provides:          hpdad
# Required-Start:    $ALL
# Default-Start:     3 5
# Default-Stop:      0 1 2 6
# Short-Description: HPDA Node Daemon
# Description:       Daemon of local node of the HPDA distributed cache
#	Manages a node of the HPDA distributed cache pool.
### END INIT INFO

### System configuration settings
# ------------------
# Ensure that the following settings match your local installation. All
# but SYSCONFIG may be set in the "$SYSCONFIG/hpda" file, which will
# overwrite *all* settings in this file.

# System configuration directory
# SYSCONFIG=/etc/sysconfig/

# This script configuration file
# INITD_CONFIG=/etc/sysconfig/hpda

# The application configuration files
# APP_CONFIG=/etc/hpda/config.d/*.cfg

# The application location
# HPDA_APP=

# Executable for python version 2.7
# PYTHON27=

### No settings to be manually edited after this point
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

_APP_NAME="HPDA"

# helper functions
function _show_usage
{
	echo "usage:"
	echo "  service $(basename $BASH_SOURCE '.sh') command [options]"
	echo "  $BASH_SOURCE command [options]"
}
function _show_help
{
	_show_usage
	echo
	echo "commands:"
	echo "  start         Start service"
	echo "  stop          Stop service."
	echo "  restart       If the service is running, stop it and then restart it."
	echo "                If it is not running, start it."
	echo "  reload        Reload the configuration without stopping and restarting"
	echo "                the service."
	echo "  force-reload  Reload the configuration if the service supports this."
	echo "                Otherwise, do the same as if restart had been given."
	echo "  status        Show current status of service."
	echo
	echo "options:"
	echo "  -h|--help     Show this help and exit"
	echo "  -v|--verbose  Show runtime information of this script"
	echo "  -vv           Same as '-v', but add runtime information from the node"
	echo "  -vvv          Same as '-vv', but add full runtime information from the node"
	echo
	echo "Since the service does not support reconfiguration, the command"
	echo "'reload' will always fail and 'force-reload' is an alias to the"
	echo "'restart' command."
}
function _guess_py27
{
	# find python version 2.7
	# always trust explicitly set version
	if [[ -n "$PYTHON27" ]]; then
		[[ "$VERBOSITY" -ge 1 ]] && _done "Using PYTHON27 '$PYTHON27'"
		return 0
	fi
	# default installation locations
	for candidate in "python" "/usr/local/bin/python2.7" "/usr/bin/python2.7"; do
		if [[ $(${candidate} --version 2>&1 | grep " 2.7.") ]]; then
			PYTHON27=${candidate}
			[[ "$VERBOSITY" -ge 1 ]] && _done "Guessed PYTHON27 '$PYTHON27'"
			return 0
		fi
	done
	# RedHat 6
	if [[ -d "/opt/rh/python27/root" ]] && [[ -f "/opt/rh/python27/enable" ]]; then
		source /opt/rh/python27/enable && PYTHON27="python"
		[[ "$VERBOSITY" -ge 1 ]] && _done "Guessed PYTHON27 '$PYTHON27' (from '/opt/rh/python27/')"
		return 0
	fi
	false
}

# convenience functions
function _info
{
	echo "[ INFO  ]" $@
}
function _done
{
	echo "[SUCCESS]" $@
}
function _fail
{
	echo "[>ERROR<]" $@
}
function _fatal
{
	echo "[ FATAL ]" $1
	exit ${2:-1}
}

# reap CLI arguments
VERBOSITY=0
INIT_COMMAND=$1
if [[ -z "$INIT_COMMAND" ]] || [[ "$INIT_COMMAND" == '-'* ]]; then
	_show_help
	exit 1
fi
shift 1
while [[ $1 ]]; do
	case $1 in
		'-h'|'--help')
			_show_help
			exit 0
			;;
		'-v'|'--verbose')
			VERBOSITY=$(( $VERBOSITY + 1 ))
			shift 1
			;;
		'-vv')
			VERBOSITY=$(( $VERBOSITY + 2 ))
			shift 1
			;;
		'-vvv')
			VERBOSITY=$(( $VERBOSITY + 3 ))
			shift 1
			;;
		*)
			echo "Unknown CLI argument:"
			echo $1
			echo
			_show_help
			exit 1
			;;
	esac
done

[[ "$VERBOSITY" -ge 1 ]] && _info "Using verbosity level $VERBOSITY/3"
# set up pseudo pipes for daemon stdout/stderr
exec 3>/dev/null
exec 4>/dev/null
if [[ $VERBOSITY -ge 2 ]]; then
	exec 3>&1
	exec 4>&2
fi

# load settings from sysconfig
if [[ -z "$SYSCONFIG" ]]; then
	SYSCONFIG="/etc/sysconfig/"
fi
if [[ -z "$INITD_CONFIG" ]]; then
	INITD_CONFIG=/etc/sysconfig/hpda
fi
if [[ -f "$INITD_CONFIG" ]] && source "$INITD_CONFIG"; then
	[[ "$VERBOSITY" -ge 1 ]] && _done "Loading INITD_CONFIG '$INITD_CONFIG'"
else
	[[ "$VERBOSITY" -ge 1 ]] && _fail "Loading INITD_CONFIG '$INITD_CONFIG'"
fi

if [[ -z "$HPDA_APP" ]]; then
	# guess application location
	if [[ $(which hpda_node.py 2>/dev/null 1>&2) ]]; then
		HPDA_APP=$(which hpda_node.py)
		[[ "$VERBOSITY" -ge 1 ]] && _done "Guessed HPDA_APP '$HPDA_APP'"
	elif [[ -x "/opt/hpda/bin/hpda_node.py" ]]; then
		HPDA_APP="/opt/hpda/bin/hpda_node.py"
		[[ "$VERBOSITY" -ge 1 ]] && _done "Guessed HPDA_APP '$HPDA_APP'"
	elif [[ -d "/opt/hpda/repo/hpda" ]] && _guess_py27; then
		HPDA_APP="${PYTHON27} /opt/hpda/repo/hpda"
		[[ "$VERBOSITY" -ge 1 ]] && _done "Guessed HPDA_APP '$HPDA_APP'"
	fi
fi

if [[ -z "$HPDA_APP" ]]; then
	[[ "$VERBOSITY" -ge 1 ]] && _info "No HPDA_APP defined"
	[[ "$VERBOSITY" -ge 1 ]] && _fail "HPDA_APP from \$PATH via 'which'"
	[[ "$VERBOSITY" -ge 1 ]] && _fail "HPDA_APP from '/opt/hpda/bin/hpda_node.py'"
	[[ "$VERBOSITY" -ge 1 ]] && _fail "HPDA_APP from '/opt/hpda/repo/hpda' with python2.7"
	_fatal "HPDA application not found" 127
fi
[[ "$VERBOSITY" -ge 1 ]] && _info "Using HPDA_APP '$HPDA_APP'"

HPDA_ARGS=()
if [[ -n "$APP_CONFIG" ]]; then
	HPDA_ARGS+=("-c")
	HPDA_ARGS+=("$APP_CONFIG")
fi
if [[ "$VERBOSITY" -ge 3 ]]; then
	HPDA_ARGS+=("--log-level")
	HPDA_ARGS+=("DEBUG")
fi
[[ "$VERBOSITY" -ge 1 ]] && _info "Using HPDA_ARGS '${HPDA_ARGS[@]}'"

# core functions
function _start
{
	$HPDA_APP ${HPDA_ARGS[@]} --daemon start 1>&3 2>&4
	if [[ $? -eq 0 ]]; then
		_done "daemon start"
	else
		_fail "daemon start"
	fi
}
function _stop
{
	$HPDA_APP ${HPDA_ARGS[@]} --daemon stop 1>&3 2>&4
	if [[ $? -eq 0 ]]; then
		_done "daemon stop"
	else
		_fail "daemon stop"
	fi
}
function _status
{
	$HPDA_APP ${HPDA_ARGS[@]} --daemon status
}
function _restart
{
	$HPDA_APP ${HPDA_ARGS[@]} --daemon restart 1>&3 2>&4
	if [[ $? -eq 0 ]]; then
		_done "daemon restart"
	else
		_fail "daemon restart"
	fi
}

if [ -z "$INIT_COMMAND" ]; then
  _show_help
  exit 1
fi

case "$INIT_COMMAND" in
	'start')
		_start
		;;
	'stop')
		_stop
		;;
	'status')
		_status
		;;
	'restart')
		_restart
		;;
	'reload')
		# we don't support re-configuration
		false
		;;
	'force-reload')
		# we don't support re-configuration
		_restart
		;;
	*)
		echo "Unknown command $INIT_COMMAND: use --help for help"
		exit 1
		;;
esac
