

``-h, --help``
  Print a help message to stderr and exit

``-c CONFIG [CONFIG ...], --config CONFIG [CONFIG ...]``
  Configuration file(s) to use for the node. Files are digested sequentially in
  the order as given in this option; later files may overwrite or modify any
  settings from previous files.

``--init-database``
  Initialize a new database if none exists. This is intended as a safeguard against
  silently ignoring an existing database. This option only has an effect on Nodes
  running a *Coordinator* service.