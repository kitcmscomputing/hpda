
``--daemon {start,stop,restart,status}``
  Perform operation as/on daemon. In *daemon* mode, the node process is detached
  from any launching terminal, shell or process, and may remain operational as long
  as the system is operational.

  ``start``
    Start service as a new daemon. This creates the node as normal, but forfeits
    process ownership before the node begins operation. If the daemon is already
    running, no operation is performed.

  ``stop``
    Stop the running daemon process. This will signal a damon node to gracefully
    shut down. If the daemon is not running already, no operation is performed.

  ``restart``
    Ensure that the daemon is not running, then start start it again. Equivalent
    to *stop* followed by *start*.

  ``status``
    Check if the daemon is running. If the daemon is running, prints a message to
    *stderr* and the PID to *stdout*, then exits with succes. Otherwise prints a
    message to *stderr*, a newline to *stdout* and exits with failure.

``--daemon-pidfile PIDFILE``
  Set the pidfile to use for all *daemon* operations. The pidfile is used to store
  the process id of a daemon and thus uniquely identifies an instance of the
  daemon.