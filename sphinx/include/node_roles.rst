
An HPDA cluster consists of several nodes, which together provide the distributed
caching, tracking and decision logic. A node is an instance of the hpda application
connected to other instances, typically on different machines. Each node may run
one or more of the clusters services.

Cache
  Controller for storage devices on a machine. It stages, maintains and retires
  files on local caches. The service may autonomously retire files, but new files
  are suggested from the outside.

Locator
  Lookup service for file locations, used for scheduling jobs closest to their
  desired files. The service is only a frontend to information generated elsewhere;
  it can be lost without corrupting the cluster.

Coordinator