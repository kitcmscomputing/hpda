.. HPDA tools man page

==========
hpda tools
==========

Summary
-------

**mount_unioncache.py [OPTIONS]**
  Create, destroy and/or configure a cache mount.

**config_pp.py [OPTIONS]**
  Pretty print and syntax check configurations.

Mount Cache
===========

The mount_unioncache.py utility helps in properly configuring the usage of cache
devices on top of storage. Given a number of data source directories and cache
directories, it may generate an appropriate union mount and cache node configuration.

Options
-------

``-h, --help``
  Print a help message to stderr and exit

``action {"mkdir","mount","umount","fstab","config"}``
  The operation to perform. This tells the program where to apply the settings
  generated from other inputs.

  ``mkdir``
    Create all required mount point directories.

  ``mount``
    Create unioned cache mount of cache directories on top of source directories.

  ``umount``
    Destroy an existing cache mount. This will not clear any data from the cache
    directories.

  ``fstab``
    Create an entry for ``fstab`` to automatically create the cache mount on boot.

  ``config``
    Create an appropriate cache node configuration.

``-d, --dry-run``
  No action is performed on resources. Instead, corresponding commands are printed
  to stdout.

``-c CACHE_DIR [CACHE_DIR ...], --cache-dir CACHE_DIR [CACHE_DIR ...]``
  Directories to use as caches. This option is sensitive to ordering: during access,
  the directories are traversed left-to-right. This implies that directories are
  specified in order of descending device performance.

``-s SOURCE_DIR [SOURCE_DIR ...], --source-dir SOURCE_DIR [SOURCE_DIR ...]``
  Directories to use as data sources. This is insensitive to ordering: every source
  directory has a cache mount overlayed individually.

``-m MOUNT_DIR, --mount-dir MOUNT_DIR``
  Format string for computing the mount directory paths. Supported format keys
  are *prefix*, *sourcebase* and *sourcedir*. Defaults to ``%(prefix)s/%(sourcebase)s``,
  i.e. prepends the ``MOUNT_PREFIX`` string to the source directory basename.

``-p MOUNT_PREFIX, --mount-prefix MOUNT_PREFIX``
  Supply a prefix to use in generating the mount directories. There is generally
  no point in setting this option in accord with ``MOUNT_DIR`` as the later is
  a superset of functionality.

Example
-------

``mount_unioncache.py --action mount mkdir --mount-prefix /storage -t aufs -c /hpda/cache/ssda /hpda/cache/hddr1 -s /hpda/storage/5 /hpda/storage/a /hpda/storage/jbod``

Usage Details
-------------

The tool overlays **all** cache directories on top of **each** source directory.
Given the cache directories *cdir1, dcir2*, source directories *sdir1, sdir2*, and
the mount directory *mdir*, the resulting mounts will look like this:

::

  /mdir/sbase1    /mdir/sbase1
           A               A
  - mount -|- - - - - - - -|- -
  /cdir1/sdir1    /cdir1/sdir1
           A               A
           |               |
  /cdir2/sdir1    /cdir2/sdir1
           A               A
           |               |
        /sdir1          /sdir1


Note that union filesystems usually operate on underlying filesystems, not the
directories provided by VFS (i.e. what a user sees). Thus, it is necessary to use
directories at or inside the mount point, not directories containing the mount.
For example, if a device is mounted at */foo/bar/base*, then both */foo/bar/base*
and */foo/bar/base/baz* can be used, while using */foo/bar* will only supply the
pre-mount directory */foo/bar/base*, which is usually empty.

See Also
--------

:manpage:`aufs(5)`, :manpage:`unionfs(2)`
