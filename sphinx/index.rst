.. HPDA documentation master file, created by
   sphinx-quickstart on Mon Jan 12 13:08:46 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

High Performance Data Aanalysis
===============================

Manual Pages
------------

The following ``man`` pages are available:

* :doc:`man_hpda`
* :doc:`man_hpda_tools`

The following overview pages are available:

* :doc:`topics/user_guide`
* :doc:`topics/configuration`

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

