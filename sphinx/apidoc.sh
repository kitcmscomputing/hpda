#!/usr/bin/env bash

if [[ $(sphinx-apidoc --version | grep 1.3) ]]; then
	sphinx-apidoc -M -e -o api ../hpda -f
else
	# sphinx 1.2 default output
	sphinx-apidoc -o api ../hpda -f
fi