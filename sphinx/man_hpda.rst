.. HPDA man page

=========
hpda node
=========

Synopsis
--------

**[python] hpda [-c|--config CONFIG [CONFIG ...]] [--daemon {start,stop,restart,status} [--daemon-pidfile PIDFILE]]**

Description
-----------

HPDA is a middleware for distributed caching in batch cluster computing. The hpda
application allows launching the nodes that form the cache cluster.

Options
-------

The following command line options can be provided at startup:

.. include:: include/cliopt.rst
.. include:: include/clidaemon.rst

Environment Variables
---------------------

HPDA_CONFIG
  If no configuration is specified, the path pointed to by this variable is used
  if it points to a file.


Node Roles
----------

 .. include:: include/node_roles.rst

See Also
--------

:manpage:`hpda_tools(2)`, :manpage:`hpda_htcondor(2)`