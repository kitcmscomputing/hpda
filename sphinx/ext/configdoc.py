"""
Custom sphinx autodoc Documenter to create documentation of configuration files.
"""
from collections import OrderedDict, Iterable
from hpda.utility.utils import listItems, NotSet
from hpda.utility.configuration.interface import Section, Group, Choice, ConfigAttribute, ConfigAttributeArray
from sphinx.ext.autodoc import mock_import, ModuleLevelDocumenter, prepare_docstring, force_decode, text_type, setup as autodoc_setup

def type_reference(value_type):
	"""
	Reference an arbitrary type, e.g. foo.bar.baz.my_type
	"""
	module_name = getattr(value_type, "__module__", None)
	type_name = value_type.__name__
	if module_name in ["__builtin__",None]:
		ref = type_name
	else:
		ref = "%s.%s"%(module_name, type_name)
	return ":py:class:`~%s`" % ref

def value_doc(value):
	"""
	Document the value allowed/expected for a setting

	( :py:class:`~str`: ``bar`` {``foo``, ``bar``} )
	( list[:py:class:`~int`]: [``1``,``2``] {``1``, ``2``, ``3``} )
	"""
	def format_default():
		if value.default is NotSet:
			return None
		if str(value.default) == "":
			return "<empty>"
		if isinstance(value.default, Iterable) and not isinstance(value.default, basestring):
			if not value.default:
				return "[]"
			return "[``%s``]"%", ".join(str(item) for item in value.default)
		return "``%s``" % value.default
	def format_type():
		if isinstance(value, ConfigAttributeArray):
			return "list[%s]"%type_reference(value.value_type)
		if isinstance(value, ConfigAttribute):
			return type_reference(value.value_type)
		raise ValueError("Unknown Config Attribute type")
	def format_choice():
		if value.choices is None:
			return None
		return "{%s}"%listItems(*value.choices, quotes="``", separator=", ", relation=", ")
	try:
		type_str = format_type()
		default_str = format_default()
		choice_str = format_choice()
	except KeyError:
		return repr(value)
	else:
		val_doc = type_str
		if default_str is not None:
			val_doc += " = " + default_str
		val_doc = "( %s )"%val_doc
		if choice_str is not None:
			val_doc += " -> " + choice_str
		return val_doc

def make_doc(target):
	if isinstance(target, Section):
		return section_doc(target)
	if isinstance(target, Choice):
		return choice_doc(target)
	if isinstance(target, Group):
		return group_doc(target)
	return []

def section_doc(section):
	docs = ""
	if section.descr is not None:
		docs += "\n" + section.descr + "\n"
	for attribute in sorted(section.attributes):
		if hasattr(section.attributes[attribute], "variants"):
			docs += "\n**%(name)s**" % {"name": attribute}
			if section.attributes[attribute].descr:
				docs += "\n  %s"%section.attributes[attribute].descr
			docs += "\n"
			for variant in section.attributes[attribute].variants:
				docs += "\n  %(sig)s" % {"sig" : value_doc(variant)}
				if variant.descr:
					docs += "\n    %s" % variant.descr
			docs += "\n"
		else:
			docs += "\n**%(name)s** %(sig)s"% {
			"name": attribute,
			"sig" : value_doc(section.attributes[attribute]),
			}
			if section.attributes[attribute].descr:
				docs += "\n  %s"%section.attributes[attribute].descr
			else:
				docs += "\n  <undocumented>"
			docs += "\n"
	return docs

def choice_doc(choice):
	docs = ""
	if choice.descr is not None:
		docs += "\n" + choice.descr + "\n"
	targets_to_names = OrderedDict()
	for section in choice.sections:
		if not choice.sections[section] in targets_to_names:
			targets_to_names[choice.sections[section]] = []
		targets_to_names[choice.sections[section]].append(section)
	for target in targets_to_names:
		docs += "\n**%s** is %s\n"%(choice.key, listItems(*targets_to_names[target], quotes="``", separator=", "))
		for line in section_doc(target).splitlines():
			docs += "  " + line + "\n"
	return docs

def group_doc(group):
	docs = ""
	if group.descr is not None:
		docs += "\n" + group.descr + "\n"
	docs += make_doc(group.target)
	return docs

def section_name(section_def):
	if isinstance(section_def, (Section, Choice)):
		return "[%s]"%section_def.name
	if isinstance(section_def, Group):
		return "[%s.nick1], [%s.nick2], ..."%(section_def.name, section_def.name)

class ConfigurationDocumenter(ModuleLevelDocumenter):
	objtype = 'config'
	member_order = 200
	titles_allowed = False

	def __init__(self, *args, **kwargs):
		ModuleLevelDocumenter.__init__(self, *args, **kwargs)

	@classmethod
	def can_document_member(cls, member, membername, isattr, parent):
		# never document automatically
		return False

	def document_members(self, all_members=False):
		pass

	def add_directive_header(self, sig):
		# no :py:class:-ish directive, just ReST formatted name
		name = self.format_name()
		self.add_line(u'**%s**' % name,'<autodoc>')

	def format_name(self):
		# use the documented section's name
		return section_name(self.object)

	def get_doc(self, encoding=None, ignore=1):
		docstring = make_doc(self.object)
		# make sure we have Unicode docstrings, then sanitize and split
		# into lines
		if isinstance(docstring, text_type):
			return [prepare_docstring(docstring, ignore)]
		elif isinstance(docstring, str):  # this will not trigger on Py3
			return [prepare_docstring(force_decode(docstring, encoding),
									  ignore)]
		# ... else it is something strange, let's ignore it
		return []


def setup(app):
	autodoc_setup(app)
	app.add_autodocumenter(ConfigurationDocumenter)
