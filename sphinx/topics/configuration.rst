Configuration
=============

HPDA uses configuration files with an extended ``ini`` style. In addition, a
number of command line arguments may be used for convencience.

The HPDA system consits of two generic component types: **Nodes** that host the
core services, and **Hooks** that link to an existig batch system.
In order to setup an HPDA cluster on top of a batch system, several components
must be configured:

**Cache**
  On every worker node with cache devices, an HPDA Node with a **CacheMaster**
  must manage the caches. It maintains the files on the caches and provides file
  and device meta-data to other components.

**Locator**
  A Frontend to the Cache meta-data, used for scheduling jobs to worker nodes.
  At least one **Locator** is required to be accessible from every submit host.
  Ideally, there is one **Locator** running on every host used for user job
  submission.

**Coordinator**
  The scheduler of the HPDA cluster, deciding which files to stage and where to
  put them. Exactly one **Coordinator** is required, and it must support
  bi-directional communication to all **Caches** and **Locators**.

**Collector**
  Trackers for job, user and file usage statistics. Currently a subcomponent of
  the **Coordinator**, so it cannot be explicitly setup.

**Job Hooks**
  The link between job meta-data and scheduling on the one hand and the HPDA
  components on the other. When using an **HTCondor** batch system, these must
  be configured on every submit host (*schedd*).

Syntax
------

The configuration file format supports key, value pairs organized in sections,
optionally annotated with comments. Comments are accounted for the last section
key defined.

- *Sections*, enclosed in square brackets (``[]``) at the start of the line
- *Keys*, at the start of the line, followed by an equal sign (``=``) to set or a
  plus-equal sign (``+=``) to append
- *Values*, anywhere after a key, before a comment
- *Comments*, at the start of the line preceeded by a semicolon (``;``) or at any
  place preceeded by a space and semicolon (``;``)

Both Sections and Keys are parsed with included whitespace implicitly converted
to underscores. Values are parsed as-is, provided they do not interfere with the
parsing of other components. A special rule applies if a list value is expected:
the newline character is the exclusive delimeter value. All sections, keys, and
comments are stripped of surrounding whitespace.

Sections appearing multiple times are treated as if concatenated. Attributes
appearing multiple times are concatenated if defined with plus-equal sign (``+=``)
or reset if defined with equal sign (``=``). It is not an error to concatenate to
an undefined attribute - this is equivalent to a new definition.

Any attributes appearing before any section definition are treated differently.
They are parsed as-is and cloned into every section defined in the file before
further attributes are parsed.

Example
.......

::

  ; This is comment is attributed to the source
  ; and spans several lines
  globalkey_a = foo
  Globalkey_B = bar ; this is a value comment

  [FirstSection] ; section comment
  ; section comment continuation
  ; that could stand on its own
  f key a = footoo ; translates to f_key_a : footoo
  f_key a +=       ; this will add to f_key_a
    foobee
  freebee          ; this comment will also add to f_key_a's comment
  F_Key b = bazbazbazbaz
  [SecondSection]
  ; yet another section
  s key =
  	[NoSection]
  	not a key = but a value
  	this is a value; and this is not a comment
  [NotA];section either
  	this is a value ; but this is a comment

  ; the above is an example of defining
  ; values that look like other types
  ; but are parsed purely as values
  [firstSection]
  ; overwrite previous setting
  f key a =

General
-------

Logging
.......

All components provide logging facilities based on the Python :py:mod:`logging`
package. This allows sending debugging and runtime information to streams and
log-files. (Other :py:mod:`logging` targets, e.g. email, may be supported in the
future.)

It is generally sufficient to configure Handlers. Configuring Loggers provides
detailed control when a single Handler receivess messages from multiple Loggers.

.. autoconfig:: hpda.utility.report.configuration.HandlerConfig

.. autoconfig:: hpda.utility.report.configuration.LoggerConfig


Nodes
-----

Node
....

Every node, regardless of active components, provides a core of essential
functionality. This infrastructure is shared by all components running on the
node.

The ``NodeServer`` should be configured with respect to firewall settings and
other local services  (the default port ``8080`` is the http alternative
default). The ``NodeMapper`` requires to be configured with respect to the
components running on the node (see below).

.. autoconfig:: hpda.common.node_master.NodeMasterConfig

.. autoconfig:: hpda.common.node_master.NodeServerConfig

.. autoconfig:: hpda.common.node_master.HeartbeatConfig

Cache
.....

At least one ``Cache`` and ``Storage`` is required for the node to fullfil its
functionality. The catalogue should reside on a media that is as persistent as
the caches.

Nodes hosting a **Cache** component should address all other **Locator** and
**Coordinator** nodes.

.. autoconfig:: hpda.cache.journal.catalogue.CatalogueConfig

.. autoconfig:: hpda.cache.backend.configuration.CacheConfig

.. autoconfig:: hpda.cache.storage.configuration.StorageConfig

.. autoconfig:: hpda.common.worker.AnyWorkerConfig

Hooks
-----

.. autoconfig:: hpda.interfaces.htcondor.router_hooks._HTCTranslateConfig
