User Guide
==========

HPDA provides a caching layer between Batch Clusters and File Servers.
When deploying your jobs on a HPDA Cluster, read operations can be considerably sped up.

To benefit from the HPDA extension, only small adjustments must be made to your job.
The system will extract most information it requires directly from the batch system
without any explicit work required from you.

HTCondor
--------

Your job must include an explicit list of input files.
By default, this must be written to the job ClassAd attribute ``Input_Files``.
The following formats are allowed:

**Explicit list**
  An HTCondor StringList of file names, i.e. comma-separated and enclosed with
  double quotes.
  Example: ``"/input/dir/file1,/input/dir/file2,/input/dir/file3"``

**File catalogue**
  The path to a file containing a line-separated list of file name.
  Example: ``"/input/dir/catalogue"``

**Z-Lib compressed list**
  A string of comma-separated file names, compressed with zlib and prepended
  by ``Z:``.
  Example: ``"Z:x����+(-�O�,�O��I5��G0D0ƴ�"``

:note: You can append attributes to your job ClassAd with the ``+Key = Value``
       notation in you JDL file; see ``condor_submit`` for details.
       Example: ``+Input_Files = "/input/file1,/input/file2,/input/file3"``

Example
.......

::

  executable  = analysis.exe
  arguments   = $(Process)
  output      = job.$(Cluster).$(Process).stdout
  error       = job.$(Cluster).$(Process).stderr
  log         = job.$(Cluster).log
  universe    = vanilla
  +InputFiles = "/input/file$(Process).root"

Supported Systems
-----------------

**Batch Systems**
  HTCondor

**Data Sources**
  POSIX