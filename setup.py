#!/usr/bin/python
#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
Distutils installer/packager
"""

from distutils.core import setup
import pkgutil
import re

# regexp of packages to ignore
package_blacklist = ["external."]

setup(
	# versioning
	name='HPDA',
	version='0.5',
	# info
	description='Server and client for the HPDA coordinated caching infrastructure',
	long_description=
	"The HPDA infrastructure allows to setup a series of cache agents, which are"
	"coordinated to improve locality for jobs executed via batch systems.",
	author='Max Fischer',
	author_email='max.fischer@kit.edu',
	url="https://bitbucket.org/kitcmscomputing/hpda/overview",
	license="GPL",
	keywords=['cache','distributed','coordinated','batch','batch processing'],
	# dependencies
	requires=['cherrypy','pycurl','crontab','sqlalchemy','RestClient'],
	# content
	packages=[
		pkg for _, pkg, is_pkg in pkgutil.walk_packages(["./hpda"], "hpda.")
			if is_pkg and not any (re.search(pattern, pkg) for pattern in package_blacklist) ],
	data_files=[
		('/etc/init.d', ['scripts/init.d/hpdad.sh']),
	],
	scripts=[
		'bin/hpda_node.py',
		'bin/htc_finalize.py',
		'bin/htc_translate.py',
		'bin/htc_update.py',
	]
	)