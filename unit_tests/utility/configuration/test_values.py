# -*- coding: utf-8 -*-
#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
import random
import tempfile
import unittest
import sys, os
import itertools

from hpda.utility.configuration.values import stream, boolean, percent, seconds, permission

class TestStream(unittest.TestCase):
	def setUp(self):
		self.test_files = ["%s-%x"%(tempfile.mktemp(), random.getrandbits(16)) for _ in xrange(10)]
		for fname in self.test_files:
			open(fname, 'a').close()

	def test_std_stream(self):
		self.assertEqual(stream("stdin"), sys.stdin, "Load ``stdin``")
		self.assertEqual(stream("stdout"), sys.stdout, "Load ``stdout``")
		self.assertEqual(stream("stderr"), sys.stderr, "Load ``stderr``")

	def test_explicit_file(self):
		modes = ["w","wb","r","rb","a","ab","w+","r+","a+"]
		for idx, path in enumerate(self.test_files):
			stream_file = stream("%s:file:%s"%(modes[idx%len(modes)],path))
			self.assertIsInstance(stream_file, file, "File type" )
			self.assertEqual(stream_file.name, path, "File name")
			self.assertEqual(stream_file.mode, modes[idx%len(modes)], "File mode (%s vs %s)"%(stream_file.mode, modes[idx%len(modes)]))

	def test_explicit_file_no_mode(self):
		for idx, path in enumerate(self.test_files):
			stream_file = stream("file:%s"%path)
			self.assertIsInstance(stream_file, file, "File type")
			self.assertEqual(stream_file.name, path, "File name")

	def test_implicit_file(self):
		for idx, path in enumerate(self.test_files):
			stream_file = stream(path)
			self.assertIsInstance(stream_file, file, "File type")
			self.assertEqual(stream_file.name, path, "File name")

	def tearDown(self):
		for path in self.test_files:
			if os.path.exists(path):
				os.unlink(path)


class TestBoolean(unittest.TestCase):
	def test_direct(self):
		for true_word in boolean.str_true:
			self.assertEqual(boolean(true_word), True, "True (%s vs %s)"%(true_word, True))
		for false_word in boolean.str_false:
			self.assertEqual(boolean(false_word), False, "False (%s vs %s)"%(false_word, False))

	def test_lowercase(self):
		converter = lambda word: word.lower()
		for true_word in map(converter, boolean.str_true):
			self.assertEqual(boolean(true_word), True, "True (%s vs %s)"%(true_word, True))
		for false_word in map(converter, boolean.str_false):
			self.assertEqual(boolean(false_word), False, "False (%s vs %s)"%(false_word, False))

	def test_uppercase(self):
		converter = lambda word: word.upper()
		for true_word in map(converter, boolean.str_true):
			self.assertEqual(boolean(true_word), True, "True (%s vs %s)"%(true_word, True))
		for false_word in map(converter, boolean.str_false):
			self.assertEqual(boolean(false_word), False, "False (%s vs %s)"%(false_word, False))

	def test_capitalcase(self):
		converter = lambda word: word.capitalize()
		for true_word in map(converter, boolean.str_true):
			self.assertEqual(boolean(true_word), True, "True (%s vs %s)"%(true_word, True))
		for false_word in map(converter, boolean.str_false):
			self.assertEqual(boolean(false_word), False, "False (%s vs %s)"%(false_word, False))

	def test_invertedcapitalcase(self):
		converter = lambda word: word.capitalize().swapcase()
		for true_word in map(converter, boolean.str_true):
			self.assertEqual(boolean(true_word), True, "True (%s vs %s)"%(true_word, True))
		for false_word in map(converter, boolean.str_false):
			self.assertEqual(boolean(false_word), False, "False (%s vs %s)"%(false_word, False))

	def test_error(self):
		for error_word in ("jepp","yeah","Geofrey","foo","bar"):
			self.assertRaises(ValueError, boolean, error_word)


class TestPercent(unittest.TestCase):
	def test_signed(self):
		for number in (random.random()*20000 for _ in xrange(50)):
			literal = "%f%%"%number
			self.assertAlmostEqual(percent(literal), number/100, 5, "Signed (%s vs %f)"%(literal, number/100))

	def test_unsigned(self):
		for number in (random.random()*20000 for _ in xrange(50)):
			literal = "%f"%number
			self.assertAlmostEqual(percent(literal), number, 5, "Unsigned (%s vs %f)"%(literal, number))

	def test_illsigned(self):
		for number in (random.random()*20000 for _ in xrange(50)):
			for sign in ("!","§","$","&","/","=","?","@","°","^","~"):
				literal = "%f%s"%(number, sign)
				self.assertRaises(ValueError, percent, literal)


class TestPermissions(unittest.TestCase):
	def setUp(self):
		self._perms = "rwxrwxrwx"
		self._pbits = (0o400,0o200,0o100,0o040,0o020,0o010,0o004,0o002,0o001)
		self._tests = list(sum(i) for r in range(len(self._pbits),0,-1) for i in list(itertools.combinations(self._pbits,r)))

	def _to_symbol(self, perm_int):
		perms = "".join(self._perms[idx] if self._pbits[idx] & perm_int else "-" for idx in range(len(self._perms)))
		return perms

	def test_plain(self):
		# test raw integers
		for perm in self._tests:
			self.assertEqual(permission(perm),perm)

	def test_literal_int(self):
		for perm in self._tests:
			self.assertEqual(permission(str(perm)),perm)

	def test_literal_octal(self):
		for perm in self._tests:
			self.assertEqual(permission("%#o"%perm),perm)

	def test_literal_symbol_long(self):
		for perm in self._tests:
			symbol = self._to_symbol(perm)
			self.assertEqual(permission(symbol),perm)

	def test_literal_symbol_short(self):
		for perm in self._tests:
			symbol = self._to_symbol(perm).rstrip("-")
			self.assertEqual(permission(symbol),perm)

	def test_comparisons_ascending(self):
		variants = itertools.permutations(range(8))
		for variant in variants:
			new = 0
			for idx in variant:
				old = new
				new = new | self._pbits[idx]
				self.assertNotEqual(permission(new), permission(old))
				self.assertLessEqual(permission(new), permission(new))
				self.assertLessEqual(permission(old), permission(new))
				self.assertLess(permission(old), permission(new))
				self.assertGreaterEqual(permission(new), permission(new))
				self.assertGreaterEqual(permission(new), permission(old))
				self.assertGreater(permission(new), permission(old))

	def test_comparisons_decending(self):
		variants = itertools.permutations(range(8))
		for variant in variants:
			new = sum(self._pbits)
			for idx in variant:
				old = new
				new = new ^ self._pbits[idx]
				self.assertNotEqual(permission(new), permission(old))
				self.assertGreaterEqual(permission(new), permission(new))
				self.assertGreaterEqual(permission(old), permission(new))
				self.assertGreater(permission(old), permission(new))
				self.assertLessEqual(permission(new), permission(new))
				self.assertLessEqual(permission(new), permission(old))
				self.assertLess(permission(new), permission(old))

class TestSeconds(unittest.TestCase):
	def setUp(self):
		self.tests  = [-20000, -15, 0, 1, 59, 60, 61, 3599, 3600, 3601, 60*60*24*365*10]
		self.tests += [random.randint(-60*60*24*365, -60*60*24*365) for _ in xrange(20)]
		self.tests += [random.uniform(-60*60*24*365, -60*60*24*365) for _ in xrange(20)]

	# conversion functions func(seconds:float)->(literal:str, expected_seconds:float)
	def _literal_plain(self, seconds):
		literal = "%.5f"%seconds
		return literal, float(literal)
	def _literal_unit(self, seconds):
		if abs(seconds) > 60*60*24*30*10:
			return "%.5fM"%(seconds/60*60*24*30), float(seconds/60*60*24*30)*60*60*24*30
		if abs(seconds) > 60*60*24*10:
			return "%.5fd"%(seconds/60*60*24), float(seconds/60*60*24)*60*60*24
		if abs(seconds) > 60*60*10:
			return "%.5fh"%(seconds/60*60), float(seconds/60*60)*60*60
		if abs(seconds) > 60*10:
			return "%.5fm"%(seconds/60), float(seconds/60)*60
		return "%ss"%seconds, seconds
	def _literal_literal(self, seconds):
		steps = (24*60*60, 60*60, 60)
		remain = abs(seconds)
		literal = "-" if seconds < 0 else ""
		for step in steps:
			if abs(seconds) > step:
				literal += "%d:" % (remain/step)
				remain  %= step
		literal += "%f" % remain
		return literal, seconds

	def test_plain(self):
		for literal, reference in map(self._literal_plain, self.tests):
			self.assertAlmostEqual(seconds(literal), reference, 5, "Literal %r ( %f vs %f)" %(literal, seconds(literal), reference))

	def test_unit(self):
		for literal, reference in map(self._literal_unit, self.tests):
			self.assertAlmostEqual(seconds(literal), reference, 5, "Literal %r ( %f vs %f)" %(literal, seconds(literal), reference))

	def test_literal(self):
		for literal, reference in map(self._literal_literal, self.tests):
			self.assertAlmostEqual(seconds(literal), reference, 5, "Literal %r ( %f vs %f)" %(literal, seconds(literal), reference))

if __name__ == "__main__":
	unittest.main()