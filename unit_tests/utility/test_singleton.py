#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
import unittest
from hpda.utility.singleton import BaseSingleton, BaseWeakSingleton, singleton__init

class TestBaseSingleton(unittest.TestCase):
	my_baseclass = BaseSingleton
	def test_same_simple(self):
		class BareSingleton(self.my_baseclass):
			pass
		self.assertIs(BareSingleton(), BareSingleton())

	def test_same_complex(self):
		class ParamSingleton(self.my_baseclass):
			@singleton__init
			def __init__(self, foo, bar=1):
				self.foo = foo
				self.bar = bar
		self.assertIs(ParamSingleton(1), ParamSingleton(1), "Instantiation with required parameters")
		self.assertIs(ParamSingleton(2,1), ParamSingleton(2,1), "Instantiation with all parameters")
		self.assertIs(ParamSingleton(foo=2,bar=1), ParamSingleton(foo=2,bar=1), "Instantiation with kw parameters")
		self.assertIs(ParamSingleton(foo=2,bar=1), ParamSingleton(2,1), "Instantiation with mixed parameters")

	def test_unique_simple(self):
		class BareSingleton(self.my_baseclass):
			pass
		self.assertIs(BareSingleton(), BareSingleton())

	def test_unique_complex(self):
		class ParamSingleton(self.my_baseclass):
			@singleton__init
			def __init__(self, foo, bar=1):
				self.foo = foo
				self.bar = bar
		self.assertIsNot(ParamSingleton(2), ParamSingleton(4), "Instantiation with required parameters")
		self.assertIsNot(ParamSingleton(2,1), ParamSingleton(4,3), "Instantiation with all parameters")
		self.assertIsNot(ParamSingleton(foo=2,bar=1), ParamSingleton(foo=4,bar=3), "Instantiation with kw parameters")
		self.assertIsNot(ParamSingleton(foo=2,bar=1), ParamSingleton(4,3), "Instantiation with mixed parameters")
		self.assertIsNot(ParamSingleton(foo=2,bar=1), ParamSingleton(4), "Instantiation with some parameters")
		self.assertIsNot(ParamSingleton(foo=2,bar=1), ParamSingleton(2), "Instantiation with some parameters")

	def test_same_param_complex(self):
		class ParamSingleton(self.my_baseclass):
			@singleton__init
			def __init__(self, foo, bar=1):
				self.foo = foo
				self.bar = bar
			@classmethod
			def _singleton_signature(cls, foo, bar=1):
				return bar
		self.assertIs(ParamSingleton(1), ParamSingleton(1), "Instantiation with required parameters")
		self.assertIs(ParamSingleton(2,1), ParamSingleton(2,1), "Instantiation with all parameters")
		self.assertIs(ParamSingleton(foo=2,bar=1), ParamSingleton(foo=2,bar=1), "Instantiation with kw parameters")
		self.assertIs(ParamSingleton(foo=2,bar=1), ParamSingleton(2,1), "Instantiation with mixed parameters")
		self.assertIs(ParamSingleton(foo=2,bar=1), ParamSingleton(2), "Instantiation with some parameters")

	def test_unique_param_complex(self):
		class ParamSingleton(self.my_baseclass):
			@singleton__init
			def __init__(self, foo, bar=1):
				self.foo = foo
				self.bar = bar
			@classmethod
			def _singleton_signature(cls, foo, bar=1):
				return bar
		self.assertIsNot(ParamSingleton(2,1), ParamSingleton(4,3), "Instantiation with all parameters")
		self.assertIsNot(ParamSingleton(foo=2,bar=1), ParamSingleton(foo=4,bar=3), "Instantiation with kw parameters")
		self.assertIsNot(ParamSingleton(foo=2,bar=1), ParamSingleton(4,3), "Instantiation with mixed parameters")
		self.assertIsNot(ParamSingleton(foo=2,bar=3), ParamSingleton(2), "Instantiation with some parameters")

class TestBaseWeakSingleton(TestBaseSingleton):
	my_baseclass = BaseWeakSingleton


if __name__ == "__main__":
	unittest.main()