#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
import unittest
import tempfile, shutil, os, random
import gc

from hpda.utility.persistency import PersistentDict

class WeakRefAble(object):
	pass

class TestPersistentDict(unittest.TestCase):
	def setUp(self):
		self._test_folder = tempfile.mkdtemp()
	def tearDown(self):
		shutil.rmtree(self._test_folder)

	def test_persists(self):
		# content is persistently stored
		pd1 = PersistentDict(os.path.join(self._test_folder, "test_persist"))
		key, value = random.random(), random.random()
		pd1[key]=value
		del pd1
		gc.collect()
		pd2 = PersistentDict(os.path.join(self._test_folder, "test_persist"))
		self.assertEqual(pd2[key],value)
		del pd2[key]
		del pd2
		gc.collect()
		pd3 = PersistentDict(os.path.join(self._test_folder, "test_persist"))
		self.assertRaises(KeyError, lambda pdict, dkey: pdict[dkey], pd3, key)

	def _test_content_identity(self, cache=True):
		# active content is provided as is, not cloned
		values = [WeakRefAble() for _ in xrange(0,200)]
		pd_cached = PersistentDict(os.path.join(self._test_folder, "test_persist"), chunk_count=64, cache_size=(cache and 64 or 0))
		for idx in xrange(0,200):
			pd_cached[idx] = values[idx]
		gc.collect()
		for idx in xrange(0,200):
			self.assertIs(pd_cached[idx], values[idx], "Stored object is identical to retrieved object")

	def test_content_identity_cached(self):
		# active content is provided as is, not cloned - explicitly cached
		self._test_content_identity(cache=True)

	def test_content_identity_uncached(self):
		# active content is provided as is, not cloned - implicitly cached
		self._test_content_identity(cache=False)

	def test_resize(self):
		# can safely re-chunk without loss of data
		kvs = zip(xrange(200),xrange(1,201))
		pd1 = PersistentDict(os.path.join(self._test_folder, "test_persist"))
		pd1.update(kvs)
		for chunk_count in (1,2,4,16,32,64,128,256,512):
			pd1.chunk_count=chunk_count
			gc.collect()
			for key, value in kvs:
				self.assertEqual(pd1[key], value)
		# no loss from incorrect calls
		def set_count(pdict, value):
			pdict.chunk_count = value
		self.assertRaises(ValueError, set_count, pd1, -1)
		gc.collect()
		for key, value in kvs:
			self.assertEqual(pd1[key], value)

if __name__ == "__main__":
	unittest.main()
