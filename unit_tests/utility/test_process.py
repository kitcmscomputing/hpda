#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
import unittest
import tempfile, random, os, shutil, time

from hpda.utility.process import ConsumedProcess, BufferedProcess

def _timeit(target, *args, **kwargs):
	stime = time.time()
	target(*args, **kwargs)
	return time.time() - stime

def _wait_call(target, *args, **kwargs):
	returnee = target(*args, **kwargs)
	time.sleep(0.2)
	return returnee

class BaseTestProcess(unittest.TestCase):
	# some buffer size is to be expected in usage
	_test_size = 8
	_target_class = None
	def setUp(self):
		self._test_folder = tempfile.mkdtemp()
		for count in xrange(0, self._test_size):
			open(os.path.join(self._test_folder, "%d-%s"%(count,random.getrandbits(16))), 'a').close()
	def tearDown(self):
		shutil.rmtree(self._test_folder)

	def test_size(self):
		self.assertGreaterEqual(len(self._target_class("ls",["-l",self._test_folder]).stdout), self._test_size) # must read all input (found files)
		self.assertLessEqual(len(self._target_class("ls",["-l",self._test_folder]).stdout), self._test_size+2) # must read exakt input (only files+meta)

	def test_poll(self):
		self.assertIs(self._target_class("sleep", ["10"]).poll(), None) # return ``None`` on running process
		self.assertIs(_wait_call(self._target_class,"true").poll(), None) # return ``0`` on successfull process
		self.assertGreater(_wait_call(self._target_class,"false").poll(), None) # return ``>0`` on failing process
		kill_proc = self._target_class("sleep", ["10"])
		kill_proc.kill()
		self.assertGreater(kill_proc.poll(), None, "return ``<0`` on signaled process")

	def test_wait(self):
		self.assertIs(self._target_class("true").wait(), 0) # return ``0`` on successfull process
		self.assertGreater(self._target_class("false").wait(), None) # return ``>0`` on failing process
		self.assertIs(self._target_class("sleep",["20"]).wait(timeout=1, kill=False), None) # return ``None`` on running process
		self.assertIs(self._target_class("sleep",["20"]).wait(timeout=1, kill=True), None) # return ``<0`` on killed process

	def test_wait_time(self):
		self.assertAlmostEqual(_timeit(self._target_class("sleep", ["1"]).wait), 1, 0) # wait equals process time
		self.assertAlmostEqual(_timeit(self._target_class("sleep", ["10"]).wait, timeout=1, kill=True), 1, 0) # wait equals timeout time (kill)
		self.assertAlmostEqual(_timeit(self._target_class("sleep", ["10"]).wait, timeout=1, kill=False), 1, 0) # wait equals timeout time (no kill)


class TestConsumedProcess(unittest.TestCase):
	# must be able to read lots of input
	_test_size    = 1024
	_target_class = ConsumedProcess


class TestBufferedProcess(unittest.TestCase):
	_target_class = BufferedProcess

# Do not test the baseclass
del BaseTestProcess

if __name__ == "__main__":
	unittest.main()