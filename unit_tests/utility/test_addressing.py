#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
import random
import unittest

from hpda.utility.addressing import IPV4Address


class TestIPV4Address(unittest.TestCase):
	# Input generation helper functions
	@staticmethod
	def input_tuple_int(*components):
		return tuple(int(bit) for bit in components)
	@staticmethod
	def input_tuple_str(*components):
		return tuple(str(bit) for bit in components)
	@staticmethod
	def input_tuple_float(*components):
		return tuple(float(bit) for bit in components)
	@staticmethod
	def input_numeric_int(*components):
		components = components[::-1]
		return sum(int(components[idx])*255**idx for idx in xrange(len(components)))
	@staticmethod
	def input_string(*components):
		return ".".join(str(byte) for byte in components)

	# address range
	def test_range_high_tuple(self):
		self._test_range_high(self.input_tuple_int, self.input_tuple_str, self.input_tuple_float)
	def test_range_high_int(self):
		for high_byte in (256, 35565, 3e12):
			self.assertRaises(ValueError, IPV4Address, self.input_numeric_int(high_byte, random.randrange(256), random.randrange(256), random.randrange(256)))
	def test_range_high_str(self):
		self._test_range_high(self.input_string)

	def test_range_low_tuple(self):
		self._test_range_low(self.input_tuple_int, self.input_tuple_str, self.input_tuple_float)
	def test_range_low_int(self):
		for low_byte in (-1, -35565, -3e12):
			self.assertRaises(ValueError, IPV4Address, self.input_numeric_int(low_byte, random.randrange(256), random.randrange(256), random.randrange(256)))
	def test_range_low_str(self):
		self._test_range_low(self.input_string)

	def test_range_accept_tuple(self):
		self._test_range_accept(self.input_tuple_int, self.input_tuple_str, self.input_tuple_float)
	def test_range_accept_int(self):
		self._test_range_accept(self.input_numeric_int)
	def test_range_accept_str(self):
		self._test_range_accept(self.input_string)

	def _test_range_high(self, *fkts):
		for high_byte in (256, 35565, 3e12):
			for input_fkt in fkts:
				self.assertRaises(ValueError, IPV4Address, input_fkt(high_byte, random.randrange(256), random.randrange(256), random.randrange(256)))
				self.assertRaises(ValueError, IPV4Address, input_fkt(random.randrange(256), high_byte, random.randrange(256), random.randrange(256)))
				self.assertRaises(ValueError, IPV4Address, input_fkt(random.randrange(256), random.randrange(256), high_byte, random.randrange(256)))
				self.assertRaises(ValueError, IPV4Address, input_fkt(random.randrange(256), random.randrange(256), random.randrange(256), high_byte))

	def _test_range_low(self, *fkts):
		for low_byte in (-1, -35565, -3e12):
			for input_fkt in fkts:
				self.assertRaises(ValueError, IPV4Address, input_fkt(low_byte, random.randrange(256), random.randrange(256), random.randrange(256)))
				self.assertRaises(ValueError, IPV4Address, input_fkt(random.randrange(256), low_byte, random.randrange(256), random.randrange(256)))
				self.assertRaises(ValueError, IPV4Address, input_fkt(random.randrange(256), random.randrange(256), low_byte, random.randrange(256)))
				self.assertRaises(ValueError, IPV4Address, input_fkt(random.randrange(256), random.randrange(256), random.randrange(256), low_byte))

	def _test_range_accept(self, *fkts):
		for right_byte in (1, 127, 255, 23, 165, 250, random.randrange(256), random.randrange(256), random.randrange(256)):
			for input_fkt in fkts:
				self.assertIsNotNone(IPV4Address(input_fkt(right_byte, random.randrange(256), random.randrange(256), random.randrange(256))))
				self.assertIsNotNone(IPV4Address(input_fkt(random.randrange(256), right_byte, random.randrange(256), random.randrange(256))))
				self.assertIsNotNone(IPV4Address(input_fkt(random.randrange(256), random.randrange(256), right_byte, random.randrange(256))))
				self.assertIsNotNone(IPV4Address(input_fkt(random.randrange(256), random.randrange(256), random.randrange(256), right_byte)))

	# address length
	def test_range_short(self):
		for addr_range in xrange(4):
			for input_fkt in (self.input_tuple_int, self.input_tuple_str, self.input_tuple_float, self.input_string):
				self.assertRaises(ValueError, IPV4Address, input_fkt(*[random.randrange(256) for _ in xrange(addr_range)]))

	def test_range_long(self):
		for addr_range in xrange(8,4,-1):
			for input_fkt in (self.input_tuple_int, self.input_tuple_str, self.input_tuple_float, self.input_numeric_int, self.input_string):
				self.assertRaises(ValueError, IPV4Address, input_fkt(*[random.randrange(256) for _ in xrange(addr_range)]))

if __name__ == "__main__":
	unittest.main()