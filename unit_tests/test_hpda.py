#!/usr/bin/env python
#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
Collection for all unittest testcases
"""
from __future__ import print_function
import unittest

if __name__ == "__main__":
	try:
		import hpda
	except ImportError:
		try:
			import os, sys
			sys.path.append(os.path.join(sys.path[0],".."))
			import hpda
		except ImportError:
			print("Unable to locate package 'hpda'; searched cwd, $PYTHONPATH and parent directory", file=sys.stderr)
			sys.exit(1)
	import argparse
	main_parser = argparse.ArgumentParser("Run HPDA")
	main_parser.add_argument("-d","--dir",help="directory to start search for tests in", default=".")
	main_parser.add_argument("-v","--verbosity",action="count", help="increase verbosity of test run (max 2)", default=0)
	settings = main_parser.parse_args()
	# run all tests located in subfolders
	unittest.TextTestRunner(verbosity=settings.verbosity).run(
		unittest.defaultTestLoader.discover(start_dir=settings.dir)
	)