# README #

This is a prototype implementation of the *H*igh *P*erformance *D*ata *A*nalysis middleware for controlled caching in distributed cluster computing environments.

### What is this repository for? ###

This repository houses the code base and related files for the HPDA cluster nodes. An HPDA node is similar to a node of a Batch System, with different flavours of nodes serving specific tasks; when nodes on multiple hosts work together in an HPDA cluster, they form a distributed, controllable cache.

The design currently has the following elements:

* Node: The underlying platform for all components; exposes the APIs for inter-node and external communication.
* Cache: supervisor for (local) cache devices; distributes and maintains copies of files on caches
* Coordinator: supervises the Cache nodes; assigns and retires files from them
* Predictor: monitors user jobs; nominates files to stage on the Cache pool
* Locator: collects file->host mapping, exposes it to users/batch systems

About:

* Developers: Kalrsuhe Institute for Technology - EKP CMS Computing Group
* Version: Beta 0.5
* Supported Batch System: HTCondor 8+
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Client (batch system submit node)
	* Download this repository
	* Depends: python 2.6/2.7 with pycurl
	* See **./docs/htcondor.md**
* Core Node
	* Download this repository
	* Depends: python 2.7 with pycurl, cherrypy
	* See **./docs/core.md**

* Dependencies
	* See **./docs/dependencies.md**
	* Python
	Suggested version is 2.7; compatibility is provided for 2.6.
	Required packages:
	cherrypy 3.6
	pycurl (curl 7.19)

* How to run tests
	* unit tests are located in **./unit_tests**
	* all tests can be run at once using **./unit_tests/test_hpda.py**


### Contribution guidelines ###

* Coding style
	* [PEP-8](http://legacy.python.org/dev/peps/pep-0008/) and [Google Style Guide](https://google-styleguide.googlecode.com/svn/trunk/pyguide.html)
	* Indentation is *1 indent == 1 tab*
	* Docstrings use [epydoc](http://epydoc.sourceforge.net/fields.html) formatting
	* Inspect/validate code using **PyCharm** IDE, **pychecker** and/or **pylint**

* Writing tests
	* All test-able functionality should feature a unit test in **./unit_test**
	* Unit tests are to be written for the python `unittest` module
	* The **./unit_test** folder/filder structure should be the same as the main package

* Repository management
    * See [Git branching model](http://nvie.com/posts/a-successful-git-branching-model/)

### Who do I talk to? ###

* Repo owner or admin
	* Please contact **max.fischer[]kit.edu** or **manuel.giffels[]kit.edu**
