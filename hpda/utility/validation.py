#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
The tools and utilities of this module provide means for validating data, both
for consistency and security checks. The SchemaMember classes define an expected
data structure by type, constraints, etc, against which python object can be
tested.
"""

# base imports

# library imports

# custom imports
from .exceptions import BasicException, AbstractError
from .enum       import BaseEnum
from .utils      import NotSet, listItems, FlatList, iterableitems

class SchemaElementValidity(BaseEnum):
	"""
	Enum for describing the result of a schema element validation

	:cvar VALID: The object matches its schema specification.
	:cvar INVALID_MISSING: The object does not exist but is required.
	:cvar INVALID_TYPE: The object is of a wrong type.
	:cvar INVALID_CONSTRAINTS: The object does not match the constraints.
	:cvar INVALID_SIZE: The object is a container of wrong size.
	"""
	__members__ = ['VALID', 'INVALID_MISSING', 'INVALID_TYPE', 'INVALID_CONSTRAINTS', 'INVALID_SIZE']

class SchemaValidationFailure(BasicException):
	"""Validation of a schema failed, as opposed to evaluating to valid/invalid"""
	pass

def _type_str(obj):
	"""Helper: return a string representation of the type of obj"""
	printable = obj
	if not isinstance(obj, type):
		printable = type(obj)
	try:
		return printable.__name__
	except:
		return str(printable)

class SchemaMember(object):
	"""
	Description of features that a data member must match
	
	Subclasses may use the following parameters:

	:param types: types the data member must belong to
	:type types: list[type]
	:param members: schemas representing the members of a container
	:type members: :py:class:`SchemaMember`
	:param required: Whether the data member is required to exist
	:type required: bool
	:param constraints: functions that must evaluate to True for a given object.
	:type constaints: list[callable]
	:param minsize: inclusive lower boundary for the size of a container
	:type minsize: int
	:param maxsize: inclusive upper boundary for the size of a container
	:type maxsize: int
	"""
	def validate(self, obj):
		"""Validate that obj (and members) conform to schema"""
		raise AbstractError
	def report(self, obj):
		"""
		Report list of failed validity check with hierarchical names, starting at
		obj. Names are represented as ``<type>`` for primitive checks, and as ``repr(<key>):<name>``
		for containers. Hierarchy is added by reference, for example ``list=>2:dict=>'my_key':int``

		A report is a list of tuples of the form ``( <name>, <validity>, <detail> )``.
		"""
		raise AbstractError


# noinspection PyUnresolvedReferences
class SchemaPrimitive(SchemaMember):
	"""
	Schema for a basic data member

	This class is intended for checking basic elements, for example that a value
	exists, is of type ``int`` and satisfies ``func(x)-> x > 3``.

	:see: :py:class:`~.SchemaMember` for parameter description.
	"""
	def __init__(self, types, required = True, constraints = None):
		self.types       = tuple(FlatList(types))
		self.required    = required
		self.constraints = constraints or []

	def __repr__(self):
		return "[%s](req=%s)"%(",".join(_type_str(tp) for tp in self.types),self.required)

	def validate(self, obj):
		if obj == NotSet:
			if not self.required:
				return SchemaElementValidity.VALID
			return SchemaElementValidity.INVALID_MISSING
		if not isinstance(obj, self.types):
			return SchemaElementValidity.INVALID_TYPE
		for constraint in self.constraints:
			if not constraint(obj):
				return SchemaElementValidity.INVALID_CONSTRAINTS
		return SchemaElementValidity.VALID

	def report(self, obj):
		primitive_validity = self.validate(obj)
		if primitive_validity is SchemaElementValidity.VALID:
			return []
		if primitive_validity is SchemaElementValidity.INVALID_MISSING:
			return [ (
				"<None>",
				str(primitive_validity),
				"Got '<None>', expected %s" % listItems(*(_type_str(tp) for tp in FlatList(self.types))),
				str(self)
				) ]
		if primitive_validity is SchemaElementValidity.INVALID_TYPE:
			return [ (
				_type_str(obj),
				str(primitive_validity),
				"Got '%s', expected %s" % (_type_str(obj), listItems(*(_type_str(tp) for tp in FlatList(self.types)))),
				str(self)
				) ]
		if primitive_validity is SchemaElementValidity.INVALID_CONSTRAINTS:
			for key, constraint in iterableitems( self.constraints ):
				if not constraint(obj):
					return [ (
						_type_str(obj),
						str(primitive_validity),
						"Constraint '%s' not satisfied" % key,
						str(self)
						)]
		raise SchemaValidationFailure


# noinspection PyUnresolvedReferences,PyUnresolvedReferences
class SchemaContainer(SchemaMember):
	"""
	Schema for a data member containing other elements

	This is the most general implementation of a Container; the only specific
	constraint is that members are checked by **both** their key (mapping key or
	sequence index) and their value.

	Most use cases should be satisfied by the :py:class:`~.SchemaArray`, :py:class:`~.SchemaTuple`
	and :py:class:`~.SchemaMap` subclasses.

	:see: :py:class:`.SchemaMember` for parameter description."""
	def __init__(self, types, members, required = True, constraints = None, minsize = None, maxsize = None):
		self.primitive   = SchemaPrimitive(types = types, required = required, constraints = constraints)
		self.members     = members
		self.minsize     = minsize
		self.maxsize     = maxsize
		self._len        = len

	def __repr__(self):
		return "%s(max=%s,min=%s,members=%s)"%(self.primitive,self.minsize,self.maxsize,self.members)

	# container protocol
	def __len__(self):
		return self._len(self.members)
	def __iter__(self):
		return iter(self.members)
	def __getitem__(self, item):
		return self.members[item]
	def __contains__(self, item):
		return item in self.members

	def validate(self, obj):
		primitive_validity = self.primitive.validate(obj)
		if primitive_validity is not SchemaElementValidity.VALID:
			return primitive_validity
		container_validity = self._validate_container(obj)
		if container_validity is not SchemaElementValidity.VALID:
			return primitive_validity
		return self._validate_members(obj)

	def size(self, obj):
		return self._len(obj)

	def _validate_container(self, obj):
		if obj is NotSet:
			return SchemaElementValidity.VALID
		if self.minsize is not None and self.size(obj) < self.minsize:
			return SchemaElementValidity.INVALID_SIZE
		if self.maxsize is not None and self.size(obj) > self.maxsize:
			return SchemaElementValidity.INVALID_SIZE
		return SchemaElementValidity.VALID

	def _validate_members(self, obj):
		if obj is NotSet:
			return SchemaElementValidity.VALID
		for key, member in iterableitems(self.members):
			try:
				member_validity = member.validate(obj[key])
			except ( KeyError, IndexError ):
				member_validity = member.validate(NotSet)
			if member_validity is not SchemaElementValidity.VALID:
				return member_validity
		return SchemaElementValidity.VALID

	def report(self, obj):
		if self.primitive.validate(obj) is not SchemaElementValidity.VALID:
			return self.primitive.report(obj)
		if self._validate_container(obj) is not SchemaElementValidity.VALID:
			return self.primitive.report(obj) + self._report_container(obj)
		return self.primitive.report(obj) + self._report_container(obj) + self._report_members(obj)

	def _report_container(self, obj):
		container_validity = self._validate_container(obj)
		if container_validity is not SchemaElementValidity.VALID:
			if container_validity is SchemaElementValidity.INVALID_SIZE:
				return [ (
					_type_str(obj),
					str(container_validity),
					"Length %d, expected %s" % (
						self.size(obj),
						listItems(
							*(
								size for size in (
									self.minsize is not None and "> %s" % self.minsize or "",
									self.maxsize is not None and "< %s" % self.maxsize or "",
									)
								if size
							),
							relation = "and"
							)
						)
					) ]
			return [ (
				_type_str(obj),
				str(container_validity),
				"Unspecified"
				)]
		return []

	def _report_members(self, obj):
		member_reports = []
		member_validity = self._validate_members(obj)
		if member_validity is not SchemaElementValidity.VALID:
			for key, member in iterableitems(self.members):
				try:
					member_report = member.report(obj[key])
				except ( KeyError, IndexError ):
					member_report = member.report(NotSet)
				for report in member_report:
					member_reports.append((
						"%s=>%s:%s"%(_type_str(obj), key, report[0]),
						report[1],
						report[2],
						),
					)
		return member_reports


class SchemaArray(SchemaContainer):
	"""
	Schema for a data member containing a sequence of similar elements

	Special subclass of :py:class:`~.SchemaContainer` for :py:class:`list`s or :py:class:`tuple`s
	containing members from a selection of candidates. Members of a checked container
	are compared against all members of the ``members`` parameter; only a single
	match is required to accept the object. The key or index is ignored entirely.

	:param members: list of members that are allowed in the array
	:type members: list[:py:class:`~.SchemaPrimitive`] or list[:py:class:`.SchemaContainer`]
	"""
	def __init__(self, members, required = True, constraints = None, minsize = None, maxsize = None):
		if isinstance(members, SchemaPrimitive):
			members = (members,)
		SchemaContainer.__init__(
			self,
			types = (list,tuple),
			members = members,
			required = required,
			constraints = constraints,
			minsize = minsize,
			maxsize = maxsize
		)

	def _validate_members(self, obj):
		if obj is NotSet:
			return SchemaElementValidity.VALID
		for obj_key, obj_member in iterableitems(obj):
			for key, member in iterableitems(self.members):
				try:
					member_validity = member.validate(obj_member)
				except ( KeyError, IndexError ):
					member_validity = member.validate(NotSet)
				if member_validity is SchemaElementValidity.VALID:
					break
			else:
				return member_validity
		return SchemaElementValidity.VALID

	def _report_members(self, obj):
		member_reports = []
		member_validity = self._validate_members(obj)
		if member_validity is not SchemaElementValidity.VALID:
			for obj_key, obj_member in iterableitems(obj):
				for key, member in iterableitems(self.members):
					try:
						member_report = member.report(obj_member)
					except ( KeyError, IndexError ):
						member_report = member.report(NotSet)
					for report in member_report:
						member_reports.append((
							"%s=>%s:%s"%(_type_str(obj), obj_key, report[0]),
							report[1],
							report[2],
							)
						)
		return member_reports


class SchemaTuple(SchemaContainer):
	"""
	Schema for a data member containing a sequence of predefined elements

	Special subclass of :py:class:`~.SchemaContainer` for tuples of fixed order
	and length.
	"""
	def __init__(self, members, required = True, constraints = None):
		SchemaContainer.__init__(
			self,
			types = type(members),
			members = members,
			required = required,
			constraints = constraints,
			minsize = len(members),
			maxsize = len(members)
		)


class SchemaMap(SchemaContainer):
	"""
	Schema for a data member containing a sequence of key indexed elements

	Special subclass of :py:class:`~.SchemaContainer` for mappings with fixed
	members.
	"""
	def __init__(self, members, required = True, constraints = None):
		SchemaContainer.__init__(
			self,
			types = type(members),
			members = members,
			required = required,
			constraints = constraints,
			minsize = 0,
			maxsize = len(members)
		)

	def iteritems(self):
		return self.members.iteritems()

class Schema(object):
	"""
	Helper for providing high-level interface to a hierarchy of schema members.

	:param root : root of the schema hierarchy
	:type root: SchemaMember

	"""
	def __init__(self, root):
		self._root = root
	def validate(self, obj):
		return self._root.validate(obj) is SchemaElementValidity.VALID
	def report(self, obj):
		return self._root.report(obj)