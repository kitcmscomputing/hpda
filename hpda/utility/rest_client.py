#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# standard library imports
import threading
import json
import pycurl
import weakref

# third party imports
from ..external.RestClient.RestApi import RestApi
from ..external.RestClient.ProxyPlugins.Socks5Proxy import Socks5Proxy
from ..external.RestClient.ErrorHandling.RestClientExceptions import HTTPError

# application/library imports
from .exceptions    import BasicException, APIError, RethrowException
from .utils         import Noop
from .enum          import BaseEnum
from .singleton     import BaseSingleton, singleton__init

"""
Client API for using http REST APIs. This module provides a for-convencience
interface to simplify and encapsule REST calls.
"""

class ConnectionError(BasicException):
	"""A request failed because of the connection"""
	pass

class ServerError(BasicException):
	"""A request failed because of the server"""
	pass

class ClientError(BasicException):
	"""A request failed because of the client"""
	pass

class RequestType(BaseEnum):
	"""
	HTTP request types

	:cvar GET: Request information on a resource
	:cvar PUT: Provide information on a resource
	:cvar POST: Provide input for a resource
	:cvar DELETE: Remove a resource
	"""
	__members__ = ["GET","PUT","POST","DELETE"]

# content_type->header_params, data_transformation
_content_type_mapper = {
	None   : ({}, lambda data: data),
	'json' : ({"Content-Type" : "application/json"}, lambda data: data if isinstance(data, basestring) else json.dumps(data))
}
# response_type->response_transformation
_response_type_mapper = {
	None   : (lambda response: response.body),
	'json' : (lambda response: json.loads(response) if isinstance(response, basestring) else response)
}

class RestClientPool(BaseSingleton):
	"""
	Pool of REST clients sharing authentication

	The Pool contains a number of REST clients, each corresponding to a
	single thread of execution. Threads performing REST requests through
	the pool are automatically handed their proper client.

	Ideally, a single thread creates the pool and passes it along to any
	other thread that should use the same authentications.

	:param timeout: maximum duration in seconds to wait for a connection
	:type timeout: int or float
	:param auth_func: pycurl authentication function
	:param proxy_url: url for pycurl proxy
	:param curl_options: additional options to pass to PyCurl
	"""
	@singleton__init
	def __init__(self, timeout=60, auth_func=Noop, proxy_url=None, curl_options=None):
		self._auth_func = auth_func
		self._proxy     = Socks5Proxy(proxy_url=proxy_url) if proxy_url is not None else None
		self._clients   = weakref.WeakKeyDictionary()
		self._curl_opts = curl_options or {}
		self._curl_opts.update({pycurl.NOSIGNAL: 1})
		if timeout is not None:
			self._curl_opts.update({pycurl.CONNECTTIMEOUT: timeout})

	@classmethod
	def _singleton_signature(cls, timeout=30, auth_func=Noop, proxy_url=None, curl_options=None):
		return None

	def release_client(self):
		"""
		Remove a client from the pool

		This releases any client belonging to the calling thread from any
		references the pool may hold, allowing it to be garbage collected
		if no further references are held.

		:return:
		"""
		try:
			del self._clients[threading.currentThread()]
		except KeyError:
			pass

	def get_client(self):
		"""
		Return the client corresponding to the current thread

		:return: RestApi
		"""
		try:
			return self._clients[threading.currentThread()]
		except KeyError:
			new_api = RestApi(
				auth=self._auth_func(),
				proxy=self._proxy,
				additional_curl_options=self._curl_opts,
				use_shared_handle=hasattr(pycurl, "LOCK_DATA_SSL_SESSION")
			)
			self._clients[threading.currentThread()] = new_api
			return new_api

	def _dispatch_request(self, request, url, api, data=None, content_type='json', response_type='json'):
		request_header, data = _content_type_mapper[content_type][0],_content_type_mapper[content_type][1](data)
		try:
			if request == 'get':
				response = self.get_client().get(url, api, params={}, data=data, request_headers=request_header).body
			elif request == 'put':
				response = self.get_client().put(url, api, params={}, data=data, request_headers=request_header).body
			elif request == 'post':
				response = self.get_client().post(url, api, params={}, data=data, request_headers=request_header).body
			elif request == 'delete':
				response = self.get_client().delete(url, api, params={}, data=data, request_headers=request_header).body
			else:
				raise APIError
		except pycurl.error as err:
			if err[0]==7:
				raise ConnectionError("Connection refused")
			if err[0]==28:
				raise ConnectionError("Connection timeout")
			raise RethrowException("Unhandled pycurl error")
		except HTTPError as err:
			if err.code // 100 == 4:
				raise ClientError(err)
			if err.code // 100 == 5:
				raise ServerError(err)
			raise RethrowException("Unhandled HTTP error")
		except Exception:
			raise RethrowException("Unhandled exception")
		return _response_type_mapper[response_type](response)


	def get(self, url, api, data=None, content_type='json', response_type='json'):
		return self._dispatch_request(request='get', url=url, api=api, data=data, content_type=content_type, response_type=response_type)

	def put(self, url, api, data=None, content_type='json', response_type='json'):
		return self._dispatch_request(request='put', url=url, api=api, data=data, content_type=content_type, response_type=response_type)

	def post(self, url, api, data=None, content_type='json', response_type='json'):
		return self._dispatch_request(request='post', url=url, api=api, data=data, content_type=content_type, response_type=response_type)

	def delete(self, url, api, data=None, content_type='json', response_type='json'):
		return self._dispatch_request(request='delete', url=url, api=api, data=data, content_type=content_type, response_type=response_type)
