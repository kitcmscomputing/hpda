#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
try:
	import posixpath
except ImportError:
	raise ImportError("Not implemented for non-posix plattforms")

import os
import errno
import time
from threading import Lock, current_thread
from hpda.utility.utils import ensure_rm

class PIDLock(object):
	"""
	Inter Process lock exposing the PID of the owning process

	Implements a lock owned across independent processes. The lock is by design
	reentrant. In addition, an acquired lock also exposes the PID of the owning
	process.

	By default, the lock does not provide multi *thread* safety in a given
	process; an optional :py:class:`Lock` (or similar) can be provided. The
	combined lock is thread safe and persistent on multiple acquiring threads
	(the lock is only released when no thread has or waits for the lock).

	:param lock_name: the name of the lock (file)
	:type lock_name: :py:class:`str`
	:param lock_permission: permissions to apply to the lock object
	:type lock_permission: :py:class:`int` or :py:class:`~hpda.utility.configuration.values.permission`
	:param internal_lock: Auxilliary lock thread synchronization
	:type internal_lock: :py:class:`threading.Lock` or :py:class:`multiprocessing.Lock`
	:param purge_delay: delay after creation of a lock before it can be purged
	:type purge_delay: :py:class:`int`, :py:class:`float` or :py:class:`~hpda.utility.configuration.values.seconds`

	:note: Processes spawned from the same executable can be better synchronized
	       using :py:class:`multiprocessing.Lock`.
	"""
	def __init__(self, lock_name, lock_permission=0o664, internal_lock=None, purge_delay=2):
		self.lock_name = lock_name
		self.lock_perm = lock_permission
		self._internal = internal_lock
		self._state_lock = Lock()
		self._pid_file = None
		self._lock_owners = set()
		self._purge_delay = purge_delay

	def __repr__(self):
		return "%s(lock_name=%r, lock_permission=%s, internal_lock=%s, purge_delay=%s, owner_pid=%s)"%(self.__class__.__name__, self.lock_name, self.lock_perm, self._internal, self._purge_delay, self._read_pid())

	def __del__(self):
		self._release_pid_lock()

	## Acquire and Release
	## -------------------
	def acquire(self, blocking=True):
		"""
		Acquire the pid lock and optional internal lock

		:param blocking: whether to block until the lock can be acquired
		:type blocking: :py:class:`bool`
		:return: whether the lock was acquired
		:rtype: :py:class:`bool`
		"""
		self._lock_owners.add(current_thread().ident)
		with self._state_lock:
			if self._pid_file or self._acquire_pid_lock(blocking=blocking):
				if self._internal is None:
					return True
				if self._internal.acquire(blocking):
					return True
			# not trying to get a lock after failure - remove claim
			self._lock_owners.remove(current_thread().ident)
			if ( not self._lock_owners ) and ( self._pid_file is not None ):
				self._release_pid_lock()
			return False

	def release(self):
		"""Release the pid lock and optional internal lock"""
		self._lock_owners.remove(current_thread().ident)
		with self._state_lock:
			if self._internal is not None:
				self._internal.release()
			if not self._lock_owners:
				self._release_pid_lock()

	def _release_pid_lock(self):
		"""Release the actual pid lock"""
		if self._pid_file and self._read_pid() == os.getpid():
			self._pid_file.close()
			ensure_rm(self.lock_name)
		self._pid_file = None

	def _acquire_pid_lock(self, blocking=False):
		"""Acquire the actual pid lock"""
		try:
			self._pid_file = os.fdopen(
				os.open(self.lock_name, os.O_CREAT|os.O_EXCL|os.O_WRONLY),
				'w'
			)
		except OSError as err:
			if err.errno != errno.EEXIST:
				raise
		else:
			self._write_pid()
			return True
		if not self._purge_stale_lock() and not blocking:
			return False
		return self._acquire_pid_lock()

	def _purge_stale_lock(self):
		"""Remove a lock IF it is stale (not held by any process)"""
		if time.time() - os.stat(self.lock_name).st_ctime < self._purge_delay:
			return False
		if self.get_active_pid() in (None, os.getpid()):
			ensure_rm(self.lock_name, "PID lock")
			return True
		return False

	# Context
	__enter__ = acquire
	def __exit__(self, exc_type, exc_val, exc_tb):
		self.release()
		return False

	## Querrying and Bookkeeping
	## -------------------------
	def get_active_pid(self):
		"""
		Return the PID of the process holding the lock

		This will extract the PID and check whether the process is still alive.
		If either no process claims the lock or the claiming process has died
		before releasing the lock, :py:class:`None` is returned.

		:return: PID of the owning process
		:rtype: :py:class:`int` or :py:class:`None`
		"""
		pid = self._read_pid()
		if pid is None:
			return pid
		try:
			os.kill(pid, 0)
		except OSError as err:
			if err.errno is errno.EPERM: # not allowed to kill EXISTING process
				return pid
			if err.errno is errno.ESRCH: # no such process
				return None
			raise
		else:
			return pid

	def update_pid(self):
		"""
		Update the lock's PID to match the current process

		This is a function for advanced program flows. It is only required when
		the program changes its PID, e.g. due to ``fork``\ ing.

		:raises RuntimeError: if the lock is not owned by the process
		"""
		self._write_pid()

	def _read_pid(self):
		try:
			with open(self.lock_name,'r') as pid_file:
				return int(pid_file.read().strip())
		except (OSError, IOError) as err:
			if err.errno == errno.ENOENT:
				return None
			raise
		except ValueError:
			return None

	def _write_pid(self):
		if self._pid_file is None:
			raise RuntimeError("PID may only be written by lock owner")
		self._pid_file.seek(0)
		self._pid_file.write("%d\n"%os.getpid())
		self._pid_file.flush()