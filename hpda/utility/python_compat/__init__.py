#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
package python_compat

This package re-implements various functions and modules to ensure backwards
compatibility of critical code sections to older python versions.

func **check_output** (:py:func:`subprocess.check_output`)
  Run command with arguments and return its output as a byte string.
  Source: python 2.7

class **OrderedDict** (:py:class:`collections.OrderedDict`)
  Ordered dictionaries are just like regular dictionaries but they remember the
  order that items were inserted. When iterating over an ordered dictionary, the
  items are returned in the order their keys were first added. Also patched in
  as a member of the ``collections`` module.
  Source: python 2.7

class **WeakSet** (:py:class:`weakref.WeakSet`)
  A ``set`` holding only weak references to its members; otherwise equivalent to the
  default ``set`` object. Also patched in as a member of the ``weakref`` module.
  Source: python 2.7

module **argparse** (:py:mod:`argparse`)
  A parser for command line arguments; similar but superior to the ``optparse``
  module.
  Source: python 2.7

module **importlib** (:py:mod:`importlib`)
  Convenience wrapper around the low-level ``__import__``
  Source: python 2.7
"""

import sys as _sys

try:
	from weakref import WeakSet as _WeakSet
except ImportError:
	from _weakset import WeakSet as _WeakSet
	import weakref as _weakref
	_weakref.WeakSet = _WeakSet

try:
	from subprocess import check_output as _check_output
except ImportError:
	from _subprocess import check_output as _check_output
	import subprocess as _subprocess
	_subprocess.check_output = _check_output

try:
	from collections import OrderedDict as _OrderdDict
except ImportError:
	from _collections import OrderedDict as _OrderdDict
	import collections as _collections
	_collections.OrderedDict = _OrderdDict

try:
	import argparse as _argparse
except ImportError:
	import _argparse
	_sys.modules["argparse"] = _argparse

try:
	import importlib as _importlib
except ImportError:
	import _importlib
	_sys.modules["importlib"] = _importlib

__all__ = []
