#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
module values

Pseudo-types representing complex types. The primary use for these types is the
support of human readable settings, e.g. time as an ``hh:mm:ss`` literal.

These types are not required to be classical types (i.e. subclasses of type),
but merely guarantee the existence of type.__name__ and that when called with a
string representation of a valid value, they will return a useable representation
of it.
"""

# standard library imports
import re
import sys

# third party imports

# application/library imports
try:
	from crontab import CronTab
except ImportError:
	class CronTab(object):
		"""Placeholder if crontab is not available but not required either"""
		def __init__(self, *args, **kwargs):
			raise ImportError('No module named crontab')

class __value_metaclass__(type):
	def __repr__(cls):
		return "<type '%s.%s'>"%(__package__.split(".",1)[0], cls.__name__)

class stream(file):
	"""
	File-like stream from string

	Input formats:

	* Reserved strings ``"stdout"``, ``"stderr"`` and ``"stdin"`` to their ``sys`` equivalents
	* Explicit file objects as ``"[<mode>:]file:<path>"``, e.g. ``"w+:file:/tmp/foo"`` to ``open("/tmp/foo","w+")``
	* Implicit file objects as ``"<path>"``, e.g. ``"/tmp/bar"`` to ``open("/tmp/bar")``
	"""
	__metaclass__ = __value_metaclass__
	def __new__(cls, literal):
		if isinstance(literal, file):
			return literal
		if literal.lower() in ["stdout","stderr","stdin"]:
			return getattr(sys, literal.lower())
		if "file:" in literal:
			mode, _, path = literal.rpartition("file:")
			if mode.endswith(":"):
				if mode.endswith(":"):
					return open(path, mode=mode[:-1])
				raise ValueError("File mode must be delimeted with ':'")
			return open(path)
		return open(literal)

class boolean(int):
	"""
	Bool from string
	"""
	__metaclass__ = __value_metaclass__
	str_true = ("yes","y","true","1","on")
	str_false = ("no","no","false","0","off")
	def __new__(cls, literal):
		if isinstance(literal, bool):
			return literal
		if isinstance(literal, (str, unicode)):
			if literal.lower() in cls.str_true:
				return True
			if literal.lower() in cls.str_false:
				return False
		raise ValueError("Expected bool, %s for True, %s for False" % (cls.str_true, cls.str_false))


class percent(float):
	"""
	Percentage as float

	Input formats:

	* Explicit string as ``"%f\%"``, e.g. ``"69.2%"`` to ``0.692``
	* Floating string as ``"%f"``, e.g. ``"0.692"`` to ``0.692``
	"""
	__metaclass__ = __value_metaclass__
	def __new__(cls, literal):
		try:
			if isinstance(literal, basestring):
				if literal[-1]=='%':
					return float(literal[:-1]) / 100.0
			return float(literal)
		except:
			raise ValueError("Expected numeric type, '%f[%]'")


class crontab(CronTab):
	"""
	Crontab as datetime generator

	Input formats:

	* crontab string of the form ``Minute Hour Day-of-Month Month Day-of-Week [Year]``

	Each entry may be defined as a number (``1``), list of numbers (``1,2,3``),
	range of numbers (``1-5``), sequence of numbers in a range (``10-20/5``
	equals ``10,15,20``) or any allowed value (``*``). In addition, month or
	day-of-week entries support 3 letter abbreviations (``mon``,``tue`` etc. and
	``jan``, ``feb`` etc.).
	"""
	__metaclass__ = __value_metaclass__


class seconds(float):
	"""
	Time as float in seconds

	Input formats:

	* Plain seconds as ``"%f"``, e.g. ``"120"`` to ``120.0``
	* With unit as ``"%f[s|m|h|d|M]"``, e.g. ``"2m"`` to ``120.0``
	* Literal as ``"[[[%d:]%d:]%d:]%f"`` (dd:hh:mm:ss), e.g. ``"01:02:03"`` to ``3723.0``
	"""
	__metaclass__ = __value_metaclass__
	def __new__(cls, literal):
		try:
			# raw time as "123315135"
			try:
				return float(literal)
			except ValueError:
				pass
			# with unit as "34523d"
			if literal[-1]in "smhdM":
				return float(literal[:-1]) * {'s':1,'m':60,'h':60*60,'d':60*60*24,'M':60*60*24*30}[literal[-1]]
			# as time as 23:19:03
			if literal.startswith("-"):
				sign = -1
				literal = literal[1:]
			else:
				sign = 1
			mults = (1,60,60*60,60*60*24)
			parts = literal.split(":")
			return sign * sum(float(parts[-ind-1])*mults[ind] for ind in xrange(len(parts)))
		except:
			raise ValueError("Expected numeric type, '%f[s|m|h|d|M]' or '[[[%d:]%d:]%d:]%f' (dd:hh:mm:ss)")

class permission(int):
	"""
	File/Directory permissions as (oktal) :py:class:`int`

	Input formats:

	* Dezimal permission, e.g. ``"256"`` to ``r---------``
	* Oktal permission, e.g. ``"0744"`` to ``rwxr--r--``
	* Symbolic permissions, e.g. ``"rwxr--r--"`` to ``rwxr--r--``

	Symbolic permissions may be truncated to the right, e.g. ``"rwxr--r"`` is
	equivalent to ``"rwxr--r--"``.

	Integer as well as dezimal and oktal literals allow storing higher bit
	content. This allows capturing ``st_mode`` of :py:func:`os.stat` without
	loosing object type information.

	Comparison operations ignore any additional higher bits. Otherwise, they
	behave equivalent :py:class:`set` comparisons of bits.

	.. method:: other == perm

	   Test that *other* has *all* the same permission bits set and none else.

	.. method:: other != perm

	   Same as ``not self == other``, i.e. differs at least in one bit.

	.. method:: other > perm

	   Test that *other* has *all* the same and *more* permission bits set, i.e.
	   is strictly more permissive.

	.. method:: other < perm

	   Test that *other* has *no other* and *less* permission bits set, i.e.
	   is strictly less permissive.

	.. method:: perm <= other
	            perm >= other

	   Shorthand for ``permission < other or permission == other`` and
	   ``permission > other or permission == other``, respectively.

	.. describe:: x in permission

	   Same as ``x <= permission``, but also accepting literals for `x`.
	"""
	__metaclass__ = __value_metaclass__
	_perms = "rwxrwxrwx"
	_pbits = (0o400,0o200,0o100,0o040,0o020,0o010,0o004,0o002,0o001)
	def __new__(cls, literal):
		if isinstance(literal, int):
			return int.__new__(cls, literal)
		try:
			# Dezimal/Oktal/...
			return int.__new__(cls, literal,0)
		except ValueError:
			# symbolic
			if len(literal) > len(cls._perms):
				raise ValueError("Trailing content after group flags")
			perm = 0
			for idx in range(len(literal)):
				if cls._perms[idx]==literal[idx]:
					perm += cls._pbits[idx]
				elif literal[idx]!="-":
					raise ValueError("Literal must be combination of '%s' and '%s'"%(cls._perms, "-"*len(cls._perms)))
			perm = sum(cls._pbits[idx] if cls._perms[idx]==literal[idx] else 0 for idx in range(len(literal)))
			return int.__new__(cls, perm)

	def __contains__(self, item):
		if isinstance(item, str):
			item = self.__class__(item)
		return item <= self

	def __eq__(self, other):
		if not isinstance(other, int):
			return False
		return not ((self ^ other) & 0o777)

	def __ne__(self, other):
		return not self == other

	def __lt__(self, other):
		if not isinstance(other, int):
			raise TypeError
		return self <= other and not self == other

	def __le__(self, other):
		if not isinstance(other, int):
			raise TypeError
		return ( self | other ) & 0o777 == other & 0o777

	def __gt__(self, other):
		if not isinstance(other, int):
			raise TypeError
		return self >= other and not self == other

	def __ge__(self, other):
		if not isinstance(other, int):
			raise TypeError
		return ( self | other ) & 0o777 == self & 0o777

	def __repr__(self):
		return "%s('%s')"%(self.__class__.__name__, str(self))

	def __str__(self):
		return "".join(self._perms[idx] if self._pbits[idx] & self else "-" for idx in range(len(self._perms)))

class bytesize(float):
	"""
	Size as float in bytes

	Input formats:

	* Plain bytes as ``"%f"``, e.g. ``"23"`` to ``23.0``
	* As literal as ``"%f[Y|Z|E|P|T|G|M|k][B|iB]"``, e.g. ``"4kiB"`` to ``4096.0``
	"""
	__metaclass__ = __value_metaclass__
	def __new__(cls, literal):
		try:
			try:
				return float(literal)
			except ValueError:
				pass
			numerical, unit = re.match("(\d*)(\D+)", literal).groups()
			numerical, unit = float(numerical), unit.strip()
			if not unit:
				return numerical
			bases = ( ('iB', 1024), ('B', 1000), ('', 1000))
			powers = ( ('Y', 8), ('Z', 7), ('E', 6), ('P', 5), ('T', 4), ('G', 3), ('M', 2), ('k', 1), ('', 0))
			base, power = None, None
			for pre, power in powers:
				if unit.startswith(pre):
					unit = unit[len(pre):]
					break
			for end, base in bases:
				if unit.endswith(end):
					unit = unit[:-len(end)]
					break
			if unit:
				raise ValueError
			return numerical * (base ** power)
		except:
			raise ValueError("Expected numeric type or '%f[Y|Z|E|P|T|G|M|k][Byte|B|biByte|iB]'")

