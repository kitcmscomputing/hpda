#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
The building blocks for defining how to process a configuration. In most cases,
it is sufficient to work with these elements alone.

Currently there are two major element types and one auxiliary type:

Section, Choice, Group
  These elements represent sections in the configuration. They define what
  attributes are expected and how to apply them. This is done by transforming
  the input from a configuration to a ``**kwargs`` representation and passing
  it on to a target callable.

ConfigAttribute, ConfigAttributeArray, ...
  These elements represent individual attributes inside a section. They define
  how to handle an attribute value (usually by providing a conversion ``type``)
  and allow setting defaults as well as providing descriptions.

Loader
  An auxiliary utility to simplify defining the application of a configuration.
  The loader combines multiple Sections and their Attributes with an interface
  for digesting configuration input. Simple modules intended for standalone use
  should provide a Loader, which is then invoked with a configuration path.
"""


# standard library imports
import inspect
from collections import OrderedDict

# third party imports

# application/library imports
from hpda.utility.exceptions import APIError
from hpda.utility.utils    import NotSet
from .values          import percent, seconds, bytesize, stream, permission, crontab
from .exceptions      import ConfigSectionError, ConfigKeyError, ConfigValueError
from .container       import Configuration

# Section primitives
class Section(object):
	"""
	Individual section of the configuration for a single type of object

	:param name: Section name in the Configuration
	:type name: str
	:param attributes: Expected attributes in the section
	:type attributes: dict[str,ConfigAttribute]
	:param target: target to receive attributes as kwargs
	:type target: callable
	:param descr: documentation for this Section
	:type descr: str
	"""
	def __init__(self, attributes, target=None, name=None, descr=None):
		self.name = name
		self.target = target
		self.attributes = attributes
		self.descr = descr

	def kwargs(self, configuration, name=None):
		"""
		Extract the corresponding section from a configuration to a dict representation

		:param configuration: configuration object to kwargs from
		:type configuration: Configuration
		:param name: alternate section name to use
		:type name: str or unicode
		"""
		my_name = name or self.name
		if not my_name in configuration and any(attr.default is NotSet for attr in self.attributes.values()):
			raise ConfigSectionError(section=my_name, required_keys=[key for key, attr in self.attributes.items() if attr.default is NotSet])
		kwargs = {}
		for key in self.attributes:
			try:
				if not my_name in configuration or not key in configuration[my_name]:
					kwargs[key] = self.attributes[key].digest(NotSet)
					configuration.get(my_name).add_attribute(
						key,
						self.attributes[key].values(),
						self.attributes[key].descr
					)
				else:
					kwargs[key] = self.attributes[key].digest(configuration[my_name][key].value)
			except KeyError:
				raise ConfigKeyError(
					section=my_name,
					key=key
				)
			except ValueError:
				raise ConfigValueError(
					section=my_name,
					key=key,
					value=configuration.get(my_name).get_attribute(key).value,
					config_attribute=self.attributes[key],
				)
		return kwargs

	def apply(self, configuration, name=None, target_kwargs=None):
		"""
		Apply settings from a configuration to target

		:param configuration: configuration object to kwargs from
		:type configuration: Configuration
		:param name: alternate section name to use
		:type name: str or unicode
		:param target_kwargs: additional (default) kwargs to pass to the target
		:type target_kwargs: dict
		"""
		if self.target is None:
			raise APIError("Cannot apply a section without target callable")
		target_kwargs = target_kwargs  and target_kwargs.copy() or {}
		target_kwargs.update(self.kwargs(configuration=configuration, name=name))
		return self.target(**target_kwargs)

	def __repr__(self):
		return "%s(target=%s, name=%s, attributes=%s)"%(self.__class__.__name__, self.target, self.name, self.attributes)

	@classmethod
	def from_callable(cls, target, types=None, ignore=(), name=None):
		"""
		Create a Section from a target's call signature

		 This functions inspects the callable target for its call signature and attempts to construct a Section object capable of digesting parameters from a configuration.
		 For every call parameter, the function performs a stepwise, lazy lookup and uses the first match found:

		1. types has a ConfigAttribute instance indexed by the parameter:
		   the ConfigAttribute instance is used
		2. types contains a primitive type indexed by the parameter:
		   a new ConfigAttribute of the type is created; the default is set to
		   the call signature default, if any
		3. types contains a list of primitive type indexed by the parameter:
		   a new ConfigAttributeArray of the type is created; the default is set
		   to the call signature default, if any
		4. the signature default is a primitive type:
		   a new ConfigAttribute of the type is created with the default
		5. the signature default is a list of primitive type:
		   a new ConfigAttributeArray of the type is created with the default
		6. An exception is raised

		:attention: No checks for conformity of the callable/generated signature to the configuration infrastructure is performed.

		:param target: callable to be configured from the section
		:param types: mapping of parameter name to type/CfgAttr to use
		:param ignore: parameter names to ignore
		:param name: section name in the configuration
		"""
		types = types or {}
		ignore = set(ignore)
		ignore.add("self")
		attributes = {}
		type_map = {str:CfgStr, int:CfgInt, float:CfgFloat, bool:CfgBool}
		type_array_map = {str:CfgStrArray, int:CfgIntArray, float:CfgFloatArray, bool:CfgBoolArray}
		try:
			argspec = inspect.getargspec(target)
		except TypeError:
			argspec = inspect.getargspec(target.__init__)
		defaults = [NotSet]*(len(argspec.args)-len(argspec.defaults))
		defaults.extend(argspec.defaults)
		for parameter, default in zip(reversed(argspec.args), reversed(defaults)):
			if parameter in ignore:
				continue
			argtype = types.get(parameter, type(default))
			if not issubclass(argtype, ConfigAttribute):
				try:
					argtype = type_map[argtype]
				except KeyError:
					try:
						argtype = type_array_map[type(default[0])]
					except (KeyError, IndexError, TypeError):
						raise ValueError("Unable to find type for parameter '%s'"%parameter)
			attributes[parameter]=argtype(default=default)
		return cls(target=target, attributes=attributes, name=(name or target.__name__))


class Choice(object):
	"""
	Individual section of the configuration for similar types of object

	When digesting a configuration, the Choice class will evaluate the attribute
	specified by criteria. This is used as the key to sections in order to select
	the appropriate Section.

	Sections can be either an iterable or mapping of sections. If an iterable,
	it is transformed to a mapping where each Section key is its target name. If
	a mapping is provided, its keys are not modified.

	:param sections: Candidates that could apply to the section
	:type sections: list[Section] or dict[str,Section]
	:param name: the name of the section
	:type name: str
	:param type_key: attribute name for selecting target type
	:type type_key: str
	:param type_default: name of type to use if none is specified
	:type type_default: str
	:param type_descr: description for the type selection argument
	:type type_descr: str
	:param descr: documentation for this Choice
	:type descr: str
	"""
	def __init__(self, sections, name=None, type_key="type", type_default=NotSet, type_descr="Attribute to select target", descr=None):
		self.name = name
		self.key  = type_key
		self.attr = CfgStr(default=type_default, descr=type_descr)
		if not hasattr(sections, "keys"):
			self.sections = OrderedDict((section.target.__name__, section) for section in sections)
		else:
			self.sections = sections
		self.descr = descr

	def read(self, configuration, name=None):
		"""
		Extract the corresponding section from a configuration

		:param configuration: configuration object to kwargs from
		:type configuration: Configuration
		:param name: alternate section name to use
		:type name: str or unicode
		:returns: tuple of target name and section
		"""
		my_name = name or self.name
		try:
			if not my_name in configuration or not self.key in configuration[my_name]:
				my_type = self.attr.digest(NotSet)
				configuration.get(my_name).add_attribute(
					self.key,
					self.attr.values(),
					self.attr.descr
				)
			else:
				my_type = self.attr.digest(configuration[my_name][self.key].value)
		except KeyError:
			raise ConfigKeyError(
				section=my_name,
				key=self.key
			)
		except ValueError:
			raise ConfigValueError(
				section=my_name,
				key=self.key,
				value=configuration.get(my_name).get_attribute(self.key).value,
				config_attribute=self.attr,
			)
		return my_name, my_type, self.sections[my_type]

	def apply(self, configuration, name=None, target_kwargs=None):
		"""
		Apply settings from a configuration
		"""
		my_name, my_type, my_section = self.read(configuration, name=name)
		return my_section.apply(configuration, name=my_name, target_kwargs=target_kwargs)


class Group(object):
	"""
	Group of sections in the configuration for similar types of object

	When digesting a configuration, all sections beginning with name followed by
	a dot (e.g. ``[section.a]`` or ``[section.]``)are passed on to target for
	digestion.

	:param target: object capable of applying a configuration section
	:type target: Section or Choice
	:param name: root name for all sections to be digested
	:type name: str or unicode
	:param section_kw: keyword for passing the section sub-name to the target
	:type section_kw: str
	:param order: perform width-first ordered evaluation
	:type order: None, bool or list
	:param descr: documentation for this Section
	:type descr: str

	If ``target`` is a :py:class:`~.Choice` instance, sections are evaluated in
	random order depth-first by default. This means that for each section, the
	applicable choice is applied before moving to the next section. Setting
	``order`` applies sections in an order specified by their type.

	``order`` is ``True``
	  Sections are evaluated in the order provided by :py:class:`~.Choice`. This
	  is only useful if :py:class:`~.Choice` is instantiated with an ordered
	  collection.

	``order`` is ``list(key)``
	  Sections are evaluated in the order provided by ``order``. Every valid
	  identifier for the underlying :py:class:`~.Choice` must be a ``key`` in
	  ``list(key)``
	"""
	def __init__(self, target, name, section_kw=None, order=None, descr=None):
		self.target = target
		self.name = name
		self.section_kwarg = section_kw
		if order is True:
			self.order = self.target.sections.keys()
		else:
			self.order = order
		self.descr = descr

	def apply(self, configuration, name=None, target_kwargs=None):
		name = name or self.name + '.'
		if self.order:
			return self._apply_width_first(configuration, base_name=name, target_kwargs=target_kwargs)
		return self._apply_depth_first(configuration, base_name=name, target_kwargs=target_kwargs)

	def _apply_depth_first(self, configuration, base_name, target_kwargs=None):
		returnees = []
		for section_name in configuration:
			if section_name.startswith(base_name):
				target_kwargs = target_kwargs and target_kwargs.copy() or {}
				if self.section_kwarg is not None:
					target_kwargs[self.section_kwarg] = section_name.partition(".")[2]
				returnees.append(self.target.apply(configuration, section_name, target_kwargs=target_kwargs))
		return returnees

	def _apply_width_first(self, configuration, base_name=None, target_kwargs=None):
		assert isinstance(self.target, Choice), "ordered evaluation requires target to be an instance of Choice"
		sections = []
		for section_name in self._get_sections(configuration, base_name):
			my_name, my_key, my_section = self.target.read(configuration, name=section_name)
			my_target_kwargs = target_kwargs and target_kwargs.copy() or {}
			if self.section_kwarg is not None:
				my_target_kwargs[self.section_kwarg] = my_name.partition(".")[2]
			sections.append((my_key, my_section, my_name, my_target_kwargs))
		sections.sort(key=lambda elem: self.order.index(elem[0]))
		return [elem[1].apply(configuration, name=elem[2], target_kwargs=elem[3]) for elem in sections]

	def _get_sections(self, configuration, base_name):
		for section_name in configuration:
			if section_name.startswith(base_name):
				yield section_name


# Type Primitives
class ConfigAttribute(object):
	"""
	Expected key=>value attribute of the configuration

	:param value_type: callable that transforms strings to the appropriate type
	:type value_type: type
	:param default: value to use if none found in the configuration
	:param val2str: callable that transforms the desired type to strings
	:param descr: documentation for this attribute
	:type descr: str
	:param choices: listing of valid values, each as literal or valid type
	:type choices: None or tuple

	Default values may be specified in one of four variants:

	:py:class:`~hpda.utility.utils.NotSet` (default)
	  There is no default; a value **must** be specified in the configuration.

	:py:const:`None`
	  Returns :py:const:`None`, circumventing any type conversions/checks.

	Type
	  An instance of the ``value_type``, e.g. ``5`` for :py:class:`int`.

	Literal
	  A string containing a literal of the ``value_type``, e.g. ``"4MB"`` for
	  :py:class:`~hpda.utility.configuration.values.bytesize`.

	:note: Both the Type and Literal variant are type checked as part of the evaluation
	       pipeline. A Type is internally converted to a Literal, from which both
	       versions are again converted by a call to ``value_type(Literal)``. This
	       also implies some leeway in configuration semantics, e.g. ``5.3`` is a
	       valid value for ``int`` attributes (it gets converted to ``5``).

	:note: Default values are checked for being compliant with ``choices`` as
	       well. The exception is the default value :py:const:`None`, which is
	       always valid. For :py:class:`~ConfigAttributeArray` and derivatives,
	       include the empty string ``""`` in ``choices`` to allow for empty
	       arrays.

	:see: Derived classes for common, type specific uses.
	"""
	def __init__(self, value_type=str, default=NotSet, val2str=str, descr=None, choices=None):
		self.value_type = value_type
		self.default = default
		self.val2str = val2str
		self.descr   = descr
		self.value   = None
		self.choices = choices

	def values(self, value=None):
		"""
		Return our value as would be provided by the configuration
		"""
		if value is None:
			value = self.value
		if value is None:
			return []
		return [self.val2str(value)]

	def digest(self, str_value=NotSet):
		"""
		Read a literal or type to set our value

		:raises ValueError: if the input cannot be digested
		"""
		if str_value is NotSet:
			if self.default is NotSet:
				raise KeyError
			if self.default is None:
				return None
			if isinstance(self.default, basestring):
				str_value = self.default
			else:
				str_value = self.val2str(self.default)
		try:
			value = self.value_type(str_value)
			if self.choices is not None:
				for choice in self.choices:
					if isinstance(choice, basestring):
						if value == self.value_type(choice):
							break
					elif value == choice:
						break
				else:
					raise ValueError
		except:
			raise ValueError
		else:
			self.value = value
			return self.value

	def __repr__(self):
		return "%s(value_type=%s, default=%s, val2str=%s, descr='%s', choices=%s)" % (self.__class__.__name__, self.value_type, self.default, self.val2str, self.descr, self.choices)

class ConfigAttributeArray(ConfigAttribute):
	"""
	Expected key=>value1,value2,... attribute of the configuration

	:see: :py:class:`ConfigAttribute` for constructor arguments and their meaning
	"""
	def values(self, value=None):
		if value is None:
			value = self.value
		if value is None:
			return []
		return [self.val2str(val) for val in value]

	def digest(self, str_value=NotSet):
		if str_value is NotSet:
			if self.default is NotSet:
				raise KeyError
			if self.default is None:
				return None
			if isinstance(self.default, basestring):
				str_value = self.default
			else:
				str_value = "\n".join(self.val2str(val) for val in self.default)
		try:
			# empty list
			if str_value == "" and (self.choices is None or "" in self.choices):
				return []
			values = [ self.value_type(val) for val in str_value.split("\n")]
			if self.choices is not None:
				for value in values:
					for choice in self.choices:
						if isinstance(choice, basestring):
							if value == self.value_type(choice):
								break
						elif value == choice:
							break
					else:
						raise ValueError
		except:
			raise ValueError
		else:
			self.value = values
			return self.value


class ConfigMultiAttribute(object):
	"""
	A key=>vallue attribute that may be interpreted by multiple :py:class:`~ConfigAttribute`\ s

	When evaluating a value, variants are tried in order until a match is found.
	If none matches, a type hint for all variants is produced.

	:note: Internally, a variant "matching" is defined as "not raising a
	       ValueError".

	:param variants:
	:type variants: list[:py:class:`~.ConfigAttribute`]
	"""
	def __init__(self, variants=(), descr=None):
		assert variants, "Require at least one possible variant for attribute"
		self.variants = variants
		self.descr = descr

	def values(self, value=None):
		for variant in self.variants:
			values = variant.values(value=value)
			if values is not None:
				return values
		return []

	def digest(self, str_value=NotSet):
		for variant in self.variants:
			try:
				return variant.digest(str_value=str_value)
			except (ValueError, KeyError):
				pass
		if str_value is NotSet:
			raise KeyError
		raise ValueError


class CfgStr(ConfigAttribute):
	def __init__(self, default=NotSet, descr=None, choices=None):
		ConfigAttribute.__init__(self, value_type=str, default=default, val2str=str, descr=descr, choices=choices)

class CfgInt(ConfigAttribute):
	def __init__(self, default=NotSet, descr=None, choices=None):
		ConfigAttribute.__init__(self, value_type=int, default=default, val2str=str, descr=descr, choices=choices)

class CfgFloat(ConfigAttribute):
	def __init__(self, default=NotSet, descr=None, choices=None):
		ConfigAttribute.__init__(self, value_type=float, default=default, val2str=str, descr=descr, choices=choices)

class CfgBool(ConfigAttribute):
	def __init__(self, default=NotSet, descr=None, choices=None):
		ConfigAttribute.__init__(self, value_type=bool, default=default, val2str=str, descr=descr, choices=choices)

class CfgPercent(ConfigAttribute):
	def __init__(self, default=NotSet, descr=None, choices=None):
		ConfigAttribute.__init__(self, value_type=percent, default=default, val2str=str, descr=descr, choices=choices)

class CfgSeconds(ConfigAttribute):
	def __init__(self, default=NotSet, descr=None, choices=None):
		ConfigAttribute.__init__(self, value_type=seconds, default=default, val2str=str, descr=descr, choices=choices)

class CfgCronTab(ConfigAttribute):
	def __init__(self, default=NotSet, descr=None, choices=None):
		ConfigAttribute.__init__(self, value_type=crontab, default=default, val2str=str, descr=descr, choices=choices)

class CfgBytes(ConfigAttribute):
	def __init__(self, default=NotSet, descr=None, choices=None):
		ConfigAttribute.__init__(self, value_type=bytesize, default=default, val2str=str, descr=descr, choices=choices)

class CfgStream(ConfigAttribute):
	def __init__(self, default=NotSet, descr=None, choices=None):
		ConfigAttribute.__init__(self, value_type=stream, default=default, val2str=str, descr=descr, choices=choices)

class CfgPerms(ConfigAttribute):
	def __init__(self, default=NotSet, descr=None, choices=None):
		ConfigAttribute.__init__(self, value_type=permission, default=default, val2str=str, descr=descr, choices=choices)


class CfgStrArray(ConfigAttributeArray):
	def __init__(self, default=NotSet, descr=None, choices=None):
		ConfigAttributeArray.__init__(self, value_type=str, default=default, val2str=str, descr=descr, choices=choices)

class CfgIntArray(ConfigAttributeArray):
	def __init__(self, default=NotSet, descr=None, choices=None):
		ConfigAttributeArray.__init__(self, value_type=int, default=default, val2str=str, descr=descr, choices=choices)

class CfgFloatArray(ConfigAttributeArray):
	def __init__(self, default=NotSet, descr=None, choices=None):
		ConfigAttributeArray.__init__(self, value_type=float, default=default, val2str=str, descr=descr, choices=choices)

class CfgBoolArray(ConfigAttributeArray):
	def __init__(self, default=NotSet, descr=None, choices=None):
		ConfigAttributeArray.__init__(self, value_type=bool, default=default, val2str=str, descr=descr, choices=choices)

class CfgPercentArray(ConfigAttributeArray):
	def __init__(self, default=NotSet, descr=None, choices=None):
		ConfigAttributeArray.__init__(self, value_type=percent, default=default, val2str=str, descr=descr, choices=choices)

class CfgSecondsArray(ConfigAttributeArray):
	def __init__(self, default=NotSet, descr=None, choices=None):
		ConfigAttributeArray.__init__(self, value_type=seconds, default=default, val2str=str, descr=descr, choices=choices)

class CfgBytesArray(ConfigAttributeArray):
	def __init__(self, default=NotSet, descr=None, choices=None):
		ConfigAttributeArray.__init__(self, value_type=bytesize, default=default, val2str=str, descr=descr, choices=choices)

class CfgStreamArray(ConfigAttributeArray):
	def __init__(self, default=NotSet, descr=None, choices=None):
		ConfigAttributeArray.__init__(self, value_type=stream, default=default, val2str=str, descr=descr, choices=choices)

class CfgPermsArray(ConfigAttributeArray):
	def __init__(self, default=NotSet, descr=None, choices=None):
		ConfigAttributeArray.__init__(self, value_type=permission, default=default, val2str=str, descr=descr, choices=choices)


class Loader(object):
	"""
	Convencience wrapper for loading and applying configuration

	:param consumers: Consumables for configurations
	:type consumers: list[Section|Choice|Group]
	"""
	def __init__(self, *consumers):
		self.consumers = consumers

	def configure(self, *config_sources):
		cfg = Configuration(*config_sources)
		returnees = []
		for consumer in self.consumers:
			returnee = consumer.apply(cfg)
			if returnee is None:
				continue
			try:
				returnees.extend(returnee)
			except TypeError:
				returnees.append(returnee)
		return returnees
