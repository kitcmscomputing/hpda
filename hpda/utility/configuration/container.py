#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
The container objects serve to read, construct and hold the state of the content
of the configuration. For the most part, these objects can be considered implementation
detail. Merely the ``Configuration`` class is of relevance for actually loading
or modifying a configuration.

:see: The sub-module :py:mod:`.interface` provides the building blocks for defining
      an interface for applying a configuration to your objects, functions and modules.

Configuration Formats
=====================

Files
-----

The configuration file format supports key, value pairs organized in sections,
optionally annotated with comments. Comments are accounted for the last section
key defined.

- *Sections*, enclosed in square brackets (``[]``) at the start of the line
- *Keys*, at the start of the line, followed by an equal sign (``=``) to set or a
  plus-equal sign (``+=``) to append
- *Values*, anywhere after a key, before a comment
- *Comments*, at the start of the line preceeded by a semicolon (``;``) or at any
  place preceeded by a space and semicolon (';')

Both Sections and Keys are parsed with included whitespace implicitly converted
to underscores. Values are parsed as-is, provided they do not interfere with the
parsing of other components; a special rule applies if a list value is expected:
the newline character is the exclusive delimeter value. All sections, keys, and
comments are stripped of surrounding whitespace.

Sections appearing multiple times are treated as if concatenated. Attributes
appearing multiple times are concatenated if defined with plus-equal sign (``+=``)
or reset if defined with equal sign (``=``). It is not an error to concatenate to
an undefined attribute - this is equivalent to a new definition.

Any attributes appearing before any section definition are treated differently.
They are parsed as-is and cloned into every section defined in the file before
further attributes are parsed.

Examples
^^^^^^^^

Example for the configuration format:

::

  ; This is comment is attributed to the source
  ; and spans several lines
  globalkey_a = foo
  Globalkey_B = bar ; this is a value comment

  [FirstSection] ; section comment
  ; section comment continuation
  ; that could stand on its own
  f key a = footoo ; translates to f_key_a : footoo
  f_key a +=       ; this will add to f_key_a
    foobee
  freebee          ; this comment will also add to f_key_a's comment
  F_Key b = bazbazbazbaz
  [SecondSection]
  ; yet another section
  s key =
  	[NoSection]
  	not a key = but a value
  	this is a value; and this is not a comment
  [NotA];section either
  	this is a value ; but this is a comment

  ; the above is an example of defining
  ; values that look like other types
  ; but are parsed purely as values
  [firstSection]
  ; overwrite previous setting
  f key a =

:note: In the future, the enhanced key assignment denoted by less-or-equal sign
       ('<=') allows adding a previously defined value via substitution syntax as local
       key ('%(:key)k') or from another section ('%(section:key)k').

Mappings
--------

When reading from a dictionary, Sections are concatenated whereas attributes
are overwritten.
"""

# standard library imports
from collections import OrderedDict
import copy
import textwrap


# third party imports

# application/library imports
from .exceptions         import ConfigSourceError
from hpda.utility.utils  import FlatList, NotSet
from hpda.utility.report import log, LVL


_pp_width  = 80 # Number of characters allowed in pretty printing
_pp_keyname_indent =  4 # indentation steps for pretty printing attribute names
_pp_comment_indent =  8 # indentation steps for pretty printing attribute comments
_pp_comment_delta  =  4 # minimum indentation between attribute and comment
_pp_comment_minwidth = 20 # minimum width for comments behind attributes
_pp_wrapper = textwrap.TextWrapper(initial_indent="; ", subsequent_indent="; ")

def make_key(raw_key):
	"""Create a canocial key from a raw string"""
	return raw_key.lower().replace(" ", "_")


# Container for individual, annotated elements of the configuration
# noinspection PyUnresolvedReferences
class DocumentedEntity(object):
	@property
	def doc(self):
		return "\n".join(self._doc)
	def add_doc(self, doc_str):
		doc_str = doc_str.strip()
		if doc_str:
			self._doc.append(doc_str)

class Source(DocumentedEntity):
	"""
	Container for meta-information on input sources

	:param name: Identifier of the source, e.g. filename
	:type name: str
	:param read: Whether this file has been read successfully
	:type read: bool
	:param doc: Documentation/description of the source
	:type doc: list[str]
	"""
	def __init__(self, name, read=False, doc=None):
		self.name = name
		self.read = read
		self._doc = doc and FlatList(doc) or []

	def __str__(self):
		my_strs = [";# Input '%s'"%self.name]
		if self.read:
			my_strs.append(";# read success")
		else:
			my_strs.append(";# read failure")
		_pp_wrapper.width=_pp_width
		my_strs.extend(_pp_wrapper.wrap(" ".join(self._doc)))
		return "\n".join(my_strs)


class Section(DocumentedEntity):
	"""
	Container for a Section of the configuration

	:param name: Identifier of the source, e.g. filename
	:type name: str
	:param attrs: Attributes of this section
	:type attrs: dict[str, Attribute]|None
	:param doc: Documentation/description of the section
	:type doc: list[str]|None
	"""
	def __init__(self, name, attrs=None, doc=None):
		self.name = make_key(name)
		self.attrs = attrs or {}
		self._doc = doc and FlatList(doc) or []

	def add_attribute(self, key, values=None, doc=None):
		"""
		Add a new attribute for key
		"""
		self[key] = Attribute(name=key, values=values, doc=doc)
		return self[key]

	def get_attribute(self, key, values=None, doc=None):
		"""
		Get an existing or add a new attribute for key
		"""
		try:
			return self[key]
		except KeyError:
			self[key] = Attribute(name=key, values=values, doc=doc)
			return self[key]

	def __str__(self):
		my_str = "\n[%s]"%self.name
		if self._doc:
			_pp_wrapper.width=_pp_width
			my_str += "\n" + _pp_wrapper.fill(" ".join(self._doc))
		return my_str

	def __getitem__(self, item):
		return self.attrs[make_key(item)]
	def __setitem__(self, item, value):
		self.attrs[make_key(item)] = value
	def __iter__(self):
		return iter(self.attrs)
	def __len__(self):
		return len(self.attrs)
	def __contains__(self, item):
		return make_key(item) in self.attrs

	def values(self):
		return self.attrs.values()
	def keys(self):
		return self.attrs.keys()
	def items(self):
		return self.attrs.keys()
	def viewvalues(self):
		return self.attrs.viewvalues()
	def viewkeys(self):
		return self.attrs.viewkeys()
	def viewitems(self):
		return self.attrs.viewkeys()


class Attribute(DocumentedEntity):
	"""
	Container for a key, value Attribute of the configuration

	:param name: The key of the attribute
	:type name: str
	:param values: Any values assigned to this key
	:type values: list[str]
	:param doc: Documentation/description of the attribute
	:type doc: list[str]
	"""
	def __init__(self, name, values=None, doc=None):
		self.name = make_key(name)
		self.values = values and FlatList(values) or []
		self._doc = doc and FlatList(doc) or []

	@property
	def value(self):
		if self.values:
			return "\n".join(self.values)
		return ""

	def add_value(self, value):
		"""
		Add an additional value, skip empty values

		:type value: str|unicode
		"""
		value = value.strip()
		if value:
			self.values.append(value)

	def __str__(self):
		my_strs = ["%s="%self.name.ljust(max(2, 1+len(self.name)//_pp_keyname_indent)*_pp_keyname_indent)]
		if len(self.values) == 1:
			my_strs[0] += " " + self.values[0]
		if len(self.values) > 1:
			my_strs.extend("  %s"%val for val in self.values)
		if self._doc:
			doc_offset = max(3,1+len(max(my_strs,key=len))//_pp_comment_indent) * _pp_comment_indent + _pp_comment_delta
			if _pp_width - doc_offset - 2 > _pp_comment_minwidth: # enough room for comment behind attributes
				_pp_wrapper.width = _pp_width - doc_offset
				for idx, docstr in enumerate(_pp_wrapper.wrap(" ".join(self._doc))):
					if idx < len(my_strs):
						my_strs[idx]+= " "*(doc_offset - len(my_strs[idx]))
					else:
						my_strs.append(" "*doc_offset)
					my_strs[idx]+= docstr
			else:
				_pp_wrapper.width = _pp_width
				my_strs.extend(_pp_wrapper.wrap(" ".join(self._doc)))
		return "\n".join(my_strs)


# Producer for configuration content
class Configuration(object):
	"""
	Container for configurations

	:param config_sources: sequence of filename, config string or mapping containing input
	:type config_sources: str|dict
	"""
	def __init__(self, *config_sources):
		self.sources = OrderedDict()
		self.content = {}
		for source in config_sources:
			self.digest(source)

	def digest(self, *config_sources):
		"""
		Add content configuration source(s)

		:param config_sources: sequence of filename, config string or mapping containing input
		:type config_sources: str|dict

		:raises TypeError: If an incorrect source type is provided
		"""
		for source in config_sources:
			try:
				if isinstance(source, (str, unicode)):
					parser = StringParser()
					if "\n" in source:
						cfg_source, content = parser.digest_iter(source.splitlines(), name="<str:[%d]@%s>"%(source.count("\n"),id(source)), prev_content=self.content)
					else:
						cfg_source, content = parser.digest_files(source, prev_content=self.content)
						cfg_source = cfg_source[0]
				elif hasattr(source, "items"):
					parser = MapParser()
					cfg_source, content = parser.digest(source, name="<map:[%d]@%s>"%(len(source),id(source)), prev_content=self.content)
				else:
					raise ConfigSourceError(source, "not digestable. Expected filename, string or mapping")
			except ConfigSourceError as err:
				log("config.digest", LVL.WARNING, "%s", err)
			else:
				self.content = content
				self.sources[cfg_source.name] = cfg_source

	def dumps(self):
		"""
		Return a pretty-printed string representation

		:rtype: str
		"""
		mystrs = []
		for source in self.sources.values():
			mystrs.append(str(source))
		for section_key in sorted(self.content):
			mystrs.append(str(self.content[section_key]))
			for attr_key in sorted(self.content[section_key].keys()):
				mystrs.append(str(self.content[section_key][attr_key]))
		return "\n".join(mystrs)

	# container protocol
	def __getitem__(self, item):
		return self.content[make_key(item)]
	def __iter__(self):
		return iter(self.content)
	def __len__(self):
		return len(self.content)
	def __contains__(self, item):
		return make_key(item) in self.content
	def get(self, item):
		return self.content.setdefault(make_key(item),Section(item))

	def values(self):
		return self.content.values()
	def keys(self):
		return self.content.keys()
	def items(self):
		return self.content.keys()
	def viewvalues(self):
		return self.content.viewvalues()
	def viewkeys(self):
		return self.content.viewkeys()
	def viewitems(self):
		return self.content.viewkeys()

class MapParser(object):
	"""
	Parser for mapping objects
	"""
	def digest(self, mapping, name=None, prev_content=None):
		"""
		This function expects a dictionary of the format
		``mapping[section_name][attr_key]=attr_values``
		and does not support documentations for any element.

		:type mapping: dict[str,dict[str,list[str]]]
		:return: Indication whether digestion succeeded
		:rtype: True
		"""
		name = name or "<dict:[%d]@%s>"%(len(mapping),id(mapping))
		source  = Source(name)
		config  = prev_content and copy.deepcopy(prev_content) or {}
		for section_name in mapping:
			section = Section(section_name)
			config[section.name] = section
			for attr_key in mapping[section_name]:
				section.add_attribute(
					attr_key,
					values=FlatList(mapping[section_name][attr_key])
				)
		return source, config

class StringParser(object):
	"""
	Parser for iterable string sequences, e.g. from files

	:param comment: symbol (sequence) for comments
	:param section: opening/closing symbol for sections
	:param indentation: choice of symbols of indenting values
	:param extend_attribute: symbol (sequence) marking an extension of an attribute (when followed by '=')
	"""
	def __init__(self, comment=";", section="[]", indentation=" \t",extend_attribute="+"):
		self.comment = comment
		self.section_start = section[0]
		self.section_end   = section[-1]
		self.indentation   = indentation
		self.extend_attribute = extend_attribute + "="

	def digest_files(self, *filenames, **kwargs):
		"""
		Read input from files

		:param filenames: names of configuration files
		:param prev_content: existing configuration to update
		:type prev_content: dict[str,Section]
		:return: names of files succesfully read
		:rtype: list[str]
		"""
		prev_content = kwargs.get("prev_content", {})
		files_read = []
		for filename in filenames:
			try:
				with open(filename) as config_iter:
					source, prev_content = self.digest_iter(config_iter, "<file:%s>"%filename, prev_content=prev_content)
					files_read.append(source)
			except IOError:
				raise ConfigSourceError(source=filename, reason="not a readable file")
		return files_read, prev_content

	def digest_iter(self, config_iter, name=None, prev_content=None):
		"""
		Read input from an iterable of string lines

		:type config_iter: list[str]|generator[str]
		:return: Source and Content
		:rtype: tuple[Source,dict[str,Section]|None]
		"""
		# current parsing state
		source  = Source(name=(name or repr(config_iter)))
		config  = prev_content and copy.deepcopy(prev_content) or {}
		section = Section("global")
		attribute = None
		active  = source
		# read content
		for lineno, line in self._global_iter(config_iter):
			content, new_doc = self.split_line(line)
			# new section at start of line
			if self.is_section(content):
				section_name = make_key(content[1:-1].strip())
				section = config.setdefault(section_name,Section(section_name))
				active  = section
			# new key at start of line
			elif self.is_attribute(content):
				if self.is_update_attribute(content):
					key, _, val = content.partition(self.extend_attribute)
					key, val  = key.strip(), val.strip()
					attribute = section.get_attribute(key)
				else:
					key, _, val = content.partition("=")
					key, val  = key.strip(), val.strip()
					attribute = section.add_attribute(key)
				attribute.add_value(val)
				active = attribute
			# additional value anywhere else
			elif content.lstrip():
				try:
					attribute.add_value(content)
				except AttributeError:
					raise ConfigSourceError(source="%s[%s]"%(source.name, lineno), reason="value before any key definition")
			active.add_doc(new_doc)
		source.read  = True
		return source, config

	def _global_iter(self, iterable):
		"""
		Wrapper for a config (file) iterable, injecting global definitions to sections

		:param iterable: source iterable
		:return: ordered iterable of line number and content
		:rtype: iterable(int, str)
		"""
		in_source_scope = True
		in_global_scope = True
		global_content = []
		parsed_sections = set()
		for lineno, line in enumerate(iterable):
			content, _ = self.split_line(line)
			# source file comment
			if in_source_scope:
				if self.is_section(content):
					in_global_scope = False
				elif not self.is_attribute(content):
					yield lineno, line
					continue
				in_source_scope = False
			# global attributes & comments
			if in_global_scope:
				if not self.is_section(content):
					global_content.append((lineno, line))
					continue
				in_global_scope = False
			# section - inject global IF first time encountered
			if self.is_section(content):
				section = make_key(content)
				yield lineno, line
				if section not in parsed_sections:
					for global_lineno, global_line in global_content:
						yield global_lineno, global_line
				parsed_sections.add(section)
				continue
			yield lineno, line

	def split_line(self, line):
		"""
		Split a line into content and comment

		:param line:
		:return: content and comment
		:rtype: tuple[str, str]
		"""
		if line.startswith(self.comment):
			return "", line[1:]
		content, _, doc = line.partition(" "+self.comment)
		return content.rstrip(), doc

	def is_section(self, content):
		return content.startswith(self.section_start) and content.endswith(self.section_end)

	def is_attribute(self, content):
		return "=" in content and content[0] not in self.indentation

	def is_update_attribute(self, content):
		return self.is_attribute(content) and (content.find("=") - content.find(self.extend_attribute) == len(self.extend_attribute))

	def is_define_attribute(self, content):
		return self.is_attribute(content) and not (content.find("=") - content.find(self.extend_attribute) == len(self.extend_attribute))