#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# standard library imports

# third party imports

# application/library imports
from ...utility.exceptions    import BasicError
from ...utility.utils         import listItems, NotSet

class ConfigSourceError(BasicError):
	"""A configuration source is erronous"""
	def __init__(self, source, reason):
		BasicError.__init__(self, "%s %s" % (source, reason))

class ConfigSectionError(BasicError):
	"""A section with required keys requested from the config is not available"""
	def __init__(self, section, required_keys):
		BasicError.__init__(self, "[%s] undefined, required keys {%s}" % (section, listItems(*required_keys)))

class ConfigKeyError(BasicError):
	"""A key requested from the config is not available"""
	def __init__(self, section, key):
		BasicError.__init__(self, "[%s] '%s' undefined" % (section, key))

def _format_value_error(section, key, value, config_attribute):
		if config_attribute.choices is not None:
			cfg_repr = "[%s] '%s' = %s{%s} got '%s'" % (section, key, config_attribute.value_type, listItems(*config_attribute.choices), value)
		else:
			cfg_repr = "[%s] '%s' = %s{???} got '%s'" % (section, key, config_attribute.value_type, value)
		if config_attribute.default is not NotSet:
			cfg_repr += " (default '%s')" % config_attribute.default
		return cfg_repr

class ConfigValueError(BasicError):
	"""A value requested from the config is not adequate"""
	def __init__(self, section, key, value, config_attribute):
		try:
			error_str = ""
			for variant in config_attribute.variants:
				error_str += "\n" + _format_value_error(section, key, value, variant)
		except AttributeError:
			error_str = _format_value_error(section, key, value, config_attribute)
		BasicError.__init__(self, error_str)