#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
Abstraction of the ``subprocess`` module, primarily the ``Popen`` class. Each of
the ``Process`` classes implements an application specific usecase, offering the
same interface in every case.
"""

# standard library imports
import os
import time
import subprocess
import threading

# third party imports

# application/library imports
import logging
from functools import wraps
from .utils  import flatList, niceTime
from .enum   import BaseEnum

def _log_failure(method):
	"""Decorator for logging failure messages"""
	@wraps(method)
	def logged_method(self, *args, **kwargs):
		answer = method(self, *args, **kwargs)
		if self.log_failure and getattr(self, 'logged') is False:
			self.log(failure_only=True)
		return answer
	return logged_method

class Process(object):
	"""
	Baseclass for the process handlers

	:param cmd: name of the executable to run
	:type cmd: str
	:param args: arguments to pass to executable
	:type args: list[str]
	:param descr: human readable description; defaults to ``basename(cmd)``
	:type descr: str
	:param logger: an alternate logger to use; default to ``"proc.%(cmd)s``
	:type logger: str
	:param log_failure: create a log message if the process fails
	:type log_failure: bool
	:param buffer: equivalent to the ``Popen(bufsize)`` parameter
	:type buffer: int
	"""
	def __init__(self, cmd, args = None, descr = None, logger = None, log_failure=False, buffer=-1):
		self.descr   = descr or '%s' % (os.path.basename(cmd))
		self.cmd     = cmd
		self.args    = flatList(args or [])
		self.log_failure = log_failure
		self.logged  = False
		self.start   = time.time()
		self.logger  = logging.getLogger(logger if logger is not None else ('proc.%(cmd)s' % {'cmd' : os.path.basename(cmd)}))

	def __repr__(self):
		return "%s(%s)"%(self.__class__.__name__, self.descr)

	def status(self, wait = False, timeout = -1, kill = True):
		"""Return a logical representation of execution status"""
		if wait:
			self.wait(timeout = timeout, kill = kill)
		if self.poll() is None:
			return ProcessStatus.RUNNING
		if self.poll() == 0:
			return ProcessStatus.SUCCESS
		if self.poll() > 0:
			return ProcessStatus.ERR_FAILED
		if self.poll() < 0:
			return ProcessStatus.SIG_KILL

	@_log_failure
	def poll(self):
		"""
		Get the process status, without waiting for termination

		:return: Current process status
		:rtype: int or None
		:see: :py:func:`subprocess.Popen.poll`
		"""
		return self.proc.poll()

	@_log_failure
	def wait(self, timeout = -1, kill = True):
		"""
		Get the process status, waiting for termination

		:param timeout: maximum time in seconds to wait for process completion
		:type timeout: int or float
		:param kill: whether to kill the process if it does not finish before timeout
		:type kill: bool
		"""
		if not timeout > 0:
			return self.proc.wait()
		while timeout > ( time.time() - self.start ) and self.poll() is None :
			time.sleep(0.2)
		if kill and timeout < ( time.time() - self.start ):
			self.kill()
		if self.log_failure:
			self.log(failure_only=True)
		return self.poll()

	@_log_failure
	def terminate(self):
		"""Order the process to terminate"""
		self.proc.terminate()

	@_log_failure
	def kill(self):
		"""End the process forcefully"""
		self.proc.kill()

	def log(self, failure_only=True):
		"""
		Log debugging information about the process

		:param failure_only: skip logging if the process did not fail, i.e. still runs or succeeded
		:type failure_only: bool
		"""
		if self.proc.poll() in (0,None) and failure_only:
			return
		self.logged = True
		self.logger.log(logging.WARNING,
			"Process '%s' (PID: %s) exited with %2d", self.descr, self.proc.pid, self.poll())
		self.logger.log(logging.INFO,
			'Process %d: startTime %s, retCode %d', self.proc.pid, niceTime(self.start), self.poll())
		self.logger.log(logging.DEBUG,
			'Process %d: cmd %s, args %s', self.proc.pid, self.cmd, self.args)
		self.logger.log(logging.DEBUG,
			'Process %d: stdout\n%s', self.proc.pid,str.join(' ',self.stdout))
		self.logger.log(logging.INFO,
			'Process %d: stderr\n%s', self.proc.pid,str.join(' ',self.stderr))

class ConsumedProcess(Process):
	"""
	A subprocess with stdout and stderr consumed in real-time

	This is a tight wrapper around the standard streams of a process. Each stream
	is read in (almost) realtime by a separate thread. This has the advantage of
	being un-blocking and re-readable. The cost is the overhead of threads and the
	memory requirement for storing the output.

	:note: The memory overhead is roughly equivalent to the string size of the
	       process output. This is not a problem with individual processes, unless
	       massive output is generated, but objects must be released to avoid buildup.
	"""
	def __init__(self, cmd, args = None, descr = None, logger = None, buffer=-1):
		Process.__init__(self, cmd, args, descr, logger, buffer)
		self.proc    = subprocess.Popen( [self.cmd] + self.args, stdout = subprocess.PIPE, stderr = subprocess.PIPE, universal_newlines = True)
		self.stdout = []
		self.stderr = []
		threading.Thread(target=self._consume, args=(self.proc.stdout,self.stdout))
		threading.Thread(target=self._consume, args=(self.proc.stderr,self.stderr))
		self.logger.log(logging.INFO, "%r started", self)

	def _consume(self, source, target):
		self.logger.log(logging.DEBUG, "Consuming '%s'", source)
		for line in source:
			target.append(line)
		self.logger.log(logging.DEBUG, "Consumed  '%s'", source)

# Process Wrappers
class BufferedProcess(Process):
	"""
	A subprocess wrapped with buffered stdout and stderr

	This is a lightweight wrapper around an underlying process, adding mostly just
	the enhanced logging and information retrieval. It directly exposes the underlying
	subprocess standard streams. Thus, stdour and stderr may be read only once.

	:note: Most applications will block if the read buffer is full and writing to
	       stdout/stderr is not possible. Use this only for processes with little
	       output or when regularly reading from the output streams.
	"""
	def __init__(self, cmd, args = None, descr = None, logger = None, buffer=-1):
		Process.__init__(self, cmd, args, descr, logger, buffer)
		self.proc    = subprocess.Popen( [self.cmd] + self.args, stdout = subprocess.PIPE, stderr = subprocess.PIPE, universal_newlines = True)
		self.stdout  = self.proc.stdout
		self.stdout  = self.proc.stdout
		self.logger.log(logging.INFO, "%r started", self)

class ProcessStatus(BaseEnum):
	"""
	Enum for describing the exit status of a process.

	_Members:___(no error)__________________________________________________
	> RUNNING
	The process is still running.
	> SUCCESS
	The process exited without error.
	_Members:___(errors)____________________________________________________
	> ERR_FAILED
	The process exited with a generic error.
	> ERR_USAGE
	The process exited due to incorrect usage.
	> ERR_NOTFOUND
	A file or folder was not found.
	_Members:___(signals)___________________________________________________
	> SIG_KILL
	The process was killed.
	"""
	__members__ = ['RUNNING', 'SUCCESS', 'ERR_FAILED', 'ERR_USAGE', 'ERR_NOTFOUND', 'SIG_KILL']


