#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.

# custom modules
from .exceptions import BasicError

"""
Module enum

Implements enum style meta- and baseclasses. Derived classes support pickling of
members.
"""

__all__ = ['MetaEnum', 'BaseEnum', 'StaticEnum']

class Unique(object):
	"""
	Collectionless unique element

	This is a beautification of using :py:class:`object` as unique identifiers.
	It offers configurable str and repr for verbosity. In addition, it supports
	equality comparisons.
	"""
	def __init__(self, name="UniqueObj", representation=None):
		self.name = name
		self.representation = representation or name

	def __str__(self):
		return self.name

	def __repr__(self):
		return "<%s@%d>"%(self.representation, id(self))

	def __eq__(self, other):
		return self is other
	def __ne__(self, other):
		return not self is other
	def __gt__(self, other):
		return NotImplemented
	def __lt__(self, other):
		return NotImplemented
	def __ge__(self, other):
		return NotImplemented
	def __le__(self, other):
		return NotImplemented

## Exceptions
#############
class EnumMemberError(BasicError):
	"""Member is not part of Enum"""
	def __init__(self, enum, member):
		Exception.__init__(self, "'%s' is not a member of enum %s" % (member, enum))
class EnumExtensionError(BasicError):
	"""Member is not part of Enum"""
	def __init__(self, enum):
		Exception.__init__(self, "Enum %s cannot be extended" % enum)

class MetaEnum(type):
	"""Custom metaclass for enum magic, e.g. __members__ attribute"""
	def __new__(mcs, name, bases, class_dict):
		cls = type.__new__(mcs, name, bases, class_dict)
		for member in cls.__members__:
			cls(member)
		return cls
	@property
	def members(cls):
		"""Return the instances of all members"""
		members = set()
		for member in cls.__members__:
			members.add(getattr(cls, member))
		return members
	# Representations
	def __str__(self):
		return self.__name__
	def __repr__(self):
		return "<enum %s[%s]>" %( self.__name__, "|".join(member.__name__ for member in self.members))
	# Container protocol
	def __len__(self):
		return len(self.__members__)
	def __contains__(self, member):
		return isinstance(member, self) and member.__name__ in self.__members__
	def __iter__(self):
		return iter(self.members)
	# attribute access
	def __getattr__(self, name):
		raise EnumMemberError(self, name)

class BaseEnum(object):
	"""
	Enum BaseClass for unnumbered singleton Members
	
	All members of the special attribute __members__ are converted on creation.
	"""
	__members__ = []
	__metaclass__ = MetaEnum

	def __new__(cls, member):
		"""Returns the singleton instance of member in this Enum"""
		try:
			return getattr(cls, member)
		except EnumMemberError:
			return cls._add_member(member)

	@classmethod
	def _add_member(cls, member):
		"""add a new member"""
		newmember = object.__new__(cls)
		newmember.__name__ = member
		setattr(cls, member, newmember)
		if not member in cls.__members__:
			cls.__members__.append(member)
		return newmember

	## pickling
	#  Use the low-level reduce mechanic to utilize
	#  the singleton metaclass instantiation
	def __reduce__(self):
		return self.__class__, (self.__name__, )
	# no value attached, assume True
	def __nonzero__(self):
		return True
	# Representations
	def __str__(self):
		return "%s.%s" %( self.__class__.__name__, self.__name__)
	def __repr__(self):
		return "<member %s of enum %s>" %( self.__name__, self.__class__.__name__)

class StaticEnum(BaseEnum):
	"""
	Enum BaseClass for static, unnumbered singleton Members
	
	All members of the special attribute __members__ are converted on creation.
	Post-creation of members is not allowed.
	"""
	def __new__(cls, member):
		try:
			return getattr(cls, member)
		except EnumMemberError:
			if member not in cls.__members__:
				raise EnumExtensionError(cls)
			return cls._add_member(member)

