#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# core modules

# advanced modules
import json
from collections import namedtuple

# custom modules
from .exceptions import InstallationError
from .caching    import LRUcache
from .utils      import nicePath, which, Bunch
from .process    import BufferedProcess, ProcessStatus

"""
module rsync

Wrapper for the rsync (remote) file syncing tool.
"""

# no point in going further if there is no rsync installed
try:
	rsyncexe = which('rsync')
except InstallationError:
	raise ImportError("Module 'rsync' is not available: failed to locate executable 'rsync'")

__all__ = ['BufferedRsync', 'Itemizer']

RsyncItems = namedtuple('RsyncItems', ['action', 'type', 'filechange', 'metachange'])

class Itemizer(object):
	def __init__(self, *infoArgs):
		self.features = {
			'items' : ('%i', self.parseItemized),
			'name'  : ('%f', str),
			'sent'  : ('%b', int),
			'md5'   : ('%C', str),
			'size'  : ('%l', int),
			}
		self._args = infoArgs or self.features.keys()

	@property
	def rsyncFormat(self):
		return '{%s}' % ', '.join('"%s" : "%s"' % (key, self.features[key][0]) for key in self._args)

	@staticmethod
	def parseItemized(itemstring):
		result = {
			'action': itemstring[0],
			'type': itemstring[1],
			'filechange': False,
			'metachange': False
		}
		if filter(lambda item: item not in ['.',' '], itemstring[5:]):
			result['metachange'] = True
		if filter(lambda item: item not in ['.',' '], itemstring[2:5]):
			result['filechange'] = True
		return RsyncItems(**result)

	def parseLine(self, itemizedLine):
		itemdict = json.loads(itemizedLine)
		for key in itemdict.keys():
			itemdict[key] = self.features[key][1](itemdict[key])
		return Bunch(**itemdict)


@LRUcache(12)
def getRsyncArgs(checktime = True, dryrun = False, checksum = False, compress = False, timewindow = None, bwlimit = None, timeout = None, remoteshell = None, rshport = None, itemizer = Itemizer()):
	"""
	Create arguments for starting a GNU rsync process.
	
	_Optional:_____________________|type____________________|default________
	> dryryn                        bool                     False
	  Do not copy files, only report what would be copied.
	> checktime                     bool                     True
	  Use modification time for detecting file updates.
	> checksum                      bool                     False
	  Use file checksum for detecting file updates.
	> timewindow                    time                     <unset>
	  Time difference allowed for checking timestamps.
	> compress                      bool                     False
	  Compress data during transfer.
	> bwlimit                       bytes                    <unset>
	  Bandwidth limit for transfer per second, in bytes (not kilobytes).
	> timeout                       time                     <unset>
	  Timeout for transfer.
	> remoteshell                   string                   <unset>
	  The remote shell to use for remote access. The option '-p <rshport>' is
	  assumed to be valid. Defaults to 'ssh' if rshport is given.
	> rshport                       integer                  <unset>
	  The port to use for connecting with any remote shell.
	"""
	# always preserve time meta data
	args = ['-t']
	# always report changes 
	args.append("--out-format=%s" % itemizer.rsyncFormat)
	if dryrun:
		args.append('--dry-run')
	# checksum makes time&size checks void
	if checksum:
		args.append('--checksum')
	if not checktime:
		args.append('--size-only')
	if timewindow is not None:
			args.append("--modify-window=%d" % timewindow)
	if compress:
		args.append('--compress')
	if bwlimit is not None:
		args.append('--bwlimit=%d' % (bwlimit / 1000))
	if timeout is not None:
		args.append('--timeout=%d' % timeout)
	if rshport is not None:
		remoteshell = remoteshell or 'ssh'
		remoteshell = '%s -p %d' % (remoteshell, rshport)
	if remoteshell is not None:
		args.append("--rsh='%s'" % remoteshell)
	return args

class BufferedRsync(BufferedProcess):
	exitStatus = {
		1  : ProcessStatus.ERR_USAGE,
		23 : ProcessStatus.ERR_NOTFOUND,
		24 : ProcessStatus.ERR_NOTFOUND,
		}
	def __init__(self, source, dest, logger = None, **rsyncargs):
		"""
		Create arguments for starting a GNU rsync process.
		
		_Required:_____________________|type____________________________________
		> source                        string
		> dest                          string
		  Copies any GNU rsync supported type from source to dest. This includes
		  files, links, and folders, and supports [USER@]HOST:FILE.

		_Optional:_____________________|type____________________|default________
		> **rsyncargs                   kwargs                   <unset>
		  Arguments as defined by getRsyncArgs.
		> **rsyncargs                   kwargs                   <unset>
		  Arguments as defined by getRsyncArgs.
		"""
		# info
		self.source = source
		self.dest   = dest
		self.logger = logger
		# start
		self._itemizer = rsyncargs.get('itemizer', Itemizer())
		BufferedProcess.__init__(self,
			cmd = rsyncexe,
			args = getRsyncArgs(**rsyncargs) + [source, dest],
			descr = 'rsync (%s=>%s)' % (nicePath(source), nicePath(dest)),
			logger = logger,
			)

	def status(self, wait = False, timeout = -1, kill = True):
		if wait:
			self.wait(timeout = timeout, kill = kill)
		try:
			return self.exitStatus[self.poll()]
		except KeyError:
			return BufferedProcess.status(self, timeout = timeout, kill = kill)

	def changes(self):
		return self.getChanges(wait = False, kill = False)

	def iterChanges(self, wait = True, timeout = -1, kill = True):
		if wait:
			self.wait(timeout=timeout, kill=kill)
		for line in self.stdout:
			yield self._itemizer.parseLine(line)

	def getChanges(self, wait = True, timeout = -1, kill = True):
		return list(self.iterChanges(wait = wait, timeout = timeout, kill = kill))
