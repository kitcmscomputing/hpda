#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
Base classes and tools for creating singleton and pseudo-singleton classes. The
base class :py:class:`~.BaseSingleton` creates per runtime unique classes; its
derivate class :py:class:`~.BaseWeakSingleton` creates classes that may be
destroyed when not referenced, i.e. temporarily unique instances.

Any class deriving from either base class should decorate its ``__init__`` method
with :py:func:`~.singleton__init` to avoid duplicate setup. This can be skipped if
there is no ``__init__`` method required or if changing state is desired.

Classes that depend on the instantiation parameters should implement their own
classmethod :py:meth:`~._singleton_signature`, which identifies instances by their
constructor signature.

:note: Instances are tracked per primary subclass of :py:class:`~.BaseSingleton`
       or :py:class:`~.BaseWeakSingleton`. Given the inheritance
       :py:class:`~.BaseWeakSingleton` -> ``Foo`` -> ``Bar``, both ``Foo`` and
       ``Bar`` will share the same instances.
"""

# standard library imports
import weakref

# third party imports

# application/library imports

def singleton__init(init_fkt):
	"""Decorator for __init__ to run only once per singleton"""
	def unique_init(self, *args, **kwargs):
		callsigkey = self._singleton_signature(*args, **kwargs)
		if callsigkey in self._instances:
			return
		# init does not return, so this is safe and avoids inheritance problems
		init_fkt(self, *args, **kwargs)
		self._instances[callsigkey] = self
	return unique_init


class MetaSingleton(type):
	"""
	MetaClass for Singletons

	Responsible for creating the instance dictionary of sub-classes.
	"""
	def __new__(mcs, name, bases, class_dict):
		cls = type.__new__(mcs, name, bases, class_dict)
		if cls.__name__ not in ("BaseSingleton", "BaseWeakSingleton"):
			if cls in BaseSingleton.__subclasses__() or cls in BaseWeakSingleton.__subclasses__():# and cls is not BaseWeakSingleton:
				if not hasattr(cls, "_instances"):
					cls._instances = cls._instances_type()
		return cls


class BaseSingleton(object):
	_instances_type = dict
	__metaclass__ = MetaSingleton
	def __new__(cls, *args, **kwargs):
		"""Get the singleton matching the parameters"""
		callsigkey = cls._singleton_signature(*args, **kwargs)
		try:
			return cls._instances[callsigkey]
		except KeyError:
			new_instance = object.__new__(cls)
			return new_instance

	@singleton__init
	def __init__(self, *args, **kwargs):
		pass

	@classmethod
	def _singleton_signature(cls, *args, **kwargs):
		"""Mapping of ctor parameters to unique instances"""
		return frozenset(args) | frozenset(kwargs.values())

	@classmethod
	def instances(cls):
		return cls._instances.viewvalues()


class BaseWeakSingleton(BaseSingleton):
	_instances_type = weakref.WeakValueDictionary
