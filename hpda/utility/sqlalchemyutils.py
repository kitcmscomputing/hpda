#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.sql.functions import FunctionElement

class Round(FunctionElement):
	name = 'round'

	def __init__(self, *clauses, **kwargs):
		self.ndigits=kwargs.pop("ndigits", 3)
		FunctionElement.__init__(self, *clauses, **kwargs)

@compiles(Round)
def compile_round(element, compiler, **kw):
	return "ROUND(%s, %d)" % (compiler.process(element.clauses), element.ndigits)
