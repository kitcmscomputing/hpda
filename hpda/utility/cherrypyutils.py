#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
module cherrypy

Various additions to cherrypy
"""
# standard library imports

# third party imports
import cherrypy

# application/library imports

# If available we use cjson
json_type = 'json'
try:
	import cjson

	json_type = 'cjson'
except ImportError:
	import json

def json_out(func):
	def wrapper(self, *args, **kwargs):
		cherrypy.response.headers['Content-Type']= 'application/json'
		ret = func(self, *args, **kwargs)
		if json_type == 'cjson':
			return cjson.encode(ret)
		else:
			return json.dumps(ret)
	return wrapper

def json_handler(*args, **kwargs):
	value = cherrypy.serving.request._json_inner_handler(*args, **kwargs)
	if json_type == 'cjson':
		return cjson.encode(value)
	else:
		return json.dumps(value)

def jsonp(func):
	def wrapper(self, *args, **kwargs):
		callback, _ = None, None
		if 'callback' in kwargs and '_' in kwargs:
			callback, _ = kwargs['callback'], kwargs['_']
			del kwargs['callback'], kwargs['_']
		ret = func(self, *args, **kwargs)
		if callback is not None:
			ret = '%s(%s)' % (callback, json.dumps(ret))
		return ret

	return wrapper


def auth_user(func):
	def wrapper(self, *args, **kwargs):
		try:
			hdr = cherrypy.request.headers['Authorization']
			start = hdr.find('Digest username="')
			end = hdr.find('"', start + 17)
			cherrypy.request.user = hdr[start + 17:end]
		except KeyError:
			cherrypy.request.user = None

		return func(self, *args, **kwargs)
	return wrapper
