#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
Provides a range of utilities for querying data from volumes/devices.
All sizes are returned in bytes, though no guarantee of resolution is given.
"""

# standard library imports
import os

# third party imports

# application/library imports

# TODO: Add equivalents for windows

def stats(path):
	return VolumeStats(path)

class VolumeStats(object):
	"""
	Features of a data volume

	:param path: path into any folder on the mounted volume
	:type path: str
	:ivar total: total space of the volume, in bytes
	:type total: int or float
	:ivar avail: free space of the volume, in bytes
	:type avail: int or float
	:ivar used: used space of the volume, in bytes
	:type used: int or float
	"""
	def __init__(self, path):
		self.path = path
		self.total, self.avail, self.used = None,None,None
		self.refresh()

	def refresh(self):
		"""Update the available information"""
		rawstats = os.statvfs(self.path)
		self.total = rawstats.f_blocks * rawstats.f_frsize
		self.avail = rawstats.f_bavail * rawstats.f_frsize
		self.used  = self.total - self.avail
