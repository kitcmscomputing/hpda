# -*- coding: utf-8 -*-
#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# standard library imports
import threading
import ctypes
import time

# third party imports

# application/library imports
from hpda.utility.exceptions import BasicException, AbstractError, ExceptionFrame
from hpda.utility.utils      import interval_sleeper, crontab_sleeper


# Locking/Synchronization
#########################
lock_types = ( type(threading.Lock()), type(threading.RLock()))
def is_lock(obj):
	"""Test if ``obj`` is a lock"""
	return isinstance(obj, lock_types)

class TerminateThread(BasicException):
	"""Reserved signal for stopping threads"""
	pass

def _raise_in_thread(thread_id, exception_type):
	"""
	Raise an exteption in a thread

	:param thread_id: token of the thread
	:type thread_id: int
	:param exception_type: exception class to raise in thread
	:type exception_type: :py:class:`Exception`
	:raises ValueError: if the thread does not exist
	:raises RuntimeError: if more than one thread got matched
	"""
	modified_threads = ctypes.pythonapi.PyThreadState_SetAsyncExc(ctypes.c_long(thread_id), ctypes.py_object(exception_type))
	if modified_threads == 0:
		raise ValueError("Thread '%s' does not exist" % thread_id)
	elif modified_threads > 1:
		ctypes.pythonapi.PyThreadState_SetAsyncExc(ctypes.c_long(thread_id), None)
		raise RuntimeError("ctypes.pythonapi modified %d threads for id '%s'! Expected <= 1 threads." %(modified_threads, thread_id))

def terminate_thread(thread, exception_type=SystemExit):
	"""
	Terminate a thread by raising an exception

	:param thread: a Thread object or its token
	:type thread: :py:class:`~threading.Thread` or int
	:param exception_type: exception to raise in thread
	:type exception_type: :py:class:`Exception`
	"""
	thread_id = getattr(thread,"ident", thread)
	try:
		_raise_in_thread(thread_id=thread_id, exception_type=exception_type)
	except ValueError:
		pass

class ThreadMaster(object):
	"""
	Baseclass for Threads that can be restarted

	There are several features that define the work performed when the thread
	is executed:

	**looping**
	  If an instance has the ``loop_interval`` attribute defined (via subclassing
	  or duck-typing), its payload is called repeatedly in an infinite loop every
	  ``loop_interval`` seconds. The optional attribute ``loop_variance`` defines
	  a range by which the interval is randomly varied.

	**payload**
	  The payload may be defined either as a method on a subclass or as a parameter
	  to the constructor. The later option takes precedence.

	**main**
	  A subclass may define a ``main`` method that is called when a Thread
	  (re-)starts. This will invalidate any of the previous functionality. See
	  below for conventions of ``main``.

	:param daemon: ungracefully stop any active threads if the main thread shuts down
	:type daemon: bool
	:param payload: callable to execute in active thread
	:type payload: callable
	:param args: sequence of parameters to pass to payload as ``*args``
	:type args: list or tuple
	:param kwargs: mapping of parameters to pass to payload as ``*kwargs``
	:type kwargs: dict
	:param loop_interval: time to between loops as interval or generator of intervals
	:type loop_interval: int, float or generator
	:param loop_variance: variance on ``loop_interval``
	:type loop_variance: int or float

	:note: The protocol expects ``main`` to listen for two mechanisms to stop:
	       An exception of type ``TerminateThread`` must end execution. It is
	       usually sufficient simply not to catch this exception. Since theere
	       may a long delay (several seconds) before the exception is raised
	       after dispatching it, main may optionally listen for the
	       ``_should_terminate`` Event as well.
	"""
	def __init__(self, daemon = True, payload=None, args=None, kwargs=None, loop_interval=None, loop_variance=0):
		self._activeThread = None
		self.daemon = daemon
		if payload is not None:
			self.payload = payload
		self.args = args or []
		self.kwargs = kwargs or {}
		self._should_terminate = threading.Event()
		self._should_terminate.clear()
		# set loop values with respect to properties
		if not hasattr(self.__class__, "loop_interval"):
			self.loop_interval = loop_interval
		if not hasattr(self.__class__, "loop_variance"):
			self.loop_variance = loop_variance

	def is_alive(self):
		"""Return whether the thread is alive"""
		if self._activeThread:
			return self._activeThread.isAlive()
		return False

	def isAlive(self):
		"""Alias for ``is_alive``"""
		return self.is_alive()

	def start(self):
		"""
		Start execution as a thread if none is running

		:return: whether a thread was started
		:rtype: bool
		"""
		self._should_terminate.clear()
		if not self.is_alive():
			self._activeThread = threading.Thread(target = self.main)
			self._activeThread.daemon = self.daemon
			self._activeThread.start()
			return True
		return False

	def stop(self):
		"""
		Gracefully stop any executing thread

		:return: whether the Thread has shut down
		:rtype: bool

		:note: If a thread was running, this function will sleep for 1 μs to
		       release the *GIL* and allow the thread to shut down. If the thread
		       requires more time to shut down, this function will report it as
		       *still* alive. This is the most precise information available, as
		       a thread may completely ignore a shutdown.
		"""
		self._should_terminate.set()
		if self.is_alive():
			time.sleep(0.001)
		return not self.is_alive()

	def terminate(self):
		"""
		Forcefully stop any executing thread

		:return: whether the Thread has shut down
		:rtype: bool

		:see: Note on :py:meth:`~.ThreadMaster.stop` concerning return value.
		"""
		if not self.stop():
			try:
				_raise_in_thread(self._activeThread.ident, TerminateThread)
			except ValueError:
				# thread already died before we could kill it
				pass
			else:
				time.sleep(0.001)
		return not self.is_alive()

	def restart(self, timeout=10):
		"""
		Stop and start any executing thread

		:param timeout: timeout for the executing thread to abort
		:type timeout: int or float
		:return: whether a thread was started
		:rtype: bool
		"""
		self.stop()
		self.join(timeout)
		return self.start()

	def join(self, timeout = None):
		"""
		Wait for the underlying thread to terminate

		:see: ``threading.thread.join``
		"""
		if self.isAlive():
			self._activeThread.join(timeout)
		return not self.isAlive()

	@property
	def threadName(self):
		"""Name of the underlying thread"""
		if self._activeThread:
			return self._activeThread.getName()
		return '<no_thread>'

	# default implementations
	def payload(self, *args, **kwargs):
		"""Placeholder for callable to execute in thread"""
		raise AbstractError

	def main(self):
		"""Main routine for thread execution"""
		with ExceptionFrame(logger=("%s.main"%self.__class__.__name__)):
			try:
				loop_interval = getattr(self, "loop_interval")
			except AttributeError:
				# no looping set, just run once
				self.payload(*self.args, **self.kwargs)
			else:
				# get proper loop_sleeper
				try:
					loop_interval = float(loop_interval)
				except ValueError:
					loop_sleeper = crontab_sleeper(loop_interval, termination_event=self._should_terminate)
				else:
					loop_sleeper = interval_sleeper(loop_interval, getattr(self, "loop_variance", 0), termination_event=self._should_terminate)
				# loop with selected sleeper
				while True:
					for sleep_delta in loop_sleeper:
						self.payload(*self.args, **self.kwargs)

	def __repr__(self):
		return "%s(alive=%s, daemon=%s, shutdown=%s)"%(self.__class__.__name__, self.is_alive(), self.daemon, self._should_terminate.is_set())


