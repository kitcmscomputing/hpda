#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
module caching

Various utilities for in-application caching.

:warning: It is the programmers responsibility that caching mechanism are used
          only where applicable. For example, an LRU cache function return cache
          may cause unexpected behaviour if the function may produce different
          outputs for the same parameters or if the output is alike but mutable.
"""

# standard library imports
import time
from collections import deque
from weakref     import WeakSet

# third party imports

# application/library imports

class CacheTracker(object):
	"""
	Utility class for tracking and rating caches
	"""
	def __init__(self):
		self.total_cache_count = 0
		self._active_caches = WeakSet()

	@property
	def active_cache_count(self):
		return len(self._active_caches)

	def register_cache(self, cache):
		if not cache in self._active_caches:
			self._active_caches.add(cache)
			self.total_cache_count += 1

	def report(self):
		caches = list(self._active_caches)
		caches.sort(key=lambda cache: 1.0*cache.hits/max(cache.misses,1))
		ret_strs = ["# CacheReport - %d/%d [active/total]" % (self.active_cache_count, self.total_cache_count)]
		for cache in caches:
			ret_strs.append(cache.report())
		return "\n".join(ret_strs)

tracker = CacheTracker()


class BaseCacheDecorator(object):
	def __init__(self):
		self.hits    = 0
		self.misses  = 0
		self.wrapped = None

	def wrap(self, target):
		for attr in ("__name__","__doc__"):
			try:
				setattr(self, attr, getattr(target, attr))
			except AttributeError:
				pass
		self.wrapped = target
		tracker.register_cache(self)
		return self

	# Descriptor protocol for methods and stacked decorators
	def __get__(self, instance, owner):
		return self.wrapped.__get__(instance, owner)

	def report(self):
		return "%s\t-> %s\t%d/%d [H/M]" %(self.__class__.__name__, self.wrapped, self.hits, self.misses)

	def __repr__(self):
		return "%s(%s)"%(self.__class__.__name__, repr(self.wrapped))
	def __str__(self):
		return str(self.wrapped)


def hashkey(identifier):
	"""Provide a 32Bit key for the given identifier"""
	return '%08X' % (hash(identifier)&0xffffffff)


def LRUcache(maxSize = 64):
	"""
	Decorator for applying a least-recently-used cache to a function call

	When applied to a function, the decorator will cache the last few function
	calls and their return values. If a call with the same parameters is made,
	the corresponding cached result is returned.

	Function calls are identified by their call signature (``*args, **kwargs``)
	and stored until maxSize new calls have been made.

	:param maxSize: Number of unique function calls to store
	"""
	def decorator(function):
		def functionProxy(*args, **kwargs):
			# Hit: get cached, clear last used index
			callsigkey = tuple(args) + tuple(set(kwargs.keys()))
			try:
				retItem = functionProxy.retCache[callsigkey]
				functionProxy.sequence.remove(callsigkey)
				functionProxy.stats[0] += 1
			# Miss: get returnItem, update cache
			except KeyError:
				retItem = functionProxy.function(*args, **kwargs)
				functionProxy.retCache[callsigkey] = retItem
				functionProxy.stats[1] += 1
			# Access: set last used index, dismiss outdated
			functionProxy.sequence.appendleft(callsigkey)
			while len(functionProxy.sequence) > maxSize:
				del(functionProxy.retCache[functionProxy.sequence.pop()])
			return retItem
		# LRU defaults
		functionProxy.sequence = deque()
		functionProxy.retCache = {}
		functionProxy.function = function
		functionProxy.stats    = [0,0] # hits, misses
		return functionProxy
	return decorator

class LRUCache(BaseCacheDecorator):
	"""
	Decorator class for applying a least-recently-used cache to a call

	When applied to a function, the decorator will cache the last few function
	calls and their return values. If a call with the same parameters is made,
	the corresponding cached result is returned.

	Function calls are identified by their call signature (``*args, **kwargs``)
	and stored until maxSize new calls have been made.

	:param maxSize: Number of unique function calls to store
	"""
	def __init__(self, max_size=64):
		BaseCacheDecorator.__init__(self)
		self.call_sequence = deque()
		self.return_cache = {}
		self.max_size = 64

	def __call__(self, *args, **kwargs):
		call_signature = repr(args)+repr(kwargs)
		try:
			# Hit: get cached, clear signature from sequence
			return_item = self.return_cache[call_signature]
			self.call_sequence.remove(call_signature)
			self.hits += 1
		except (KeyError,ValueError):
			# Miss: create item, update cache
			return_item = self.wrapped(*args,**kwargs)
			self.return_cache[call_signature] = return_item
			self.misses += 1
		# Access: set signature as last used, dismiss outdated
		self.call_sequence.appendleft(call_signature)
		while len(self.call_sequence) > self.max_size:
			del self.return_cache[self.call_sequence.pop()]
		return return_item

class CallThrottle(BaseCacheDecorator):
	"""
	Decorator class that throttles function evaluation frequency

	When called, the decorator will check the time since the last function
	evaluation. If it is above max_delay, the function is evaluated; otherwise,
	the cached value is returned. This is intended for costly queries that can
	not be coordinated but are not vulnerable to slightly dated results.

	The cached result is automatically cleared when new call parameters are
	supplied.
	"""
	def __init__(self, max_delay=None):
		BaseCacheDecorator.__init__(self)
		self.max_delay = max_delay or 60*60*24*366*5
		self._call_sig = None
		self._ret_item = None
		self._lastcall = time.time() - self.max_delay

	def __call__(self, *args, **kwargs):
		callsigkey = tuple(args) + tuple(set(kwargs.keys()))
		if self._call_sig != callsigkey or time.time() - self._lastcall > self.max_delay:
			self.misses   += 1
			self._call_sig = callsigkey
			self._ret_item = self.wrapped(*args,**kwargs)
			self._lastcall = time.time()
		else:
			self.hits += 1
		return self._ret_item

def call_throttle(max_delay=None):
	def decorator(function):
		return CallThrottle(max_delay=max_delay).wrap(function)
	return decorator

def lru_cache(max_size=64):
	def decorator(function):
		return LRUCache(max_size=max_size).wrap(function)
	return decorator