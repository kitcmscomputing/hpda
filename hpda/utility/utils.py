# -*- coding: utf-8 -*-
#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
Provides a range of utility functions and classes. These are basic utilities
that rely on no other modules of the project, aside from exceptions.
"""

# core modules
import os
import sys
import shutil
import math
import time
import random
import glob
import subprocess
import collections

# advanced modules
import difflib

# project modules
from .exceptions import RethrowException, InstallationError, FileNotFound

# noop objects
#################
class NotSet(object):
	"""Dummy for unique, 'unset' default arguments"""
	class __metaclass__(type):
		def __nonzero__(cls):
			return False
		def __repr__(cls):
			return "<%s>"%cls.__name__
	def __nonzero__(self):
		return False
	def __repr__(self):
		return "<NotSet>"

class Noop(NotSet):
	"""Callable that does nothing"""
	def __call__(self, *args, **kwargs):
		pass
	def __repr__(self):
		return "Noop"

# iterators
######################
def iter_randstart(sequence):
	"""
	Iterate over ``sequence`` from a random index

	Picks a random index in ``sequence``, then returns all following elements,
	looping around at the end until the start element is reached. All elements
	are returned in the same order, but starting at random positions.

	:param sequence: an iterable supporting ``len(sequence)`` and ``sequence[idx]``
	:type sequence: sequence
	:return: elements from ``sequence``
	"""
	if len(sequence):
		startd_idx = random.randint(0,len(sequence)-1)
		for idx in range(startd_idx, len(sequence)):
			yield sequence[idx]
		for idx in range(0,startd_idx):
			yield sequence[idx]


# conditional utilities
######################
def xor(a,b):
	"""Evaluate exclusive or as lazily as possible"""
	if not a:
		return b
	if not b:
		return a
	return False

def wait_until(conditional, timeout = -1, poll_interval = 1):
	"""
	Wait until conditional evaluates to True
	
	:param conditional: Callable that evaluates to the break condition.
	:param timeout: Maximum time to wait for conditional
	:type timeout: int or float
	:param poll_interval: Interval in seconds during each poll of conditional
	:type poll_interval: int or float
	"""
	time_cond = lambda: True
	if timeout:
		time_cond = lambda stime = time.time(): time.time() - stime < timeout
	while time_cond() and not conditional():
		time.sleep(poll_interval)
	return conditional()

def crontab_sleeper(cron_tab, termination_event=None):
	"""
	Create generator for sleep cycles based on crontab

	This generator will try to sleep in such a way that it awakes every time
	when crontab would execute the job.

	:param cron_tab: Already configured CronTab object
 	:type cron_tab: crontab.CronTab
	:param termination_event: optional event that terminates the loop
	:type termination_event: threading.Event
	:rtype: types.GeneratorType
	"""
	def crontab_sleep():
		def sleep(duration):
			if termination_event is not None:
				termination_event.wait(duration)
				return not termination_event.is_set()
			time.sleep(duration)
			return True
		sleep_time = cron_tab.next()
		while sleep(sleep_time):
			yield sleep_time
			sleep_time = cron_tab.next()
	return crontab_sleep()

def interval_sleeper(interval, variance=0.0, termination_event=None):
	"""
	Create generator for sleep cycles

	The generator will try to sleep in such a way that it awakes every ``interval``
	seconds after its last awakening, randomly modified by ± ``variance``. A loop
	using this generator will thus run every interval seconds on average.

	:param interval: average time between wake ups
	:type interval: int or float
	:param variance: maximum difference by which to randomly differ from interval
	:type variance: int or float
	:param termination_event: optional event that terminates the loop
	:type termination_event: threading.Event
	:rtype: types.GeneratorType
	"""
	def interval_sleep():
		"""
		:return: time slept
		:rtype: float
		"""
		def sleep(duration):
			if termination_event is not None:
				termination_event.wait(duration)
				return not termination_event.is_set()
			time.sleep(duration)
			return True
		sleep_time = 0.0
		while sleep(sleep_time):
			last_awake = time.time()
			yield sleep_time
			sleep_time = max(interval - (time.time() - last_awake) + random.uniform(variance,-variance), 0)
	return interval_sleep()

## Output formatters
####################
def niceTime(usetime=None):
	if usetime:
		try:
			return time.strftime(niceTime.fmt, usetime)
		except TypeError:
			return time.strftime(niceTime.fmt, time.localtime(usetime))
	return time.strftime(niceTime.fmt)
niceTime.fmt = '%y/%m/%d-%H:%M:%S'

def listItems(*items, **kwargs):
	"""
	Create a string listing as ``"item1, item2, ..., item-2 or item-1"``
	
	:param items: objects to list. objects are converted for printing as ``str(item)``
	:param relation: separator to add between the last two elements
	:type relation: str
	:param separator: separator to add between all but the last two elements
	:type separator: str
	:param quotes: quotes to add before and after each item
	:type quotes: str
	"""
	relation  = kwargs.get('relation',"or")
	separator = kwargs.get('separator',",")
	quotes    = kwargs.get('quotes',"'")
	if not items:
		return quotes + quotes
	if len(items) == 1:
		return quotes + str(items[0]) + quotes
	joinStr = quotes + separator + quotes
	return (
		quotes + joinStr.join(str(item) for item in items[:-1]) + quotes
		+ " " + relation + " " +
		quotes + str(items[-1]) + quotes
		)

def nicePath(path, desiredlen = -1):
	"""Shorten a path to be easier to display"""
	def fitPathpartsToLen(parts, post, desiredlen, schema = ''):
		remainlen = desiredlen - len(schema) - len(post) - len(parts)*2 - 3
		partlen = 1.0 + remainlen / len(parts)
		if partlen < 2: # too short, don't try
			return [ schema, '..', post ]
		partsize  = [int(math.ceil(partlen))] * int(remainlen - math.floor(partlen)*len(parts))
		partsize += [int(math.floor(partlen))] * (len(parts) - len(partsize))
		cutpath = lambda ppart, maxlen: ppart[:maxlen]+'..' if len(ppart) > maxlen else ppart
		retparts  = [ cutpath( parts[i],partsize[i] ) for i in xrange(len(parts)) ]
		return flatList(schema, retparts, post)
	schema = ''.join(path.partition('://')[:2])
	if not schema or schema == path or len(schema)>13:
		if path[0] == os.sep:
			schema = os.sep
		else:
			schema = ''
	path = path.replace(schema, '', 1)
	pathparts = path.split(os.sep)
	if pathparts[-1] == '':
		pathparts[-2] = pathparts[-2] + os.sep
		del(pathparts[-1])
	if len(pathparts) == 1:
		return schema + pathparts[-1]
	if len(pathparts) > 5:
		pathparts = pathparts[:3] + ['..'] + pathparts[-2:]
	if desiredlen > 0:
		pathparts = fitPathpartsToLen(pathparts[:-1], pathparts[-1], desiredlen, schema)
	return os.path.join(*flatList(schema, pathparts))

_prefix_1000 = {
	-4 : "p",
	-3 : "n",
	-2 : "u",
	-1 : "m",
	0 : "",
	1 : "k",
	2 : "M",
	3 : "G",
	4 : "T",
	5 : "P",
	6 : "E",
	7 : "Z",
	8 : "Y",
}
def numeric_prefix(numeric, pow2=False):
	"""
	Convert a number to a shortened version using SI prefixes

	Shortens numbers using SI prefixes. For example, ``899812`` is converted to
	``899k``. The returned representation is always between 1 and 4 characters
	long.

	:param numeric:
	:param pow2:
	:return:
	"""
	if numeric == 0:
		return "0"
	base = pow2 and 1024 or 1000
	exp3 = int(math.log(numeric, base)) if numeric >=1 else int(math.log(numeric, base))-1
	if exp3 > 8:
		exp3 = 8
	elif exp3 < -4:
		exp3 = -4
	num_red = numeric * (base ** -exp3)
	if exp3 == 0:
		if num_red < 9.99500000001:
			return "%.2f"%(num_red)
		if num_red < 99.95:
			return "%.1f"%(num_red)
	if num_red < 9.9500000001:
		return "%.1f%s"%(num_red,_prefix_1000[exp3])
	return "%.0f%s"%(num_red,_prefix_1000[exp3])


def niceBytes(numbytes, pow2 = False):
	"""Convert a byte size to human readable format"""
	if pow2:
		return numeric_prefix(numbytes, pow2=True) + "iB"
	return numeric_prefix(numbytes, pow2=False) + "B"


class Progress(object):
	"""
	Progress tracker and indicator

	Implements a progress counter as well as a progress indicator. Progress may
	be advanced by calling :py:meth:`Progress.step` or :py:meth:`Progress.add`.
	Every ``steps``, the tracker automatically outputs a progress bar in the
	style ``Progress: [=>..17%...]``.

	:param maximum: the goal of the internal counter
	:type maximum: int or float
	:param steps: interval at which to automatically output current progress
	:type steps: 1
	:param name: name of the counter
	:type name: str
	:param bar_length: length of any progress bar, without the border
	:type bar_length: int
	:param bar_border: characters for borders around any progress bar
	:type bar_border: str
	:param bar_progress: characters for the progress bar
	:type bar_progress: str
	:param bar_remain: characters for the remainder of the progress bar
	:type bar_remain: str
	:param output_format: format string to use for the progress
	:type output_format: str
	:param out_stream: a file-like object to which automatic progress is written
	:type out_stream: :py:class:`file`

	The following format elements are available for ``output_format``:

	- **name** the ``name`` parameter
	- **rate** the rate at which the progress advances
	- **percent** progress in percent, e.g. ``26%``
	- **counter** internal counter state, e.g. ``127/873``
	- **bar** a progress bar, e.g. ``[=> .......]``
	- **percent_bar** combines **bar** and **percent**, e.g. ``[====94%=> ]``
	- **counter_bar** combines **bar** and **counter**, e.g. ``[=> .1/9...]``

	:note: In order to have the progress update the same line progressively, the
	       ``output_format`` **must** begin with a carriage return (``\\r``) and
	       **not** end with a newline (``\\n``). As there are usecase which might
	       desire non-progressive updates, this is not enforced.
	"""
	def __init__(self, maximum=100, steps=1, name="Progress", bar_length=20, bar_border="[]", bar_progress="=>", bar_remain=" .", output_format="\r%(name)s: %(percent_bar)s %(rate)s", out_stream=sys.stderr):
		self._count = 0
		self._stime = time.time()
		self.maximum = maximum
		self.steps = steps
		self.name = name
		self.bar_length = bar_length
		self.bar_border = bar_border
		self.bar_progress = bar_progress
		self.bar_remain = bar_remain
		self.output_format = output_format
		self.out_stream = out_stream

	def step(self):
		"""
		Advance the progress by 1 and display the status if appropriate
		"""
		self._count += 1
		if self._count % self.steps < 1:
			self.emit()

	def add(self, count=1):
		"""
		Advance the progress and display the status if appropriate

		:param count: how far to advance the counter
		:type count: int or float
		"""
		self._count += count
		if self._count % self.steps <= count:
			self.emit()

	def emit(self):
		"""
		Output the current progress to ``out_stream``
		"""
		elements = self._make_elements()
		self.out_stream.write(self.output_format%elements)
		self.out_stream.flush()

	def __repr__(self):
		return "%s(count=%s, maximum=%s)"%(self.__class__.__name__, self._count, self.maximum)

	def __str__(self):
		elements = self._make_elements()
		return (self.output_format%elements).replace("\n","").replace("\r","")

	def _make_elements(self):
		rate, r_max = self._make_rate()
		percent, p_max = self._make_percent()
		counter, c_max = self._make_counter()
		bar, b_max = self._make_bar()
		percent_bar, pb_max = self._make_count_bar(counter=percent, bar=bar, counter_width=p_max)
		counter_bar, cb_max = self._make_count_bar(counter=counter, bar=bar, counter_width=c_max)
		return {"name":self.name, "counter_bar":counter_bar, "counter":counter, "percent_bar":percent_bar, "bar":bar, "percent":percent, "rate":rate}

	def _make_rate(self):
		rate = self._count / (time.time() - self._stime)
		r_str = numeric_prefix(rate, pow2=False) + "Hz"
		return r_str, len(r_str)

	def _make_percent(self):
		progress = min(100.0, 100.0*self._count / self.maximum)
		return "%d%%"%progress, 3

	def _make_counter(self):
		return "%d/%d"%(self._count, self.maximum), len(str(self.maximum))*2+1

	def _make_bar(self):
		progress = min(1.0, 1.0*self._count / self.maximum)
		bar_progress = int(progress*self.bar_length)
		if bar_progress < 1:
			bar_str = self.bar_remain[-1] * self.bar_length
		elif bar_progress == 1:
			bar_str = self.bar_progress[-1] + self.bar_remain[0] + self.bar_remain[-1] * (self.bar_length-2)
		elif bar_progress == self.bar_length:
			bar_str = self.bar_progress[0] * self.bar_length
		elif bar_progress == self.bar_length - 1:
			bar_str = self.bar_progress[0] * (bar_progress-1) + self.bar_progress[-1] + self.bar_remain[0]
		else:
			bar_str = self.bar_progress[0] * (bar_progress-1) + self.bar_progress[-1] + self.bar_remain[0] + self.bar_remain[-1] * (self.bar_length-bar_progress-1)
		if self.bar_border:
			return self.bar_border[0] + bar_str + self.bar_border[-1], self.bar_length + 2
		return bar_str, self.bar_length

	def _make_count_bar(self, counter, bar, counter_width=None):
		if counter_width is None:
			counter_width = len(counter)
		# right-centered adjust:
		# counter is either equaly close to both sides
		# [> .7%...]
		# [===70%=> ]
		# or one closer to the right
		# [> ..7%...]
		# [===70%=>]
		# Since python integer division ALWAYS rounds down, the following gives
		# us ALWAYS either the equal distance or the smaller distance...
		r_barlength = (len(bar) - counter_width) / 2
		# ...and this gives us always the remaining distance.
		l_barlength=len(bar)-r_barlength-len(counter)
		count_bar = bar[:l_barlength] + counter + bar[-r_barlength:]
		return count_bar, self.bar_length + 2


## Types
########
def FlatList(*args):
	"""Construct a flat list, resolving any non-string iterable"""
	return flatList(*args)
def flatList(*args):
	"""Returns a flat list, resolving any non-string iterable"""
	if isinstance(args, basestring):
		return [ args ]
	retList = []
	for thing in args:
		try:
			# ignore non-sequence-ish containers
			if isinstance(thing, (collections.Mapping, basestring)):
				raise TypeError
			retList.extend(flatList(*thing))
		except TypeError:
			retList.append(thing)
	return retList

class Bunch(object):
	"""
	Dynamic struct like container with attribute lookup
	"""
	def __init__(self, **kws):
		self.__dict__.update(kws)
	def __repr__(self):
		return '%s(%s)' %(self.__class__.__name__, ', '.join( '%s=%s' % (key, self.__dict__[key]) for key in self.__dict__))

def confidential(thing):
	"""Decorator for hiding the content of a type from output"""
	thing.__repr__ = "%s(<hidden>)" % thing.__class__.__name__
	thing.__str__  = "%s(<hidden>)" % thing.__class__.__name__
	return thing

def iterableitems(iterable):
	"""Return a (key, value)-items iterator for iterable"""
	if hasattr(iterable, "iteritems"):
		return iterable.iteritems()
	return enumerate(iterable)

## External resources
#####################

# File and path utilities
def ensure_dir(dirName, descr = 'directory'):
	"""Ensure that a directory exists"""
	if not os.path.exists(dirName):
		try:
			os.makedirs(dirName)
		except:
			raise RethrowException('Failed creating %s "%s"' % (descr, dirName), RuntimeError)
def ensureDir(dirName, descr = 'directory'):
	ensure_dir(dirName=dirName, descr=descr)

def ensureRm(pathName, descr = 'path'):
	ensure_rm(pathName = pathName, descr = descr)
def ensure_rm(pathName, descr = 'path'):
	"""Ensure that a path does not exist"""
	if os.path.isfile(pathName) or os.path.islink(pathName):
		os.unlink(pathName)
	elif os.path.isdir(pathName):
		shutil.rmtree(pathName)

def which(executable):
	"""Locate an executable in the current environment"""
	isExecutable = lambda candidate_path: os.path.isfile(candidate_path) and os.access(candidate_path,os.X_OK)
	if os.path.dirname(executable):
		if isExecutable(executable):
			return executable
	for path in os.environ['PATH'].split(os.pathsep):
		trypath = os.path.join(path.strip('"'), executable)
		if isExecutable(trypath):
			return trypath
	raise InstallationError("Failed to locate executable '%s' as absolute and in 'PATH'" % executable)

def get_config_files(name=None, globs="*.cfg", env_vars=(), paths=()):
	"""
	Search for configuration files in standard locations

	This looks up a number of candidates, which may be either defined as
	files, directories or globs. First, if any specified environment variable
	that are set are tried. Then, explicit paths are tried. The first candidate
	that matches any file is returned.

	The format for the content of environment variables and ``paths`` may be a
	mix of:

	* globs - a string for the :py:mod:`glob` module

	* directory - a path pointing to a directory, ending with ``/``

	* files - a comma separated list of files

	* list - a python list of files, only available for ``paths``

	:note: The home dir (``~``) is supported as per the :py:func:`os.path.expanduser`
	       function.

	If ``name`` is set, the environment variables ``NAME_CONFIG`` and
	``NAME_CFG`` are added to the search. Note that they are uppercase. If
	``file_glob`` is set as well (it defaults to ``"*.cfg"``), the globs
	``/etc/%(name)s/config.d/%(glob)s`` and ``~/%(name)s/config.d/%(glob)s`` are
	used; if ``file_glob`` is :py:class:`None`, all files in the directories
	``/etc/%(name)s/config.d/`` and ``~/%(name)s/config.d/`` are used.

	:param app_name:
	:param env_name:
	:param path_globs:
	:return:
	"""
	env_vars = list(env_vars)
	if name is not None:
		env_vars.append(name.upper()+"_CONFIG")
		env_vars.append(name.upper()+"_CFG")
	candidates = []
	for var in env_vars:
		try:
			candidates.append(os.environ[var])
		except KeyError:
			pass
	candidates.extend(paths)
	if name is not None:
		if globs is None:
			candidates.append("/etc/%s/config.d/"%name)
			candidates.append("~/%s/config.d/"%name)
		else:
			candidates.append("/etc/%s/config.d/%s"%(name,globs))
			candidates.append("~/%s/config.d/%s"%(name,globs))
	for candidate in candidates:
		matches = []
		if not isinstance(candidate, basestring):
			for cand_file in candidate:
				cand_file = os.path.expanduser(cand_file)
				if os.path.isfile(cand_file):
					matches.append(cand_file)
		elif candidate.endswith("/"):
			for cand_file in os.listdir(os.path.expanduser(candidate)):
				if os.path.isfile(cand_file):
					matches.append(cand_file)
		elif "," in candidate:
			for cand_file in candidate.split(","):
				cand_file = os.path.expanduser(cand_file)
				if os.path.isfile(cand_file):
					matches.append(cand_file)
		else:
			matches = glob.glob(os.path.expanduser(candidate))
		if matches:
			return matches
	raise FileNotFound("Failed to find any configuration files. Searched environment %s and paths %s"%(env_vars, candidates))


def repo_version(repo_path='.'):
	"""
	Return version in the repository as repo type, branch, commit and timestamp
	:param repo_path:
	:return: repo type, branch, commit and timestamp
	:rtype: dict
	"""
	try:
		return _repo_version_git(repo_path)
	except ValueError:
		return {"type":"git", "branch":"N/A", "commit":"N/A", "timestamp":"N/A"}
	except InstallationError:
		pass
	return {}


def _repo_version_git(repo_path='.'):
	try:
		raw_rev_blob = subprocess.check_output(["git", "log", "-1", "--date=iso", "--decorate"], universal_newlines=True, cwd=repo_path)
	except (OSError, subprocess.CalledProcessError):
		raise InstallationError
	rev_blob = {}
	for line in raw_rev_blob.splitlines():
		line = line.strip()
		if line:
			key, value = line.split(None, 1)
			rev_blob[key.rstrip(":")] = value.strip()
	try:
		commit = rev_blob["commit"].split(None,1)[0]
		branch = rev_blob["commit"].split(None,1)[1].rpartition(", ")[-1].rstrip(")")
		timestamp = rev_blob["Date"]
	except KeyError:
		raise ValueError
	return {"type":"git", "branch":branch, "commit":commit, "timestamp":timestamp}


def version_id(app_name, app_version, repo_dict=None):
	app_str = "%s %s" %(app_name, ".".join(str(ver) for ver in app_version))
	if repo_dict is not None:
		app_str += ".{branch} ({type} {commit} @ {timestamp})".format(**repo_dict)
	return app_str

## Introspection and Internal Functionality
###########################################

# import utilities
def findClass( className, moduleSpace = None, description = 'Dynamic Class'):
	"""
	Get a class dynamically by its name, e.g. 'module.submodule.ClassName'.

	:param className: The name of the class to find, e.g. ``module.submodule.ClassName``
	:type className: str
	:param moduleSpace: mapping of names to modules to search through
	:type moduleSpace: dict[str, module]
	:returns: the class and its module name
	:rtype: type, str

	Note: classNames including module paths require a strict match, whereas pure
	classNames use the module with the shortest name.
	"""
	if '.' in className:  # e.g. module.submodule.foomodule.ClassName
		moduleName = className.rpartition('.')[0]
		classSubName  = className.rpartition('.')[1]
		try:
			module = moduleSpace[moduleName]
		except KeyError: # wrong module name
			candidates = difflib.get_close_matches(moduleName, moduleSpace.keys())
			candidateMsg = candidates and " Did you mean %s?"%listItems(candidates) or ''
			raise InstallationError("%s: module '%s' (for class '%s') is unknown.%s" % (description, moduleName, classSubName, candidateMsg))
		try:
			return getattr(module, classSubName), moduleName
		except AttributeError:
			raise InstallationError("%s: module '%s' does not contain class '%s'." %(description, moduleName, classSubName))
	# try all modules, with the most basic first
	for moduleName in sorted(moduleSpace.keys(), key = lambda name: name.count('.')):
		try:
			return getattr(moduleSpace[moduleName], className), moduleName
		except AttributeError:
			continue
	raise InstallationError("%s: class '%s' is unknown." % (description, className))

# introspection
def estimate_size(obj):
	"""
	Estimator for the (memory) size of a hierarchy of objects

	The estimation is targeted at the dynamically allocated memory for any data
	like primitives (int, float, str), containing data structures (pointers of a
	list to ints, fields of a class) or monkey-patched instance methods. Any
	constant objects, such as modules or static variables, are ignored.

	Modelled after `recipe <http://code.activestate.com/recipes/577504/>`_

	:return: size in bytes
	:rtype: int
	"""
	def account_hierarchy(cur_obj):
		cur_size = 0
		if id(cur_obj) in detected:
			return cur_size
		detected.add(id(cur_obj))
		# size of the object
		try:
			cur_size += sys.getsizeof(cur_obj)
			accounted.add(id(cur_obj))
		except:
			return cur_size
		# size of the hierarchy - try all apropriate as we avoid double-counting otherwise
		# dictionary
		try:
			cur_size += sum(account_hierarchy(key) + account_hierarchy(val) for key, val in cur_obj.items())
		except AttributeError:
			pass
		# generic iterable
		try:
			cur_size += sum(account_hierarchy(item) for item in iter(cur_obj))
		except TypeError:
			pass
		# new-style class
		try:
			cur_size += account_hierarchy(cur_obj.__dict__)
		except AttributeError:
			pass
		# new-style class with set members
		try:
			cur_size += account_hierarchy(getattr(cur_obj, attr) for attr in cur_obj.__slots__)
		except AttributeError:
			pass
		return cur_size
	detected    = set()
	accounted   = set()
	return account_hierarchy(obj)

## Exit Codes
###############

class Exit(object):
	"""Helper for enforcing and documenting exit codes"""
	_exits = {}
	def __init__(self, code):
		sys.exit(code)
	@classmethod
	def registerExit(cls, name, value, descr):
		setattr(cls, name.upper(), staticmethod(lambda: sys.exit(value)))
		getattr(cls, name.upper()).__doc__ = descr
		cls._exits[name.upper()] = (value, descr)
	@classmethod
	def getHelp(cls, short = False):
		rawfmt="%3d - %s\n\t%s"
		if short:
			rawfmt="%3d:%s%s"
		txt = []
		maxlen = max(len(retname) for retname in cls._exits) + 1
		for retname in sorted(cls._exits, key=lambda name: cls._exits[name][0]):
			txt.append(rawfmt % (
				cls._exits[retname][0],
				retname.ljust(maxlen),
				cls._exits[retname][1])
			)
		return '\n'.join(txt)
# defaults according to sysexits.h and bash convention
Exit.registerExit('success', 0, descr = 'The program exited normally')
Exit.registerExit('generic', 1, descr = 'Unspecified error')
Exit.registerExit('notfound', 127, descr = 'A vital resource (file, folder, ...) could not be found')
Exit.registerExit('usage', 64, descr = 'Incorrect usage of CLI API')
Exit.registerExit('noperm', 77, descr = 'Permission denied for operation')
Exit.registerExit('config', 78, descr = 'Provided configuration is faulty')

