# -*- coding: utf-8 -*-
#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
Persistent versions of standard containers, exposing standard interfaces backed
by a mixture of in-memory cache and persistent data collection.

:note: It is not a technical requirement that the persistency is based on actual
       files; for example, databases are technically possible as backends. There
       is no guarantee made by the interface for the backend. The term file is
       used for simplicity only.

Dictionary
^^^^^^^^^^

The mapping (``dict``-like) container :py:class:`~.PersistentDict` splits data
based on mapping keys to multiple files. Keys are mapped using :py:meth:`~.PersistentDict.map_key`
to identifiers for chunks stored in files; the ``chunk_count`` parameter defines
the number of the mapped keys in use.

This has several implications:

1. Iteration is fastest in the currently open chunk, thus the iteration protocol is
   alligned to this. Inside a chunk, iteration follows the ``dict`` protocol (which
   is effectively random).
2. The ``chunk_count`` implicitly defines the iteration order.
3. Iteration is based on the currently open chunks, but also requires opening
   chunks when iterating over the entire mapping, in turn affecting future iterations.
   Thus, the iteration order should **never** be assumed to be the same.
4. Consistency implies some side effects: ``__init__`` does not allow adding members
   via kwargs. Changing the ``chunk_count`` causes the entire persistent
   data to be reshuffled. Changing :py:meth:`~.PersistentDict.map_key` on an
   instance containing data causes undefined behaviours.

:note: In principle, any valid :py:class:`dict` key/value is also valid for this
       mapping. Since content is stored using :py:mod:`pickle`, the mapping
       may create an arbitrary number of clones over time. Both keys and
       values should thus resolve to unarbitrary objects with strictly defined
       hashes, such as primitives like :py:class:`str` or :py:class:`int` or
       containers of these types.

:note: The mapping attempts to keep track of its chunks and items that are used.
       If possible, a "live" version is returned instead of recreating an item.
       This is implemented using the :py:mod:`~weakref` module; see the module
       documentation for which objects are compatible.

:note: Objects are realiably commited persistently on any assignment (such as
       ``d[key]=value`` or :py:meth:`~PersistentDict.update`) or when explicitly
       saving using :py:meth:`~.PersistentDict.flush`. An object *may* be
       commited implicitly, namely if an object of the same chunk is stored;
       since chunk relations are complex and may change at runtime, this should
       not be relied on. If implicit commits are unwanted, a shallow copy of
       values is sufficient.

:warning: Changes made to the objects **inside** the mapping cannot be detected.
          They must be commited explicitly using :py:meth:`~.PersistentDict.flush`
          or by explicit (re-)assignment (``d[key]=value``). If an object is
          not commited before being garbage collected, all changes are lost.
"""

import os
from weakref import WeakValueDictionary
from collections import deque
import cPickle as pickle
import glob
import math
import shutil

from .utils   import ensure_dir, ensure_rm, NotSet

class WeakDict(dict):
	"""Subclass of dict allowing for weak references"""
	pass

class PersistentDict(object):
	"""
	Mapping object that is persistently stored as a file

	:param store_base_uri: base path to stored chunks
	:type store_base_uri: :py:class:`str`
	:param chunk_count: number of chunks to hold persistent data
	:type chunk_count: :py:class:`int`
	:param cache_size: number of chunks to LRU-cache in memory
	:type cache_size: :py:class:`int`
	:param cache_keys: whether to cache all keys in memory
	:type cache_keys: :py:class:`bool`

	:note: The constraint ``cache_size >= 1`` is enforced silently.

	:warning: Since ``keys`` and ``values`` are not guaranteed to be available in
	          memory, there is no equivalence for ``dict.viewkeys()``,
	          ``dict.viewvalues()`` and ``dict.viewitems()``.
	"""
	def __init__(self, store_base_uri, chunk_count=None, cache_size=3, cache_keys=True):
		ensure_dir(os.path.dirname(store_base_uri))
		self._base_uri = store_base_uri
		# LRU store for objects fetched from disk
		self._chunk_cache = deque()
		# weakref store for objects still in use
		self._active_chunks = WeakValueDictionary()
		self._active_items = WeakValueDictionary()
		# load current settings
		self._chunk_count = self._read_head() or chunk_count or 32
		self._fmt = "%%0%dX" % self._chunk_fmt_digits()
		self._keys_cache = None
		# apply new settings
		self.cache_size = max(cache_size,1)
		self.chunk_count = chunk_count or self._chunk_count
		# store new settings
		self._flush_head()
		# cache keys in memory
		if cache_keys:
			self._keys_cache = set(self.keys())

	def _chunk_fmt_digits(self):
		"""Return the number of hex digits required for the chunk name"""
		return max(int(math.ceil(math.log(self._chunk_count, 16))),1)

	# exposed settings
	@property
	def chunk_count(self):
		"""
		Get/Set the ``chunk_count`` of the persistent mapping

		:note: Setting ``chunk_count`` causes **all** chunks storing data to be
		       recreated. Until the new chunks have been created, changes to the
		       mapping content may be silently dropped.
		"""
		return self._chunk_count

	@chunk_count.setter
	def chunk_count(self, value):
		"""
		Set the ``chunk_count`` of the persistent mapping

		:param value: new chunk count to apply
		:type value: :py:class:`int`
		:raises AssertionError: if the chunk count is set smaller than 1
		"""
		if self._chunk_count != value:
			assert int(value) >= 1, "Chunk count must be >= 1"
			value = int(value)
			# load entire content
			old_stores = glob.glob(self._chunk_path("?"*self._chunk_fmt_digits()))
			content = self.copy()
			# switch to new chunk count
			self._chunk_count = value
			self._fmt = "%%0%dX" % self._chunk_fmt_digits()
			# create new data layout
			self.update(content)
			self._flush_head()
			# remove old stores
			for store in old_stores:
				ensure_rm(store)

	def map_key(self, key):
		"""
		Create the persistent key for a given memory key

		:param key: key to the content in-memory
		:return: key to the content stored persistently
		:rtype: str
		"""
		return self._fmt % ((hash(key)&0xffffffff) % self._chunk_count)

	def _read_head(self):
		"""
		Read the meta-information of the dict

		:return: meta-information of the dictionary
		:rtype: :py:class:`dict` or :py:class:`None`
		"""
		try:
			return pickle.load(open(self._head_path(),"rb"))["ckc"]
		except (IOError, EOFError):
			return None

	def _flush_head(self):
		"""
		Store the meta-information of the dict
		"""
		pickle.dump({"ckc":self.chunk_count}, open(self._head_path(),"wb"), pickle.HIGHEST_PROTOCOL)

	def _read_chunk(self, chunk_key):
		"""
		Return a chunk from disk or create a new one

		:param chunk_key: key for the entire chunk
		:return: existing chunk for ``chunk_key`` or a new one if none exists
		:rtype: :py:class:`~WeakDict`
		"""
		try:
			chunk = pickle.load(open(self._chunk_path(chunk_key), "rb"))
		except (IOError, EOFError):
			chunk = WeakDict()
		self._active_chunks[chunk_key] = chunk
		self._chunk_cache.appendleft(chunk)
		while len(self._chunk_cache) > self.cache_size:
			self._chunk_cache.pop()
		return chunk

	def _flush_chunk(self, chunk_key):
		"""
		Store a chunk on disk

		:param chunk_key: key for the entire chunk
		"""
		chunk = self._active_chunks[chunk_key]
		if not chunk:
			ensure_rm(self._chunk_path(chunk_key))
		else:
			pickle.dump(chunk, open(self._chunk_path(chunk_key)+".tmp","wb"), pickle.HIGHEST_PROTOCOL)
			shutil.move(self._chunk_path(chunk_key)+".tmp", self._chunk_path(chunk_key))

	# paths and files
	def _head_path(self):
		return self._base_uri + "head.pkl"

	def _chunk_path(self, chunk_key):
		"""
		The path to the file storing a specific chunk, given the chunk's key

		:param chunk_key: key for the entire chunk
		:return: path to the file storing the chunk
		:rtype: :py:class:`str`
		"""
		return self._base_uri + "chunk_%d_%s.pkl" % (self._chunk_count, chunk_key)

	def _chunk_keys(self):
		"""
		Get the keys of all committed chunks

		:return: ``chunk_key``\ s of all chunks
		:rtype: list[:py:class:`str`]
		"""
		chunks = glob.glob(self._chunk_path("?"*self._chunk_fmt_digits()))
		for chunk in chunks:
			yield chunk[:-4].rpartition("chunk_")[2][len(str(self._chunk_count))+1:]

	def flush(self):
		"""
		Commit all outstanding changes to persistent store
		"""
		for chunk_key in self._active_chunks.keys():
			self._flush_chunk(chunk_key)

	# dictionary interface
	def __getitem__(self, key):
		# If item is still used, return used item directly
		# If chunk is cached, return item from chunk
		# Else, fetch everything from disk
		try:
			return self._active_items[key]
		except KeyError:
			chunk_key = self.map_key(key)
			try:
				chunk = self._active_chunks[chunk_key]
			except KeyError:
				chunk = self._read_chunk(chunk_key)
			return chunk[key]

	def __setitem__(self, key, value):
		# Store item to chunk
		#   - If chunk is cached, use cached version
		#   - Else, fetch chunk
		# Modify chunk
		# Commit chunk
		# Update key cache
		# Update item cache
		chunk_key = self.map_key(key)
		try:
			chunk = self._active_chunks[chunk_key]
		except KeyError:
			chunk = self._read_chunk(chunk_key)
		chunk[key] = value
		self._flush_chunk(chunk_key)
		if self._keys_cache is not None:
			self._keys_cache.add(key)
		try:
			self._active_items[key] = value
		except TypeError:
			pass

	def __delitem__(self, key):
		# Delete item from chunk
		#   - If chunk is cached, use cached version
		#   - Else, fetch chunk
		# Modify chunk
		# Commit chunk
		# Update key cache
		# Update item cache
		chunk_key = self.map_key(key)
		try:
			chunk = self._active_chunks[chunk_key]
		except KeyError:
			chunk = self._read_chunk(chunk_key)
		del chunk[key]
		self._flush_chunk(chunk_key)
		if self._keys_cache is not None:
			self._keys_cache.discard(key)
		try:
			del self._active_items[key]
		except KeyError:
			pass

	def __contains__(self, item):
		return item in self.keys()

	def __iter__(self):
		""":see: :py:meth:`~.PersistentDict.iterkeys`"""
		read_chunks = set()
		# start with the chunks we have in memory
		for chunk_key in self._active_chunks.keys():
			for item_key in self._active_chunks[chunk_key].keys():
				yield item_key
			read_chunks.add(chunk_key)
		# pull in all chunks
		for chunk_key in self._chunk_keys():
			if chunk_key not in read_chunks:
				chunk = self._read_chunk(chunk_key)
				for item_key in chunk.keys():
					yield item_key
				read_chunks.add(chunk_key)

	def __len__(self):
		return len(self.keys())

	def __nonzero__(self):
		return len(self) > 0

	# iterations
	def iter(self):
		"""
		Return an iterator over the keys of the dictionary. This is a shortcut
		for :py:meth:`~.PersistentDict.iterkeys`.

		:note: See the note on iterator equivalency for :py:meth:`~.PersistentDict.items`.
		"""
		return self.iterkeys()

	def iterkeys(self):
		"""
		Return an iterator over the keys of the dictionary.

		This iterates over all keys in a semi-deterministic way. First, all keys
		from chunks cached in memory are returned. Following this, keys from the
		remaining chunks are returned.

		:note: This function does not benefit from ``cache_keys``.

		:note: See the note on iterator equivalency for :py:meth:`~.PersistentDict.items`.
		"""
		return iter(self)

	def keys(self):
		"""
		Return a copy of the dictionary’s list of keys.

		:note: If ``cache_keys`` is set, keys are returned in arbitrary order.
		       Otherwise, the order of :py:meth:`~.PersistentDict.iterkeys` is
		       used.

		:note: See the note on iterator equivalency for :py:meth:`~.PersistentDict.items`.
		"""
		if self._keys_cache is not None:
			return list(self._keys_cache)
		return list(iter(self))

	def iteritems(self):
		"""
		Return an iterator over the dictionary’s list of ``(key, value)`` pairs.

		:note: See the note on iterator equivalency for :py:meth:`~.PersistentDict.items`.
		"""
		for item_key in self:
			yield (item_key, self[item_key])

	def items(self):
		"""
		Return a copy of the dictionary’s list of ``(key, value)`` pairs.

		:note: Since the state of the mapping also depends on accesses, the strict
		       guarantee for iteration sequence equivalence given by ``dict`` is
		       not replicated. Thus, it cannot be assumed that
		       ``d.items() == zip(d.values(), d.keys()) == zip(d.itervalues(), d.iterkeys()) == [(v, k) for (k, v) in d.iteritems()]``
		       holds true in any case.
		"""
		return list(self.iteritems())

	def itervalues(self):
		"""
		Return an iterator over the dictionary’s values.

		:note: See the note on iterator equivalency for :py:meth:`~.PersistentDict.items`.
		"""
		try:
			for item_key in self:
				yield self[item_key]
		except KeyError:
			raise RuntimeError("dictionary changed size during iteration")

	def values(self):
		"""
		Return a copy of the dictionary’s list of values.

		:note: See the note on iterator equivalency for :py:meth:`~.PersistentDict.items`.
		"""
		return list(self.itervalues())

	# high level operations
	def clear(self):
		"""Remove all items from the dictionary."""
		# clear the persistent storage
		for chunk_key in self._chunk_keys():
			ensure_rm(self._chunk_path(chunk_key))
		# reset caches
		self._chunk_cache = type(self._chunk_cache)()
		self._active_chunks = type(self._active_chunks)()
		self._active_items = type(self._active_items)()
		self._keys_cache = None if self._keys_cache is None else type(type(self._keys_cache)())


	def copy(self):
		"""
		Return a shallow copy of the dictionary.

		:note: This will return a ``dict``, not a :py:class:`~.PersistentDict`.
		"""
		return dict(self.iteritems())

	def get(self, key, default=None):
		"""
		Return the value for key if key is in the dictionary, else default. If
		default is not given, it defaults to ``None``, so that this method never
		raises a :py:exc:`KeyError`.

		:param key: key to an item in the dictionary
		:param default: default to return if no item exists
		:raises KeyError: if no items exists and no default is given
		"""
		try:
			return self[key]
		except KeyError:
			return default

	def has_key(self, key):
		"""
		Test for the presence of key in the dictionary. :py:meth:`~.PersistentDict.has_key`
		is deprecated in favor of ``key in d``.

		:param key: key to an item in the dictionary
		"""
		return key in self

	def pop(self, key, default=NotSet):
		"""
		If key is in the dictionary, remove it and return its value, else return
		default.

		:param key: key to an item in the dictionary
		:param default: default to return if no item exists
		:raises KeyError: if no items exists and no default is given
		"""
		try:
			item = self[key]
			del self[key]
		except KeyError:
			if default is NotSet:
				raise
			item = default
		return item

	def setdefault(self, key, default=None):
		"""
		If key is in the dictionary, return its value. If not, insert key with a
		value of ``default`` and return ``default``. ``default`` defaults to
		``None``.

		:param key: key to an item in the dictionary
		:param default: default to insert and return if no item exists
		"""
		try:
			return self[key]
		except KeyError:
			self[key] = default
			return default

	def update(self, other=None, **kwargs):
		"""
		Update the dictionary with the ``(key,value)`` pairs from other,
		overwriting existing keys.

		:py:meth:`~.PersistentDict.update` accepts either another dictionary
		object or an iterable of ``(key,value)`` pairs (as tuples or other
		iterables of length two). If keyword arguments are specified, the
		dictionary is then updated with those ``(key,value)`` pairs:
		``d.update(red=1, blue=2)``.

		:param other: mapping or iterable of ``(key,value)`` pairs
		:param kwargs: ``key=value`` arguments to insert
		:return: None

		:note: This function is faster for large collections as commits are made
		       per chunk, not per item. The drawback is a larger memory consumption
		       as the entire input is sorted in memory.
		"""
		def setchunks(key_values):
			"""
			Commit entire chunks from key, value pairs

			:param key_values: iterable of ``(key, value)`` pairs
			"""
			key_values = sorted(key_values, key=lambda key_val: self.map_key(key_val[0]))
			last_chunk_key, chunk = None, None
			for key, value in key_values:
				chunk_key = self.map_key(key)
				if chunk_key != last_chunk_key:
					if last_chunk_key is not None:
						self._flush_chunk(last_chunk_key)
					last_chunk_key = chunk_key
					try:
						chunk = self._active_chunks[chunk_key]
					except KeyError:
						chunk = self._read_chunk(chunk_key)
				chunk[key] = value
				if self._keys_cache is not None:
					self._keys_cache.add(key)
				try:
					self._active_items[key] = value
				except TypeError:
					pass
			if last_chunk_key is not None:
				self._flush_chunk(last_chunk_key)
		if other is not None:
			if hasattr(other, "iteritems"):
				setchunks(other.iteritems())
			elif hasattr(other, "items"):
				setchunks(other.items())
			else:
				setchunks(other)
		setchunks(kwargs.iteritems())

	def __repr__(self):
		return "%s(store_base_uri=%s, chunk_count=%s, cache_size=%s, cache_keys=%s, items={%s})"%(
			self.__class__.__name__,
			self._base_uri,
			self.chunk_count,
			self.cache_size,
			self._keys_cache is not None,
			self.__repr_content(),
		)

	def __repr_content(self):
		reprs = []
		read_keys = set()
		for chunk_key in self._active_chunks.keys():
			try:
				chunk = self._active_chunks[chunk_key]
				if not chunk:
					continue
				reprs.append(repr(chunk)[1:-1])
				read_keys.update(chunk.keys())
			except KeyError:
				pass
		if self._keys_cache is None:
			reprs.append("<?>")
		elif self._keys_cache:
			cache_repr = ": <?>,".join(repr(key) for key in self._keys_cache if key not in read_keys)
			if cache_repr:
				reprs.append(cache_repr + ": <?>")
		return ",".join(reprs)
