#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
The ``Daemon`` is used to create and control a daemon process. This process is
entirely detached from its starting and controlling environment. It will persist
even when the starting process dies.

The actual implementation is platform dependent, but it is safe for code to import
simply the ``Daemon`` class. On import, the module will automatically provide the
proper implementation for the current platform under the alias ``Daemon``.

Supported/Tested platforms:

* Linux
* Mac OS X

"""

# standard library imports
import os
import sys
import random
import time
import errno
import signal

# third party imports

# application/library imports
from .exceptions import InstallationError, ValidationError, ExceptionFrame
from .utils      import listItems, ensure_rm
from .pidlock    import PIDLock
from .enum       import Unique

# forking signals
FORK_PARENT = Unique(name="Fork Parent")
FORK_CHILD  = Unique(name="Fork Child")

# noinspection PyUnusedLocal
def guess_pid_file(process_name=None):
	"""
	:param process_name: identifying name of the process
	:type process_name: str|None
	:return: pid-file path
	:rtype: str
	"""
	raise NotImplementedError("No implementation for this OS flavor")


def _unix_guess_pid_file(process_name=None):
	def test_directory(directory):
		test_file = "%X.tmp"%random.getrandbits(128)
		try:
			open(os.path.join(directory, test_file),"w")
			ensure_rm(test_file)
			return True
		except (IOError, OSError, RuntimeError):
			return False
	pid_dir_candidates = ("/var/run",os.path.expanduser("~"))
	process_name = process_name or sys.argv[0]
	for pid_dir in pid_dir_candidates:
		if test_directory(pid_dir):
			return os.path.join(pid_dir, "%s.pid"%process_name)
	raise InstallationError("No viable directory for PID file (tried: %s)"%listItems(pid_dir_candidates, relation="and"))


# noinspection PyUnusedLocal
class Daemon(object):
	"""
	Controller for a daemon process

	:param payload: a callable that is invoked once daemonized
	:param pid_file: path to a file that stores the identifier for the daemon process
	:param stdin: input stream for the daemon
	:param stdout: output stream for the daemon
	:param stderr: error stream for the daemon
	"""
	def __init__(self, payload=None, pid_file=None, stdin="/dev/null", stdout="/dev/null", stderr="/dev/null"):
		raise NotImplementedError
	def start(self):
		raise NotImplementedError
	def stop(self):
		raise NotImplementedError
	def restart(self):
		raise NotImplementedError
	def status(self):
		raise NotImplementedError


class UnixDaemon(object):
	"""
	Daemon for unix enviroments

	Based on "A simple unix/linux daemon in Python"	by Sander Marechal (`Source <http://www.jejik.com/articles/2007/02/a_simple_unix_linux_daemon_in_python>`_)

	:param payload: a callable that is invoked once daemonized
	:param pid_file: path to a file that stores the identifier for the daemon process
	:param stdin: input stream for the daemon
	:param stdout: output stream for the daemon
	:param stderr: error stream for the daemon
	"""
	def __init__(self, payload=None, pid_file=None, stdin="/dev/null", stdout="/dev/null", stderr="/dev/null"):
		self.payload  = payload
		self.pid_lock = PIDLock(lock_name=pid_file)
		self.stdin, self.stdout, self.stderr = stdin, stdout, stderr

	def start(self):
		"""
		Start the daemon, daemonize it and launch its payload

		:return: Success
		:raises ValidationError: if the daemon could not be started or is already running
		"""
		if self._daemonize() == FORK_CHILD:
			self.payload()
		return True

	def stop(self, timeout=None):
		"""
		Gracefully ensure that no daemon is running

		:param timeout: maximum time in seconds to try and kill the daemon
		:type timeout: int or float
		:return: whether the daemon is not running anymore
		"""
		return self._stop_kill(timeout=timeout, send_signal=signal.SIGINT, interval=30)

	def terminate(self, timeout=None):
		"""
		Forcefully ensure that no daemon is running

		:param timeout: maximum time in seconds to try and kill the daemon
		:type timeout: int or float
		:return: whether the daemon is not running anymore
		"""
		return self._stop_kill(timeout=timeout, send_signal=signal.SIGTERM, interval=5)

	def restart(self):
		"""
		Stop and start the daemon again

		:return: Success
		:raises ValidationError: if the daemon could not be started or is already running
		"""
		return self.stop() and self.start()

	def status(self):
		"""
		Check the daemon process status

		:return: whether the daemon is running
		:rtype: bool
		"""
		return self.pid_lock.get_active_pid()

	def _stop_kill(self, timeout=None, send_signal=signal.SIGTERM, interval=30):
		"""
		Stop the daemon by repeatedly performing the ``kill`` system call

		:warning: As with the ``kill`` system call, sending non-terminating
		          signals will not actually end the daemon. This will cause an
		          endless loop.

		:param timeout: maximum time in seconds to try and kill the daemon
		:type timeout: int or float
		:param interval: time between sending kill signals
		:type interval: int or float
		:return: whether the daemon is not running anymore
		"""
		daemon_pid = self.pid_lock.get_active_pid()
		if not daemon_pid:
			# daemon isn't running
			return True
		try:
			start_time = time.time()
			loop_time = start_time
			while timeout is not None or time.time()-start_time < timeout:
				os.kill(daemon_pid, send_signal)
				while time.time()-loop_time < interval:
					os.kill(daemon_pid, 0)
					time.sleep(0.1)
		except OSError as err:
			if err.errno is errno.EPERM:
				# daemon is running and we are not allowed to kill it
				return False
			if err.errno is errno.ESRCH:
				# daemon isn't running anymore
				return True
			raise
		return False

	def _daemonize(self):
		"""
		Daemonize using a double fork

		:return: Current process is daemon
		:rtype: bool
		:raise RuntimeError: Failed (first) forking of daemon
		:raise ValidationError: Daemon already exists
		"""
		with ExceptionFrame():
			if not self.pid_lock.acquire(blocking=False):
				raise ValidationError("Daemon is already running")
			# decouple from parent
			try:
				my_pid = os.fork()
				if my_pid != 0:
					return FORK_PARENT
			except OSError:
				raise RuntimeError("Failed forking daemon from parent process")
			# detach from parent environment
			os.chdir("/")
			os.setsid()
			os.umask(0)
			# decouple from parent
			try:
				my_pid = os.fork()
				if my_pid != 0:
					sys.exit(0)
			except OSError:
				raise RuntimeError("Failed forking daemon from intermediate process")
			# redirect std pipes
			sys.stdout.flush()
			sys.stderr.flush()
			stdin  = file(self.stdin, 'r')
			stdout = file(self.stdout, 'a+')
			stderr = file(self.stderr, 'a+')
			os.dup2(stdin.fileno(),sys.stdin.fileno())
			os.dup2(stdout.fileno(),sys.stdout.fileno())
			os.dup2(stderr.fileno(),sys.stderr.fileno())
			# update pid file
			self.pid_lock.update_pid()
			return FORK_CHILD

if os.name in [ 'posix', 'os2' ]:
	guess_pid_file = _unix_guess_pid_file
	Daemon = UnixDaemon