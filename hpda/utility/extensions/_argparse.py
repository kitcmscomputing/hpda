#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.

# standard library imports
import argparse
from collections import Iterable as _abc_iterable

# third party imports

# application/library imports

class ExtendAction(argparse.Action):
	def __init__(self, *args, **kwargs):
		argparse.Action.__init__(self, *args, **kwargs)
		self.default = self._extend([], self.default)
		self._defined = []

	def __call__(self, parser, namespace, values, option_string=None):
		if id(namespace) not in self._defined:
			setattr(namespace, self.dest, [])
			self._defined.append(id(namespace))
		self._extend(getattr(namespace, self.dest), values)

	@staticmethod
	def _extend(target, value):
		if isinstance(value, _abc_iterable) and not isinstance(value, basestring):
			target.extend(value)
		else:
			target.append(value)
		return target

argparse.ExtendAction = ExtendAction