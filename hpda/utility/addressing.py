#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
module addressing

Convencience tools for validation, manipulation, storage and usage of common
addresses. All address types implement a string representation evaluating to the
most commonly supported format for direct use in tools.

Address construction provides input validation in so far as any input is safely
evaluated for standard compliance, which is incompatible with standard malicious
expressions. No guarantee is given that the address represents a valid machine.
"""

# standard library imports
import re

# third party imports

# application/library imports

class HostnameAddress(object):
	"""
	Pseudo-address as a resolvable hostname

	Hostnames must be given as strings matching RFC 1123, i.e. dotted sequences
	of alphanumeric labels containing hyphens. As per RFC 1035, any label is
	limited to 63 chars and the hostname is limited to 253 chars (plus a single
	leading/trailing dot).

	:param address: Hostname as string
	:type address: str or unicode
	:raise ValueError: On any input that cannot be digested
	"""
	_hostname_regex=re.compile("^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$")
	def __init__(self, address):
		if not isinstance(address, basestring):
			raise ValueError
		rawname=address.strip(".")
		if not self._hostname_regex.match(rawname):
			raise ValueError("Hostname must be a dotted sequence of alphanumeric labels, possibly containing hyphens")
		if len(rawname) > 253:
			raise ValueError("Hostname is too long")
		self._address = rawname.split(".")

	# representations
	def __repr__(self):
		return "%s(%s)" % (self.__class__.__name__, self)
	def __str__(self):
		return ".".join(self._address)

	# comparison
	def __eq__(self, other):
		if self._address == other._address:
			return True
		return False
	def __ne__(self, other):
		return not self.__eq__(other)

	# views
	def __len__(self):
		return len(self._address)

	def __iter__(self):
		return self._address.__iter__()

	def __getitem__(self, item):
		return self._address[item]

class IPV4Address(object):
	"""
	Address of the IPV4 specification

	IPV4 may be given as string (``"127.4.0.1"``), as integer (``2106101101``), or as
	tuple (``(127,4,0,1)``) of integers.

	An address is always represented as string (``"127.4.0.1"``).

	:param address: Address as string, integer or tuple representation
	:type address: int or str or unicode or list[int] or list[basestring]
	:raise ValueError: On any input that cannot be digested
	"""
	def __init__(self, address):
		try:
			if isinstance(address, basestring): # "127.4.0.1"
				self._address = tuple(int(bit,0) for bit in address.split("."))
			elif isinstance(address, int):   # 2106101101
				self._address = (address/(255**3),address/(255**2)%255,address/255%255,address%255)
			else:                          # (127,4,0,1)
				self._address = tuple(int(address[idx]) for idx in xrange(len(address)))
		except (ValueError, TypeError):
			raise ValueError("Input '%s' is not a valid address representation" % str(address))
		if len(self._address)!=4:
			raise ValueError("Input '%s' (%s) must contain 4 sections" % (str(address), self._address))
		if any(byte<0 or byte>255 for byte in self._address):
			raise ValueError("Input '%s' (%s) section(s) out of range" % (str(address), self._address))

	# representations
	def __repr__(self):
		return "%s(%s)" % (self.__class__.__name__, self)
	def __str__(self):
		return "%d.%d.%d.%d"%self._address

	# comparison
	def __eq__(self, other):
		if self._address == other._address:
			return True
		return False
	def __ne__(self, other):
		return not self.__eq__(other)

	# views
	def __len__(self):
		return 4

	def __iter__(self):
		return iter(self._address)

	def __getitem__(self, item):
		return self._address[item]

class IPV6Address(object):
	"""
	Address of the IPV6 specification

	IPV6 may be given as string (``"2001:0db8:85a3:0000:0000:8a2e:0370:7334"``) or
	tuple of strings (``("2001","0db8","85a3","0000","0000","8a2e","0370","7334")``)
	in hexadecimal notation or as tuple of integers in decimal notation
	(``(8193, 3512, 34211, 0, 0, 35374, 880, 29492)``). If constructed from a string,
	any surrounding brackets ("[]") are removed.

	An address is by default represented as string without surrounding brakets
	(``"2001:0db8:85a3:0000:0000:8a2e:0370:7334"``).

	:param address: Address as string, integer or tuple representation
	:type address: str or unicode or list[int] or list[basestring]
	:raise ValueError: On any input that cannot be digested
	"""
	def __init__(self, address):
		try:
			if isinstance(address, basestring): # "2001:0db8:85a3:0000:0000:8a2e:0370:7334"
				self._address = tuple(int(bit,16) for bit in address.strip("[]").split(":"))
			else:
				if isinstance(address[0], basestring):
					self._address = tuple(int(bit.strip("[]"),16) for bit in address if bit != "[" and bit != "]")
				else:
					self._address = tuple(int(bit) for bit in address)
		except (ValueError, TypeError):
			raise ValueError("Input '%s' is not a valid address representation" % address)
		if len(self._address)!=8:
			raise ValueError("Input '%s' must contain 8 sections" % address)
		if any(byte<0 or byte>65535 for byte in self._address):
			raise ValueError("Input '%s' section(s) out of range" % address)

	# representations
	def __repr__(self):
		return "%s(%s)" % (self.__class__.__name__, self)

	def __str__(self):
		return "%04x:%04x:%04x:%04x:%04x:%04x:%04x:%04x"%self._address

	# comparison
	def __eq__(self, other):
		if self._address == other._address:
			return True
		return False
	def __ne__(self, other):
		return not self.__eq__(other)

	# views
	def __len__(self):
		return 8

	def __iter__(self):
		return iter(self._address)

	def __getitem__(self, item):
		return self._address[item]

class SocketAddress(object):
	"""
	Combination of IP address/hostname and port for use in URLs

	:param host: a Hostname, IPV4 or IPV6 address or representation identifying the target host
	:param port: port to address on the host
	:type port: int
	:raise ValueError: On any input that cannot be digested
	"""
	def __init__(self, host, port):
		for address_type in (IPV6Address, IPV4Address, HostnameAddress):
			if isinstance(host, address_type):
				self.host = host
				break
			try:
				self.host = address_type(host)
				break
			except (ValueError, TypeError):
				pass
		else:
			raise ValueError("Host address '%s' invalid" % host)
		self.port = int(port)
		if self.port<0 or self.port > 65535:
			raise ValueError("Port out of range (0-65535)")

	# representations
	def __repr__(self):
		return "%s(%s,%s)" % (self.__class__.__name__, self.host, self.port)
	def __str__(self):
		if isinstance(self.host, IPV6Address):
			return "[%s]:%d" % (self.host, self.port)
		return "%s:%d" % (self.host, self.port)

	# comparison
	def __eq__(self, other):
		if self.port == other.port and self.host == other._address:
			return True
		return False
	def __ne__(self, other):
		return not self.__eq__(other)

class SocketURL(object):
	"""
	Socket based URL

	Represents a URL in the form
	``<protocol>://<host>:<port>``
	for use in connections.

	:param host: a Hostname, IPV4 or IPV6 address or representation identifying the target host
	:param port: port to address on the host
	:type port: int
	:param protocol: URL protocol, such as ``http``, ``https``, ``ftp`` etc
	:type protocol: str
	:raise ValueError: On any input that cannot be digested
	"""
	def __init__(self, host, port, protocol="http"):
		self.protocol = protocol
		if protocol.endswith("://"):
			self.protocol = self.protocol[:-3]
		if self.protocol not in ["http","https"]:
			raise ValueError("Unknown protocol '%s'"%self.protocol)
		self._socket_address = SocketAddress(host, port)

	@property
	def host(self):
		return self._socket_address.host
	@property
	def port(self):
		return self._socket_address.port

	# representation
	def __repr__(self):
		return "%s(%s,%s,%s)" % (self.__class__.__name__, self.host, self.port, self.protocol)
	def __str__(self):
		return "%s://%s" %(self.protocol, self._socket_address)

	# comparison
	def __eq__(self, other):
		if self.protocol == other.protocol and self._socket_address == other._socket_address:
			return True
		return False
	def __ne__(self, other):
		return not self.__eq__(other)

	@classmethod
	def from_str(cls, socket_url):
		"""
		Construct an instance from a string representation

		:param socket_url: String representation of a SocketURL
		:type socket_url: str or unicode

		:rtype: SocketURL

		:raise ValueError: On any input that cannot be digested
		"""
		protocol, _, address = socket_url.partition("://")
		host, _, port = address.rpartition(":")
		return cls(host, port, protocol)