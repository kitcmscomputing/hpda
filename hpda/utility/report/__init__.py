#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.

# standard library imports
import sys
import logging

# third party imports

# application/library imports
from .interface import log, LVL, disable
from .formatters import TerminalFormatter
from .handlers import LogRotateHandler

__all__ = ["log", "LVL", "disable", "updateParser", "argparse_init"]

_date_fmt = '%y%m%d-%H:%M:%S'
_dflt_fmt = "[%(asctime)s] %(levelno)d:%(name)-20s >> %(message)s"

def updateParser(argumentParser):
	"""Update an argparse ArgumentParser with module specific entries"""
	report_parser = argumentParser.add_argument_group(title="report arguments")
	report_parser.add_argument(
		'--log-output',
		nargs   = '*',
		default = ['stream:stderr'],
		help    = 'Destination for initialization reports',
		)
	report_parser.add_argument(
		'--log-level',
		default = 'CRITICAL',
		choices = ['DISABLED','CRITICAL', 'ERROR', 'WARNING', 'STATUS', 'INFO', 'DEBUG'],
		help    = 'Loglevel for initialization reports',
		)



def argparse_init(argparseNamespace):
	"""Apply a basic configuration from argparse for initialization"""
	initHandlers = []
	for output in argparseNamespace.log_output:
		if output.startswith('stream:'):
			stream = output.split(':')[1]
			if stream not in ['stdout', 'stderr']:
				raise ValueError("Expected 'stream:stderr' or 'stream:stdout'")
			handler = logging.StreamHandler(getattr(sys, stream))
			handler.setFormatter(
					TerminalFormatter(
						fmt = _dflt_fmt,
						datefmt = _date_fmt,
						max_width = -1,
						)
				)
			initHandlers.append(handler)
		elif output.startswith('file:'):
			filename = output.split(':')[1]
			handler = LogRotateHandler(filename = filename, max_size = 50*1000, max_count = 5)
			handler.setFormatter(
				logging.Formatter(
					fmt = _dflt_fmt,
					datefmt = _date_fmt,
					)
			)
			initHandlers.append(handler)
		else:
			raise ValueError("Expected 'stream:stderr', 'stream:stdout' or 'file:<filename>]'")
	logging.getLogger().handlers = initHandlers
	logging.getLogger().setLevel(getattr(LVL, argparseNamespace.log_level))

