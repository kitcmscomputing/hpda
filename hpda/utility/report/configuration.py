#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.

# standard library imports
import sys
import logging

# third party imports

# application/library imports
from .formatters               import TerminalFormatter, LogFileFormatter
from .handlers                 import LogRotateHandler, BufferHandler
from .interface                import LVL
from ..configuration.interface import Section, Choice, Group, CfgStr, CfgBytes, CfgInt, CfgBool, CfgSeconds, CfgPerms, CfgStrArray

_date_fmt = '%y%m%d-%H:%M:%S'

_handlers = {}

# expanded constructors
def stream_handler_factory(**kwargs):
	handler_name = kwargs.pop("handler")
	logger = kwargs.pop("logger")
	mlevel = getattr(LVL,kwargs.pop("minimum_level"))
	handler = logging.StreamHandler(getattr(sys, kwargs["destination"]))
	handler.setFormatter(
		TerminalFormatter(
			datefmt = _date_fmt,
			max_width = 9999,
			)
	)
	handler.setLevel(mlevel)
	link_handler(logger, handler)
	_handlers[handler_name] = handler
	return handler

def file_handler_factory(**kwargs):
	handler_name = kwargs.pop("handler")
	logger = kwargs.pop("logger")
	mlevel = getattr(LVL,kwargs.pop("minimum_level"))
	destination = kwargs.pop("destination")
	handler = LogRotateHandler(destination, **kwargs)
	handler.setFormatter(
		LogFileFormatter(
			datefmt = _date_fmt,
			)
	)
	handler.setLevel(mlevel)
	link_handler(logger, handler)
	_handlers[handler_name] = handler
	return handler

def buffer_handler_factory(**kwargs):
	handler_name = kwargs.pop("handler")
	logger = kwargs.pop("logger")
	targets = [_handlers[hname] for hname in kwargs.pop("targets")]
	# unlink targets from loggers
	if kwargs.pop("unlink_targets", True):
		for target in targets:
			for logger_name in getattr(target, "logger_names", ()):
				try:
					logging.getLogger(logger_name).handlers.remove(target)
				except IndexError:
					pass
	flush_level = getattr(LVL,kwargs.pop("flush_level"))
	handler = BufferHandler(flush_level=flush_level, targets=targets, **kwargs)
	link_handler(logger, handler)
	_handlers[handler_name] = handler
	return handler


def link_handler(logger, handler):
	"""Link a handler to its appropriate logger"""
	parents = getattr(handler, "logger_names",[])
	parents.append(logger)
	setattr(handler, "logger_names", parents)
	logging.getLogger(logger).addHandler(handler)

def logger_factory(**kwargs):
	logger = logging.getLogger(kwargs.pop("logger"))
	logger.handlers = []
	lvl = kwargs.pop("minimum_level")
	logger.setLevel(getattr(LVL, lvl))
	logger.propagate = kwargs.pop("propagate")
	return logger

# Configuration Interfaces
StreamHandlerConfig = Section(
	target=stream_handler_factory,
	attributes={
		"logger"       : CfgStr(default="", descr="logger name(s) to get messages from"),
		"destination"  : CfgStr(default="stderr", descr="sys stream to direct output to"),
		"minimum_level": CfgStr(default="NOTSET", descr="Minimum required level of messages", choices=LVL.levels),
	},
	descr="Redirection of logging output to a stream, i.e. :py:class:`~sys.stdout` or :py:class:`~sys.stderr`.",
)
FileHandlerConfig = Section(
	target=file_handler_factory,
	attributes={
		"logger"       : CfgStr(default="", descr="logger name(s) to get messages from"),
		"destination"  : CfgStr(descr="name of file to direct output to"),
		"minimum_level": CfgStr(default="NOTSET", descr="Minimum required level of messages", choices=LVL.levels),
		"max_size"     : CfgBytes(default=-1, descr="Maximum file size as byte size; use -1 for no limit"),
		"max_age"      : CfgSeconds(default=-1, descr="Maximum file age; use -1 for no limit"),
		"max_count"    : CfgInt(default=3, descr="Maximum number of files to use"),
		"permissions"  : CfgPerms(default="rw-r--r--", descr="Permissions of log file"),
	},
	descr="Redirection of logging output to a file.",
)
BufferHandlerConfig = Section(
	target=buffer_handler_factory,
	attributes={
		"logger"       : CfgStr(default="", descr="logger name(s) to get messages from"),
		"targets"      : CfgStrArray(descr="Name of handler(s) to buffer"),
		"capacity"     : CfgInt(default=32, descr="Number of records to buffer at most"),
		"flush_level"  : CfgStr(default="CRITICAL", descr="Minimum required level of messages to flush"),
		"flush_full"   : CfgBool(default=True, descr="Flush buffer when full"),
		"flush_close"  : CfgBool(default=True, descr="Flush buffer when closing"),
		"unlink_targets" : CfgBool(default=True, descr="Remove original sources from targets"),
	},
	descr="Buffer for other handlers, delaying and clustering output of records.",
)
AnyHandlerConfig = Choice(
	sections={
		"stream" : StreamHandlerConfig,
		"file"   : FileHandlerConfig,
		"buffer" : BufferHandlerConfig,
	},
	type_default="file",
)

HandlerConfig = Group(
	target=AnyHandlerConfig,
	name="handler",
	section_kw="handler",
	descr="The actual output targets for logging. They must refer to loggers used in the application in order to receive any logging messages.",
	order=["stream","file","buffer"]
)

LoggerConfig = Group(
	target=Section(
		target=logger_factory,
		attributes={
			"minimum_level": CfgStr(default="NOTSET", descr="Minimum required level of messages", choices=LVL.levels),
			"propagate"    : CfgBool(default=True, descr="Propagate to all lower loggers"),
		},
	),
	name="logger",
	section_kw="logger",
	descr="Internal aggregation pipelines for logging messages. Each section's nickname is used as the name the logger configured. The root logger is always defined."
)
