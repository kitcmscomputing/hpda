#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.

# standard library imports
import os
import time
import logging, logging.handlers
import errno
import collections
from threading import Lock

# third party imports

# application/library imports
from hpda.utility.pidlock import PIDLock
from hpda.utility.utils import ensure_dir, FlatList
from hpda.utility.configuration.values import permission

class LogRotateHandler(logging.handlers.BaseRotatingHandler):
	"""
	Handler for logging to a sequence of files, rotating when either a time or
	sizelimit is reached.

	This handler works similar to the logrotate utility. When writing, the age
	and size of the log file is checked. If either exceeds the limit, a new log
	file is created. A number of old files can be kept, which are deleted
	sequentially when new logs are added.

	:param filename: the (pseudo-) file to which messages are logged.
	:type filename: str|unicde
	:param max_size: maximum log file size in bytes
	:type max_size: int|float
	:param max_age: maximum log file age in seconds
	:type max_age: int|float
	:param max_count: maximum number of log files to keep
	:type max_count: int
	:param encoding: encoding to use for writing
	:type encoding: str|unicode
	:param delay: delay opening files until first write
	:type delay: bool
	:param permissions: permissions to apply to the log files
	:type permissions: int

	:note: Since only the owner of a file (or root) may set file permissions,
	       special handling is used when the process is not the file owner. If
	       the file can be used for writting and the real permissions match the
	       desired ones, this is assumed to be on purpose. In any other case,
	       an AssertionError is raised.
	"""
	def __init__(self, filename, max_size = -1, max_age = -1, max_count = 3, encoding = None, delay = False, permissions="rw-r--r--"):
		ensure_dir(os.path.dirname(filename), 'logging directory')
		self.perms = permission(permissions)
		logging.handlers.BaseRotatingHandler.__init__(
			self,
			filename = filename,
			mode = 'a',
			encoding = encoding,
			delay = delay
			)
		self.delay     = delay # set explicitly for bugged Py2.6 logging.FileHandler
		self.maxBytes  = max_size
		self.maxAge    = max_age
		self.maxCount  = max_count
		self.timestamp = time.time()
		if os.path.exists(filename):
			self.timestamp = os.stat(filename).st_mtime
		self._log_lock = Lock() # logging already locks - don't need this but cleaner interface
		# only use process lock if required
		if "---rw----" in self.perms or "------rw-" in  self.perms:
			self._log_lock = PIDLock(lock_name="%s.lock"%self.baseFilename, internal_lock=self._log_lock)

	def shouldRollover(self, record):
		"""Test if writing would require rotating the log file first"""
		if self.stream is None:                      # is delay in effect?
			self.stream = self._open()
		if self.maxBytes > 0:                        # is size limited?
			msg = "%s\n" % self.format(record)
			self.stream.seek(0,2)                    # from RotatingHandler
			if self.stream.tell() + len(msg) > self.maxBytes:
				return True
		if self.maxAge > 0:                          # is age limited?
			if time.time() - self.timestamp > self.maxAge:
				return True
		return 0

	def doRollover(self):
		"""Rotate the collection of log files and open a new one"""
		release = False
		try:
			release = self._log_lock.acquire(False)
			if release:
				self._doRollover()
			else:
				if self.stream:
					self.stream.close()
					self.stream = None
		finally:
			# if we did the rollover, release the lock to show we're done
			if release:
				self._log_lock.release()
			# get the lock before proceeding to ensure rollover is complete
			with self._log_lock:
				if self.stream is None and not self.delay:
					self.stream = self._open()

	def _doRollover(self):
		if self.stream:                              # disable active stream
			logging.FileHandler.emit(self,logging.makeLogRecord({
			"name":"handler",
			"level":50,
			"pathname":os.path.realpath(__file__),
			"lineno":106,            # keep this up to date when changing source
			"msg":"Closing file, size %dB/%dB, write age %ds/%ds.%s",
			"args":(
				os.path.getsize(self.baseFilename),
				self.maxBytes,
				time.time() - self.timestamp,
				self.maxAge,
				" Opening new file..." if self.maxCount > 0 else ""
			),
			"exc_info":None,
			"func":"_doRollover",
			}))
			self.stream.close()
			self.stream = None
		if self.maxCount > 0:                        # backups allowed, rotate
			for ind in xrange(self.maxCount, 1, -1): # rotate backups
				newLogName = '%s.%s' % (self.baseFilename, ind)
				oldLogName = '%s.%s' % (self.baseFilename, ind - 1)
				if os.path.exists(oldLogName):       # avoid collision
					if os.path.isfile(newLogName) or os.path.islink(newLogName):
						os.unlink(newLogName)
					os.rename(oldLogName, newLogName)
			newLogName = self.baseFilename + '.1'    # rotate current
			oldLogName = self.baseFilename
			if os.path.exists(oldLogName):
					if os.path.isfile(newLogName) or os.path.islink(newLogName):
						os.unlink(newLogName)
					os.rename(oldLogName, newLogName)
		else:                                        # backups disabled, clean up
			if os.path.isfile(self.baseFilename) or os.path.islink(self.baseFilename):
				os.unlink(self.baseFilename)
		if not self.delay:
			self.stream = self._open()

	def _open(self):
		"""Open the current base file and apply the proper permissions"""
		stream = logging.handlers.BaseRotatingHandler._open(self)
		try:
			os.chmod(self.baseFilename, self.perms)
		except OSError as err:
			if err.errno != errno.EPERM:
				raise
			if permission(os.stat(self.baseFilename).st_mode) != self.perms:
				raise ValueError("Unowned log files must match permissions or allow changing mode.")
		self.timestamp = time.time()
		return stream


class BufferHandler(logging.Handler):
	"""
	Handler buffering records for later flushing

	This acts similar to :py:class:`logging.handlers.MemoryHandler` but uses a
	more generic buffering mechanism. By default, new logging records are
	buffered in memory. If a logging record is received whose logging level is
	equal or greate than ``flush_level``, all buffered records are emitted. If
	``flush_full`` is :py:class:`True`, the buffered records are also emitted
	every time the buffer is full.

	If ``flush_full`` is :py:class:`False`, the buffer will discard old records
	when it is full. Effectively, this means that it will emit the last ``capacity``
	records when receiving a record matching ``flush_level``.

	An arbitrary number of ``targets`` may be specified.

	:param capacity: number of records to buffer
	:type capacity: int
	:param flush_level: minimum level of new records to trigger flushing
	:type flush_level: int
	:param flush_full: whether to flush when the buffer is full
	:type flush_full: bool
	:param flush_close: whether to flush when the buffer is closed
	:type flush_close: bool
	:param targets: handlers to flush to
	:type targets: list[:py:class:`logging.Handler`]
	"""
	def __init__(self, capacity=32, flush_level=logging.CRITICAL, flush_full=True, flush_close=True, targets=()):
		logging.Handler.__init__(self)
		self.capacity = capacity
		self.buffer = collections.deque(maxlen=capacity)
		self.flush_level = flush_level
		self.flush_full  = flush_full
		self.flush_close = flush_close
		self.targets = FlatList(targets)

	def should_flush(self, record):
		"""
		Check for buffer full or a record at the flushLevel or higher.
		"""
		if self.flush_full and len(self.buffer) >= self.capacity:
			return True
		return record.levelno >= self.flush_level

	def emit(self, record):
		self.buffer.append(record)
		if self.should_flush(record):
			self.flush()

	def flush(self):
		with self.lock:
			for target in self.targets:
				for record in self.buffer:
					target.handle(record)
			self.buffer.clear()

	def close(self):
		with self.lock:
			if self.flush_close:
				self.flush()
			self.targets = []
			logging.Handler.close(self)