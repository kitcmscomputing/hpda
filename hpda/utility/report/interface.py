#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.

# standard library imports
import logging

# third party imports

# application/library imports

class LVL(object):
	DISABLED = logging.CRITICAL + 100
	CRITICAL = logging.CRITICAL
	ERROR    = logging.ERROR
	WARNING  = logging.WARNING
	STATUS   = (logging.WARNING + logging.INFO) / 2
	INFO     = logging.INFO
	DEBUG    = logging.DEBUG
	NOTSET   = logging.NOTSET
	levels = ["DISABLED","CRITICAL","ERROR","WARNING","STATUS","INFO","DEBUG","NOTSET"]

def log(scope, level, msg, *args, **kwArgs):
	extra = kwArgs.get('extra', {})
	logging.getLogger(scope).log(level, msg, *args, exc_info = kwArgs.get('exc_info'), extra = extra)

def disable(level=LVL.DISABLED):
	"""
	Disable all logging at or below specific level

	:param level: maximum supressed log level
	:type level: int
	"""
	logging.disable(level)
