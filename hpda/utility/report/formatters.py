#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.

# standard library imports
import re
import logging

# third party imports

# application/library imports
from .interface import LVL
from ..caching import lru_cache
from .. import textmode

class RecordFormatter(logging.Formatter):
	"""
	Formatter capable of applying advanced formats depending on record features

	:type record_fmt_mapper: Function that maps record attributes to format strings
	"""
	def __init__(self, fmt=None, datefmt=None, record_fmt_mapper = None):
		logging.Formatter.__init__(self, fmt = fmt, datefmt = datefmt)
		self.record_fmt_mapper = record_fmt_mapper or self.record_fmt_mapper

	def format(self, record):
		# See logging.Formatter
		record.message = record.getMessage()
		record.asctime = self.formatTime(record, self.datefmt)
		fmt =  self.record_fmt_mapper(record) or self._fmt
		s = fmt % record.__dict__
		if record.exc_info:
			if not record.exc_text:
				record.exc_text = self.formatException(record.exc_info)
		if record.exc_text:
			if s[-1:] != "\n":
				s += "\n"
			try:
				s += record.exc_text
			except UnicodeError:
				s += record.exc_text.decode(sys.getfilesystemencoding(), 'replace')
		return s

	def record_fmt_mapper(self, record):
		section = loggerSectionFmt(record.name)
		if record.levelno >= LVL.CRITICAL:
			return '[%(asctime)s] /!\ ' + section + ': %(message)s'
		if record.levelno >= LVL.ERROR:
			return '[%(asctime)s] <!> ' + section + ': %(message)s'
		if record.levelno >= LVL.WARNING:
			return '[%(asctime)s]  !  ' + section + ': %(message)s'
		if record.levelno >= LVL.STATUS:
			return '[%(asctime)s]  =  ' + section + ': %(message)s'
		if record.levelno >= LVL.INFO:
			return '[%(asctime)s]  ?  ' + section + ': %(message)s'
		return     '[%(asctime)s]  .  ' + section + ': %(message)s'


class TerminalFormatter(RecordFormatter):
	"""
	Formatter for creating output for terminals

	This Formatter will automatically limit the width of statements to a fixed length. In addition, automatic colorization depending on the severity level can be applied.

	:param colorize: automatically colorize messages according to their severity
	:type colorize: bool
	:param max_width: limit message length
	:type max_width: int
	"""
	_ansiRE = re.compile("\003\[\d+m")
	def __init__(self, fmt=None, datefmt=None, record_fmt_mapper = None, colorize = True, max_width = -1):
		"""
		_Optional:_____________________|type____________________|default________
		> setWidth                      integer                  -1
		  Limit output width. If 0, guess width. If -1, apply no limit.
		> colorize                      bool                     True
		  Color aoutput according to severity level.
		"""
		RecordFormatter.__init__(
			self,
			fmt = fmt, datefmt = datefmt,
			record_fmt_mapper = record_fmt_mapper
			)
		self._widthlimit = max_width
		self.colorize = colorize

	def format(self, record):
		def resize(out):
			if self._widthlimit > 0:
				ansilen  = out.count('\x1b')
				printlen = len(out) - ansilen
				if printlen > self._widthlimit:
					return out[:(self._widthlimit+ansilen*3-3)]+'...'
			return out
		s = RecordFormatter.format(self, record)
		s.replace('\t','  ')
		if self.colorize:
			return self.colorizer(record) + resize(s) + textmode.MODE.RESET
		return resize(s)

	def colorizer(self, record):
		if record.name.upper().startswith('EXCEPT'):
			return ''
		if record.levelno >= LVL.CRITICAL:
			return textmode.MODE.BOLD + textmode.MODE.ITALIC + textmode.TONE.RED
		if record.levelno >= LVL.ERROR:
			return textmode.MODE.ITALIC + textmode.TONE.RED
		if record.levelno >= LVL.WARNING:
			return textmode.MODE.BOLD + textmode.TONE.ORANGE
		if record.levelno >= LVL.STATUS:
			return textmode.MODE.BOLD + textmode.TONE.GREEN
		if record.levelno >= LVL.INFO:
			return textmode.TONE.BLUE
		return ''


class LogFileFormatter(RecordFormatter):
	"""
	Formatter for creating log file output

	This formatter filters ANSI escape sequences from messages, creating plain ASCII log files.

	:param strip_ansi: remove all ANSI text modifiers from messages
	:param strip_ansi: bool
	"""
	_ansiRE = re.compile("\x1b\[\d+m")
	def __init__(self, fmt=None, datefmt=None, record_fmt_mapper = None, strip_ansi = True):
		RecordFormatter.__init__(
			self,
			fmt = fmt, datefmt = datefmt,
			record_fmt_mapper = record_fmt_mapper
			)
		self._stripAnsi = strip_ansi

	def format(self, record):
		s = RecordFormatter.format(self, record)
		if self._stripAnsi:
			return self._ansiRE.sub('', s)
		return s


@lru_cache(16)
def loggerSectionFmt(name = "NOTSET"):
	"""Helper for neatly showing logger sections"""
	name = name.partition('.')[0].lower()
	name = name[0].upper() + name[1:]
	return name.ljust(10, '_')