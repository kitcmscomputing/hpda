#!/usr/bin/env python
#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
The main executable for launching a node

The application may be stopped by sending either ``SIGKILL``, ``SIGTERM``,
``SIGQUIT`` or ``SIGINT``. ``SIGINT`` will always attempt a gracefull
shutdown, while ``SIGKILL`` is handled by the operating system. Both ``SIGTERM``
and ``SIGQUIT`` perform an escalating shutdown: First, a gracefull shutdown is
attempted. If the signal is received again, all threads are forcefully killed.
If receiving either signal a third time, the application exits ungracefully.
"""
from __future__ import print_function
# standard library imports
import sys
import os
import logging
import signal
import threading
import time

# dynamic package initialization
#   This is a hack to allow relative imports at the top-level
#   by effectively shifting the package level one up
if __name__ == "__main__":
	__package__ = os.path.basename(os.path.dirname(os.path.realpath(__file__)))
	# By convention, sys.path[0] is directory of the executable,
	# i.e. us if __name__ == "__main__". Set it to the actual import
	# location of our package root
	try:
		# For an archive, sys.path must point to the archive itself
		sys.path[0] = os.path.dirname(os.path.realpath(__file__))
		__import__(__package__) # try import for archive
	except ImportError:
		# For a package directory, sys.path must point to the directory
		# containing the package
		sys.path[0] = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
		__import__(__package__) # try import for package
import argparse

# third party imports

# application/library imports
from hpda.constants             import py_ver_max, py_ver_min, packageName, app_version, app_name
from hpda.utility.exceptions    import ExceptionFrame, FileNotFound, ValidationError
from hpda.utility.report        import LVL, argparse_init as report_argparseInit, updateParser as report_update_parser
from hpda.utility.report.configuration import HandlerConfig, LoggerConfig
from hpda.utility.utils         import Exit, get_config_files, version_id, repo_version
from hpda.utility.daemon        import Daemon, guess_pid_file
from hpda.utility.threads       import terminate_thread
from hpda.utility.configuration.container import Configuration
from hpda.common.node_master    import NodeMasterConfig

core_parser = argparse.ArgumentParser(description="Launch an instance of the %s cache" % packageName)
core_parser.add_argument('-c', '--config',
						nargs = '+',
						help  = 'Configuration file(s) to digest',
						default = [],
						)
core_parser.add_argument('--version',
						help  = 'Show the version number and exit',
						action= "store_true"
						)

core_parser.add_argument('--daemon',
						choices= ['start','stop','restart','status','terminate'],
						help   ='Perform operation as/on daemon',
						)
core_parser.add_argument('--init-database',
						 dest='init_database',
						 action='store_true',
						 default=False,
						 help="Create a new database if none exists for the coordinator",
						)


for update_parser in [ report_update_parser ]:
	update_parser(core_parser)

log = logging.getLogger().log

# counter for how often we have been requested to shut down
_kill_sigs = 0
def _set_signal_termination(node_master):
	"""
	Set termination on receiving signals
	:param node_master: Master of the application node
	:type node_master: :py:class:`~hpda.common.node_master.NodeMaster`
	"""
	term_sigs = [(signame, getattr(signal, signame)) for signame in ("SIGQUIT","SIGTERM") if hasattr(signal, signame)]
	def _sig_handler(signalnum, frame):
		global _kill_sigs
		_kill_sigs += 1
		log(LVL.CRITICAL, "Caught termination %d/3 times (signal %s)", _kill_sigs, signalnum)
		if _kill_sigs == 1:
			del frame
			log(LVL.CRITICAL, "Performing graceful termination")
			node_master.stop()
			log(LVL.CRITICAL, "Terminated successfully")
		elif _kill_sigs == 2:
			del frame
			log(LVL.CRITICAL, "Performing forceful termination")
			kill_threads = []
			for thread in threading.enumerate():
				if thread is not threading.current_thread().ident:
					terminate_thread(thread=thread)
					kill_threads.append(thread)
			log(LVL.CRITICAL, "Waiting for termination of %d threads", len(kill_threads))
			for thread in kill_threads:
				thread.join()
			log(LVL.CRITICAL, "Terminated successfully")
		else:
			log(LVL.CRITICAL, "Performing abrupt termination")
			os._exit(1)
	for signame, signum in term_sigs:
		signal.signal(signum, _sig_handler)
		log(LVL.STATUS, "Registered termination handler for signal %d (%s)", signum, signame)


def run_node():
	with ExceptionFrame():
		cli_args = core_parser.parse_args()
		report_argparseInit(cli_args)
		if cli_args.version == True:
			print(version_id(app_name, app_version, repo_version(repo_path=os.path.dirname(__file__))), file=sys.stdout)
			Exit.SUCCESS()
		# report daemon status before any initialisation and logging
		if cli_args.daemon == 'status':
			daemon = Daemon(pid_file=guess_pid_file("hpda"))
			daemon_pid = daemon.status()
			if daemon_pid is not None:
				print("Daemon is running", file=sys.stderr)
				print(daemon_pid, file=sys.stdout)
				Exit.SUCCESS()
			print("Daemon is not running", file=sys.stderr)
			print('', file=sys.stdout)
			Exit.GENERIC()
		# stop daemon before any initialisation/configuration
		if cli_args.daemon in ('stop', 'restart', 'terminate'):
			daemon = Daemon(pid_file=guess_pid_file("hpda"))
			daemon_pid = daemon.status()
			daemon_stop = (cli_args.daemon == 'terminate') and daemon.terminate or daemon.stop
			if daemon_pid is not None:
				log(LVL.STATUS, "Stopping daemon (PID %d, timeout %ds)...", daemon_pid, 600)
				if not daemon.stop(timeout=600):
					log(LVL.CRITICAL, "Failed stopping daemon (PID %d) in %ds", daemon_pid, 600)
					Exit.GENERIC()
				else:
					log(LVL.INFO, "Stopped daemon (PID %d)", daemon_pid)
			else:
				log(LVL.INFO, "Stopped daemon (not running)")
			if cli_args.daemon == 'stop':
				Exit.SUCCESS()
		# version warning
		if sys.version_info < py_ver_min or sys.version_info[0] > py_ver_max[0]:
			log(LVL.WARNING, "Built for Python version '%s' - '%s' (running on '%s')" % (','.join(py_ver_min), ','.join(py_ver_max), sys.version))
			log(LVL.WARNING, "The program may misbehave, even if you supply missing modules!")
		# TODO: purge explicit use of config_files
		config_files = cli_args.config
		try:
			if not config_files:
				config_files = get_config_files(name="hpda")
		except FileNotFound as err:
			log(LVL.CRITICAL, str(err))
			Exit.NOTFOUND()
		else:
			log(LVL.STATUS, "Reading configuration from %s", config_files)
		# module level configuration
		configuration = Configuration(*config_files)
		# report/logging
		LoggerConfig.apply(configuration)
		HandlerConfig.apply(configuration)
		# node level configuration
		master = NodeMasterConfig.apply(configuration)
		master.configure_components(configuration=configuration, init_databases=cli_args.init_database)
		_set_signal_termination(node_master=master)
		# main program to execute
		def payload():
			try:
				log(LVL.STATUS, "Starting node (PID %s)", os.getpid())
				master.main()
			except (KeyboardInterrupt, SystemExit) as err:
				log(LVL.STATUS, "Stopping node (received %s)", err)
				master.stop()
			log(LVL.STATUS, "Stopped node")
		if cli_args.daemon is None:
			payload()
		else:
			daemon = Daemon(payload=payload, pid_file=guess_pid_file("hpda"))
			if cli_args.daemon in ('start','restart'):
				log(LVL.STATUS, "Starting daemon")
				try:
					if not daemon.start():
						raise RuntimeError
					# need some time to have daemon fully running
					time.sleep(0.05)
					daemon_pid = daemon.status()
					if daemon_pid:
						log(LVL.STATUS, "Daemon is running (PID %d)", daemon_pid)
					else:
						log(LVL.STATUS, "Daemon is running (PID ???)")
					Exit.SUCCESS()
				except ValidationError:
					daemon_pid = daemon.status()
					if daemon_pid:
						log(LVL.STATUS, "Daemon is already running (PID %d)", daemon_pid)
					else:
						log(LVL.STATUS, "Daemon is already running (PID ???)")
				except RuntimeError:
					pass
				log(LVL.CRITICAL, "Failed starting daemon")
				Exit.GENERIC()


if __name__ == "__main__":
	run_node()