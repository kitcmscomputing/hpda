#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
Utilities for working with HTCondor **ClassAd**\s. The **Class**\ ified **Ad**\ vertisement
is HTCondor's method of describing entities and their relations; the Old ClassAd
specification (as used in Condor 8.0 and previously) includes:

**ClassAd container**
  An unnested mapping of keys to values. Keys are case-insensitive strings while
  values may be either one of the ClassAd value types.

**ClassAd value types**
  Constant primitive data types (``string``, ``int``, ``float``, ``bool``), special
  types (``error``, ``undefined``) and expressions.

**ClassAd expression**
  Directives deriving values from other attributes and constants. This includes
  arithmetic (``3 * 4 + 2``, ``foo - 4``), comparison (``1 == 1.0``, ``foo >= 3``)
  and logical expression (``bar && False``), as well as functions (``round(1.3)``,
  ``stringListMember("qux","foo,bar,qux")``).

:see: For a complete description of the ClassAd language, see the HTCondor manual.

This module works with two representations: string literals as provided by the
HTCondor CLI tools and python objects. Unless otherwise noted, native data types
are used, for example integers use the plain python ``int`` type. The ``ClassAdParser``
object allow transforming from one representation to the other.

========= ======
ClassAd   native
========= ======
Integer   ``int``
TRUE      ``True``
FALSE     ``False``
Real      ``float``
String    ``str``
Undefined ``None``
========= ======

Arithmetic operators (``*``, ``/``, ``+`` and ``-``) and comparison operators (``==``,
``!=``, ``<=``, ``>=``, ``<`` and  ``>``) can be used natively. The ClassAd operators
for identity (``=?=`` and ``=!=``) and logical operators (``&&`` and ``||``) are
represented by the functions ``IS``, ``ISNOT``, ``AND`` and ``OR``.

:note: Due to the breadth of the ClassAd language, this module actually uses *two*
       parsers: one for the ClassAd mapping and one for the ClassAd expressions.
       Explicit use of the later outside of this module is usually **not** required.
"""

# standard library imports

# third party imports

# application/library imports
from hpda.utility.utils import FlatList, NotSet


# Custom types
class StringList(list):
	"""
	A list of strings, delimeted by ``,``

	The constructor takes either a literal (``"foo,bar,baz"``) or a list of strings
	(``["foo","bar","baz"]``). Any input that is ``False`` in the boolean sense,
	most importantly the empty string ``""``, creates an empty list.

	:see: ``list`` for general features
	:note: this class does **not** perform sanitisation at the moment. It is the
	       user's responsibility not to add incorrect data types.
	"""
	def __init__(self, source=None):
		if source:
			if isinstance(source, basestring):
				list.__init__(self, source.split(','))
			else:
				list.__init__(self, source)
		else:
			list.__init__(self, [])

	def __str__(self):
		return '"%s"'%','.join(self)


class Error(object):
	"""ClassAd Error value"""
	def __str__(self):
		return "ERROR"


class Expr(unicode):
	"""
	Container for ClassAd generic Expression

	This is a catch-all container for anything that cannot be interpreted
	"""
	pass


class ClassAd(dict):
	"""
	Modified dict enforcing ClassAd indexing conventions

	Special dictionary with customized functionality. Keys must be strings, but
	are handled as case-insensitive.

	:see: :py:class:`dict` for constructor parameters

	:note: The ``get``, ``copy`` methods and dict protocol (``foo["bar"]`` etc.)
	       enforce ClassAd conventions, but otherwise work as regular.
	"""
	def __init__(self, *args, **kwargs):
		tmp_dict = dict(*args, **kwargs)
		dict.__init__(self, ((key.upper(), value) for key, value in tmp_dict.iteritems()))

	def get(self, key, default=None):
		""":see: :py:meth:`dict.get`"""
		try:
			return self[key]
		except KeyError:
			return default

	def copy(self):
		"""
		:see: :py:meth:`dict.copy`
		:rtype: :py:class:`~.ClassAd`
		"""
		return ClassAd(dict.copy(self))

	def update(self, other=NotSet, **kwargs):
		""":see: :py:meth:`dict.update`"""
		if other is NotSet:
			return
		if not isinstance(other, ClassAd):
			other = ClassAd(other)
		dict.update(self, other)
		if kwargs:
			dict.update(self, ClassAd(**kwargs))

	def __getitem__(self, item):
		try:
			return dict.__getitem__(self, item.upper())
		except AttributeError:
			raise TypeError("Keys must be str or unicode")
	def __setitem__(self, item, value):
		try:
			return dict.__setitem__(self, item.upper(), value)
		except AttributeError:
			raise TypeError("Keys must be str or unicode")
	def __delitem__(self, item):
		try:
			return dict.__delitem__(self, item.upper())
		except AttributeError:
			raise TypeError("Keys must be str or unicode")
	def __contains__(self, item):
		try:
			return dict.__contains__(self, item.upper())
		except AttributeError:
			raise TypeError("Keys must be str or unicode")
	def __repr__(self):
		try:
			return "%s(%s)"%(self.__class__.__name__, dict.__repr__(self))
		except AttributeError:
			raise TypeError("Keys must be str or unicode")
	def __str__(self):
		return classad_parser.unparse(self)


def load(classad_itr):
	"""
	Load a single ClassAd from a line-iterable

	:param classad_itr: ClassAd in standard representation
	:type classad_itr: list[str] or file

	:return: ClassAd in python representation
	:rtype: ClassAd
	"""
	# we don't actually iterate here, just return the first element *if* it exists
	for classad in classad_parser.parse(classad_itr):
		return classad
	return {}


class ClassAdParser(object):
	"""
	Converter for string and python representations of ClassAds

	:param separator_lines: lines separating ClassAds
	:type separator_lines: list[str] or list[callable]
	:param skip_lines: lines to skip
	:type separator_lines: list[str] or list[callable]

	:note: The content of both ``separator_lines`` and ``skip_lines`` is interpreted
	       first as callables **test**\ (line:\ **str**\ )->\ **bool**; if this is not a permitted operation,
	       string equality is tested as ``test==line``. If a test is neither  it
	       is silently skipped.
	"""
	# translation mapping ClassAd => python
	_constants = {
		"error"     : Error(),
		"undefined" : None,
		"true"      : True,
		"false"     : False,
	}
	def __init__(self, separator_lines=("",), skip_lines=()):
		self.separator_lines = FlatList(separator_lines)
		self.skip_lines = FlatList(skip_lines)
		self.expr_parser = ExpressionParser()

	def unparse(self, classad):
		"""
		Transform a mapping to a string representation

		:param classad: mapping to unparse
		:type classad: ClassAd or dict
		:return: string representation
		:rtype: str
		"""
		return "\n".join(self.unparse_iter(classad))

	def unparse_iter(self, classad):
		"""
		Generator version of ``unparse`` yielding individual lines

		This function is more memory efficient if the representation is not
		required entirely at once.
		"""
		for key in classad:
			yield "%s = %s" % (key, self.expr_parser._unparse_value_instance(classad[key]))

	def parse(self, representation):
		"""
		Transform a string representation to mappings

		:param representation: a ClassAd in string representation
		:type representation: str or list[str]
		:return: list[ClassAd]
		"""
		return list(self.parse_iter(representation))

	def parse_iter(self, representation):
		"""
		Generator version of ``parse`` yielding individual classads

		This function is more memory efficient if the representation is not
		required entirely at once.
		"""
		def add_attr():
			if key and value:
				current_ad[key] = self.expr_parser._parse_value_literal(value)
		if isinstance(representation, basestring):
			representation = representation.splitlines()
		current_ad = ClassAd()
		key, value = None, None
		for line in representation:
			line = line.strip()
			if self._is_sep_line(line):
				print "sep", line
				add_attr()
				if current_ad:
					yield current_ad
					current_ad, key, value = {}, None, None
				continue
			if self._is_skip_line(line):
				print "skip", line
				continue
			if "=" in line:
				add_attr()
				key, _, value = line.partition("=")
				key, value = key.strip(), value.strip()
			else:
				value += "\n" + line.strip()
		add_attr()
		if current_ad:
			yield current_ad


	def _is_skip_line(self, line):
		for test in self.skip_lines:
			try:
				if test(line):
					return True
			except TypeError:
				if test==line:
					return True
		return False

	def _is_sep_line(self, line):
		for test in self.skip_lines:
			try:
				if test(line):
					return True
			except TypeError:
				if test==line:
					return True
		return False


class ExpressionParser(object):
	"""
	Converter for string and python representations of ClassAd Expressions
	"""
	# translation mapping ClassAd => python
	_constants = {
		"error"     : Error(),
		"undefined" : None,
		"true"      : True,
		"false"     : False,
	}
	def _parse_value_literal(self, literal):
		"""
		Transform a literal to a ClassAd value

		:type literal: str|unicode
		:return: Error, None, Expr, bool, str, int, float
		"""
		if literal.startswith('"') and literal.endswith('"'):
			return literal[1:-1]
		for conversion in (lambda: self._constants[literal.lower()], lambda: int(literal), lambda: float(literal)):
			try:
				return conversion()
			except (ValueError, KeyError):
				pass
		return Expr(literal)

	def _unparse_value_instance(self, value):
		"""
		Inverse transformation of ``_parse_value_literal``

		:type value: Error, None, Expr, bool, str, int, float
		:rtype: str or unicode
		"""
		if value is None:
			return "UNDEFINED"
		if isinstance(value, basestring) and not isinstance(value, (Expr, Error)):
			return '"%s"' % value
		return str(value)

classad_parser = ClassAdParser()