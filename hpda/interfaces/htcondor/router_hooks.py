#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
Implementation of hooks for the HTCondor job router, as specified by the HTCv8
standard.

The job router defines four hook types:

**Translate**
  The entry point to the route, invoked when the job is first encountered. This
  hook is reponsible for configuring both the routed job and any external
  resources. The resulting state should be sufficient for the job to complete.

**Update**
  A regular intervention point for the route. This hook is responsible for
  handling changes in the job or resources.

**Finalize**
  The exit point of a job from the route. This is the final point at which the
  job itelf may be updated by the route.

**Cleanup**
  The exit point for the route, invoked after the job is gone from the route
  (with success or failure). This is hook is responsible for cleaning up any
  leftover or dangling resources from the job.

The hooks expect jobs to provide a list of files they wish to process. For this,
hooks are configured to look for a specific attribute in the job ClassAd. This
attribute may either contain a list of files (globally quoted, comma separated as
per ClassAd StringList standard) or point to a file that contains the list (unquoted,
new-line separated).

:note: The HTC JobRouter has a limit for the size of information read from hooks
       on both stdout and stderr. This is controlled by the HTCondor setting
       ``PIPE_BUFFER_MAX`` which defaults 102400 characters.

:note: Due to the JobRouter size limit, usage of plain file lists is **strongly**
       discouraged. This feature may be removed without advance notice.

:see: The HTCondor `ClassAd Attributes <http://research.cs.wisc.edu/htcondor/manual/current/12_Appendix_A.html>`_
      specifies types and units of available attributes.
"""
from __future__ import print_function

# standard library imports
import os
import sys
import time
import random
import logging

# third party imports

# application/library imports
from .classad import load, Expr, ClassAd, StringList
from hpda.utility.report     import LVL
from hpda.utility.report.configuration import HandlerConfig, LoggerConfig
from hpda.client.coordinator import Coordinator
from hpda.utility.exceptions import AbstractError
from hpda.utility.configuration.interface import Loader, Section, CfgStrArray, CfgStr, CfgBool, CfgSeconds
from hpda.client.locator import Locator


class HTCRouter(object):
	"""
	Handler for performing a route task

	This is a base class implementing the interface to HTC router hooks; the
	derived classes implement specific functionality for individual hook.

	:param locators: URLs of locators to query for file locations
	:type locators: list[str] or list[:py:class:`~utility.addressing.SocketURL`]
	:param collectors: URLs of collectors/coordinators to report to about jobs
	:type collectors: list[str] or list[:py:class:`~utility.addressing.SocketURL`]
	:param prefix: prefix to prepend to internally used classad keys
	:type prefix: str or unicode
	:param input_key: job attribute listing file for processsing
	:type input_key: str or unicode
	:param query_all: Query all available locators (instead of first available)
	:type query_all: bool
	:param enforce_local: wait this long before running on hosts without local files
	:type enforce_local: int or float
	:param update_delay: minimum delay between two updates
	:type update_delay: float or int
	:param hook_timeout: maximum time the hook is allowed to run
	:type hook_timeout: float or int
	:param hook_variance: range of random variance on query times
	:type hook_variance: int or float
	:param instream: file-like object to read input from (per HTC specs)
	:type instream: file

	:note: The **JobRouter** daemon will update *all* jobs it routes at once, which may
	       overload collector/locator APIs. A moderate ``update_delay`` may be used
	       in conjunction with a much higher **JobRouter** frequency to spread the
	       intense work load of updates.
	:note: The **JobRouter** daemon will ignore hooks that take too long. The limit
	       appears to be 10 seconds. ``hook_timeout`` should be set accordingly
	       if queries might time out otherwise.
	"""
	def __init__(self, locators=("http://localhost:8081",), collectors=("http://localhost:8082",), prefix="HPDA", input_key="INPUT_FILES", query_all=False, enforce_local=0, update_delay=60, hook_timeout=10, hook_variance=1, instream=sys.stdin, outstream=sys.stdout):
		self.locators   = locators
		self.collectors = collectors
		self.prefix     = prefix
		self.data_attr  = input_key
		self.enforce_local = enforce_local
		self.query_all  = query_all
		self.route, self.classad = self._digest_input(instream=instream)
		self.jobid = "%s.%s@%s"%(self.classad.get("ClusterId","<N/A>"),self.classad.get("ProcId","<N/A>"), self.__class__.__name__)
		self.outstream  = outstream
		self.update_delay, self._hook_timeout, self.hook_variance, self._stime = update_delay, hook_timeout, hook_variance, time.time()
		self.logger = logging.getLogger("hook.%s"%self.__class__.__name__)
		self.query_log = logging.getLogger("hook.messages")

	def log_message(self, recipients, message):
		"""
		Log a message sent to another entity in a query

		:param recipients: recipients that received the message
		:param message: the content of the query
		"""
		self.query_log.log(LVL.INFO, "%s: %s => %s", self.jobid, recipients, message)

	def execute(self):
		"""
		Perform the routing hook step

		:return: Whether the route was successfull
		:rtype: bool
		"""
		self.logger.log(LVL.STATUS, "%s: Handling job %s", self.jobid, self.classad.get("GlobalJobId","<unknown>"))
		time.sleep(random.uniform(0,self.hook_variance))
		print(self._get_route_classad(), file=self.outstream)
		return self._publish_information()

	def _publish_information(self):
		"""
		Forward status information to a collector

		:return: success
		"""
		return True

	def _get_route_classad(self):
		"""
		Get the ClassAd output for this route task

		:return: classad to provide to condor
		:rtype: dict
		"""
		return ClassAd()

	def _digest_input(self, instream=sys.stdin):
		"""
		Consume hook input

		:param instream: filelike object delivering input
		:type instream: file
		:return: route name and job classad, if provided
		:rtype: (str, dict)
		"""
		raise AbstractError

	def _read_data_files(self, source_file, test_existence=True):
		data_files = []
		if not source_file:
			self.logger.log(LVL.WARNING, "%s: Missing source_file %r", self.jobid, source_file)
			return []
		# TODO: remove legacy mode? - MF20150223
		try:
			# ZLib compressed list
			if source_file.startswith("Z:"):
				import zlib
				data_files = [data_file.strip() for data_file in zlib.decompress(source_file[2:]).split(",")]
			# HTCondor list
			elif "," in source_file:
				data_files = StringList(source_file)
			# Catalogue List
			else:
				with open(source_file,'r') as data_file_list:
					for data_file in data_file_list:
						data_file = data_file.strip()
						if not test_existence or os.path.exists(data_file):
							data_files.append(data_file)
		except IOError as err:
			self.logger.log(LVL.CRITICAL, "%s: Failed to read data files from %s: %s", self.jobid, source_file, err)
			return []
		self.logger.log(LVL.DEBUG, "%s: Read %d data files from %s", self.jobid, len(data_files), source_file)
		return data_files

	def _classad_key(self, raw_key):
		return (self.prefix + "_" + raw_key).upper()

	def _get_default_classad(self):
		return ClassAd(**{
		self._classad_key("route") : True,
		self._classad_key("last_update") : time.time()
		})

	def _get_host_rank_classad(self, input_files, current_worker=None):
		"""
		Create an HTC rank expression for loose best host criteria

		If ``current_worker`` is set (i.e. the job is running), the
		cache hit and locality rate on this node are added to the classad.

		:param input_files: files desired by the job
		:type input_files: list[str]
		:return: HTC rank expression
		:rtype: ClassAd
		"""
		new_attributes = ClassAd()
		machine_rank, worker_ranking, queried_urls = ["0"], {}, []
		for url in random.sample(self.locators, len(self.locators)):
			if time.time() - self._stime > self._hook_timeout + 2: # timeout with spare time for others
				break
			try:
				for worker in Locator(url, timeout=1).get_hosting_workers(input_files):
					worker_ranking[worker["name"]] = worker
				queried_urls.append(url)
				if not self.query_all:
					break
			except Exception as err:
				self.logger.log(LVL.CRITICAL, "%s: Locator %s - %s", self.jobid, url, err)
		self.logger.log(LVL.INFO, "%s: Querried locators %s out of %d for %d files, got %d hosts", self.jobid, queried_urls, len(self.locators), len(input_files), len(worker_ranking))
		new_attributes[self._classad_key("locators")] = StringList(queried_urls)
		if current_worker is not None:
			cachehit_rate, locality_rate = 0.0, 0.0
			if worker_ranking:
				cachehit_rate = 1.0 * max(worker_ranking[wkey]["files"] for wkey in worker_ranking) / len(input_files)
				if current_worker in worker_ranking:
					locality_rate = 1.0 * worker_ranking[current_worker]["files"] / len(input_files)
			new_attributes[self._classad_key("locality_rate")] = locality_rate
			new_attributes[self._classad_key("cachehit_rate")] = cachehit_rate
			self.logger.log(LVL.INFO, "%s: Job executing on '%s' with %.2f/%.2f hit rate (global/local)", self.jobid, current_worker, locality_rate, cachehit_rate)
		for worker in worker_ranking:
			machine_rank.append('(( machine == "%s" ) * %d)' % (worker, worker_ranking[worker]["files"]))
		new_attributes[self._classad_key("rank")] = Expr("+".join(machine_rank))
		return new_attributes


class HTCTranslate(HTCRouter):
	"""
	Handler for an HTC Route Translate

	*From the HTC documentation:*
	  The hook [..] is invoked when the Job Router has determined that a job meets
	  the definition for a route. This hook is responsible for doing the
	  transformation of the job and configuring any resources that are external to
	  HTCondor if applicable.

	*Standard input given to the hook*
	  The first line will be the route that the job matched as defined in
	  HTCondor's configuration files followed by the job ClassAd, separated by
	  the string ``"------"`` and a new line.

	*Expected standard output from the hook*
	  The transformed job.
	"""
	def _publish_information(self):
		publish_info = ClassAd()
		job_info = {
			"job_id"    : self.classad["GlobalJobId"],
			"job_owner" : self.classad["User"],
			"file_list" : self._read_data_files(self.classad.get(self.data_attr, "")),
		}
		if not job_info["file_list"]:
			self.logger.log(LVL.WARNING, "%s: Skipping publishing (empty file list)", self.jobid)
			return True
		informed_urls = []
		for url in self.collectors:
			if time.time() - self._stime > self._hook_timeout + 0.5: # timeout with spare time for others
				break
			try:
				Coordinator(url, timeout=1).job_add(**job_info)
				informed_urls.append(url)
			except Exception as err:
				self.logger.log(LVL.CRITICAL, "%s: Collector %s - %s", self.jobid, url, err)
		self.logger.log(LVL.INFO, "%s: Informed collectors %s out of %d", self.jobid, informed_urls, len(self.collectors))
		publish_info[self._classad_key("Collectors")] = StringList(informed_urls)
		print(publish_info, file=self.outstream)
		self.log_message(informed_urls, job_info)
		return True

	def _get_route_classad(self):
		# must return entire job classad
		update_classad = self.classad.copy()
		update_classad.update(self._get_default_classad())
		# only initialize - skip all queries to avoid JobRouter limit
		if self.enforce_local:
			if self.enforce_local > 0:
				self.logger.log(LVL.INFO, "%s: Setting host locality as Requirement for %d seconds", self.jobid, self.enforce_local)
				update_classad[self._classad_key("Requirements")] = Expr('( %s > 0 ) || (CurrentTime - QDate > %d)'%(self._classad_key("rank"), self.enforce_local))
			else:
				self.logger.log(LVL.INFO, "%s: Setting host locality as Requirement indefinitely", self.jobid)
				update_classad[self._classad_key("Requirements")] = Expr('%s > 0'%self._classad_key("rank"))
			update_classad["Requirements"] = Expr("(%s) && %s" % (update_classad.get("Requirements", True),self._classad_key("Requirements")))
		update_classad[self._classad_key("rank")] = Expr('0')
		update_classad["Rank"]=Expr("(%s) + %s" % (update_classad.get("Rank", True),self._classad_key("rank")))
		return update_classad

	def _digest_input(self, instream=sys.stdin):
		"""
		Consume hook input

		:param instream: filelike object delivering input
		:type instream: file
		:return: route name and job classad, if provided
		:rtype: (str, dict)
		"""
		# fast-forward route header, separated by "------\n"
		route = ""
		while True:
			newline = instream.readline()
			if newline.startswith("------"):
				break
			route += newline
		return route, load(instream)


class HTCUpdate(HTCRouter):
	"""
	Handler for an HTC Route Update

	*From the HTC documentation:*
	  The hook [...] is invoked to provide status on the specified routed job when
	  the Job Router polls the status of routed jobs [...].

	*Standard input given to the hook*
	  The routed job ClassAd that is to be updated

	*Expected standard output from the hook*
	  The job attributes to be updated in the routed job, or nothing, if there
	  was no update.
	"""
	def execute(self):
		"""
		:see: :py:meth:`~.HTCRouter.execute`
		"""
		update_delta = time.time() - self.classad.get(self._classad_key("last_update"),0)
		if update_delta < self.update_delay:
			self.logger.log(LVL.DEBUG, "%s: Skipping update (%ds < %ds)", self.jobid, update_delta, self.update_delay)
			return True
		return HTCRouter.execute(self)

	def _get_route_classad(self):
		update_classad = self._get_default_classad()
		data_files = self._read_data_files(self.classad.get(self.data_attr, ""))
		if not data_files:
			self.logger.log(LVL.DEBUG, "%s: Skipping update (empty file list)", self.jobid)
			update_classad[self._classad_key("locators")] = ""
			return update_classad
		try:
			worker=str(self.classad["RemoteHost"].rpartition("@")[2])
		except KeyError:
			update_classad.update(self._get_host_rank_classad(data_files))
		else:
			update_classad.update(self._get_host_rank_classad(data_files, current_worker=worker))
		return update_classad

	def _digest_input(self, instream=sys.stdin):
		"""
		Consume hook input

		:param instream: filelike object delivering input
		:type instream: file
		:return: route name and job classad, if provided
		:rtype: (str, dict)
		"""
		return None, load(instream)


class HTCFinalize(HTCRouter):
	"""
	Handler for an HTC Route Update

	*From the HTC documentation:*
	  The hook [...]  is invoked when the Job Router has found that the job has
	  completed. Any output from the hook is treated as an update to the source
	  job.

	*Standard input given to the hook*
	  The routed job ClassAd that is to be updated.

	*Expected standard output from the hook*
	  The job attributes to be updated in the routed job, or nothing, if there
	  was no update.
	"""
	def _publish_information(self):
		collectors = StringList(self.classad.get(self._classad_key("Collectors"), '""'))
		job_info = {
			"job_id" : self.classad["GlobalJobId"],
			"cpu_time"  : float(self.classad.get("RemoteUserCpu",0.0)+self.classad.get("RemoteSysCpu",0.0)),
			"wall_time" : (float(self.classad.get("CommittedTime",0.0))-float(self.classad.get("CommittedSuspensionTime",0.0))),
			"memory_usage" : float(self.classad.get("ResidentSetSize",0.0)*1024),
			"exit_code" : int(self.classad["ExitCode"]),
			"node" : str(self.classad.get("RemoteHost", self.classad["LastRemoteHost"]).rpartition("@")[2]),
			"locality_rate":float(self.classad.get(self._classad_key("locality_rate"),-1.0)),
			"cachehit_rate":float(self.classad.get(self._classad_key("cachehit_rate"),-1.0)),
		}
		# pass final information to collector
		informed_urls = []
		for url in collectors:
			if time.time() - self._stime > self._hook_timeout + 0.5: # timeout with spare time for others
				break
			try:
				Coordinator(url, timeout=1).job_report(**job_info)
				informed_urls.append(url)
			except Exception as err:
				self.logger.log(LVL.CRITICAL, "%s: Collector %s - %s", self.jobid, url, err)
		self.logger.log(LVL.INFO, "%s: Informed collectors %s out of %d", self.jobid, informed_urls, len(self.collectors))
		self.log_message(informed_urls, job_info)
		return True if not collectors else informed_urls


	def _digest_input(self, instream=sys.stdin):
		"""
		Consume hook input

		:param instream: filelike object delivering input
		:type instream: file
		:return: route name and job classad, if provided
		:rtype: (str, dict)
		"""
		# fast-forward original classad, separated by "------\n"
		while True:
			newline = instream.readline()
			if newline.startswith("------"):
				break
			# old_classad.append(newline)
		return None, load(instream)


# configuration loaders
# TODO: explicitly document these
_router_attributes={
	"locators"   : CfgStrArray(default="http://localhost:8081",descr="URLs of locators to query for file locations."),
	"collectors" : CfgStrArray(default="http://localhost:8081",descr="URLs of collectors/coordinators to report to about jobs."),
	"prefix"     : CfgStr(default="HPDA",descr="Prefix to expect/apply for ClassAd insertions' keys, e.g. ``'<prefix>_RANK'``."),
	"input_key"  : CfgStr(default="INPUT_FILES",descr="Key to the job ClassAd attribute listing input files."),
	"query_all"  : CfgBool(default=False, descr="Whether to query all locators or just the first available."),
	"enforce_local" : CfgSeconds(default=0, descr="minimum time to wait before running on host without local files; ``0`` means do not wait, ``-1`` means wait indefinitely."),
	"update_delay": CfgSeconds(default=120, descr="Minimum delay between updating jobs. Even if the HTC Job Router launches more often, hooks will not perform action."),
	"hook_timeout": CfgSeconds(default=10, descr="Maximum time after which the hook aborts."),
	"hook_variance" : CfgSeconds(default=1, descr="Maxmimum range of random variance on query times."),
}
# All hooks share the same config
_HTCTranslateConfig = Section(
	target=HTCTranslate,
	name="HTCRouter",
	attributes=_router_attributes,
	descr="Hooks interfacing jobs to service and cache nodes.",
)
HTCTranslateConfigLoader = Loader(_HTCTranslateConfig,LoggerConfig,HandlerConfig)
HTCUpdateConfigLoader    = Loader(Section(target=HTCUpdate, name="HTCRouter", attributes=_router_attributes),LoggerConfig,HandlerConfig)
HTCFinalizeConfigLoader  = Loader(Section(target=HTCFinalize, name="HTCRouter", attributes=_router_attributes),LoggerConfig,HandlerConfig)