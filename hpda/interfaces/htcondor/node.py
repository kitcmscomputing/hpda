#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.

# standard library imports
import socket

# third party imports

# application/library imports
from hpda.utility.exceptions import InstallationError, CommunicationError
from hpda.utility.utils   import which
from hpda.utility.process import ConsumedProcess
from hpda.utility.caching import call_throttle
from .classad             import ClassAdParser

class HTCNode(object):
	"""
	Interface for querying an HTCondor node

	:param address: host of the node, as known to the condor cluster; defaults to the local node
	:type address:
	"""
	def __init__(self, address=socket.getfqdn()):
		self._condor_status_parser = ClassAdParser()
		self._node_address = address

	@staticmethod
	def is_installed():
		try:
			return which("condor_config_val") is not None
		except InstallationError:
			return False

	def is_worker(self):
		if not self.is_installed():
			return False
		return bool(self._startd_classads())

	def is_submitter(self):
		if not self.is_installed():
			return False
		return bool(self._schedd_classads())

	def worker_resources(self):
		"""
		Return the resources of the local worker node, if any

		This function guarantees to provide values for the node name, cpu count
		and memory size (in byte), provided that the node is a worker node.

		:return:
		"""
		if not self.is_worker():
			return {}
		worker_features = self._startd_classads()
		name = socket.getfqdn()
		if not any(True for startd in worker_features if startd["machine"] != worker_features[0]["machine"]):
			name = worker_features[0]["machine"]
		cpus = [int(startd["cpus"]) for startd in worker_features]
		memory = [float(startd["memory"]) * 1024 * 1024 for startd in worker_features] # HTCMemory is in MB and applies 1024 as conversion factor
		return {
			'name'  : name,
			'cpus'  : cpus,
			'memory': memory,
		}

	@call_throttle(120)
	def _startd_classads(self):
		status_proc = ConsumedProcess('condor_status',['-direct',self._node_address,'-startd','-long'])
		if status_proc.wait(timeout=5) != 0:
			status_proc.log()
			raise CommunicationError("HTCondor Node (via 'condor_status')")
		return self._condor_status_parser.parse(status_proc.stdout)

	@call_throttle(120)
	def _schedd_classads(self):
		status_proc = ConsumedProcess('condor_status',['-direct',self._node_address,'-schedd','-long'])
		if status_proc.wait(timeout=5) != 0:
			status_proc.log()
			raise CommunicationError("HTCondor Node (via 'condor_status')")
		return self._condor_status_parser.parse(status_proc.stdout)
