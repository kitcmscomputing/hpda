#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# base imports
import urllib

# library imports

# custom imports
from hpda.utility.rest_client import RestClientPool


class Coordinator(object):

	def __init__(self, host, timeout=None):
		if timeout:
			self._client = RestClientPool(timeout=timeout)
		else:
			self._client = RestClientPool()
		self._host = host

	def stage_request(self, owner, file_list):
		"""Send a stage request to the coordinator

		:type owner: str
		:type file_list: list[string]
		"""
		api = "coordinator/stage"
		data = {
			"owner" : owner,
			"files" : file_list
		}
		self._client.post(self._host, api, data)

	def withdraw_stage_request(self, owner, file_list):
		"""Withdraw a stage request

		:type owner: str
		:type file_list: list[string]
		"""
		api = "coordinator/stage"
		data = {
			"owner" : owner,
			"files" : file_list
		}
		self._client.delete(self._host, api, data)

	def job_add(self, job_id, job_owner, file_list):
		"""Informs the coordinator about a new job

		:type job_id: str
		:type job_owner: str
		:type file_list: list[string]
		"""
		api = "coordinator/jobs/" + urllib.quote_plus(job_id)
		data = {
			"owner" : job_owner,
			"files" : file_list
		}
		self._client.put(self._host, api, data)

	def job_report(self, job_id, cpu_time, memory_usage, wall_time, exit_code, node, locality_rate, cachehit_rate):
		"""Sends additional information after a job is done

		:type job_id: str
		:type cpu_time: float
		:type memory_usage: float
		:type wall_time: float
		:type exit_code: int
		:type node: str
		:type locality_rate: float
		:type cachehit_rate: float
		"""
		api = "coordinator/jobs/" + urllib.quote_plus(job_id)
		data = {
			"cpu_time" : cpu_time,
			"memory_usage" : memory_usage,
			"wall_time" : wall_time,
			"exit_code" : exit_code,
			"node"      : node,
			"locality_rate" : locality_rate,
			"cachehit_rate" : cachehit_rate,
		}
		self._client.post(self._host, api, data)
