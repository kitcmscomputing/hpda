#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
Adapters to the underlying communication implementation for accessing (remote)
components. This module provides proxies for objects that are accessed via IPC.

The classes of this module provide simplified interfaces to functionality of
compontents that are exposed to the cluster.

Proxies will raise :py:exc:`~hpda.utility.exceptions.APIError` if the remote side
provides invalid responses.
"""