#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.

# standard library imports
from urllib                import quote_plus as url_encode
import Queue

# third party imports

# application/library imports
from hpda.cache.rest_api        import ContentAPI, ComponentAPI
from hpda.utility.exceptions    import APIError
from hpda.utility.report import log, LVL
from hpda.utility.rest_client   import RestClientPool, ClientError, ConnectionError, \
	ServerError
from hpda.utility.validation    import SchemaPrimitive as SPrimitive, SchemaMap as SMap, SchemaArray as SArray, Schema
from hpda.utility.addressing    import SocketURL

FileListSchema = Schema(
	SMap(members={
		"files" : SArray(members=(
			SPrimitive(types=basestring),
		))
	})
)
FileInfoSchema = Schema(
	SMap(members={
		"source_uri" : SPrimitive(types=basestring),
		"filename"   : SPrimitive(types=basestring),
		"storage_id" : SPrimitive(types=basestring),
		"cache_id"   : SPrimitive(types=basestring),
		"size"       : SPrimitive(types=(int,float)),
		"validity"   : SPrimitive(types=basestring),
	})
)

def CacheProxy(target, timeout=None):
	"""
	Factory for :py:class:`~.CacheProxy` adapters

	:param target:
	:return: valid adapter to the target
	:rtype: :py:class:`~.HTTPCacheProxy`
	:raises ValueError: if the target cannot be matched to a proxy
	"""
	if isinstance(target, basestring) and (target.startswith("http://") or target.startswith("https://")):
		return HTTPCacheProxy(url=target, timeout=timeout)
	if isinstance(target, SocketURL):
		return HTTPCacheProxy(url=target, timeout=timeout)
	try:
		return HTTPCacheProxy(url=target.url, timeout=timeout)
	except AttributeError:
		pass
	raise ValueError

class HTTPCacheProxy(object):
	"""
	Proxy for accessing a cache through its HTTP API
	"""
	def __init__(self, url, timeout=None):
		if timeout:
			self._client = RestClientPool(timeout=timeout)
		else:
			self._client = RestClientPool()
		self._url = url
		self._file_bulk_size = 128

	# file information/operations
	def get_file_list(self):
		"""
		Get files on cache

		:return: List of cached file's ``source_uri``
		:rtype: list[str]
		"""
		response = self._client.get(self._url, ContentAPI.api_basepath)
		if FileListSchema.validate(response):
			return response["files"]
		else:
			raise APIError("Invalid response from server")

	def get_file_info(self, source_uri):
		"""
		Get information about a file

		:param source_uri: pool name of file
		:return: file information
		:rtype: dict
		"""
		try:
			response = self._client.get(self._url, "%s/%s"%(ContentAPI.api_basepath, url_encode(source_uri)))

			if FileListSchema.validate(response) and response["source_uri"] == source_uri:
				return response
			else:
				raise APIError("Invalid response from server")
		except (ServerError, ClientError, ConnectionError) as err:
			log('cacheproxy', LVL.WARNING, "Error on file info request to host %s: %s", self._url, err.message)
			raise APIError("Invalid response from server")

	def add_files(self, *file_groups):
		"""
		Add files to cache

		:param souce_uris: list of files to cache, each with an optional score
		:type souce_uris: list[list[dict]]
		:return: URIs of files accepted from input list
		:rtype: list[str]

		:note: Each file is expressed as a mapping. This must contain a ``source_uri``
		       as ``str`` or ``unicode`` and may contain a ``score`` as ``int``
		       or ``float``.

		:note: File data is added to the cache, but allocation of files is
		       performed at an independent point in time. Files are only
		       categorized as not accepted if they do not exist or cannot be
		       accessed by the cache.

		:warning: This is a legacy interface and will be removed in the future.
		"""
		self.stage_file_groups(file_groups)

	def stage_file_groups(self, candidate_groups):
		for group in candidate_groups:
			self.insert_files(group)

	def insert_files(self, new_files):
		"""
		Insert new files to the cache

		Each file must be expressed as a mapping of the form
		``{"source_uri": <URI of file>, "score": <int or float>}``
		. ``"score"`` is optional.

		Files are added to the cache only as meta-data. Whether they are actually
		staged is decided at the cache's next allocation cycle. Thus, the
		response will only report files which are impossible to stage (due to
		missing backends or other limitations).

		:param new_files: list of files and meta-data
		:type new_files: list[dict]
		:returns: lists of ``source_uri`` that are incompatible and those that could not be staged
		:rtype: list[str], list[str]
		"""
		failed, rejected = [], []
		for group in (new_files[idx:idx+self._file_bulk_size] for idx in xrange(0, len(new_files), self._file_bulk_size)):
			try:
				response = self._client.post(self._url, ContentAPI.api_basepath, group)
				rejected.extend(response["failed"])
			except ClientError as err:
				log('cacheproxy', LVL.WARNING, "Client error on stage request to host %s: %s", self._url, err.message)
				failed.extend(file_data["source_uri"] for file_data in group)
			except ConnectionError as err:
				log('cacheproxy', LVL.WARNING, "Connection error on stage request to host %s: %s", self._url, err.message)
				failed.extend(file_data["source_uri"] for file_data in group)
			except ServerError as err:
				log('cacheproxy', LVL.WARNING, "Server error on stage request to host %s: %s", self._url, err.message)
				failed.extend(file_data["source_uri"] for file_data in group)
		return rejected, failed

	def update_files(self, update_files):
		"""
		Update files on cache

		:note: This is currently an alias for :py:meth:`~.insert_files`. The two
		       may diverge in the future.

		:param update_files: list of files and meta-data
		:type update_files: list[dict]
		:returns: lists of ``source_uri`` that are incompatible and those that could not be staged
		:rtype: list[str], list[str]
		"""
		return self.insert_files(update_files)

	def unlink_files(self, *delete_source_uris):
		"""
		Unlink files on cache

		Unless a failure occures, the cache guarantees that any information it
		had on the files has been cleared. This does not imply that the cache
		had any information on the file. Actual unstaging of files is triggered
		immediately but may be delayed for an arbitrary time.

		:param delete_source_uris: list of files by ``source_uri``
		:type delete_source_uris: list[str]
		:returns: lists of ``source_uri`` that failed to be unlinked
		:rtype: list[str], list[str]
		"""
		failed_files = []
		for source_uri in delete_source_uris:
			try:
				response = self._client.delete(self._url,  "%s/%s"%(ContentAPI.api_basepath, url_encode(source_uri)))
			except ClientError as err:
				log('cacheproxy', LVL.WARNING, "Client error on unlink request to host %s: %s", self._url, err.message)
				failed_files.append(source_uri)
			except ConnectionError as err:
				log('cacheproxy', LVL.WARNING, "Connection error on unlink request to host %s: %s", self._url, err.message)
				failed_files.append(source_uri)
			except ServerError as err:
				log('cacheproxy', LVL.WARNING, "Server error on stage unlink to host %s: %s", self._url, err.message)
				failed_files.append(source_uri)
		return failed_files

	# component information
	def get_cache_features(self, nickname=None):
		"""
		Get information on the available cache backends

		:param nickname: Specific cache to query for
		:type nickname: str
		:return:
		"""
		if nickname is None:
			return self._client.get(self._url, "%s/cache"%ComponentAPI.api_basepath)
		return self._client.get(self._url, "%s/cache/%s"%(ComponentAPI.api_basepath, url_encode(nickname)))

	def get_storage_features(self, nickname=None):
		"""
		Get information on the available storage backends

		:param nickname: Specific storage to query for
		:type nickname: str
		:return:
		"""
		if nickname is None:
			return self._client.get(self._url, "%s/storage"%ComponentAPI.api_basepath)
		return self._client.get(self._url, "%s/storage/%s"%(ComponentAPI.api_basepath, url_encode(nickname)))

	def __repr__(self):
		return "%s(%s)"%(self.__class__.__name__,self._url)