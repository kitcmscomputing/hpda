#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# base imports
import urllib

# library imports

# custom imports
from hpda.utility.validation   import SchemaPrimitive, SchemaMap, SchemaArray, Schema
from hpda.utility.report import log, LVL
from hpda.utility.rest_client  import RestClientPool, ClientError
from hpda.utility.addressing    import SocketURL

_NodeListSchema = SchemaMap(
	members =
	{
		"nodes" : SchemaArray(members=[SchemaPrimitive(types=basestring)])
	}
)
NodeListSchema = Schema(_NodeListSchema)


_NodeSchema = SchemaMap(
	members =
	{
		"files"			: SchemaArray(members=[SchemaPrimitive(types=basestring)]),
		"lastUpdate"	: SchemaPrimitive(types=(float, int)),
		"lastHeartbeat"	: SchemaPrimitive(types=(float, int))
	}
)
NodeSchema = Schema(_NodeSchema)

def Locator(target, timeout=None):
	"""
	Factory for :py:class:`~.HTTPLocator` adapters

	:param target:
	:return: valid adapter to the target
	:rtype: :py:class:`~.HTTPLocator`
	:raises ValueError: if the target cannot be matched to a proxy
	"""
	if isinstance(target, basestring) and (target.startswith("http://") or target.startswith("https://")):
		return HTTPLocator(host=target, timeout=timeout)
	if isinstance(target, SocketURL):
		return HTTPLocator(host=target, timeout=timeout)
	try:
		return HTTPLocator(host=target.url, timeout=timeout)
	except AttributeError:
		pass
	raise ValueError

class HTTPLocator(object):
	"""

	"""
	def __init__(self, host, timeout=None):
		if timeout:
			self._client = RestClientPool(timeout=timeout)
		else:
			self._client = RestClientPool()
		self._host = host

	def get_nodes(self):
		response = self._client.get(self._host, "locator/nodes")
		if NodeListSchema.validate(response):
			return response['nodes']
		else:
			raise Exception("Schema Validation failed")

	def get_node(self, node_id):
		return_value = None
		try:
			response = self._client.get(self._host, "locator/nodes/" + urllib.quote_plus(node_id))
			if NodeSchema.validate(response):
				return_value = response
			else:
				log('client.locator', LVL.WARNING, "Get node %s failed! Reason: Schema Validation failed.", node_id)
		except ClientError as err:
			log('client.locator', LVL.WARNING, "Get node %s failed! Reason: %s", node_id, err.message)

		return return_value


	def search_file(self, source_uri):
		response = self._client.get(self._host, "locator/catalogue/" + urllib.quote_plus(source_uri))
		if NodeListSchema.validate(response):
			return response['nodes']

	def get_file_count_on_node(self, files = ()):
		data = {
			"target": "node",
			"files"	: files,
		}
		response = self._client.post(self._host, "locator/catalogue", data)
		return response['nodes']

	def get_hosting_workers(self, files):
		"""
		Get worker nodes hosting files

		:see: :py:func:`~hpda.locator.catalogue.Catalogue.get_hosting_workers` of :py:class:`~hpda.locator.catalogue.Catalogue`
		"""
		data = {
			"target": "worker",
			"files"	: files,
		}
		response = self._client.post(self._host, "locator/catalogue", data)
		return response['nodes']

	def add_file_location(self, source_uri, node_id):
		api = "locator/catalogue/" + urllib.quote_plus(source_uri) + "/" + node_id
		self._client.put(self._host, api)

	def remove_file_location(self, source_uri, node_id, life_time):
		api = "locator/catalogue/" + urllib.quote_plus(source_uri) + "/" + node_id + "?life_time=" + str(life_time)
		self._client.delete(self._host, api)

	def add_file_locations(self, node, *source_uris):
		node = getattr(node, "id", node)
		for source_uri in source_uris:
			self.add_file_location(source_uri, node)

	def remove_file_locations(self, node, *removed_files):
		node = getattr(node, "id", node)
		for file_info in removed_files:
			self.remove_file_location(file_info['source_uri'], node, file_info['life_time'])

