#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.

# standard library imports
from urllib                import quote_plus as url_encode

# third party imports

# application/library imports
from hpda.utility.exceptions    import APIError
from hpda.utility.rest_client   import RestClientPool
from hpda.common.pool_mapper    import HeartbeatServerRestApi, Node
from hpda.common.worker         import WorkerInfoAPI
from hpda.utility.validation    import SchemaPrimitive as SPrimitive, SchemaMap as SMap, Schema, SchemaArray as SArray
from hpda.utility.caching       import call_throttle


WorkerInfoSchema = Schema(
	SMap(members={
		"active" : SPrimitive(types=bool, required=True),
		"name"   : SPrimitive(types=basestring, required=False),
		"cpus"   : SArray(members=(SPrimitive(types=(int,float)),), required=False),
		"memory" : SArray(members=(SPrimitive(types=(int,float)),), required=False),
		"dynamic": SPrimitive(types=bool, required=False),
		"pool"   : SPrimitive(types=basestring, required=False),
	})
)

class NodeProxy(object):
	"""
	Interface to common features of a Node
	"""
	def __init__(self, url, timeout=None):
		if timeout:
			self._client = RestClientPool(timeout=timeout)
		else:
			self._client = RestClientPool()
		self._url = url

	@call_throttle(max_delay=20)
	def worker_info(self):
		"""
		Query the features of an attached worker node

		It is *expected* that all features as defined by the :py:mod:`~hpda.common.worker`
		class convention are provided. As this is configuration and implementation
		dependent, no guarantee is given for completeness.

		:return: Mapping of worker node features
		:rtype: dict
		"""
		response = self._client.get(self._url, WorkerInfoAPI.api_basepath)
		if WorkerInfoSchema.validate(response):
			return response
		else:
			print "\n".join(str(item) for item in WorkerInfoSchema.report(response))
			raise APIError("Invalid response from server")

	def get_node(self, node_id, node_role="nodes"):
		"""
		Get information on a single :py:class:`~hpda.common.pool_mapper.Node`

		:param node_id: the node's identifier, as specified by :py:class:`~hpda.common.pool_mapper.Node`\ .\ ``id``
		:type node_id: str
		:param node_role: optional constraint on the role the node must have
		:type node_role: str or :py:class:`~hpda.common.pool_mapper.NodeRole`
		:return:
		"""
		node = self._client.get(self._url, "%s/%s/%s"%(HeartbeatServerRestApi.api_basepath, node_role, url_encode(node_id)))
		try:
			if node is not None:
				return Node.from_repr(node)
		except ValueError:
			raise APIError("Invalid response from server")
		return None

	def get_nodes(self, node_role="nodes"):
		"""
		Get information on all :py:class:`~hpda.common.pool_mapper.Node`\ s

		:param node_role: optional constraint on the role the nodes must have
		:type node_role: str or :py:class:`~hpda.common.pool_mapper.NodeRole`
		:return:
		"""
		try:
			return [ Node.from_repr(node) for node in self._client.get(self._url, "%s/%s"%(HeartbeatServerRestApi.api_basepath, node_role)) ]
		except ValueError:
			raise APIError("Invalid response from server")