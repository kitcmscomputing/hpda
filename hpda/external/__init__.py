#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
module external

This pseudo-module allows accessing external modules and packages if they are
simply linked into its folder.
"""
import os
import sys
try:
	import importlib
except ImportError:
	from ..utility.python_compat import importlib

# Provide a finder and loader for alias imports - See PEP 302
class AliasImporter(object):
	"""
	Import hook for unique imports from external libraries collected in an alias
	directory

	This hook intercepts any imports of modules/packages located inside a path
	and replaces them with internal imports of the respective module/package.
	This avoids duplicate imports, which will break e.g. type/exception checking
	and improves the memory footprint.

	In essence, the hook ensures that
	``from external_basepath.package.module import object``
	is fully equivalent to
	``from package.module import object``
	and both imports work if either works.

	:param external_basepath: file/directory path to external modules/packages, e.g. ``foo/bar``
	:type external_basepath: str or unicode
	:param module_basepath: module path of external modules, e.g. ``foo.bar``
	:type module_basepath: str or unicode
	"""
	def __init__(self, external_basepath, module_basepath=None):
		if not external_basepath in sys.path:
			sys.path.append(external_basepath)
		self._basepath = external_basepath
		self._modpath  = module_basepath
		if self._modpath == None:
			assert "." not in external_basepath, "Got no module path and cannot uniquely resolve paths (includes a '.' dot)"
			self._modpath = external_basepath.replace(os.path.sep,'.')

	def path_hook(self, fullname):
		"""
		Hook for sys.path_hook

		Per PEP302, this must return the object capable of handling the import
		via returnee.find_module, i.e. return self, or raise ImportError.

		:param fullname: the full module name, e.g. external.package.module
		:type fullname: str or unicode
		:return: self or None
		:raises ImportError: if the module is neither an alias nor alias target
		"""
		if self.find_module(fullname) is self:
			return self
		raise ImportError


	def find_module(self, fullname, path=None):
		"""
		Check if module fullname is supported by this importer

		Per PEP302, this must return the object capable of handling the import
		via returnee.load_module, i.e. return self, or None.

		:param fullname: the full module name, e.g. external.package.module
		:type fullname: str or unicode
		:param path: submodule/subpackage path; ignored
		:return: self or None
		"""
		if fullname.startswith(self._modpath) and fullname != self._modpath:
			return self
		return None

	def load_module(self, fullname):
		"""
		Perform PEP302 compliant import of module fullname

		:param fullname: the full module name, e.g. external.package.module
		:type fullname: str or unicode
		:return: module
		"""
		assert fullname.startswith(self._modpath), "Cannot handle modules/packages outside of namespace '%s' (directory '%s/.')" %(self._modpath, self._basepath)
		# PEP302 - must return existing module if possible
		try:
			return sys.modules[fullname]
		except KeyError:
			pass
		# all module code provided by "real" module
		realname = fullname[len(self._modpath)+1:]
		try:
			realmodule = sys.modules[realname]
		except KeyError:
			realmodule = importlib.import_module(realname)
		# register existing module as alias module
		sys.modules[fullname] = realmodule
		return realmodule

# initialise public view and importer functionality
_package_dir = os.path.dirname(__file__)

# detect all packages/modules we should be providing
_import_list = set()
# keep only py modules and packages
# packaged directory
if os.path.isdir(_package_dir):
	for candidate in os.listdir(_package_dir):
		if os.path.isfile(os.path.join(_package_dir,candidate)):
			if not candidate.endswith('.py') or candidate == "__init__.py":
				continue
			candidate=candidate[:-3] # remove ".py")
		elif os.path.isdir(os.path.join(_package_dir,candidate)):
			if not os.path.isfile(os.path.join(_package_dir,candidate, "__init__.py")):
				continue
		else: # not module nor package
			continue
		if "." in candidate:
			raise ImportError("Cannot provide external '%s' (name may not contain any dots)" %candidate)
		_import_list.add(candidate)
# archive
else:
	import zipfile
	module_base = __name__.replace(".",os.sep)
	archive = zipfile.ZipFile(os.path.dirname(__file__)[:-len(__package__)].rstrip(os.sep), "r")
	for candidate in archive.namelist():
		if "__init__" in candidate and candidate.startswith(module_base) and candidate.count(os.sep) == __name__.count(".")+2:
			_import_list.add(os.path.basename(os.path.dirname(candidate)))


# provide import hints for '*' imports and similar
__all__ = list(_import_list) + ["AliasImporter"]

# provide an alias import of contained modules
__alias_importer = AliasImporter(_package_dir, __name__)
#sys.path_hooks.append(__alias_importer.path_hook)
sys.meta_path.append(__alias_importer)


