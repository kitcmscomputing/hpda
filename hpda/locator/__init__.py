#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# standard library imports

# third party imports

# application/library imports

# o Core Components

def initialize(pool_mapper, configuration):
	"""
	Initialize the components of this sub-component

	 Configures the resources and instantiates the objects this sub-component
	requires for normal operation, given the supplied configuration. Returns the
	threads to be managed by the NodeMaster and the APIs to be published.

	:param pool_mapper:
	:type pool_mapper: common.pool_mapper.HeartbeatMapper
	:param configuration: Configuration for this node
	:type configuration: :py:class:`~hpda.utility.configuration.Configuration`
	:return: (list[utility.threads.ThreadMaster],list[object])
	"""
	from .catalogue import CatalogueConfig
	from .api.catalogue import Catalogue as CatalogueAPI
	from .api.nodes import Nodes as NodesAPI
	thread_masters = []
	server_apis = []

	# Resources...

	# Threads...
	locatorCatalogue = CatalogueConfig.apply(configuration, target_kwargs={"pool_mapper":pool_mapper})
	thread_masters.append(locatorCatalogue)
	server_apis.append(CatalogueAPI(locatorCatalogue))
	server_apis.append(NodesAPI(locatorCatalogue))

	return locatorCatalogue, thread_masters, server_apis