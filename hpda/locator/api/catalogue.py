#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# base imports

# library imports
import cherrypy

# custom imports
from ..catalogue import NodeNotFound
from ...utility.exceptions	import ExceptionFrame
from ...utility.report		import log, LVL
from hpda.utility.validation import SchemaPrimitive, SchemaMap, SchemaArray, Schema


_FileSchema = SchemaMap(
	members =
	{
		"source_uri" : SchemaPrimitive(types=basestring),
		"score"      : SchemaPrimitive(types=(float, int), required = False),
	}
)
FileSchema = Schema(_FileSchema)
_StagingSchema = SchemaMap(
	members =
	{
		"action" : SchemaPrimitive(types=basestring, constraints=[lambda obj: obj == "stage"]),
		"files"  : SchemaArray(members=(_FileSchema,)),
	}
)
StagingSchema = Schema(_StagingSchema)

_FileListSchema = SchemaMap(
	members =
	{
		"target": SchemaPrimitive(types=basestring, constraints=[lambda obj: obj in ("node","worker")]),
		"files"	: SchemaArray(members=[SchemaPrimitive(types=basestring)]),
	}
)
FileListSchema = Schema(_FileListSchema)

class Catalogue(object):
	"""API for the Catalogue

	:type catalogue: locator.Catalogue
	"""
	exposed = True
	api_basepath = "locator/catalogue"

	def __init__(self, catalogue):
		self.catalogue = catalogue

	@cherrypy.tools.json_out()
	def GET(self, source_uri):
		"""Returns all nodes which have the requested file cached

		@type source_uri: str
		@param source_uri: requested file
		"""

		self.catalogue.nodes_mutex.reader_acquire()
		fileNodes = self.catalogue.get_file_nodes(source_uri)
		self.catalogue.nodes_mutex.reader_release()
		return {'nodes': fileNodes}

	@cherrypy.tools.json_out()
	@cherrypy.tools.json_in()
	def POST(self):
		with ExceptionFrame():
			message = cherrypy.request.json
			if FileListSchema.validate(message):
				# TODO: remove {"nodes": ... } wrappings
				if message["target"]=="node":
					return { "nodes" : self.catalogue.check_nodes_for_files(message['files'])}
				if message["target"]=="worker":
					return { "nodes" :self.catalogue.get_hosting_workers(message['files'])}
			else:
				validation_report = FileListSchema.report(message)
				log('server', LVL.STATUS, "Incompatible files request:\n%s",validation_report)
				raise cherrypy.HTTPError(422, "Incorrect request content. Expected FileListSchema")

	@cherrypy.tools.json_out()
	def PUT(self, source_uri, node_id):
		try:
			self.catalogue.add_file_to_node(node_id, source_uri)
		except NodeNotFound:
			log("locator", LVL.WARNING, "File %s could not be added to node %s catalogue. Reason: Node not found!", source_uri, node_id)
			pass
		return {}

	@cherrypy.tools.json_out()
	def DELETE(self, source_uri, node_id, life_time):
		try:
			self.catalogue.remove_file_from_node(node_id, source_uri, life_time)
		except NodeNotFound:
			log("locator", LVL.WARNING, "File %s could not be removed from node %s catalogue. Reason: Node not found!", source_uri, node_id)
			pass
		return {}