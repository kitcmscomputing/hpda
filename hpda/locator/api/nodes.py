#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# base imports

# library imports
import cherrypy

# custom imports
from ..catalogue import Catalogue, NodeNotFound
from ...utility.report        import log, LVL

class Nodes(object):
	"""API for node information

	:type catalogue: Catalogue
	:type exposed: bool
	"""

	exposed = True
	api_basepath = "locator/nodes"

	def __init__(self, catalogue):
		self.catalogue = catalogue

	@cherrypy.tools.json_out()
	def GET(self, node=None):
		self.catalogue.nodes_mutex.reader_acquire()
		try:
			if node is None:
				return {'nodes': self.catalogue.get_nodes()}
			return self.catalogue.get_node(node).get_dict()
		except NodeNotFound:
			raise cherrypy.HTTPError(404, "No such node: '%s'" % node)
		finally:
			self.catalogue.nodes_mutex.reader_release()