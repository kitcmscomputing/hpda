#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# base imports
import time

# library imports

# custom imports
from hpda.client.cacheproxy import CacheProxy
from hpda.client.node import NodeProxy
from hpda.common.pool_mapper import NodeRole, Node
from hpda.utility.exceptions import BasicException, APIError
from hpda.utility.report import log, LVL
from hpda.utility.rwlock import RWLock
from hpda.utility.threads import ThreadMaster
from hpda.utility.rest_client import ConnectionError, ServerError
from hpda.utility.configuration.interface import Section, CfgSeconds


class NodeNotFound(BasicException):
	"""The Host already exists"""
	pass

class NodeData(object):
	"""
	Container for the locator data attributed to a node

	This is a wrapper around an underlying :py:class:`~hpda.common.pool_mapper.Node` object
	for transparently adding meta-data.

	:param node: the Node hosting the services from which data is collected
	:type _node: Node
	:ivar files: files located on this node
	:type files: list[str]
	:ivar worker: information available on an attached worker node
	:type worker: dict
	:ivar lastUpdate: time of last, full update of locator data
	:type lastUpdate: float
	"""
	def __init__(self, node):
		self.files = []
		self.worker = {}
		self.lastUpdate = 0
		self._node = node

	def get_dict(self):
		"""
		Returns the node data as a dictionary

		:rtype: dict
		:return: The node data as dictionary
		"""
		return {
			"files"			: self.files,
			"url"			: str(self.url),
			"worker"		: self.worker,
			"lastUpdate"	: self.lastUpdate,
			"lastHeartbeat"	: self.last_heard,
		}

	def __repr__(self):
		return '%s(%s)' %(self.__class__.__name__, self._node)

	# pass-through attributes of wrapped Node
	@property
	def alive(self):
		return self._node.alive
	@property
	def url(self):
		return self._node.url
	@property
	def last_heard(self):
		return self._node.last_heard
	@property
	def roles(self):
		return self._node.roles
	@property
	def id(self):
		return self._node.id

class Catalogue(ThreadMaster):
	"""
	Stores all registered nodes and their data

	:param pool_mapper: Pool Map of our host node
	:type pool_mapper: :py:class:`~hpda.common.pool_mapper.HeartbeatMapper`
	:type _nodes_by_id: dict[NodeData]
	:type _nodeUpdateTimeout: int
	:type nodes_mutex: RWLock
	"""
	def __init__(self, pool_mapper, node_update_interval):
		ThreadMaster.__init__(self)
		self._pool_mapper = pool_mapper
		self._nodes_by_id = {}
		self._nodeUpdateTimeout = node_update_interval
		self.nodes_mutex = RWLock()
		self.loop_interval=20
		self._life_time_hooks = []

	def add_life_time_hook(self, hook):
		assert  hasattr(hook, "add_life_time"), "Hooks must conform to interface"
		self._life_time_hooks.append(hook)


	def payload(self):
		"""
		Update all external information

		This is the regularly executed main work item of the locator. It will
		maintain the nodes of interest to the locator, then attempt to fetch
		all outstanding information from nodes.
		"""
		self.maintain_nodes()
		self.update_nodes()

	def maintain_nodes(self):
		"""Remove all invalid nodes from the locator, add all new nodes from the PoolMap"""
		self.clean_nodes()
		self.populate_nodes()

	def populate_nodes(self):
		"""
		Populate the list of nodes to update

		:return: number of added nodes
		"""
		added = 0
		with self.nodes_mutex.Writer():
			for node in self._pool_mapper.get_nodes_by_role(NodeRole.CACHE):
				if node.id not in self._nodes_by_id:
					self._nodes_by_id[node.id] = NodeData(node)
					log('Locator', LVL.STATUS, "Adding new node %s", node)
					added += 1
		return added

	def update_nodes(self, force_update = False):
		"""Update the data of all nodes if necessary

		:param force_update: Force data update of all nodes
		:type force_update: bool
		:return:
		"""
		start_time = time.time()
		with self.nodes_mutex.Reader():
			update_nodes = [node for node in self._nodes_by_id.values() if node.lastUpdate < (start_time - self._nodeUpdateTimeout) or force_update ]
			log('Locator', LVL.STATUS, "Update for %d/%d node(s) required...", len(update_nodes), len(self._nodes_by_id))
			for node in update_nodes:
				self.update_node(node)
			log('Locator', LVL.STATUS, "Finished update after %03.3fs", (time.time()-start_time))

	def clean_nodes(self):
		"""
		Remove all retired nodes from the catalogue

		:return: number of unlinked nodes
		"""
		unlinked = 0
		with self.nodes_mutex.Writer():
			for node in self._nodes_by_id.values():
				if not ( node.alive and (NodeRole.CACHE in node.roles)):
					del self._nodes_by_id[node.id]
					log('Locator', LVL.WARNING, "Removing retired node %s", node)
					unlinked += 1
		return unlinked

	def update_node(self, node):
		"""
		Update all information available for a node

		Contact the node and pull in all available information. This will overwrite
		any previously available information for the node with the most current,
		up-to-date information available.

		:param node: Node to be updated
		:type node: :py:class:`~hpda.common.pool_mapper.Node` or :py:class:`~.NodeData`
		:return: Whether the update succeeded
		:rtype: bool
		"""
		if not node.id in self._nodes_by_id:
			with self.nodes_mutex.Writer():
				if isinstance(node, NodeData):
					self._nodes_by_id[node.id] = node
				elif isinstance(node, Node):
					self._nodes_by_id[node.id] = NodeData(node)
		node_data = self._nodes_by_id[node.id]
		log('Locator', LVL.DEBUG, "Updating %s", node_data)
		try:
			file_list = CacheProxy(node_data).get_file_list()
		except (ConnectionError, ServerError, APIError):
			log("Locator", LVL.WARNING, "Failed fetching file list from node %s", node_data)
			return False
		try:
			worker_info = NodeProxy(node_data.url).worker_info()
		except (ConnectionError, ServerError, APIError):
			log("Locator", LVL.WARNING, "Failed fetching worker info from node %s", node_data)
			return False
		self._nodes_by_id[node.id].files  = file_list
		self._nodes_by_id[node.id].worker = worker_info
		self._nodes_by_id[node.id].lastUpdate = time.time()
		return True

	def get_hosting_workers(self, files):
		"""
		Get worker nodes hosting files

		Checks every node if a worker node is present and if any files from ``files``
		is locally available.

		:param files: files to search for
		:type files: list[str]
		:return: list of matching workers and their features
		:rtype: dict
		"""
		workers = []
		files = set(files)
		with self.nodes_mutex.Reader():
			for node in self._nodes_by_id.viewvalues():
				if node.worker:
					worker = node.worker.copy()
					worker["files"] = len(files.intersection(node.files))
					if worker["files"]:
						workers.append(worker)
		return workers

	def check_nodes_for_files(self, fileList, use_key = 'url'):
		"""
		Search for nodes hosting files

		Checks every alive node if files from fileList exist and returns the number
		of files found for every node.

		:param fileList: list of files to search for
		:type fileList: list[str]
		:param use_key: Which property of the node object key should be used as key for the dict
		:type use_key: str
		:return: mapping of nodes to number of files found
		:rtype: dict[Node,int]
		"""
		nodes = {}
		with self.nodes_mutex.Reader():
			for node_id in (nid for nid, node in self._nodes_by_id.viewitems() if node.alive):
				if not hasattr(self._nodes_by_id[node_id], use_key):
					log("Locator", LVL.WARNING, "NoteData do not have key %s", use_key)
					break
				key = str(getattr(self._nodes_by_id[node_id], use_key))
				nodes[key] = 0
				for filePath in fileList:
					if filePath in self._nodes_by_id[node_id].files:
						nodes[key] += 1
		return nodes

	def get_stage_status(self, file_groups):
		pass


	def get_file_nodes(self, filePath):
		"""
		Returns a list of all nodes which have a specific file cached

		:param filePath:
		:type filePath: str
		:rtype: list[NodeData]
		:return: NoteData of the Node
		"""
		fileNodes = []
		with self.nodes_mutex.Reader():
			for node_id, node in self._nodes_by_id.items():
				if filePath in node.files:
					fileNodes.append(str(node.url))
		return fileNodes

	def get_node(self, node_id):
		"""
		Return a specific node

		:param node_id: ID of the node
		:type node_id: str
		:return: Data of the Node
		:rtype: :py:class:`~.NodeData`
		"""
		try:
			return self._nodes_by_id[node_id]
		except KeyError:
			raise NodeNotFound

	def get_nodes(self):
		"""
		Return all nodes the locator knows

		:return: id of all known nodes
		:rtype: list[str]
		"""
		return self._nodes_by_id.keys()

	def add_file_to_node(self, node_id, source_uri):
		node = self.get_node(node_id)
		if not source_uri in node.files:
			node.files.append(source_uri)

	def remove_file_from_node(self, node_id, source_uri, life_time):
		node = self.get_node(node_id)
		if source_uri in node.files:
			node.files.remove(source_uri)

			for hook in self._life_time_hooks:
				hook.add_life_time(node.url, source_uri, life_time)


CatalogueConfig = Section(
	target=Catalogue,
	attributes={
		"node_update_interval" : CfgSeconds(default="2h",descr="Interval between full updates of information from individual nodes."),
	},
	name="Locator",
	descr="Frontend for information of caches."
)