#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.

# standard library imports
import socket

# third party imports

# application/library imports

class StaticWorker(object):
	"""
	A statically configured worker information

	:param name: name by which the batch system knows the node
	:param cpus: number of cores available per slot
	:type cpus: list[int]
	:param memory: number of memory available per slot in Bytes
	:type memory: list[int|float]
	:param dynamic: whether slots are dynamically partitioned
	:type dynamic: bool
	:param pool: identifier for the owning batch system pool
	:type pool: str
	"""
	def __init__(self, name=socket.getfqdn(), cpus=(), memory=(), dynamic=False, pool="default"):
		self.name = name
		self.cpus = cpus
		self.memory = memory
		self.dynamic = dynamic
		self.pool = pool

	def active(self):
		return any((self.name, self.cpus, self.memory))

	def resources(self):
		return {
			'name'  : self.name,
			'cpus'  : self.cpus,
			'memory': self.memory,
			'dynamic': self.dynamic,
			'pool'  : self.pool,
		}
