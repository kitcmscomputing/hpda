#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
Interface to worker nodes, providing data about attached worker nodes.

Worker nodes are represented as simple collections of compute slots. Each slot
is considered capable of executing a single job on a given share of cores and
memory.

The following features are provided for a worker node:

**active**
  Whether the worker node is running. This only states that the node is capable of
  executing jobs in principal. It does not imply that the node is running jobs or
  has free slots capable of running jobs.

**name**
  The name by which the batch system identifies the node. Primarily intended
  for providing hints for scheduling.

**cpus**
  The number of CPU cores (both physical and logical) available per slot.

**memory**
  The amount of memory in bytes available per slot.

**dynamic**
  Whether slots may be re-allocated dynamically by the batch system.

**pool**
  Arbitrary identifier for the batch system the node is attached to. This value
  is meaningless unless several batch-systems (or a logically partitioned one) are
  managed at once.
"""

# standard library imports
import socket
import multiprocessing

# third party imports
import cherrypy

# application/library imports
from hpda.utility.exceptions import ExceptionFrame
from hpda.utility.configuration.interface import Choice, Section, CfgStr, CfgBool, CfgIntArray, CfgFloatArray
from .static import StaticWorker
from .htcondor import HTCondorWorker

StaticWorkerConfig = Section(
	target=StaticWorker,
	attributes={
		"name" : CfgStr(default=socket.getfqdn(), descr="Name of the worker node in the batch system."),
		"cpus" : CfgIntArray(default=(1,)*multiprocessing.cpu_count(), descr="Cores available per slot."),
		"memory" : CfgFloatArray(default=(), descr="Memory available per slot."),
		"dynamic": CfgBool(default=False, descr="Whether slots may be dynamically resized."),
		"pool" : CfgStr(default="default", descr="Nickname of the batch system pool. Only relevant for multiple batch systems."),
	})
HTCondorWorkerConfig = Section(
	target=HTCondorWorker,
	attributes={
		"dynamic": CfgBool(default=False, descr="Whether slots may be dynamically resized."),
		"pool" : CfgStr(default="default"),
	})
AnyWorkerConfig = Choice(
	sections={
		"static"  : StaticWorkerConfig,
		"htcondor": HTCondorWorkerConfig,
		"htc"     : HTCondorWorkerConfig,
	},
	name="WorkerInfo",
	type_default="static",
	descr="Source of information to publish about an attached worker node. Used for scheduling."
)

class WorkerInfoAPI(object):
	"""
	API for querrying information about a local woker node

	:param worker: a worker information interface to a local node
	"""
	exposed = True
	api_basepath = "node/worker"
	def __init__(self, worker):
		self.worker = worker

	@cherrypy.tools.json_out()
	def GET(self):
		with ExceptionFrame(ignore=[cherrypy.HTTPError]):
			features = {'active': self.worker.active()}
			features.update(self.worker.resources())
			return features
