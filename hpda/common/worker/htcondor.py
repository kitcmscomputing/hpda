#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.

# standard library imports

# third party imports

# application/library imports
from hpda.interfaces.htcondor.node import HTCNode

class HTCondorWorker(object):
	"""
	A dynamically queried HTCondor worker information

	:param dynamic: whether slots are dynamically partitioned
	:type dynamic: bool
	:param pool: identifier for the owning batch system pool
	:type pool: str
	"""
	def __init__(self, dynamic=False, pool="default"):
		self._htc_node = HTCNode()
		self.dynamic = dynamic
		self.pool = pool

	def active(self):
		return self._htc_node.is_worker()

	def resources(self):
		resources = self._htc_node.worker_resources()
		resources['dynamic'] = self.dynamic
		resources['pool'] = self.pool
		return resources