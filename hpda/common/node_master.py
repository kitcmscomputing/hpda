#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# base imports
import time
import logging
import collections

# library imports

# custom imports
from hpda.utility.report     import LVL
from hpda.utility.threads    import ThreadMaster
from hpda.utility.utils      import listItems

from hpda.common.pool_mapper import NodeRole, HeartbeatServerRestApi, HeartbeatConfig
from hpda.common.api_server  import NodeServerConfig
from hpda.utility.configuration.interface import Section, CfgSeconds, CfgStrArray
"""
The NodeMaster is the central process, spawning and supervising the individual
cache components.
"""

class NodeMaster(ThreadMaster):
	"""
	Supervisor for all cache threads and data.

	Starts and controls Allocator, Worker and Catalogue.
	"""
	def __init__(self, restart_delay_add, restart_delay_min, restart_delay_max, restart_interval, components):
		ThreadMaster.__init__(self)
		self.logger = logging.getLogger("node.%s"%self.__class__.__name__)
		self.loop_interval = restart_interval or min(restart_delay_add, restart_delay_min) or max(restart_delay_add, restart_delay_min) or 20
		self._restart_delay_min = restart_delay_min
		self._restart_delay_add = restart_delay_add
		self._restart_delay_max = restart_delay_max
		self._roles = [ NodeRole(role.upper()) for role in components ]
		self._thread_stats = collections.OrderedDict()

	def configure_components(self, configuration, init_databases):
		# core elements
		self.logger.log(LVL.STATUS, 'Preparing COMMON node')
		api_server = NodeServerConfig.apply(configuration)
		pool_mapper = HeartbeatConfig.apply(configuration, target_kwargs={"my_url":api_server.address, "my_roles": self._roles})
		api_server.add_apis(HeartbeatServerRestApi(pool_mapper))
		self.add_threads(pool_mapper, api_server)
		# initialize common functionality
		from hpda.common import initialize as initialize_common
		threads, apis = initialize_common(pool_mapper, configuration)
		self.add_threads(*threads)
		api_server.add_apis(*apis)
		locator_catalogue = None
		if NodeRole.CACHE in self._roles:
			from hpda.cache import initialize as initialize_cache
			self.logger.log(LVL.STATUS, 'Preparing CACHE node')
			threads, apis = initialize_cache(pool_mapper, configuration)
			self.add_threads(*threads)
			api_server.add_apis(*apis)
		if NodeRole.LOCATOR in self._roles or NodeRole.COORDINATOR in self._roles:
			from hpda.locator import initialize as initialize_locator
			self.logger.log(LVL.STATUS, 'Preparing LOCATOR node')
			locator_catalogue, threads, apis = initialize_locator(pool_mapper, configuration)
			self.add_threads(*threads)
			api_server.add_apis(*apis)
		if NodeRole.COORDINATOR in self._roles:
			from hpda.coordinator import initialize as initialize_coordinator
			self.logger.log(LVL.STATUS, 'Preparing COORDINATOR node')
			threads, apis = initialize_coordinator(pool_mapper, locator_catalogue, configuration, init_databases)
			self.add_threads(*threads)
			api_server.add_apis(*apis)

	def add_threads(self, *threads):
		for thread in threads:
			self._thread_stats[thread] = [None, 0]

	def payload(self):
		self.logger.log(LVL.STATUS, 'Checking %d thread lifesigns', len(self._thread_stats))
		for thread in self._thread_stats:
			if thread.is_alive():
				self._thread_stats[thread][1] = max(0, self._thread_stats[thread][1]-1)
			else:
				if self._thread_stats[thread][0] is None:
					self.logger.log(LVL.STATUS, 'Thread %s is dead', thread)
					restart_delta = self._get_restart_delta(self._thread_stats[thread][1])
					self._thread_stats[thread][1] += 1
					self._thread_stats[thread][0]  = time.time() + restart_delta
					if restart_delta > 0:
						self.logger.log(LVL.STATUS, 'Restarting %s in %1.1fs', thread, restart_delta)
						continue
				if time.time() >= self._thread_stats[thread][0]:
					self.logger.log(LVL.STATUS, 'Restarting %s now', thread)
					thread.start()
					self._thread_stats[thread][0] = None
				else:
					self.logger.log(LVL.INFO, 'Restarting %s in %1.1fs', thread, (self._thread_stats[thread][0] - time.time()))

	def _get_restart_delta(self, death_count):
		return min(self._restart_delay_min + death_count * self._restart_delay_add, self._restart_delay_max)

	def stop(self):
		self.logger.log(LVL.STATUS, 'Shutting down')
		# disregard whether we are "alive" - we might be the main thread
		self.logger.log(LVL.INFO, 'Shutting down %s (thread revival)', self)
		ThreadMaster.stop(self)
		# gracefully shut down all slave
		self.logger.log(LVL.INFO, 'Shutting down %d component threads', len(self._thread_stats))
		shutdown_start = time.time()
		for thread in reversed(self._thread_stats):
			thread.stop()
		# terminate anything that remains
		for thread in reversed(self._thread_stats):
			thread.join(5 - (time.time() - shutdown_start))
			if thread.is_alive():
				self.logger.log(LVL.WARNING, 'Terminating %s', thread)
				thread.terminate()
		termination_cycle = 0
		while any(thread.is_alive() for thread in self._thread_stats):
			if termination_cycle % 5 == 0:
				self.logger.log(LVL.INFO, 'Waiting for termination of %s', listItems(*(thread for thread in self._thread_stats if thread.is_alive()),quote="",relation="and"))
			termination_cycle += 1
			for thread in reversed(self._thread_stats):
				thread.join(10 - (time.time() - shutdown_start))
				thread.terminate()
			time.sleep(1)
		self.logger.log(LVL.INFO, 'All threads exited')

# configuration
NodeMasterConfig = Section(
	attributes={
		"restart_interval"  : CfgSeconds(default='0',   descr="interval for checking thread lifesigns and restarts"),
		"restart_delay_add" : CfgSeconds(default='10s', descr="delay to add to thread resurrection per death"),
		"restart_delay_min" : CfgSeconds(default='0',   descr="minimum delay for thread resurrection after death"),
		"restart_delay_max" : CfgSeconds(default='10m', descr="minimum delay for thread resurrection after death"),
		"components" : CfgStrArray(default='', descr="components to start on this node"),
	},
	target=NodeMaster,
	name="NodeMaster",
	descr="Controller for any component's main threads. The NodeMaster regularly checks component lifesigns, restarting them in case of failures."
)
