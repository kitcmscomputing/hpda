#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
The coordination in the distributed infrastructure relies on knowledge about
the nodes available. This module provides the tools and classes for creating
a mapping of relevant nodes via a heartbeat mechanism.

Every node may be configured to address one or more *Listeners*. This causes
the node to regularly send messages to the Listener, detailing the node's
type and functionality. In turn, the Listener uses these messages to map out the
portion of the infrastructure relevant to it; missing hearbeats offer a means to
detect nodes leaving the pool.
"""

# base imports
import time
import itertools
import random

# library imports
import cherrypy

# custom imports
from hpda.utility.caching       import hashkey
from hpda.utility.exceptions    import ExceptionFrame
from hpda.utility.report        import log, LVL
from hpda.utility.enum          import StaticEnum, EnumExtensionError
from hpda.utility.singleton     import BaseWeakSingleton, singleton__init
from hpda.utility.addressing    import SocketURL
from hpda.utility.threads       import ThreadMaster
from hpda.utility.rest_client   import RestClientPool, ConnectionError, ServerError
from hpda.utility.configuration.interface import Section, CfgInt, CfgSeconds, CfgStrArray

class NodeRole(StaticEnum):
	"""
	Roles supplied by a Node of the pool

	:cvar CACHE: Node may cache files for reading
	:cvar LOCATOR: Node offers cache location service
	:cvar COORDINATOR: Node coordinates caches
	"""
	__members__ = ["CACHE","LOCATOR","COORDINATOR"]

class Node(BaseWeakSingleton):
	"""
	Basic identification of a pool node

	:param url: hostname, IPv4 or IPv6 address of the entity's host
	:type url: SocketURL
	:param roles: roles the node offers
	:type roles: list[NodeRole]
	"""
	@classmethod
	def _singleton_signature(cls, url, roles=None):
		return url

	@singleton__init
	def __init__(self, url, roles = None):
		self.url   = url
		self.roles = roles or []
		self.id    = hashkey(repr(self.url))
		self.last_heard = time.time()
		self.alive = True

	def heartbeat(self, roles=None):
		self.last_heard = time.time()
		if roles is not None and roles != self.roles:
			self.roles = roles

	def __repr__(self):
		return '<%s:%s@%s[%s]>' %(self.__class__.__name__, self.id, self.url, "|".join(role.__name__ for role in self.roles))

	def get_repr(self):
		"""Provide simple representation for reconstruction"""
		return {
			"url"  : str(self.url),
			"roles": [role.__name__ for role in self.roles]
		}

	@classmethod
	def from_repr(cls, raw_repr):
		"""
		Reconstruct object from simple representation

		:raise ValueError: on incorrect input"""
		try:
			roles = [NodeRole(role) for role in raw_repr["roles"]]
		except EnumExtensionError:
			raise ValueError("Unknown role")
		try:
			url = SocketURL.from_str(raw_repr["url"])
		except ValueError:
			raise ValueError("Unknown url format")
		return cls(url, roles)


class MyNode(Node):
	@property
	def last_heard(self):
		return time.time()
	@last_heard.setter
	def last_heard(self, value):
		pass


class HeartbeatMapper(ThreadMaster):
	"""
	Mapper for the pool nodes, based on heartbeats

	This class provides the infrastructure to send and listen for heartbeats
	identifying nodes in the pool. Based on heartbeats, a mapping of nodes in
	the pool is created and exposed.

	:param my_url: URL of this node
	:type my_url: :py:class:`~hpda.utility.addressing.SocketURL`
	:param listeners: nodes to inform about us
	:type listeners: list[:py:class:`~hpda.utility.addressing.SocketURL`]
	:param interval: delay between sending/receiving heartbeats
	:type interval: int or float
	:param variance: variance on interval
	:type variance: int or float
	:param max_misses: allowed missing heartbeats before ignoring node
	:type max_misses: int or float
	"""
	def __init__(self, my_url, my_roles, listeners=(), interval=60, variance=0, max_misses=5):
		ThreadMaster.__init__(self)
		log('mapper', LVL.STATUS, "Instantiating PoolMapper (%s)", self.__class__.__name__)
		self.my_node = MyNode(my_url, my_roles)
		self.loop_interval = interval
		self.loop_variance = variance
		self._max_misses = max_misses
		self._clients   = {}
		self._listeners = {}
		self._api = HeartbeatClientRestApi(self.my_node)
		for listener_url in listeners:
			listener_node = Node(listener_url)
			self._listeners[listener_node.id] = listener_node
		self._timestamp = 0

	@property
	def nodes(self):
		return dict(self._clients, **{self.my_node.url : self.my_node})

	def payload(self):
		self._timestamp = time.time()
		self._check_lifesigns()
		self._send_heartbeat()

	def get_node(self, node_id):
		"""
		Get the node corresping to a specific id

		:type node_id: str
		:return: Node
		:raise KeyError: if the node is not known
		"""
		return self.nodes[node_id]

	def get_nodes_by_role(self, node_role):
		"""
		Get all known nodes if a specific role

		:type node_role: :py:class:`.NodeRole`
		:return: list[Node]
		"""
		return [node for node in self.nodes.values() if node_role in node.roles]

	def receive_heartbeat(self, node):
		"""Receive and digest the hearbeat from node"""
		try:
			self._clients[node.id].heartbeat(roles=node.roles)
			log('mapper.listen', LVL.DEBUG, "Updated %s", node)
		except KeyError:
			self._clients[node.id] = node
			log('mapper.listen', LVL.INFO, "Added %s", node)
		node.alive = True

	def _send_heartbeat(self):
		log('mapper.heartbeat', LVL.STATUS, "Sending heartbeat to %d listeners, %d clients", len(self._listeners), len(self._clients))
		# iterate receivers randomly in case any block for too long
		receivers = list(set(itertools.chain(self._clients.values(),self._listeners.values())))
		random.shuffle(receivers)
		for node in receivers:
			log('mapper.heartbeat', LVL.DEBUG, "Sending heartbeat to %s", node)
			if not self._api.send_alive(node.url):
				log('mapper.heartbeat', LVL.WARNING, "Failed sending heartbeat to %s", node)

	def _check_lifesigns(self):
		log('mapper.lifesigns', LVL.STATUS, "Checking lifesigns of %d known nodes", len(self._clients))
		for node in self._clients.values():
			log('mapper.lifesigns', LVL.DEBUG, "Checking lifesign for %s", node)
			if node.last_heard < self._timestamp - self._max_misses * self.loop_interval + 2 * self.loop_variance:
				log('mapper.lifesigns', LVL.WARNING, "Dismissing %s (No heartbeat for at least %d x %fs)", node, self._max_misses, self.loop_interval)
				node.alive = False
				del self._clients[node.id]

class HeartbeatClientRestApi(object):
	"""
	The Client REST API of the heartbeat

	:note: This API is intended for use by the :py:class:`~.HeartbeatMapper`, serving
	       to exchange heartbeats. Any application level usage should be via the
	       higher-level :py:class:`~hpda.common.client.node.NodeProxy` API.

	:param host_node: the node this hearbeat mapper resides on
	:type host_node: Node
	"""
	def __init__(self, host_node):
		self._host_node = host_node
		self._client_api = RestClientPool(timeout=10)

	def send_alive(self, url, node=None):
		"""
		Notify the listener at url that node is alive

		:param url: The url of the listener
		:type url: str or unicode or SocketURL
		:param node: The node that is alive, or None for the host node
		:type node: Node or None
		"""
		node = node or self._host_node
		try:
			self._client_api.put(
				url,
				"%s/%s"%(HeartbeatServerRestApi.api_basepath, node.id),
				data = node.get_repr()
				)
		except (ConnectionError, ServerError):
			return False
		return True

	def send_dying(self, url, node=None):
		"""
		Notify the listener at url that node is dying/shutting down
		"""
		pass

class HeartbeatServerRestApi(object):
	"""
	The HTTP REST API of the heartbeat

	:param mapper:
	:type mapper: HeartbeatMapper
	"""
	exposed = True
	api_basepath = "heartbeat"
	def __init__(self, mapper):
		self._mapper = mapper

	@cherrypy.tools.json_out()
	def GET(self, node_class="nodes", node_identifier=None):
		"""
		**GET** information on nodes of the Heartbeat PoolMapper

		In order to query for specific nodes, ``node_class`` and ``node_identifier``
		may be used. If both are used, the node of id ``node_identifier`` must have
		the role ``node_class``, or nothing is returned.

		The ``node_class`` may be ``"nodes"`` for all types of nodes or either of
		``"cache"``, ``"locator"``, ``"coordinator"`` for a specific role.

		:param node_class: the types of nodes to query for; case insensitive
		:type node_class: str
		:param node_identifier: the ``id`` of a specific :py:class:`~.Node` searched for
		:type node_identifier: str
		:returns: list of node matching class, single node matching identifier or ``None``
		:rtype: list[:py:class:`~.Node`] or :py:class:`~.Node` or None
		:raises: :py:class:`.HTTPError` (404) on unknown ``node_class``
		"""
		with ExceptionFrame(ignore=[cherrypy.HTTPError]):
			node_class = node_class.upper()
			if node_class != "NODES" and not hasattr(NodeRole, node_class):
				raise cherrypy.HTTPError(404, "Unknown node_class '%s'"%node_class)
			if node_identifier is None:
				if node_class == "NODES":
					return [ node.get_repr() for node in self._mapper.nodes.values()]
				return [ node.get_repr() for node in self._mapper.get_nodes_by_role(getattr(NodeRole, node_class))]
			try:
				node = self._mapper.nodes[node_identifier]
				if node_class == "NODES" or (getattr(NodeRole, node_class) in node.roles):
					return node.get_repr()
			except KeyError:
				pass
			return None

	@cherrypy.tools.json_in()
	@cherrypy.tools.json_out()
	def PUT(self, node_name):
		"""
		Add/update a node heartbeat
		"""
		with ExceptionFrame(ignore=[cherrypy.HTTPError]):
			message = cherrypy.request.json
			try:
				put_node = Node.from_repr(message)
			except ValueError:
				raise cherrypy.HTTPError(422, "Incorrect request content. Expected node representation")
			if not put_node.id == node_name:
				raise cherrypy.HTTPError(422, "Incorrect request: Node name does not match computed id; version mismatch?")
			self._mapper.receive_heartbeat(put_node)
			return {}

HeartbeatConfig = Section(
	attributes={
		"listeners" : CfgStrArray(default="", descr="URIs of nodes to actively notify about this nodes."),
		"max_misses" : CfgInt(default="5", descr="Allowed missed heartbeats before ignoring node"),
		"interval" : CfgSeconds(default="1m", descr="Delay between sending/checking heartbeats"),
		"variance" : CfgSeconds(default="5s", descr="Variance of delay"),
	},
	target=HeartbeatMapper,
	name="NodeMapper",
	descr="Discovery of other nodes via hearbeats. For either two nodes to be able to interact, at least one must notify the other as a ``listener``",
)