#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.

# base imports
import logging
import socket

# library imports
import cherrypy

# application/library imports
from hpda.utility.exceptions import ExceptionFrame
from hpda.utility.report     import LVL
from hpda.utility.threads    import ThreadMaster
from hpda.utility.addressing import SocketURL
from hpda.utility.configuration.interface import Section, CfgStr, CfgInt, CfgSeconds

_cherrypy_rest_config = {
	'/' : {
		'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
	}
}

class APIServer(ThreadMaster):
	"""
	Server for the API of components

	:param address: the address (interface) to listen on
	:type address: str, :py:class:`~hpda.utility.addressing.HostnameAddress`, :py:class:`~hpda.utility.addressing.IPV4Address` or :py:class:`~hpda.utility.addressing.IPV6Address`
	:param port: the port on which to publish the apis
	:type port: int
	:param threads: number of worker threads to use for servicing requests
	:type threads: int
	:param timeout: maximum connection duration
	:type timeout: int or float
	:param apis: instances of REST apis that define an api_basepath
	:type apis: list
	"""
	def __init__(self, host='0.0.0.0', port=8080, threads=5, timeout=30, apis=()):
		assert threads > 0, "API worker threads must be positive number"
		assert 0 < port < 65535, "port must be in valid port range (0-65535)"
		ThreadMaster.__init__(self)
		self.address = SocketURL(protocol="http", host=host, port=port)
		self.thread_count = threads
		self._apis = apis and set(apis) or set()
		self.logger = logging.getLogger("server.%s"%self.__class__.__name__)
		self.timeout = timeout

	@property
	def host(self):
		return self.address.host
	@property
	def port(self):
		return self.address.port

	def add_apis(self, *apis):
		self.logger.log(LVL.INFO, "Adding APIs %s",",".join(api.__class__.__name__ for api in apis))
		self._apis.update(apis)

	def main(self):
		"""Launch a server for handling REST requests"""
		with ExceptionFrame():
			self.logger.log(LVL.STATUS, "Starting cherrypy server")
			self.logger.log(LVL.INFO, "Server binding to %s",self.address)
			cherrypy.config.update(
				{
					'server.socket_port' : int(self.port),
					'server.socket_host' : str(self.host),
					'server.thread_pool' : self.thread_count,
					'response.timeout' : int(self.timeout),
					'engine.autoreload.on': False,
					'log.screen' : False,
					'log.access_file' : None,
					'log.error_file' : None,
				}
			)
			for instance in self._apis:
				self.logger.log(LVL.INFO, "Mounting API %s at %s/%s", instance.__class__.__name__, self.address, instance.api_basepath)
				cherrypy.tree.mount(instance, "/%s"% instance.api_basepath, _cherrypy_rest_config)
			cherrypy.engine.start()
			try:
				cherrypy.engine.wait(cherrypy.process.wspbus.states.EXITING, interval=1, channel='main')
			except (KeyboardInterrupt, IOError):
				self.logger.log(LVL.STATUS, 'Keyboard Interrupt: shutting down bus')
			except SystemExit:
				self.logger.log(LVL.WARNING, 'SystemExit raised: shutting down bus')
			else:
				self.logger.log(LVL.STATUS, 'Shutting down API server')
			cherrypy.engine.exit()

	def stop(self):
		cherrypy.engine.exit()

# configuration
NodeServerConfig = Section(
	attributes={
		'host' : CfgStr(default=socket.getfqdn(), descr="The address/hostname the server is listening on."),
		'port' : CfgInt(default=8080, descr="The port the server is listening on."),
		'threads'  : CfgInt(default=5, descr="Maximum number of worker threads to service concurrent requests"),
		'timeout'  : CfgSeconds(default="30s", descr="Timeout for incomming requests."),
	},
	target=APIServer,
	name="NodeServer",
	descr="HTTP server used by all components for inter-node communication."
)