#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
from sqlalchemy import TypeDecorator, String
from sqlalchemy.sql.expression import ColumnElement
from sqlalchemy.ext.compiler import compiles

class Hash(ColumnElement):
    def __init__(self, arg):
        self.arg = arg

@compiles(Hash, 'mysql')
def compile_sha1(element, compiler, **kw):
    return 'SHA1(%s)' % compiler.process(element.arg, **kw)

@compiles(Hash)
def default_false(element, compiler, **kw):
    raise Exception("Not yet implemented for this dialect!")

class HashType(TypeDecorator):
    impl = String(40)

    def bind_expression(self, bind_value):
        return Hash(bind_value)