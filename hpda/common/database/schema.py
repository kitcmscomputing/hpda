#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# third party imports
from sqlalchemy import 	create_engine, MetaData, Table, select, desc, Column,\
						Integer, BigInteger, SmallInteger, Float, String, text,\
						Text, TIMESTAMP, ForeignKey,\
						UniqueConstraint, and_, func, join, delete, outerjoin, cast
from hpda.common.database.columns import HashType


class DatabaseSchema(object):

	def __init__(self):
		self._meta_data = MetaData()
		self._tables = {}
		self._execute_on_init = []

	def init_database(self, engine):
		self._meta_data.create_all(engine)

		for entry in self._execute_on_init:
			if entry['driver'] == "all" or engine.driver == entry['driver']:
				engine.execute(entry['sql'])

	def add_table(self, name, *args, **kwargs):
		if name in self._tables:
			raise Exception("Table already exist!")
		self._tables[name] = Table(name, self._meta_data, *args, **kwargs)

	def add_execute_on_init(self, sql, driver="all"):
		self._execute_on_init.append({
			'sql': sql,
			'driver': driver
		})

	def get_table(self, name):
		"""

		:param name:
		:return:
		:rtype Table
		"""
		if name in self._tables:
			return self._tables[name]
		else:
			raise Exception("Table schema not defined!")


database_schema = DatabaseSchema()
database_schema.add_table(
	"owners",
	Column("id", BigInteger, primary_key=True, autoincrement=True),
	Column("name", String(255), unique=True, nullable=False),
	Column("level", SmallInteger, server_default=text('0')),
	mysql_engine="InnoDB",
	mysql_charset='utf8'
)
database_schema.add_table(
	"files",
	Column("id", BigInteger, primary_key=True, autoincrement=True),
	Column("hash", HashType, unique=True, nullable=False),
	Column("path", Text, nullable=False),
	Column("size", Integer, server_default=text('NULL')),
	Column("score", Float, server_default=text('0'), index=True),
	mysql_engine="InnoDB",
	mysql_charset='utf8')
database_schema.add_table(
	"nodes",
	Column("id", Integer, primary_key=True, autoincrement=True),
	Column("host", String(255), unique=True, nullable=False),
	mysql_engine="InnoDB",
	mysql_charset='utf8'
)
database_schema.add_table(
	"jobs",
	Column("id", BigInteger, primary_key=True, autoincrement=True),
	Column("batch_job_id", String(255), unique=True, nullable=False),
	Column("owner_id", BigInteger, ForeignKey("owners.id", ondelete="CASCADE")),
	Column("creation_time", TIMESTAMP, server_default=text("CURRENT_TIMESTAMP")),
	Column("file_group_id", BigInteger, ForeignKey("file_groups.id", ondelete="RESTRICT"), nullable=False),
	Column("cpu_time", Float, server_default=text("NULL")),
	Column("memory_usage", Float, server_default=text("NULL")),
	Column("wall_time", Float, server_default=text("NULL")),
	Column("exit_code", SmallInteger, server_default=text("NULL")),
	Column("node_id", Integer, ForeignKey("nodes.id"), server_default=text("NULL")),
	Column("locality_rate", Float, server_default=text("NULL")),
	Column("cachehit_rate", Float, server_default=text("NULL")),
	mysql_engine="InnoDB",
	mysql_charset='utf8'
)
database_schema.add_table(
	"file_groups",
	Column("id", BigInteger, primary_key=True, autoincrement=True),
	Column("hash", String(40), unique=True),
	Column("score", Float, server_default=text('0'), index=True),
	mysql_engine="InnoDB",
	mysql_charset='utf8'
)
database_schema.add_table(
	"file_group_files",
	Column("file_group_id", BigInteger, ForeignKey("file_groups.id", ondelete="CASCADE")),
	Column("file_id", BigInteger, ForeignKey("files.id", ondelete="CASCADE")),
	mysql_engine="InnoDB",
	mysql_charset='utf8'
)
database_schema.add_table(
	"stage_requests",
	Column("id", BigInteger, primary_key=True, autoincrement=True),
	Column("owner_id", BigInteger, ForeignKey("owners.id", ondelete="CASCADE")),
	Column("file_group_id", BigInteger, ForeignKey("file_groups.id", ondelete="CASCADE")),
	Column("request_time", TIMESTAMP, server_default=text("CURRENT_TIMESTAMP")),
	Column("priority", SmallInteger, server_default=text('0')),
	UniqueConstraint("owner_id", "file_group_id", name="owner_file_group_uq"),
	mysql_engine="InnoDB",
	mysql_charset='utf8'
)
database_schema.add_table(
	"life_time_log",
	Column("id", BigInteger, primary_key=True, autoincrement=True),
	Column("time", TIMESTAMP, server_default=text("CURRENT_TIMESTAMP")),
	Column("file_id", BigInteger, ForeignKey("files.id", ondelete="CASCADE")),
	Column("node_id", Integer, ForeignKey("nodes.id", ondelete="CASCADE")),
	Column("life_time", Integer, nullable=False),
	mysql_engine="InnoDB",
	mysql_charset='utf8'
)
database_schema.add_table(
	"coordinator_timing",
	Column("id", BigInteger, primary_key=True, autoincrement=True),
	Column("time", TIMESTAMP, server_default=text("CURRENT_TIMESTAMP"), index=True),
	Column("file_amount", BigInteger, nullable=False),
	Column("file_group_amount", BigInteger, nullable=False),
	Column("file_size_update", Integer, nullable=False),
	Column("file_group_scores", Integer, nullable=False),
	Column("update_file_scores", Integer, nullable=False),
	Column("update_node_scores", Integer, nullable=False),
	Column("fetch_available_space", Integer, nullable=False),
	Column("stage_requests", Integer, nullable=False),
	mysql_engine="InnoDB",
	mysql_charset='utf8'
)
database_schema.add_table(
	"cache_status_log",
	Column("id", BigInteger, primary_key=True, autoincrement=True),
	Column("time", TIMESTAMP, server_default=text("CURRENT_TIMESTAMP"), index=True),
	Column("node_id", Integer, ForeignKey("nodes.id", ondelete="CASCADE")),
	Column("max_size", BigInteger, nullable=False),
	Column("score_min", Float, nullable=False),
	Column("score_max", Float, nullable=False),
	Column("score_total", Float, nullable=False),
	Column("allocated", BigInteger, nullable=False),
	Column("files_total", BigInteger, nullable=False),
	mysql_engine="InnoDB",
	mysql_charset='utf8'
)
