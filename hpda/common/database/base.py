#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# standard library imports
import hashlib
from datetime import datetime, timedelta

# third party imports
from sqlalchemy import 	create_engine, MetaData, Table, select, desc, Column,\
						Integer, BigInteger, SmallInteger, Float, String, text,\
						Text, TIMESTAMP, ForeignKey,\
						UniqueConstraint, and_, func, join, delete, outerjoin, cast
from sqlalchemy.engine import Engine
from sqlalchemy.exc import IntegrityError

# application/library imports
from hpda.common.database.schema import database_schema
from hpda.utility.report import log, LVL
from hpda.utility.sqlalchemyutils import Round
from hpda.utility.configuration.interface import Section, CfgStr


def generate_file_group_hash(files):
	"""
	Generates the file group hash for a group with the given files

	:param files: List of file group files
	:type files: list[str]
	:return: hash of the file group
	:rtype: str
	"""
	files.sort()
	return hashlib.sha1(repr(",".join(files))).hexdigest()

class db_context(object):
	"""
	:type _database: database
	:type _conn_ctx: Engine._trans_ctx
	:type _conn: sqlalchemy.engine.base.Connection
	"""

	def __init__(self, db, close_with_result=False):
		self._database = db
		self._conn_ctx = db.getEngine().begin(close_with_result)

	def __enter__(self):
		self._conn = self._conn_ctx.__enter__()
		return self

	def __exit__(self, exc_type, exc_val, exc_tb):
		self._conn_ctx.__exit__(exc_type, exc_val, exc_tb)

	def get_owner_id(self, owner_name):
		"""
		Get the id for a given user. Creates one if the user does not exist.

		:param owner_name: Name of the owner
		:type owner_name: str
		:return: Id of the owner
		:rtype: int
		"""
		owners = self._database.getTable("owners")

		owner_select = select([owners.c.id]).where(owners.c.name == owner_name).limit(1)
		owner = self._conn.execute(owner_select).fetchone()

		if owner is None:
			owner_create = owners.insert().values(name = owner_name)
			try:
				owner_result = self._conn.execute(owner_create)
				owner = owner_result.inserted_primary_key
			except IntegrityError, exc:
				reason = exc.message
				if reason.endswith('is not unique'):
					owner = self._conn.execute(owner_select).fetchone()
					if owner is None:
						log('coordinator', LVL.WARNING, "Something bad happened with user %s", owner_name)
						return None

		return int(owner[0])

	def get_node_id(self, host):
		"""
		Get the node id for a given hostname. Creates one if the node does not exist.

		:param host: Hostname of the node
		:type host: str
		:return: Id of the node
		:rtype: int
		"""
		nodes = self._database.getTable("nodes")

		node_select = select([nodes.c.id]).where(nodes.c.host == host).limit(1)
		node = self._conn.execute(node_select).fetchone()

		if node is None:
			node_create = nodes.insert().values(host = host)
			try:
				node_result = self._conn.execute(node_create)
				node = node_result.inserted_primary_key
			except IntegrityError, exc:
				reason = exc.message
				if reason.endswith('is not unique'):
					node = self._conn.execute(node_select).fetchone()
					if node is None:
						log('coordinator', LVL.WARNING, "Something bad happened with node %s", host)
						return None

		return int(node[0])


	def get_file_group_id(self, files):
		"""
		Get the file group id for a list of files

		:param files: Files of the file group
		:type files: list[str]
		:return: Id of the file group
		:rtype: int
		"""
		file_groups = self._database.getTable("file_groups")
		file_groups_select = select([file_groups.c.id]).where(file_groups.c.hash == generate_file_group_hash(files)).limit(1)
		file_group = self._conn.execute(file_groups_select).fetchone()
		if file_group is None:
			return None
		else:
			return int(file_group[0])

	def get_file_group_stats(self, fields):
		file_groups = self._database.getTable("file_groups")
		file_group_files = self._database.getTable("file_group_files")
		files = self._database.getTable("files")
		availableFields = {
			'score': Round(file_groups.c.score),
			'files_amount': func.count(files.c.id).label('files_amount'),
			'size': cast(func.ifnull(func.sum(files.c.size), -1), String).label('size')
		}

		selectFields = [availableFields[field] for field in fields]
		join_tables = join(file_groups, file_group_files).join(files)
		file_groups_select = select(selectFields).select_from(join_tables).where(
			file_groups.c.score != None
		).group_by(file_groups.c.id)

		file_groups_result = self._conn.execute(file_groups_select)
		if file_groups_result is None:
			return None
		else:
			return [file_group.values() for file_group in file_groups_result]

	def get_file_groups(self, min_score = None, file_id = None, sort_score = False):

		file_groups = self._database.getTable("file_groups")
		file_groups_select = select([file_groups.c.id, file_groups.c.score])

		if file_id is not None:
			file_group_files = self._database.getTable("file_group_files")
			file_groups_select = file_groups_select.select_from(file_group_files.join(file_groups))
			file_groups_select = file_groups_select.where(file_group_files.c.file_id == file_id)

		if min_score is not None:
			file_groups_select = file_groups_select.where(file_groups.c.score > min_score)

		if sort_score:
			file_groups_select = file_groups_select.order_by(desc(file_groups.c.score))

		file_groups_result = self._conn.execute(file_groups_select)
		if file_groups_result is None:
			return None
		else:
			return file_groups_result.fetchall()

	def get_file_group_files(self, file_group_id):
		file_group_files = self._database.getTable("file_group_files")
		files = self._database.getTable("files")
		select_group_files = select([files.c.id, files.c.path, files.c.score, files.c.size]).select_from(file_group_files.join(files)).where(file_group_files.c.file_group_id == file_group_id)
		group_files = self._conn.execute(select_group_files)

		if group_files is None:
			return None
		else:
			return group_files.fetchall()

	def file_group_add(self, files):
		file_groups = self._database.getTable("file_groups")
		file_group_create = file_groups.insert().values(hash=generate_file_group_hash(files))
		file_group_result = self._conn.execute(file_group_create)
		file_group = file_group_result.inserted_primary_key
		file_group_id = int(file_group[0])

		file_group_files = self._database.getTable("file_group_files")
		for _file_id in files:
			file_group_file_insert = file_group_files.insert().values(file_group_id=file_group_id, file_id=int(_file_id))
			self._conn.execute(file_group_file_insert)

		return file_group_id

	def file_groups_cleanup(self):
		file_groups = self._database.getTable("file_groups")
		jobs = self._database.getTable("jobs")
		stage_requests = self._database.getTable("stage_requests")
		join_tables = outerjoin(file_groups, jobs).outerjoin(stage_requests)
		file_groups_select = select([file_groups.c.id]).select_from(join_tables).where(and_(jobs.c.id == None, stage_requests.c.id == None))
		file_groups_select_result = self._conn.execute(file_groups_select)
		delete_ids = [file_group.id for file_group in file_groups_select_result.fetchall()]
		if len(delete_ids) > 0:
			file_groups_delete = file_groups.delete().where(file_groups.c.id.in_(delete_ids))
			result = self._conn.execute(file_groups_delete)
			file_group_files = self._database.getTable("file_group_files")
			file_group_files_delete = file_group_files.delete().where(file_group_files.c.file_group_id.in_(delete_ids))
			result = self._conn.execute(file_group_files_delete)
			return result.rowcount
		else:
			return 0

	def file_group_update_score(self, score, fileGroupId = None):
		file_groups = self._database.getTable("file_groups")

		update_file_group_score = file_groups.update().values(score = score)
		if fileGroupId is not None:
			update_file_group_score = update_file_group_score.where(file_groups.c.id == fileGroupId)

		self._conn.execute(update_file_group_score)

	def get_jobs(self, file_group_id = None):
		jobs = self._database.getTable("jobs")
		file_group_jobs_select = select([jobs.c.creation_time, jobs.c.cpu_time, jobs.c.wall_time, jobs.c.locality_rate, jobs.c.memory_usage, jobs.c.exit_code])

		if file_group_id is not None:
			file_group_jobs_select = file_group_jobs_select.where(jobs.c.file_group_id == file_group_id)

		jobs_result = self._conn.execute(file_group_jobs_select)
		if jobs_result is None:
			return None
		else:
			return jobs_result.fetchall()

	def get_job_stats(self, fields):
		jobs = self._database.getTable("jobs")
		file_group_files = self._database.getTable("file_group_files")
		files = self._database.getTable("files")

		availableFields = {
			'creation_time': func.unix_timestamp(jobs.c.creation_time),
			'owner_id': jobs.c.owner_id,
			'node_id': jobs.c.node_id,
			'wall_time': jobs.c.wall_time,
			'cpu_time': jobs.c.cpu_time,
			'locality_rate': Round(jobs.c.locality_rate).label('locality_rate'),
			'cachehit_rate': Round(jobs.c.cachehit_rate).label('cachehit_rate'),
			'exit_code': jobs.c.exit_code,
			'memory_usage': Round(jobs.c.memory_usage).label('memory_usage'),
			'files_amount': func.count(files.c.id).label("file_amount"),
			'file_size': func.cast(func.sum(files.c.size), BigInteger).label("file_size")
		}

		selectFields = [availableFields[field] for field in fields]
		join_tables = jobs.join(file_group_files, jobs.c.file_group_id == file_group_files.c.file_group_id).join(files)
		job_stats_select = select(selectFields).select_from(join_tables).where(
			and_(
				jobs.c.cpu_time != None,
				jobs.c.wall_time != None,
				jobs.c.wall_time > 0,
				jobs.c.exit_code != None,
				jobs.c.memory_usage != None
			)).group_by(jobs.c.id)

		jobs_result = self._conn.execute(job_stats_select)
		if jobs_result is None:
			return None
		else:
			return [entry.values() for entry in jobs_result.fetchall()]

	def job_add(self, batch_job_id, owner_id, file_group_id):
		jobs = self._database.getTable("jobs")
		job_create = jobs.insert().values(batch_job_id = batch_job_id, owner_id = owner_id, file_group_id=file_group_id)
		self._conn.execute(job_create)

	def job_report(self, batch_job_id, cpu_time, memory_usage, wall_time, exit_code, node_id, locality_rate, cachehit_rate):
		jobs = self._database.getTable("jobs")
		job_update = jobs.update().values(cpu_time = cpu_time, memory_usage = memory_usage, wall_time = wall_time, exit_code = exit_code, node_id = node_id, locality_rate = locality_rate, cachehit_rate = cachehit_rate).where(jobs.c.batch_job_id == batch_job_id)
		self._conn.execute(job_update)

	def jobs_cleanup(self, timeout):
		jobs = self._database.getTable("jobs")
		timeout_datetime = datetime.now() - timedelta(seconds=timeout)
		jobs_delete = jobs.delete().where(jobs.c.creation_time < timeout_datetime)
		self._conn.execute(jobs_delete)

	def get_file_id(self, file_path):
		files = self._database.getTable("files")
		file_select = select([files.c.id]).where(files.c.hash == file_path).limit(1)
		file_data = self._conn.execute(file_select).fetchone()

		if file_data is None:
			return None
		else:
			return int(file_data[0])

	def get_file_ids(self, files):
		file_ids = []
		for file_path in files:
			file_id = self.get_file_id(file_path)

			if file_id is None:
				file_id = self.file_add(file_path)

			file_ids.append(str(file_id))
		return file_ids

	def file_add(self, file_path):
		files = self._database.getTable("files")
		file_create = files.insert().values(hash=file_path, path = file_path)
		file_result = self._conn.execute(file_create)
		return int(file_result.inserted_primary_key[0])

	def files_cleanup(self):
		files = self._database.getTable("files")
		file_group_files = self._database.getTable("file_group_files")
		life_time_log = self._database.getTable("life_time_log")
		join_tables = outerjoin(files, file_group_files)
		files_select = select([files.c.id]).select_from(join_tables).where(file_group_files.c.file_group_id == None)
		files_select_result = self._conn.execute(files_select)
		delete_ids = [delete_file.id for delete_file in files_select_result.fetchall()]
		if len(delete_ids) > 0:
			life_time_log_delete = life_time_log.delete(life_time_log.c.file_id.in_(delete_ids))
			self._conn.execute(life_time_log_delete)
			files_delete = files.delete().where(files.c.id.in_(delete_ids))
			files_delete_result = self._conn.execute(files_delete)
			return files_delete_result.rowcount
		else:
			return 0

	def file_update_score(self, file_id, score):
		files = self._database.getTable("files")
		file_update = files.update().values(score = score).where(files.c.id == int(file_id))
		self._conn.execute(file_update)

	def update_file_scores(self):
		updated_files_count = 0
		if self._database.getEngine().driver == 'mysqldb':
			result = self._conn.execute("""UPDATE files
				SET score = IFNULL((
					SELECT MAX(file_groups.score) as score
						FROM file_group_files
						LEFT JOIN file_groups ON file_groups.id = file_group_files.file_group_id
						WHERE file_group_files.file_id = files.id  GROUP BY file_group_files.file_id
				), 0)""")
			updated_files_count = result.rowcount
		else:
			files = [file_data.id for file_data in self.get_files()]
			updated_files_count = len(files)
			for file_id in files:
				file_group_scores = [file_group.score for file_group in self.get_file_groups(file_id = file_id)]
				if len(file_group_scores) > 0:
					file_score = max(file_group_scores)
					log('coordinator.database', LVL.DEBUG, "Update file  %d with new score %d", file_id, file_score)
					self.file_update_score(file_id, file_score)
		return updated_files_count

	def update_file_group_scores(self, score_evaluator):
		"""

		:param score_evaluator:
		:type score_evaluator: hpda.coordinator.ScoreEvaluator.ScoreEvaluator
		:return: Amount of updated file groups
		:rtype: int
		"""
		updated_file_groups_count = 0
		if self._database.getEngine().driver == 'mysqldb':
			self._conn.execute("SET @now = NOW()")
			self._conn.execute("SET @access_timeout = " + str(score_evaluator.file_group_usage_timeout))
			self._conn.execute("SET @access_factor = " + str(score_evaluator.stage_request_factor))
			self._conn.execute("SET @request_timeout = " + str(score_evaluator.stage_request_timeout))
			self._conn.execute("SET @request_factor = " + str(score_evaluator.stage_request_factor))
			self._conn.execute("SET @min_group_size = " + str(score_evaluator.min_file_group_size))
			self._conn.execute("SET @max_group_size = " + str(score_evaluator.max_file_group_size))
			result = self._conn.execute("""UPDATE file_groups SET score = (
				SELECT ((IFNULL(access_score, 0) * @access_factor + IFNULL(request_score, 0) * @request_factor) * size_score * null_score) AS score
					FROM (SELECT file_group_id, (SUM(calculate_time_score(@now, creation_time, @access_timeout)) * MAX(calculate_time_score(@now, creation_time, @access_timeout)) ) AS access_score FROM `jobs` GROUP BY `file_group_id`) AS acccess
				  		LEFT JOIN (SELECT file_group_id, (SUM(calculate_time_score(@now, request_time, @request_timeout)) * MAX(calculate_time_score(@now, request_time, @request_timeout)) ) AS request_score FROM `stage_requests` GROUP BY `file_group_id`) AS requests USING(file_group_id)
				  		LEFT JOIN (SELECT file_group_id, (IFNULL(SUM(files.size), 0) BETWEEN @min_group_size AND @max_group_size) as size_score, IF(SUM(IF(files.size IS NOT NULL, 0, 1)) > 0, 0, 1) as null_score FROM file_group_files LEFT JOIN files ON file_group_files.file_id = files.id GROUP BY file_group_id) as size USING(file_group_id)
					WHERE file_group_id = file_groups.id
				)""")
			updated_file_groups_count = result.rowcount

		else:
			now = datetime.now()
			file_groups = self.get_file_groups()
			updated_file_groups_count = len(file_groups)
			for file_group in file_groups:
				file_group_jobs = self.get_jobs(file_group.id)
				stage_requests = self.get_stage_requests(file_group.id)
				file_group_files = self.get_file_group_files(file_group.id)
				access_list = [job.creation_time for job in file_group_jobs]
				request_list = [stage_request.request_time for stage_request in stage_requests]
				file_sizes = [file_group_file.size for file_group_file in file_group_files]
				group_score = score_evaluator.calculate_file_group_score(now, access_list, request_list, file_sizes)
				log('coordinator.database', LVL.DEBUG, "Update file group  %d with new score %d", file_group.id, group_score)
				self.file_group_update_score(group_score, file_group.id)
		return updated_file_groups_count

	def file_update_size(self, file_id, size):
		files = self._database.getTable("files")
		file_update = files.update().values(size = size).where(files.c.id == int(file_id))
		self._conn.execute(file_update)

	def get_files(self, paths = None, size_is_null = False):
		files = self._database.getTable("files")
		files_select = select([files.c.id, files.c.path, files.c.score])

		if paths is not None:
			files_select = files_select.where(files.c.path.in_(paths))

		if size_is_null:
			files_select = files_select.where(files.c.size == None)

		result = self._conn.execute(files_select)
		if result is None:
			return []
		else:
			return result.fetchall()

	def stage_request_add(self, owner_id, file_group_id):
		stage_requests = self._database.getTable("stage_requests")
		try:
			stage_request_create = stage_requests.insert().values(owner_id = owner_id, file_group_id = file_group_id)
			self._conn.execute(stage_request_create)
		except IntegrityError, exc:
			reason = exc.message
			if reason.endswith('is not unique'):
				stage_request_update = stage_requests.update().values(request_time = text("CURRENT_TIMESTAMP")) \
											.where(and_(stage_requests.c.owner_id == owner_id, stage_requests.c.file_group_id == file_group_id))
				self._conn.execute(stage_request_update)

	def stage_request_cleanup(self, timeout):
		stage_requests = self._database.getTable("stage_requests")
		timeout_datetime = datetime.now() - timedelta(seconds=timeout)
		stage_requests_delete = stage_requests.delete().where(stage_requests.c.request_time < timeout_datetime)
		self._conn.execute(stage_requests_delete)

	def stage_request_remove(self, owner_id, file_group_id):
		stage_requests = self._database.getTable("stage_requests")
		stage_request_delete = stage_requests.delete().where(and_(stage_requests.c.file_group_id == file_group_id, stage_requests.c.owner_id == owner_id))
		self._conn.execute(stage_request_delete)

	def get_stage_requests(self, file_group_id = None):
		stage_requests = self._database.getTable("stage_requests")
		stage_request_select = select([stage_requests.c.request_time])

		if file_group_id is not None:
			stage_request_select = stage_request_select.where(stage_requests.c.file_group_id == file_group_id)

		result = self._conn.execute(stage_request_select)
		if result is None:
			return None
		else:
			return result.fetchall()

	def life_time_log_add(self, file_id, node_id, life_time):
		life_time_log = self._database.getTable("life_time_log")
		log_entry_create = life_time_log.insert().values(file_id = file_id, node_id = node_id, life_time = life_time)
		self._conn.execute(log_entry_create)

	def life_time_log_cleanup(self, timeout):
		life_time_log = self._database.getTable("life_time_log")
		timeout_datetime = datetime.now() - timedelta(seconds=timeout)
		life_time_log_delete = life_time_log.delete().where(life_time_log.c.time < timeout_datetime)
		self._conn.execute(life_time_log_delete)

	def coordinator_timing_add(self, file_amount, file_group_amount, file_size_update, file_group_scores, update_file_scores, update_node_scores, fetch_available_space, stage_requests):
		coordinator_timing = self._database.getTable("coordinator_timing")
		log_entry_create = coordinator_timing.insert().values(
			file_amount = file_amount,
			file_group_amount = file_group_amount,
			file_size_update = file_size_update,
			file_group_scores = file_group_scores,
			update_file_scores = update_file_scores,
			update_node_scores = update_node_scores,
			fetch_available_space = fetch_available_space,
			stage_requests = stage_requests
		)
		self._conn.execute(log_entry_create)

	def coordinator_timing_cleanup(self, timeout):
		coordinator_timing = self._database.getTable("coordinator_timing")
		timeout_datetime = datetime.now() - timedelta(seconds=timeout)
		coordinator_timing_delete = coordinator_timing.delete().where(coordinator_timing.c.time < timeout_datetime)
		self._conn.execute(coordinator_timing_delete)

	def coordinator_timing_stats(self, fields):
		coordinator_timing = self._database.getTable("coordinator_timing")

		availableFields = {
			'time': func.unix_timestamp(coordinator_timing.c.time),
			'file_amount': coordinator_timing.c.file_amount,
			'file_group_amount': coordinator_timing.c.file_group_amount,
			'file_size_update': coordinator_timing.c.file_size_update,
			'file_group_scores': coordinator_timing.c.file_group_scores,
			'update_file_scores': coordinator_timing.c.update_file_scores,
			'update_node_scores': coordinator_timing.c.update_node_scores,
			'fetch_available_space': coordinator_timing.c.fetch_available_space,
			'stage_requests': coordinator_timing.c.stage_requests
		}

		selectFields = [availableFields[field] for field in fields]
		coordinator_timing_select = select(selectFields)
		coordinator_timing_results = self._conn.execute(coordinator_timing_select)
		if coordinator_timing_results is None:
			return None
		else:
			return [entry.values() for entry in coordinator_timing_results.fetchall()]

	def cache_status_add(self, node_id, max_size, score_min, score_max, score_total, allocated, files_total):
		cache_status_log = self._database.getTable("cache_status_log")
		cache_status_create = cache_status_log.insert().values(
			node_id = node_id,
			max_size = max_size,
			score_min = score_min,
			score_max = score_max,
			score_total = score_total,
			allocated = allocated,
			files_total = files_total
		)
		self._conn.execute(cache_status_create)

	def cache_status_cleanup(self, timeout):
		cache_status_log = self._database.getTable("cache_status_log")
		timeout_datetime = datetime.now() - timedelta(seconds=timeout)
		cache_status_log_delete = cache_status_log.delete().where(cache_status_log.c.time < timeout_datetime)
		self._conn.execute(cache_status_log_delete)

	def cache_status_stats(self, fields):
		cache_status_log = self._database.getTable("cache_status_log")
		availableFields = {
			'time': func.unix_timestamp(cache_status_log.c.time),
			'node_id': cache_status_log.c.node_id,
			'max_size': cache_status_log.c.max_size,
			'score_min': cache_status_log.c.score_min,
			'score_max': cache_status_log.c.score_max,
			'score_total': cache_status_log.c.score_total,
			'allocated': cache_status_log.c.allocated,
			'files_total': cache_status_log.c.files_total
		}
		selectFields = [availableFields[field] for field in fields]

		cache_status_log_select = select(selectFields)
		cache_status_results = self._conn.execute(cache_status_log_select)
		if cache_status_results is None:
			return None
		else:
			return [entry.values() for entry in cache_status_results.fetchall()]

class database(object):
	"""

	:type _url: str
	:type _engine: Engine
	:type _schema: DatabaseSchema
	"""

	def __init__(self, url, context = db_context, echo = False, init_database = False):
		self._url = url
		self._engine = create_engine(self._url, echo=echo)
		self._schema = database_schema
		self._context = db_context

		if init_database:
			self._schema.init_database(self._engine)

	def getEngine(self):
		"""

		:rtype: Engine
		"""
		return self._engine

	def getTable(self, name):
		"""Get the sqlAlchemy Table object for the given table

		:param name: Name of the table
		:type name: str
		:return: Table object of the table
		:rtype: Table
		"""
		return self._schema.get_table(name)

	def begin(self, close_with_result=False):
		"""

		:rtype: db_context
		"""
		return self._context(self, close_with_result)


DatabaseConfig = Section(
	name="database",
	target=database,
	attributes={
		"url":	CfgStr(default="sqlite:///:memory:")
	}
)