#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.

# standard library imports

# third party imports

# application/library imports

def initialize_node(configuration):
	"""
	Initialize the basic services of the node

	Unlike the component initialisation, this function provides the most basic
	building blocks that other components rely on.

	:param configuration: Configuration for this node
	:type configuration: hpda.utility.configuration.Configuration
	:return: PoolMapper and APIServer
	:rtype: pool_mapper
	"""
	raise NotImplementedError

def initialize(pool_mapper, configuration):
	"""
	Initialize the components of this sub-component

	The common components are not part of the normal operation of the cluster.
	Instead, they are interfaces to attached resources, for example a Batch System
	worker node on an HPDA cache node.

	:param pool_mapper: mapper for the pool and its nodes
	:type pool_mapper: common.pool_mapper.HeartbeatMapper
	:param configuration: Configuration for this node
	:type configuration: hpda.utility.configuration.Configuration

	:return: Threads to monitor and APIs to publish
	:rtype: (list[utility.threads.ThreadMaster],list[object])
	"""
	from worker import AnyWorkerConfig, WorkerInfoAPI
	thread_masters = []
	server_apis = []
	# information services
	worker_interface = AnyWorkerConfig.apply(configuration)
	server_apis.append(WorkerInfoAPI(worker_interface))
	return thread_masters, server_apis
