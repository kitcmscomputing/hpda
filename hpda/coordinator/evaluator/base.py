#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# base imports
import time

# library imports

# custom imports
from hpda.coordinator.evaluator.ScoreEvaluator import ScoreEvaluator
from hpda.utility.report import log, LVL
from hpda.utility.threads import ThreadMaster
from hpda.utility.configuration.interface import Section, CfgSeconds, ConfigMultiAttribute, CfgCronTab


class Evaluator(ThreadMaster):
	"""

	:type _database: database
	:type _pool_mapper: HeartbeatMapper
	:type _scoreEvaluator: ScoreEvaluator
	"""

	def __init__(self, database, update_interval, store_jobs_timeout, store_stage_requests_timeout, scoreEvaluator):
		"""Initializes the Coordinator

		:type database: database
		:type update_interval: int
		:type scoreEvaluator: ScoreEvaluator
		:rtype: void
		"""
		ThreadMaster.__init__(self)
		self._database = database
		self.loop_interval = update_interval
		self._store_jobs_timeout = store_jobs_timeout
		self._store_stage_requests_timeout = store_stage_requests_timeout
		self._scoreEvaluator = scoreEvaluator

	def payload(self):

		start_time = time.time()
		log('coordinator.evaluator', LVL.STATUS, "Cleanup database!")
		self._cleanup()
		cleanup_time = time.time() - start_time
		log('coordinator.evaluator', LVL.STATUS, "Finished cleanup after %03.3fs", cleanup_time)

		start_time = time.time()
		log('coordinator', LVL.STATUS, "Updating file group scores...")
		file_group_amount = self._calculate_file_group_scores()
		file_group_scores_time = time.time() - start_time
		log('coordinator.evaluator', LVL.STATUS, "Finished file group update after %03.3fs", file_group_scores_time)

		start_time = time.time()
		log('coordinator.evaluator', LVL.STATUS, "Updating file scores...")
		file_amount = self._calculate_file_scores()
		update_file_scores_time = time.time( ) -start_time
		log('coordinator.evaluator', LVL.STATUS, "Finished file update after %03.3fs", update_file_scores_time)

	def _cleanup(self):
		with self._database.begin() as db:
			db.jobs_cleanup(self._store_jobs_timeout)
			db.stage_request_cleanup(self._store_stage_requests_timeout)
			db.file_groups_cleanup()
			db.files_cleanup()

	def _calculate_file_group_scores(self):
		"""
		Calculates the score for all file groups

		:rtype: void
		"""
		with self._database.begin() as db:
			updated_amount = db.update_file_group_scores(self._scoreEvaluator)
			log('coordinator.evaluator', LVL.INFO, "Amount of updated file groups:  %d", updated_amount)
			return updated_amount

	def _calculate_file_scores(self):
		"""
		Calculates the score for all files based on their file group scores

		:rtype: void
		"""
		with self._database.begin() as db:
			updated_amount = db.update_file_scores()
			log('coordinator.evaluator', LVL.INFO, "Amount of updated files:  %d", updated_amount)
			return updated_amount

EvaluatorConfig = Section(
	name="CoordinatorEvaluator",
	target=Evaluator,
	attributes={
		"update_interval":	ConfigMultiAttribute(
			variants=(
				CfgSeconds(),
				CfgCronTab(default="0 * * * *")
			)
		),
		"store_jobs_timeout": CfgSeconds(default="6M"),
		"store_stage_requests_timeout": CfgSeconds(default="6M")
	}
)
