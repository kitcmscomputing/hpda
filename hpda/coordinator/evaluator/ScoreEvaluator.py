#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# base imports

# library imports

# custom imports
from hpda.utility.configuration.interface import Section, CfgSeconds, CfgFloat, CfgBytes

class ScoreEvaluator(object):
	"""
	:type file_group_usage_timeout: int
	:type file_group_usage_factor: float
	:type stage_request_timeout: int
	:type stage_request_factor: float
	"""

	def __init__(self, file_group_usage_timeout, file_group_usage_factor,
					stage_request_timeout, stage_request_factor,
					min_file_group_size, max_file_group_size):
		"""Initializes the ScoreEvaluator

		:param file_group_usage_timeout: Time a file group access should be taken into account
		:type file_group_usage_timeout: int
		:param file_group_usage_factor: Weighting factor for file accesses
		:type file_group_usage_factor: float
		:param stage_request_timeout: Time a stage request should be taken into account
		:type stage_request_timeout: int
		:param stage_request_factor: Weighting factor for stage requests
		:type stage_request_factor: float
		:rtype: void
		"""
		self.file_group_usage_timeout = file_group_usage_timeout
		self.file_group_usage_factor = file_group_usage_factor
		self.stage_request_timeout = stage_request_timeout
		self.stage_request_factor = stage_request_factor
		self.min_file_group_size = min_file_group_size
		self.max_file_group_size = max_file_group_size

	def calculate_file_group_score(self, now, access_list, request_list, file_sizes):
		"""Calculate the score for a file group

		:param now: Time of the calculation
		:type now: datetime.datetime
		:param access_list: Access times of the file group
		:type access_list: list[datetime.datetime]
		:param request_list: Times of the stage requests for the file group
		:type request_list: list[datetime.datetime]
		:rtype: void
		"""
		score = 0
		score += self._calculate_usage_score(now, access_list)
		score += self._calculate_requests_score(now, request_list)
		score *= self._calculate_sizes_factor(file_sizes)
		return score

	@staticmethod
	def _calculate_time_score(now, ts, timeout):
		"""Calculates a score

		:type now: datetime.datetime
		:type ts: datetime.datetime
		:type timeout: int
		:rtype: float
		"""
		return 1 - min(1, (now - ts).total_seconds() / timeout)

	def _calculate_access_score(self, now, last_access):
		"""Calculates the score of a file group access

		:type now: datetime.datetime
		:type last_access: datetime.datetime
		:rtype: float
		"""
		return self._calculate_time_score(now, last_access, self.file_group_usage_timeout)

	def _calculate_request_score(self, now, request_time):
		"""Calculates the score of a stage request

		:type now: datetime.datetime
		:type request_time: datetime.datetime
		:rtype: float
		"""
		return self._calculate_time_score(now, request_time, self.stage_request_timeout)

	def _calculate_usage_score(self, now, access_list):
		"""Calculates the usage score of a file group

		:type now: datetime.datetime
		:type access_list: list[datetime.datetime]
		:rtype: float
		"""
		score_sum = 0
		if len(access_list) < 1:
			return score_sum

		for access_time in access_list:
			score_sum += self._calculate_access_score(now, access_time)

		return score_sum * self._calculate_access_score(now, max(access_list)) * self.file_group_usage_factor

	def _calculate_requests_score(self, now, request_list):
		"""Calculates the score for all stage requests of the file group

		:type now: datetime.datetime
		:type request_list: list[datetime.datetime]
		:type: float
		"""
		score_sum = 0
		if len(request_list) < 1:
			return score_sum

		for request_time in request_list:
			score_sum += self._calculate_request_score(now, request_time)

		return score_sum * self.stage_request_factor


	def _calculate_sizes_factor(self, file_sizes):
		if len(file_sizes) < 1:
			return 0.0

		file_group_size = 0
		for file_size in file_sizes:
			if file_size is None:
				return 0.0
			file_group_size += file_size

		if file_group_size < self.min_file_group_size or file_group_size > self.max_file_group_size:
			return 0.0
		else:
			return 1.0

ScoreEvaluatorConfig = Section(
	name="ScoreEvaluator",
	target=ScoreEvaluator,
	attributes={
		"file_group_usage_timeout": CfgSeconds(default="31d"),
		"file_group_usage_factor": CfgFloat(default=10.0),
		"stage_request_timeout": CfgSeconds(default="31d"),
		"stage_request_factor": CfgFloat(default=10.0),
		"min_file_group_size": CfgBytes(default="1MB"),
		"max_file_group_size": CfgBytes(default="100GB")
	}
)