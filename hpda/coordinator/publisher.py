#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# base imports
import time
import random

# library imports

# custom imports
from hpda.client.cacheproxy import CacheProxy
from hpda.common.pool_mapper import NodeRole
from hpda.coordinator.evaluator.ScoreEvaluator import ScoreEvaluator
from hpda.utility.report import log, LVL
from hpda.utility.threads import ThreadMaster
from hpda.utility.configuration.interface import Section, CfgSeconds, CfgInt, CfgCronTab, ConfigMultiAttribute


class Publisher(ThreadMaster):
	"""
	:type _database: database
	:type _pool_mapper: HeartbeatMapper
	:type _catalogue: hpda.locator.catalogue.Catalogue
	:type _max_threads: int
	:type _justStarted: bool
	"""

	def __init__(self, pool_mapper, catalogue, database, update_interval, max_threads):
		"""Initializes the coordinator publisher

		:type pool_mapper: HeartbeatMapper
		:type catalogue: hpda.locator.catalogue.Catalogue
		:type database: database
		:type update_interval: int
		:type scoreEvaluator: ScoreEvaluator
		:rtype: void
		"""
		ThreadMaster.__init__(self)
		self._database = database
		self.loop_interval = update_interval
		self._max_threads = max_threads
		self._pool_mapper = pool_mapper
		self._catalogue = catalogue
		self._node_min_scores = {}
		self._justStarted = True

	def payload(self):
		if self._justStarted:
			log('coordinator.publisher', LVL.STATUS, "Coordinator is just started! Skip publishing...")
			self._justStarted = False
			return

		self._catalogue.maintain_nodes()
		self._catalogue.update_nodes(True)

		start_time = time.time()
		log('coordinator.publisher', LVL.STATUS, "Updating scores on nodes...")
		self._update_file_scores_on_nodes()
		update_node_scores_time = time.time()-start_time
		log('coordinator.publisher', LVL.STATUS, "Finished nodes update after %03.3fs", update_node_scores_time)

		start_time = time.time()
		log('coordinator.publisher', LVL.STATUS, "Get available space information from nodes...")
		self._fetch_available_space()
		fetch_available_space_time = time.time()-start_time
		log('coordinator.publisher', LVL.STATUS, "Finished get available space information after %03.3fs", fetch_available_space_time)

		start_time = time.time()
		log('coordinator.publisher', LVL.STATUS, "Try Staging files...")
		self._stage_files()
		stage_requests_time = time.time() - start_time
		log('coordinator.publisher', LVL.STATUS, "Finished staging after %03.3fs", stage_requests_time)


	def _update_file_scores_on_nodes(self):
		"""
		Update the file scores on each node

		:rtype: void
		"""
		cacheNodes = self._pool_mapper.get_nodes_by_role(NodeRole.CACHE)
		if not cacheNodes:
			log('coordinator.publisher', LVL.WARNING, "Update file scores on nodes not possible: No Cache found!")
			return

		self._node_min_scores.clear()
		with self._database.begin() as db:
			for cacheNode in cacheNodes:
				node_min_score = None
				node_data = self._catalogue.get_node(cacheNode.id)
				if node_data is not None:
					if len(node_data.files)  < 1:
						log('coordinator.publisher', LVL.INFO, "Update file scores is not necessary on node %s, because there are no files staged.", cacheNode.url)
						self._node_min_scores[cacheNode.id] = 0
						continue

					update_files = []
					unknown_files = list(node_data.files)
					for file_data in db.get_files(node_data.files):
						update_files.append({"source_uri": file_data.path, "score": file_data.score})
						if file_data.path in unknown_files:
							unknown_files.remove(file_data.path)

						if node_min_score is None or node_min_score > file_data.score:
							node_min_score = file_data.score

					if len(unknown_files) > 0:
						log('coordinator.publisher', LVL.WARNING, "There are %d unknown files on node %s!", len(unknown_files), cacheNode.url)
						node_min_score = 0
						for path in unknown_files:
							update_files.append({"source_uri": path, "score": 0})

					log('coordinator.publisher', LVL.STATUS, "Update %d file scores on node %s", len(update_files), cacheNode.url)
					cache = CacheProxy(cacheNode, 120)
					cache.update_files(update_files)
					log('coordinator.publisher', LVL.INFO, "Minimum File Score of node %s: %d", cacheNode.url, node_min_score)
					self._node_min_scores[cacheNode.id] = node_min_score
				else:
					log('coordinator.publisher', LVL.WARNING, "Update file scores on node %s failed! Reason: No Data from Locator.", cacheNode.url)

	def _fetch_available_space(self):
		"""Fetches the available space from cache nodes

		:rtype: void
		"""
		cacheNodes = self._pool_mapper.get_nodes_by_role(NodeRole.CACHE)
		self._available_space = {}
		for node in cacheNodes:
			cache = CacheProxy(node)
			available_space = 0
			allocation_data = cache.get_cache_features()
			for cacheDevice in allocation_data:
				available_space += cacheDevice['allocation']['max_size'] - cacheDevice['allocation']['allocated']
			log('coordinator', LVL.INFO, "Available Space on node %s: %d", node.url, available_space)
			self._available_space[node.id] = available_space

	def _get_stage_nodes_candidates(self, score, size):
		"""
		Return a list of cache nodes which have a lower minimum score than given
		or enough free space available

		:param score: Score which have to be cached
		:type score: int
		:param size: Required free space
		:type size: int
		:return: Node Ids of the cache candidates
		:rtype list[str]
		"""
		cacheNodes = self._pool_mapper.get_nodes_by_role(NodeRole.CACHE)
		candidates = []
		for node in cacheNodes:
			if score <= self._node_min_scores[node.id] and self._available_space[node.id] < size:
				continue
			candidates.append(node.id)
		return candidates

	def _stage_files(self):
		"""
		Send stage requests to caches

		:rtype: void
		"""
		cacheNodes = self._pool_mapper.get_nodes_by_role(NodeRole.CACHE)
		if not cacheNodes:
			log('coordinator.publisher', LVL.WARNING, "Staging file not possible: No Cache Node found!")
			return
		log('coordinator.publisher', LVL.INFO, "There are %d cache nodes available for staging!", len(cacheNodes))

		node_requests = {}
		with self._database.begin() as db:
			file_groups = db.get_file_groups(min_score=0, sort_score=True)
			log('coordinator.publisher', LVL.INFO, "There are %d file groups to check for staging!", len(file_groups))
			for file_group in file_groups:
				group_files = db.get_file_group_files(file_group.id)

				files_to_stage = []
				files_to_stage_coordinator = []
				file_group_min_score = None
				file_group_size = 0
				for group_file in group_files:
					file_info = {
						"source_uri": group_file.path,
						"score"		: group_file.score,
					}
					files_to_stage.append(group_file.path)
					files_to_stage_coordinator.append(file_info)

					# If the group_file size is None something have gone wrong, skip this group
					if group_file.size is None:
						log('coordinator.publisher', LVL.WARNING, "Size of file %d is None, skipping file group %d!", file_group.id, group_file.id)
						break

					file_group_size += group_file.size

					if file_group_min_score is None or file_group_min_score > group_file.score:
						file_group_min_score = group_file.score
				else:
					file_group_cached = False
					file_count_on_nodes = self._catalogue.check_nodes_for_files(files_to_stage, "id")

					for node_id, node_file_count in file_count_on_nodes.items():
						if node_file_count >= len(files_to_stage):
							file_group_cached = True
							log('coordinator.publisher', LVL.DEBUG, "File group %d is already staged on node %s", file_group.id, node_id)
							break

					if file_group_cached:
						continue

					candidate_nodes = self._get_stage_nodes_candidates(file_group_min_score, file_group_size)
					if len(candidate_nodes) < 1:
						log('coordinator.publisher', LVL.INFO, "All nodes have higher scores and not enough free space. Done with staging.")
						log('coordinator.publisher', LVL.INFO, "File Group: %d\tScore: %d\tSize %d", file_group.id, file_group_min_score, file_group_size)
						break

					cache_node_id = random.choice(candidate_nodes)
					for node_id in candidate_nodes:
						if cache_node_id is None or file_count_on_nodes[node_id] > file_count_on_nodes[cache_node_id]:
							cache_node_id = node_id

					if cache_node_id is None:
						log('coordinator.publisher', LVL.WARNING, "No Cache node found to cache file group %d!", file_group.id)
						continue

					if not node_requests.has_key(cache_node_id):
						node_requests[cache_node_id] = []

					log('coordinator.publisher', LVL.DEBUG, "Add file group %d to stage list of node %s", file_group.id, node_id)
					node_requests[cache_node_id].append (files_to_stage_coordinator)
					self._available_space[cache_node_id] -= file_group_size

		for node_id in node_requests.keys():
			node = self._pool_mapper.get_node(node_id)
			cache = CacheProxy(node)
			log('coordinator.publisher', LVL.STATUS, "Send stage request for %d file groups to node %s", len(node_requests[node_id]), node.url)

			cache.stage_file_groups(node_requests[node_id])

PublisherConfig = Section(
	name="CoordinatorPublisher",
	target=Publisher,
	attributes={
		"update_interval":	ConfigMultiAttribute(
			variants=(
				CfgSeconds(),
				CfgCronTab(default="0 * * * *")
			)
		),
		"max_threads": CfgInt(default=1)
	}
)