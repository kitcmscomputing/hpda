#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# standard library imports

# third party imports

# application/library imports
from hpda.coordinator.collector.base import CollectorConfig
from hpda.coordinator.collector.api.jobs import JobsAPI
from hpda.coordinator.collector.api.stage import StageAPI
from hpda.coordinator.statistics.api.stats import StatsAPI
from hpda.coordinator.evaluator.ScoreEvaluator import ScoreEvaluator, ScoreEvaluatorConfig
from hpda.common.database.base import DatabaseConfig
from hpda.common.database.schema import database_schema
from hpda.coordinator.evaluator.base import EvaluatorConfig
from hpda.coordinator.publisher import PublisherConfig
from hpda.coordinator.statistics.base import StatisticsConfig
from hpda.utility.configuration.interface import Section, CfgStrArray
from hpda.utility.enum import StaticEnum


class CoordinatorComponent(StaticEnum):
	"""
	Roles supplied by a Node of the pool

	:cvar Collector: Node collects information and store them into database
	:cvar Evaluator: Node evaluates file and file group scores
	:cvar Publisher: Node take care of populating the caches
	:cvar Statistics: Node gathers statistics and make them available
	"""
	__members__ = ['Collector', 'Evaluator', 'Publisher', 'Statistics']

CoordinatorConfig = Section(
	name="Coordinator",
	attributes={
		"components" : CfgStrArray(default='')
	}
)

database_schema.add_execute_on_init("DROP FUNCTION IF EXISTS calculate_time_score", "mysql")
database_schema.add_execute_on_init("""
	CREATE FUNCTION calculate_time_score(now_ts DATETIME, ts DATETIME, timeout INT) RETURNS FLOAT
		BEGIN
			DECLARE score FLOAT DEFAULT 0;

			IF now_ts IS NULL OR ts IS NULL OR timeout IS NULL THEN
				RETURN 0;
			END IF;

			SET score =  1 - (TIMESTAMPDIFF(SECOND, ts, now_ts) / timeout);
			IF score > 1 THEN
				RETURN 1.0;
			END IF;

			IF score < 0 THEN
				RETURN 0.0;
			END IF;

			RETURN score;
		END
""", "mysql")

def initialize(mapper, locator_catalogue, configuration, init_database):
	"""
	Initialize the components of this sub-component

	 Configures the resources and instantiates the objects this sub-component
	requires for normal operation, given the supplied configuration. Returns the
	threads to be managed by the NodeMaster and the APIs to be published.

	:type mapper: hpda.common.pool_mapper.HeartbeatMapper
	:type locator_catalogue hpda.locator.catalogue.Catalogue
	:type init_database: Boolean
	:return: (list[..utility.threads.ThreadMaster],list[object])
	"""

	thread_masters = []
	server_apis = []

	coordinator_config = CoordinatorConfig.kwargs(configuration)
	coordinator_components = [CoordinatorComponent(component) for component in coordinator_config['components']]

	# Shared Resources...
	db = DatabaseConfig.apply(configuration, target_kwargs=dict(init_database=init_database))

	if CoordinatorComponent.Collector in coordinator_components:
		collectorThread = CollectorConfig.apply(configuration, target_kwargs=dict(pool_mapper=mapper, catalogue=locator_catalogue, database=db))
		thread_masters.append(collectorThread)
		server_apis.append(JobsAPI(collectorThread))
		server_apis.append(StageAPI(collectorThread))

	if CoordinatorComponent.Evaluator in coordinator_components:
		scoreEvaluator = ScoreEvaluatorConfig.apply(configuration)
		evaluatorThread = EvaluatorConfig.apply(configuration, target_kwargs=dict(database=db, scoreEvaluator=scoreEvaluator))
		thread_masters.append(evaluatorThread)

	if CoordinatorComponent.Publisher in coordinator_components:
		publisherThread = PublisherConfig.apply(configuration, target_kwargs=dict(pool_mapper=mapper, catalogue=locator_catalogue, database=db))
		thread_masters.append(publisherThread)

	if CoordinatorComponent.Statistics in coordinator_components:
		statisticsThread = StatisticsConfig.apply(configuration, target_kwargs=dict(pool_mapper=mapper, database=db))
		thread_masters.append(statisticsThread)
		server_apis.append(StatsAPI(statisticsThread))

	return thread_masters, server_apis