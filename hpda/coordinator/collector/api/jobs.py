#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# base imports

# library imports
import cherrypy

# custom imports
from hpda.utility.exceptions import APIError
from hpda.utility.report        import log, LVL
from hpda.utility.validation    import SchemaPrimitive, SchemaMap, SchemaArray, Schema

_JobSchema = SchemaMap(
	members =
	{
		"owner"  : SchemaPrimitive(types=basestring, required = True),
		"files"  : SchemaArray(members=[SchemaPrimitive(types=basestring)], minsize=1, required = True)
	}
)
JobSchema = Schema(_JobSchema)

_JobReportSchema = SchemaMap(
	members=
	{
		"cpu_time" : SchemaPrimitive(types=float, required = True),
		"memory_usage" : SchemaPrimitive(types=float, required = True),
		"wall_time" : SchemaPrimitive(types=float, required = True),
		"exit_code" : SchemaPrimitive(types=int, required = True),
		"node"		: SchemaPrimitive(types=basestring, required = True),
		"locality_rate" : SchemaPrimitive(types=float, required = True),
		"cachehit_rate" : SchemaPrimitive(types=float, required = True),
	}
)
JobReportSchema = Schema(_JobReportSchema)

class JobsAPI(object):
	"""

	:type _collector: hpda.coordinator.collector.base.Collector
	"""

	exposed = True
	api_basepath = "coordinator/jobs"

	def __init__(self, collector):
		self._collector = collector

	@cherrypy.tools.json_out()
	@cherrypy.tools.json_in()
	def POST(self, job_id):
		message = cherrypy.request.json

		if JobReportSchema.validate(message):
			self._collector.job_report(job_id, message['cpu_time'], message['memory_usage'], message['wall_time'], message['exit_code'], message['node'], message['locality_rate'], message['cachehit_rate'])
			return {}
		else:
			validation_report = JobReportSchema.report(message)
			log('server', LVL.STATUS, "Incompatible request:\n%s",validation_report)
			raise cherrypy.HTTPError(422, "Incorrect request content. Expected JobReportSchema")

	@cherrypy.tools.json_out()
	@cherrypy.tools.json_in()
	def PUT(self, job_id):
		message = cherrypy.request.json

		if JobSchema.validate(message):
			try:
				self._collector.job_add(job_id, message['owner'], message['files'])
			except APIError as error:
				raise cherrypy.HTTPError(422, "API Error: " + error.message)

			return {}
		else:
			validation_report = JobSchema.report(message)
			log('server', LVL.STATUS, "Incompatible request:\n%s",validation_report)
			raise cherrypy.HTTPError(422, "Incorrect request content. Expected JobSchema")