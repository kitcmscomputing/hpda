#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# base imports

# library imports
import cherrypy

# custom imports
from hpda.utility.report        import log, LVL
from hpda.utility.validation    import SchemaPrimitive, SchemaMap, SchemaArray, Schema

_StageRequestSchema = SchemaMap(
	members =
	{
		"owner"  : SchemaPrimitive(types=basestring, required = True),
		"files"  : SchemaArray(members=[SchemaPrimitive(types=basestring)], minsize=1, required = True)
	}
)
StageRequestSchema = Schema(_StageRequestSchema)

class StageAPI(object):
	"""

	:type _collector: hpda.coordinator.collector.base.Collector
	"""

	exposed = True
	api_basepath = "coordinator/stage"

	def __init__(self, collector):
		self._collector = collector

	@cherrypy.tools.json_out()
	@cherrypy.tools.json_in()
	def POST(self):
		message = cherrypy.request.json
		if StageRequestSchema.validate(message):
			self._collector.stage_request(message['owner'], message['files'])
			return {}
		else:
			validation_report = StageRequestSchema.report(message)
			log('server', LVL.STATUS, "Incompatible request:\n%s",validation_report)
			raise cherrypy.HTTPError(422, "Incorrect request content. Expected StageRequestSchema")

	@cherrypy.tools.json_out()
	@cherrypy.tools.json_in()
	def DELETE(self):
		message = cherrypy.request.json
		if StageRequestSchema.validate(message):
			self._collector.remove_stage_request(message['owner'], message['files'])
			return {}
		else:
			validation_report = StageRequestSchema.report(message)
			log('server', LVL.STATUS, "Incompatible request:\n%s",validation_report)
			raise cherrypy.HTTPError(422, "Incorrect request content. Expected StageRequestSchema")