#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# base imports
import threading
import time
from datetime import datetime
import random

# library imports

# custom imports
import math
from hpda.client.cacheproxy import CacheProxy
from hpda.common.pool_mapper import NodeRole
from hpda.utility.report import log, LVL
from hpda.utility.exceptions import ExceptionFrame, APIError
from hpda.utility.threads import ThreadMaster
from hpda.utility.configuration.interface import Section, CfgSeconds, ConfigMultiAttribute, CfgCronTab


class Collector(ThreadMaster):
	"""

	:type _database: database
	:type _pool_mapper: HeartbeatMapper
	:type _catalogue: hpda.locator.catalogue.Catalogue
	"""

	def __init__(self, pool_mapper, catalogue, database, update_interval):
		"""Initializes the Coordinator

		:type pool_mapper: HeartbeatMapper
		:type catalogue: hpda.locator.catalogue.Catalogue
		:type database: database
		:type update_interval: int
		:rtype: void
		"""
		ThreadMaster.__init__(self)
		self._database = database
		self.loop_interval = update_interval
		self._pool_mapper = pool_mapper
		self._catalogue = catalogue
		self._catalogue.add_life_time_hook(self)

	def payload(self):
		start_time = time.time()
		log('coordinator.collector', LVL.STATUS, "Updating file sizes...")
		self._update_file_sizes()
		file_size_update_time = time.time() - start_time
		log('coordinator.collector', LVL.STATUS, "Finished file sizes update after %03.3fs", file_size_update_time)

	def _update_file_sizes(self):
		"""
		Ensures that the size of all files is stored in the database.

		:rtype: void
		"""
		cacheNodes = self._pool_mapper.get_nodes_by_role(NodeRole.CACHE)

		if not cacheNodes:
			log('coordinator.collector', LVL.WARNING, "Update file sizes not possible: No Cache found!")
			return

		cacheAPI = CacheProxy(random.choice(cacheNodes))
		with self._database.begin() as db:
			files = db.get_files(size_is_null=True)
			for file_data in files:
				try:
					file_info = cacheAPI.get_file_info(file_data.path)
					log('coordinator.collector', LVL.DEBUG, "File %d has size:  %d", file_data.id, file_info['size'])
					db.file_update_size(file_data.id, file_info['size'])
				except APIError:
					log('coordinator.collector', LVL.INFO, "Can't fetch file info for file '%s'!", file_data.path)


	def job_add(self, batch_job_id, job_owner, file_list):
		"""Add a job to database

		:param batch_job_id: Batch System ID of the Job
		:type batch_job_id: str
		:param job_owner: Owner of the Job
		:type job_owner: str
		:param file_list: List of files used by the Job
		:type file_list: list[str]
		:rtype: void
		"""

		with self._database.begin() as db:
			owner_id = db.get_owner_id(job_owner)
			files = db.get_file_ids(file_list)
			if len(files) < 1:
				raise APIError("File List required")

			file_group_id = db.get_file_group_id(files = files)
			if file_group_id is None:
				file_group_id = db.file_group_add(files)

		with self._database.begin() as db:
			log('coordinator.collector', LVL.DEBUG, "Received job add for job %s", batch_job_id)
			db.job_add(batch_job_id, owner_id, file_group_id)


	def job_report(self, batch_job_id, cpu_time, memory_usage, wall_time, exit_code, node, locality_rate, cachehit_rate):
		"""Update a job in the database with the job report data

		:param batch_job_id: Batch System ID of the Job
		:type batch_job_id:str
		:param cpu_time: CPU time of the job
		:type cpu_time: float
		:param memory_usage: Memory usage of the job
		:type memory_usage: float
		:param wall_time: Wall time of the job
		:type wall_time: float
		:param exit_code: Exit code of the job
		:type exit_code: int
		:param node: Hostname of the Node on which the job have been executed
		:type node: str
		:param locality_rate: Locality rate of the files required by the job
		:type locality_rate: float
		:param cachehit_rate: Cachehit rate of the job
		:type cachehit_rate: float
		:rtype: void
		"""
		with self._database.begin() as db:
			log('coordinator.collector', LVL.DEBUG, "Received job report for job %s", batch_job_id)
			node_id = db.get_node_id(node)
			db.job_report(batch_job_id, cpu_time, memory_usage, wall_time, exit_code, node_id, locality_rate, cachehit_rate)

	def stage_request(self, owner, file_list):
		"""Add a stage request for a file group to database

		If the same file group was already requested from the user, only the request_time is updated

		:param owner: User who send the stage request
		:type owner: str
		:param file_list: List of files in the file group
		:type file_list: list[str]
		:rtype: void
		"""
		with self._database.begin() as db:
			owner_id = db.get_owner_id(owner)

			files = db.get_file_ids(file_list)
			if len(files) < 1:
				raise APIError("File List required")

			file_group_id = db.get_file_group_id(files = files)
			if file_group_id is None:
				file_group_id = db.file_group_add(files)

			log('coordinator.collector', LVL.DEBUG, "Received stage request for file group %d", file_group_id)
			db.stage_request_add(owner_id, file_group_id)

	def remove_stage_request(self, owner, file_list):
		"""Withdraw a file request of a file group

		:param owner: User who have send the stage request
		:type owner: str
		:param file_list: List of files in the file group
		:type file_list: list[str]
		:rtype: void
		"""
		with self._database.begin() as db:
			owner_id = db.get_owner_id(owner)

			files = db.get_file_ids(file_list)
			if len(files) < 1:
				raise APIError("File List required")

			file_group_id = db.get_file_group_id(files = files)
			if file_group_id is None:
				file_group_id = db.file_group_add(files)

			log('coordinator.collector', LVL.DEBUG, "Withdraw stage request for file group %d", file_group_id)
			db.stage_request_remove(owner_id, file_group_id)

	def add_life_time(self, node, file_path, life_time):
		"""Adds an entry to the life time log for a file on a node

		:param node: Host of the node
		:type node: str
		:param file_path: Source URI of the file
		:type file_path: str
		:param life_time: Lifetime of the file on the node
		:type life_time: int
		:rtype: void
		"""
		with self._database.begin() as db:
			node_id = db.get_node_id(node)
			file_ids = db.get_file_ids([file_path])
			db.life_time_log_add(file_ids[0], node_id, life_time)

CollectorConfig = Section(
	name="CoordinatorCollector",
	target=Collector,
	attributes={
		"update_interval":	ConfigMultiAttribute(
			variants=(
				CfgSeconds(),
				CfgCronTab(default="0 * * * *")
			)
		)
	}
)