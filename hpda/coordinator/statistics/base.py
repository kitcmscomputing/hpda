#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# base imports
from time import time

# library imports

# custom imports
from hpda.client.cacheproxy import CacheProxy
from hpda.common.pool_mapper import NodeRole
from hpda.utility.report import log, LVL
from hpda.utility.threads import ThreadMaster
from hpda.utility.configuration.interface import Section, CfgSeconds, CfgCronTab, ConfigMultiAttribute


class Statistics(ThreadMaster):
	"""

	:type _database: hpda.common.database.base.database
	:type _pool_mapper: hpda.common.pool_mapper.HeartbeatMapper
	"""

	_db_table_mapper = {
		'jobs': (lambda db, fields: db.get_job_stats(fields)),
		'file_groups': (lambda db, fields: db.get_file_group_stats(fields)),
		'coordinator_timing': (lambda db, fields: db.coordinator_timing_stats(fields)),
		'cache_status': (lambda db, fields: db.cache_status_stats(fields)),
	}

	def __init__(self, pool_mapper, database, update_interval, log_timeout):
		ThreadMaster.__init__(self)
		self.loop_interval = update_interval
		self._log_timeout = log_timeout
		self._database = database
		self._pool_mapper = pool_mapper

	def payload(self):
		log('statistics', LVL.STATUS, "Fetching statistics....")
		self._create_node_logs()
		log('statistics', LVL.STATUS, "Done with fetching statistics.")
		self._cleanup()

	def _cleanup(self):
		with self._database.begin() as db:
			db.cache_status_cleanup(self._log_timeout)
			db.coordinator_timing_cleanup(self._log_timeout)
			db.life_time_log_cleanup(self._log_timeout)

	def _create_node_logs(self):
		with self._database.begin() as db:
			cacheNodes = self._pool_mapper.get_nodes_by_role(NodeRole.CACHE)
			for cacheNode in cacheNodes:
				cache = CacheProxy(cacheNode)
				max_size = 0
				allocated = 0
				score_min = None
				score_max = 0
				score_total = 0
				files_total = 0
				allocation_data = cache.get_cache_features()
				for cacheDevice in allocation_data:
					max_size += cacheDevice['allocation']['max_size']
					allocated += cacheDevice['allocation']['allocated']
					score_total += cacheDevice['allocation']['score_total']
					files_total += cacheDevice['allocation']['files_total']
					if score_min is None or score_min > cacheDevice['allocation']['score_min']:
						score_min = cacheDevice['allocation']['score_min']
					if score_max < cacheDevice['allocation']['score_max']:
						score_max = cacheDevice['allocation']['score_max']

				node_id = db.get_node_id(cacheNode.url)
				db.cache_status_add(node_id, max_size, score_min, score_max, score_total, allocated, files_total)

	def get_statistics(self, table, fields):
		start_time = time()
		with self._database.begin() as db:
			entries = self._db_table_mapper[table](db, fields)
		log('statistics', LVL.STATUS, "Finished building entries '%s' after %03.3fs", ", ".join(fields), (time()-start_time))
		return entries

StatisticsConfig = Section(
	name="Statistics",
	target=Statistics,
	attributes={
		"update_interval":	ConfigMultiAttribute(
			variants=(
				CfgSeconds(),
				CfgCronTab(default="0 * * * *")
			)
		),
		"log_timeout": CfgSeconds(default="6M")
	}
)


