#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# base imports

# library imports
import cherrypy

# custom imports
from hpda.utility.cherrypyutils import json_handler


class StatsAPI(object):
	"""

	:type _stats: hpda.coordinator.statistics.base.Statistics
	"""

	exposed=True
	api_basepath = "coordinator/stats"

	def __init__(self, stats):
		self._stats = stats

	@cherrypy.tools.json_out(handler=json_handler)
	def GET(self, source, fields = []):
		if type(fields) in [basestring, str, unicode]:
			fields = [fields]
		if len(fields) < 1:
			return {}

		return  self._stats.get_statistics(source, fields)
