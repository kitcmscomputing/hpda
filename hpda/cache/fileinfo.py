#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# base imports
import time

# library imports

# custom imports
from hpda.utility.exceptions import APIError
from hpda.utility.utils      import niceBytes, NotSet
from hpda.cache.backend.base import CacheAPIBase
from hpda.cache.storage.base import StorageAPIBase


class FileViewDiff(object):
	"""
	Baseclass for FileViews, tracking attribute changes

	The :py:class:`~.FileViewDiff` objects provide a managed **view** to :py:class:`.FileInfo`
	objects: attributes may be read freely, but any modification is stored on the
	:py:class:`~.FileViewDiff` itself. Changes are accessible via :py:attr:`~.FileViewDiff.diff`,
	which allows updating :py:class:`.FileInfo` when modifications are done.

	:param file_info: underlying file information that is viewed/modified
	:type file_info: :py:class:`.FileInfo`
	:ivar timestamp: time of last change
	:type timestamp: float
	"""
	read_only = ('source_uri',)
	def __init__(self, file_info):
		object.__setattr__(self, "source_uri", file_info.source_uri)
		self.file_info = file_info
		self._changed  = dict()
		self.timestamp = 0

	# linked APIs - see FileInfo
	@property
	def storage(self):
		return StorageAPIBase(self.storage_id)
	@property
	def cache(self):
		if self.cache_id in (None, NotSet):
			return self._cache_id
		return CacheAPIBase(self.cache_id)
	@cache.setter
	def cache(self, value):
		if value in (None,NotSet):
			self.cache_id = value
		else:
			self.cache_id = value.nickname

	def __getattr__(self, item):
		"""View allows access to FileInfo transparently overlayed with changes"""
		try:
			return self._changed[item]
		except KeyError:
			return getattr(self.file_info, item)

	def __setattr__(self, name, value):
		"""View stores changes of FileInfo data internally"""
		if name in self.read_only:
			raise APIError("FileInfo/View attribute '%s' is read only", name)
		if name in FileInfo.__slots__ and not name[:4]=="prev" or name == "cache_id":
			self._changed[name] = value
			object.__setattr__(self, "timestamp", time.time())
		object.__setattr__(self, name, value)

	def condense(self):
		"""Pull in all information of the underlying :py:class:`~.FileInfo`"""
		for attr in self.file_info.__slots__:
			if not attr in self._changed:
				self._changed[attr] = getattr(self.file_info, attr)

	@property
	def updated(self):
		"""Set of updated attributes"""
		return self._changed.keys()

	@property
	def diff(self):
		"""Map of updated attributes and their values"""
		return self._changed

	@property
	def json(self):
		"""JSON representation"""
		json_out = {"source_uri":self.source_uri}
		for my_key, dest_key in (("storage_id", "storage"),("cache_id", "cache"), ("size", "size")):
			try:
				json_out[dest_key] = self._changed[my_key]
			except KeyError:
				pass
		return json_out

	def __repr__(self):
		return "%s(%s=>%r)"%(self.__class__.__name__, self._changed.keys(), self.file_info)

	def __str__(self):
		return self.source_uri

	# pickling
	def __getstate__(self):
		return {"suri":self.source_uri, "_changed":self._changed, "time":self.timestamp}

	def __setstate__(self, state):
		object.__setattr__(self, "file_info", None)
		object.__setattr__(self, "source_uri", state["suri"])
		object.__setattr__(self, "_changed", state["_changed"])
		object.__setattr__(self, "timestamp", state["time"])
		if "cache" in self._changed:
			del self._changed["cache"]
		if "storage" in self._changed:
			del self._changed["storage"]


class FileInfo(object):
	"""
	Container for meta-information of a file
	
	:param source_uri: original URI of the file, as addressed by users
	:type source_uri: str
	:param filename: filename (dirname+basename) as it would be addressed on the local filesystem
	:type filename: str
	:param storage: the storage API providing the source file
	:type storage: :py:class:`~.StorageAPIBase`
	:param size: size of the source file, in bytes
	:type size: int or float
	:param cache: the cache API currently supervising a local copy
	:type cache: :py:class:`~.CacheAPIBase` or None
	:param score: absolute importance of the file
	:type score: int or float

	:ivar cache_time: timestamp when the file was initially cached
	:type cache_time: int
	"""
	__slots__ = ['source_uri','filename','cache_id','storage_id','size','cache_time','score', '__weakref__']
	def __init__(self, source_uri, filename, storage, size, cache = NotSet, score=None):
		# intern source_uri as its the identifier for all lookups
		self.source_uri = intern(source_uri)
		self.filename   = filename
		self.storage_id = storage.nickname
		self.cache_id   = cache if cache in (None, NotSet) else cache.nickname
		self.size       = size
		self.cache_time = time.time()
		self.score      = score

	# linked APIs
	@property
	def storage(self):
		return StorageAPIBase(self.storage_id)

	@property
	def cache(self):
		if self.cache_id in (None, NotSet):
			return self.cache_id
		return CacheAPIBase(self.cache_id)
	@cache.setter
	def cache(self, value):
		if value in (None, NotSet):
			self.cache_id = value
		else:
			self.cache_id = value.nickname


	@classmethod
	def from_diff(cls, file_diff):
		"""
		Create a new instance from a file_diff

		:param file_diff: container with sufficient information for reconstruction
		:type file_diff: :py:class:`~.FileViewDiff` or :py:class:`~.FileInfo`
		:return: the new FileInfo
		:rtype: :py:class:`~.FileInfo`
		"""
		if isinstance(file_diff, FileInfo):
			return file_diff
		kwargs = {"cache":None, "score":None}
		kwargs.update(file_diff.diff)
		kwargs.pop("cache_id", None)
		kwargs.pop("validity", None)
		return cls(**kwargs)

	def apply_diff(self, file_diff):
		"""
		Update the information about the file from a diff.
		
		:param file_diff: diff to new state of file
		:type file_diff: :py:class:`~.FileViewDiff`
		:returns: the updated information
		:rtype: :py:class:`~.FileInfo`
		"""
		diff = file_diff.diff
		for attr in diff:
			setattr(file_diff, "prev_"+attr, getattr(self, attr))
			setattr(self, attr, diff[attr])
		return self

	# display & representations
	def __str__(self):
		return self.source_uri
	def __repr__(self):
		return "%(name)s(cache_id='%(cID)s', storage_id='%(sID)s', size='%(size)s', score=%(score)r, filename='%(fname)s', source_uri='%(uri)s')" % dict(
			name  = self.__class__.__name__,
			uri   = self.source_uri,
			fname = self.filename,
			sID   = self.storage_id,
			cID   = self.cache_id,
			size  = niceBytes(self.size),
			score = self.score,
			)

	def get_dict(self):
		"""Return all information as a dictionary"""
		return self.__getstate__()

	@property
	def json(self):
		"""JSON representation"""
		return {
			"source_uri" : self.source_uri,
			"storage" : self.storage_id,
			"cache" : (self.cache_id if self.cache_id is not NotSet else str(NotSet)),
			"size" : self.size,
			"cache_time" : self.cache_time,
		}

	# explicit pickling for __slots__
	def __getstate__(self):
		return dict((name, getattr(self, name)) for name in self.__slots__ if name != '__weakref__')

	def __setstate__(self, state):
		self.source_uri = intern(state.pop("source_uri"))
		for name in state:
			try:
				object.__setattr__(self, name, state[name])
			except AttributeError:
				pass
