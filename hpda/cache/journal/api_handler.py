#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# standard library imports
import logging

# third party imports

# application/library imports
from hpda.utility.exceptions import InstallationError
from hpda.utility.report import LVL

class APIHandler(object):
	"""
	Manager of available backends.
	"""
	def __init__(self):
		self._cache_apis = {}
		self._storage_apis = {}
		self._cache_blacklist = set()
		self.log = logging.getLogger("catalogue.%s"%self.__class__.__name__).log

	@property
	def cache_apis(self):
		return dict((name, api) for (name, api) in self._cache_apis.items() if name not in self._cache_blacklist)

	@property
	def storage_apis(self):
		return self._storage_apis.copy()

	def add_cache_apis(self, *apis):
		"""
		Add a cache API to the handler.

		:param apis: new API to add
		:type apis: :py:class:`~.CacheAPIBase`
		"""
		for api in apis:
			self._cache_apis[api.nickname] = api
			self.log(LVL.STATUS, "Added cache API %s", api)
			if hasattr(api, "slaves"):
				for slave in api.slaves:
					slave.sequence = -1
					self.ignore_api(slave)
					self.log(LVL.STATUS, "Ignoring slaved cache API %s", slave)

	def add_storage_apis(self, *apis):
		"""
		Add a storage API to the handler.

		:param apis: new API to add
		:type apis: :py:class:`~.StorageAPIBase`
		"""
		for api in apis:
			self._storage_apis[api.nickname] = api
			self.log(LVL.STATUS, "Added storage API %s", api)

	def ignore_api(self, api):
		"""
		Mark an API to be ignored in automatic access.

		This will block APIs from being provided by the :py:meth:`~.cache_allocators`
		and :py:attr:`~.cache_apis`.

		:param api: cache API by reference or nickname to block
		:type api: str or :py:class:`~.CacheAPIBase`
		:raises :py:exc:`ValueError`: if no known API corresponds to the input
		"""
		try:
			if not isinstance(api, str):
				api = api.nickname
		except (KeyError, AttributeError):
			raise ValueError("%s is not a known cache API"%api)
		else:
			self._cache_blacklist.add(api)

	def get_storage_api(self, source_uri):
		"""
		Provides a storage API capable of handling ``source_uri``.

		If several APIs match, the one with the highest sequence is selected. No
		guarantee is given that the file actually exists.

		:param source_uri: URI to the file
		:type source_uri: str
		:returns: storage APIs supporting the URI
		:rtype: :py:class:`~.StorageAPIBase`
		:raises InstallationError: if no API supports the URI
		"""
		for api in sorted(self._storage_apis.values(), key=lambda item: item.sequence, reverse=True):
			if api.sequence < 0:
				break
			if api.supports_uri(source_uri):
				return api
		raise InstallationError("File %s not supported by any storageAPI"%source_uri)



