#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
foo

:todo: file update callback interface
:todo: memory-only catalogue
:todo: backend (cache, storage) api interface
"""
# standard library imports
import logging

# third party imports

# application/library imports
from hpda.utility.report  import LVL
from hpda.utility.utils   import NotSet
from hpda.utility.configuration.interface import Section, CfgStr, CfgInt
from .filestate import FileState
from .deltaqueue import TaskManager
from .api_handler import APIHandler


class Catalogue(object):
	def __init__(self, tasks_base_path, state_base_path, state_chunk_count, state_cache_size):
		self.log = logging.getLogger("catalogue.%s"%self.__class__.__name__).log
		self.log(LVL.STATUS, "Instantiating Cache Catalogue (%s)", self.__class__.__name__)
		self.state = FileState(catalogue=self, store_uri=state_base_path, chunk_count=state_chunk_count, chunk_cache_size=state_cache_size)
		self.tasks = TaskManager(delta_dir=tasks_base_path)
		self.backend_apis = APIHandler()
		self.file_hooks = []

	## Files
	########
	# container protocol
	def __iter__(self):
		return iter(self.state)
	def __len__(self):
		return len(self.state)
	def __getitem__(self, key):
		return self.state[key]
	def __contains__(self, key):
		return key in self.state

	def add_file_hook(self, hook):
		"""
		Register a hook for receiving updates on file changes

		A registered hook will receive events on insertion, update and deletion
		of files. The hook must expose the following functions/methods:

		.. py:function:: file_insert(*file_infos)->None

		.. py:function:: file_update(*file_diffs)->None

		.. py:function:: file_remove(*file_infos)->None

		:param hook: new hook to add for file changes
		"""
		assert hasattr(hook, "file_insert") and hasattr(hook, "file_update") and hasattr(hook, "file_remove"), "Hooks must conform to interface"
		self.file_hooks.append(hook)

	def insert_file_info(self, *file_infos):
		"""
		Add a new file information

		:param file_infos: information to add
		:type file_infos: list[:py:class:`~hpda.cache.fileinfo.FileInfo`] or list[`~hpda.cache.fileinfo.FileViewDiff`]
		"""
		self.state.insert_file_info(*file_infos)

	def update_file_info(self, *file_diff):
		"""
		Update file information

		:param file_diff: new state of the file
		:return: list[:py:class:`~hpda.cache.fileinfo.FileViewDiff`]
		"""
		self.state.update_file_info(*file_diff)

	def unlink_file_info(self, file_info):
		"""
		Release file information

		:param file_info: information to release
		:type file_info: :py:class:`~hpda.cache.fileinfo.FileInfo`, :py:class:`~hpda.cache.fileinfo.FileViewDiff`
		"""
		file_info.cache_id = NotSet
		self.state.unlink_file_info(file_info)

	def get_group(self, file_info):
		"""
		Get all files from the same file group

		:param file_info: a file from the collection
		:return: :py:class:`~hpda.cache.filestate.FileInfo`
		"""
		return self.state.get_group(file_info)

	def get_files(self, cache_id=None):
		"""
		Return the information on cached files

		:param cache_id: specific cache to list
		:return: information of files on specified caches
		:rtype: list[:py:class:`~hpda.cache.fileinfo.FileInfo`]
		"""
		return self.state.get_files(cache_id=cache_id)

	## Tasks
	########
	def add_tasks(self, *tasks):
		"""
		Register a group of tasks for later execution and persistency

		:param tasks: final states to achieve
		:type tasks: list[:py:class:`~hpda.cache.fileinfo.FileViewDiff`]
		"""
		for task in tasks:
			try:
				if not task.source_uri in self:
					task.condense()
			except AttributeError:
				pass
		self.tasks.add_tasks(*tasks)

	def get_task(self):
		return self.tasks.get_task()

	def end_task(self, task, result):
		"""
		Mark a task as successfully ended

		:param task: task to end
		:param result: final state of the modified file
		:type result: :py:class:`~hpda.cache.fileinfo.FileViewDiff` or :py:class:`~hpda.cache.fileinfo.FileInfo`

		:note: ``task`` must be the currently outstanding task. This is used for
		       persistency checking.
		"""
		self.tasks.end_task(task)

	def skip_task(self, task, reason):
		"""
		Dismiss a task from future execution

		:param task: the task to dismiss
		:param reason: explanation for ignoring the task
		:return:
		"""
		self.tasks.skip_task(task, reason)

	def iter_tasks(self):
		"""
		Iterate over outstanding tasks
		"""
		return iter(self.tasks)


CatalogueConfig = Section(
	attributes={
		"tasks_base_path" : CfgStr(descr="Directory to store outstanding tasks."),
		"state_base_path" : CfgStr(descr="Directory to store current state."),
		"state_chunk_count" : CfgInt(default=64, descr="Number of chunks to store state in."),
		"state_cache_size" : CfgInt(default=4, descr="Number of chunks to cache in memory."),
	},
	target=Catalogue,
	name="CacheCatalogue",
	descr="Persistent store for the state of the meta-data of the cache."
)