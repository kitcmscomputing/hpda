#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
Container for changes of the state of files in the cache. The ``FileDelta``
objects store outstanding tasks, changes to files that must be implemented to
achieve a desired state of the cache.

Internally, tasks are represented by :py:class:`~hpda.cache.fileinfo.FileViewDiff`
objects which define the final state.
"""

# standard library imports
import os
from collections import deque, Counter
import time
import cPickle as pickle
import glob
import logging
import threading
import copy

# third party imports

# application/library imports
from hpda.utility.exceptions import APIError
from hpda.utility.report import LVL
from hpda.utility.utils import ensure_rm, ensure_dir
from hpda.utility.caching import hashkey

class FileDelta(object):
	"""
	Difference in state of a single file at a given time

	:param file_diff: new state of the file
	:type file_diff: :py:class:`~hpda.cache.fileinfo.FileViewDiff`
	"""
	__slots__ = ("timestamp","state","task_id")
	def __init__(self, file_state):
		self.timestamp = time.time()
		self.state = file_state
		self.task_id = '%s_%s_%s' % (time.strftime('%Y%m%d%H%M%S'), hashkey(file_state.source_uri), hashkey(file_state.cache_id))

	@property
	def subject(self):
		return self.state.source_uri
	@property
	def target(self):
		return self.state.cache_id

	def __eq__(self, other):
		if not isinstance(other, type(self)):
			return False
		return self.task_id == other.task_id

	def __repr__(self):
		return "%s(id=%s,uri=%s)"%(self.__class__.__name__, self.task_id,self.state.source_uri)

	def __getstate__(self):
		return self.timestamp, self.state, self.task_id
	def __setstate__(self, state):
		self.timestamp, self.state, self.task_id = state

class TaskQueue(object):
	"""
	Ordered queue for changes on the state of the catalogue

	This is effectively a FIFO queue for tasks.
	"""
	def __init__(self, delta_dir, chunk_size=50):
		self.log = logging.getLogger("catalogue.%s"%self.__class__.__name__).log
		self._chunk_size = chunk_size
		self._pickle_dir = delta_dir
		ensure_dir(self._pickle_dir)
		self.outstanding_tasks = deque()
		self._task_pickles = {} # stores {<task>:<task group file>}
		self._pickles_tasks = Counter() # stores {<task group file>:<number of task>}
		self._load()

	def add_tasks(self, *target_diffs):
		"""
		Register a group of tasks for later execution and persistency

		:param target_diffs: final states to achieve
		:type target_diffs: list[:py:class:`~hpda.cache.fileinfo.FileViewDiff`]
		"""
		self.log(LVL.INFO, "Adding %d tasks", len(target_diffs))
		deltas = [FileDelta(diff) for diff in target_diffs]
		for delta_chunk in (deltas[idx:idx+self._chunk_size] for idx in xrange(0,len(deltas),self._chunk_size)):
			task_file = 'tasks_%s_%d_%s.pkl' % (
				time.strftime('%Y%m%d%H%M%S'),
				len(delta_chunk),
				hashkey(str([diff.state.source_uri for diff in delta_chunk]))
			)
			pickle.dump(delta_chunk, open(os.path.join(self._pickle_dir, task_file),"wb"), pickle.HIGHEST_PROTOCOL)
			self.outstanding_tasks.extend(delta_chunk)
			for task in delta_chunk:
				self._task_pickles[task] = task_file
				self._pickles_tasks[task_file] += 1
			self.log(LVL.DEBUG, "Added outstanding task file %s (%d tasks)", task_file, self._pickles_tasks[task_file])
		return deltas

	def _load(self):
		"""Load all outstanding tasks"""
		tasks = []
		task_files = glob.glob(os.path.join(self._pickle_dir, "tasks_*.pkl"))
		self.log(LVL.INFO, "Loading tasks from %d files", len(task_files))
		for task_file in task_files:
			self.log(LVL.DEBUG, "Loading task file '%s'", task_file)
			try:
				deltas = pickle.load(open(task_file,"rb"))
			except (OSError, IOError, TypeError):
				self.log(LVL.WARNING, "Failed unpickling %s, removing it", task_file)
				ensure_rm(task_file)
			else:
				tasks.extend(deltas)
				for task in deltas:
					self._task_pickles[task] = task_file
					self._pickles_tasks[task_file] += 1
		tasks.sort(key = lambda diff: diff.timestamp)
		self.outstanding_tasks = deque(tasks)
		self.log(LVL.INFO, "Loaded %d tasks", len(tasks))

	def get_task(self):
		try:
			return self.outstanding_tasks[0]
		except IndexError:
			return None

	def _remove_task(self, task):
		"""Remove a task from the queue"""
		self.outstanding_tasks.remove(task)
		task_file = self._task_pickles.pop(task)
		self._pickles_tasks[task_file] -= 1
		if self._pickles_tasks[task_file] == 0:
			self.log(LVL.DEBUG, "Removing applied task file %s", task_file)
			ensure_rm(os.path.join(self._pickle_dir,task_file))
			del self._pickles_tasks[task_file]

	def end_task(self, task):
		if task != self.outstanding_tasks[0]:
			raise APIError("Ended task %s is not active task %s"%(task, self.outstanding_tasks[0]))
		self._remove_task(task)

	def skip_task(self, task, reason):
		self.log(LVL.WARNING, "Skipping task %s: %s", task, reason)
		self._remove_task(task)

	def __iter__(self):
		while True:
			next_task = self.get_task()
			if next_task is None:
				break
			yield next_task

	def iter_tasks(self):
		"""
		Iterate active, outstanding tasks

		Since only a single task (the oldest one queued) is considered active,
		this iterator will yield the same task until it has been successfully
		handled.

		:return:
		"""
		return iter(self)

	def subject_task(self, subject):
		"""
		Return the appropriate task for a given subject
		"""
		for task in reversed(self.outstanding_tasks):
			if task.subject == subject:
				return task

	def subject_target(self, subject):
		"""
		Return the appropriate target for a given subject

		:note: For file move tasks (the only type in use), this equals the
		       ``cache_id``.
		"""
		return self.subject_task(subject).target


class TaskManager(TaskQueue):
	"""
	Unordered collection for outstanding changes of the state of the cache.

	The collection automatically attempts to optimize state changes. This
	primarily includes removal of duplicate and redundant changes.
	"""
	def __init__(self, delta_dir, chunk_size=50):
		self._active_task = None
		self._mutex = threading.RLock()
		self._subject_tasks = {} # stores {<subject>:<current task>}
		TaskQueue.__init__(self, delta_dir=delta_dir, chunk_size=chunk_size)

	def add_tasks(self, *target_diffs):
		"""
		Register a group of tasks for later execution and persistency

		:param target_diffs: final states to achieve
		:type target_diffs: list[:py:class:`~hpda.cache.fileinfo.FileViewDiff`]
		"""
		with self._mutex:
			# ensure persistency first
			new_tasks = TaskQueue.add_tasks(self, *target_diffs)
			# cleanup active tasks
			outdated_tasks = 0
			for task in new_tasks:
				if task.subject in self._subject_tasks:
					outdated_tasks += 1
					self._remove_task(self._subject_tasks[task.subject])
				self._subject_tasks[task.subject] = task
			if outdated_tasks:
				self.log(LVL.WARNING, "Removed %d redundant tasks", outdated_tasks)
			return new_tasks

	def get_task(self):
		try:
			self._active_task = self.outstanding_tasks[0]
		except IndexError:
			self._active_task = None
		return self._active_task

	def end_task(self, task):
		if task != self._active_task:
			raise APIError("Ended task %s is not active task %s"%(task, self.outstanding_tasks[0]))
		self._remove_task(task)

	def _remove_task(self, task):
		with self._mutex:
			try:
				TaskQueue._remove_task(self, task)
			# ignore active tasks reaped from waiting queue
			except (ValueError, KeyError):
				pass
			if self._subject_tasks.get(task.subject) == task:
				del self._subject_tasks[task.subject]

	def _load(self):
		TaskQueue._load(self)
		handled_subjects = set()
		for task in reversed(copy.copy(self.outstanding_tasks)):
			if task.subject not in handled_subjects:
				self._subject_tasks[task.subject] = task
				handled_subjects.add(task.subject)
			else:
				self._remove_task(task)

