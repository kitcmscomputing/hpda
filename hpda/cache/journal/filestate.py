#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
Container(s) for the current state of all files in the cache. Every ``FileState``
provides access to its :py:class:`~hpda.cache.fileinfo.FileInfo` via their
``source_uri`` or ``filename`` and provides lists by ``cache_id``.

``FileState`` objects provide a :py:class:`dict`-like container protocol, indexed
with the ``source_uri`` of files. In addition, explicit access via ``filename``
and ``cache_id`` are provided. Finally, there is an implementation specific index
of ``file_group``.

The ``file_group`` represents files that are likely to change with one another
and is used to optimize scanning of files for validity. Regular iteration attempts
to scan through as many ``file_group``s as possible. When a change is detected,
all files of the specific ``file_group`` can be fetched for closer inspection.
"""
# standard library imports
import os
import random
import logging
import itertools
import weakref

# third party imports

# application/library imports
from hpda.utility.report import LVL
from hpda.utility.utils import FlatList
from hpda.utility.rwlock import RWLock
from hpda.cache.fileinfo import FileInfo
from hpda.utility.persistency import PersistentDict

class FileState(object):
	"""
	State of files known on a cache

	:param store_uri: location for storing persistent copy of the state
	:type store_uri: str
	"""
	def __init__(self, catalogue, store_uri, chunk_count, chunk_cache_size):
		self.log = logging.getLogger("catalogue.%s"%self.__class__.__name__).log
		self.catalogue = catalogue
		if "://" in store_uri:
			schema, _, path = store_uri.partition("://")
			assert schema == "file", "Require a file system URI ('file:////path/to/dir' or '/path/to/dir')"
			store_uri = path
		self.log(LVL.INFO, "Digesting persistent state at '%s'", store_uri)
		self._files_by_source_uri = PersistentDict(store_base_uri=store_uri, chunk_count=chunk_count, cache_size=chunk_cache_size, cache_keys=True)
		self._files_by_cache_id   = {}
		self._file_groups = DirectoryFileGroup()
		self._lock = RWLock()
		self.log(LVL.INFO, "Indexing %d registered files", len(self._files_by_source_uri))
		for file_info in self._files_by_source_uri.itervalues():
			self._file_groups.insert_file(file_info)
			self._files_by_cache_id.setdefault(file_info.cache_id,weakref.WeakSet()).add(file_info)

	def _insert_file_info(self, *file_infos):
		self._files_by_source_uri.update((file_info.source_uri, file_info) for file_info in file_infos)
		#self._files_by_source_uri[file_info.source_uri] = file_info
		for file_info in file_infos:
			self._files_by_cache_id.setdefault(file_info.cache_id,weakref.WeakSet()).add(file_info)
			self._file_groups.insert_file(file_info)

	def _unlink_file_info(self, *file_infos):
		for file_info in file_infos:
			try:
				del self._files_by_source_uri[file_info.source_uri]
			except KeyError:
				pass
			try:
				self._files_by_cache_id[file_info.cache_id].discard(file_info)
			except KeyError:
				pass
			self._file_groups.unlink_file(file_info)

	def _get_file_info(self, file_info_id):
		"""
		Get the file_info belonging to any identifier, i.e. :py:class:`~hpda.cache.fileinfo.FileInfo`,
		:py:class:`~hpda.cache.fileinfo.FileView` or a ``source_uri``

		:raises :py:class:`KeyError`: if the file is not known
		:raises :py:class:`TypeError`: if the file cannot be identified given the ``file_info_id``
		"""
		with self._lock.Reader():
			try:
				if isinstance(file_info_id, FileInfo):
					return file_info_id
				if isinstance(file_info_id, str):
					return self[file_info_id]
				return self[file_info_id.source_uri]
			except AttributeError:
				raise TypeError("%r is an invalid file info referent"%file_info_id)

	def insert_file_info(self, *file_infos):
		"""
		Add a new file information

		:param file_infos: information to add
		:type file_infos: list[:py:class:`~hpda.cache.fileinfo.FileInfo`]
		"""
		with self._lock.Writer():
			# deletion is safe, so make sure there is no garbage that might corrupt the state
			self._unlink_file_info(*file_infos)
			self._insert_file_info(*file_infos)
		for hook in self.catalogue.file_hooks:
			hook.file_insert(*file_infos)

	def update_file_info(self, *file_diffs):
		"""
		Update file information

		:param file_diffs: new state of the file
		:type file_diffs: list[:py:class:`~hpda.cache.fileinfo.FileViewDiff`]
		"""
		with self._lock.Writer():
			self._unlink_file_info(*(file_diff.file_info for file_diff in file_diffs))
			self._insert_file_info(*(file_diff.file_info.apply_diff(file_diff) for file_diff in file_diffs))
		for hook in self.catalogue.file_hooks:
			hook.file_update(*file_diffs)

	def unlink_file_info(self, file_info_id):
		"""
		Release file information

		:param file_info_id: information to release
		:type file_info_id: :py:class:`~hpda.cache.fileinfo.FileInfo`, :py:class:`~hpda.cache.fileinfo.FileViewDiff` or ``source_uri``
		"""
		try:
			file_info = self._get_file_info(file_info_id)
		except KeyError:
			return
		with self._lock.Writer():
			self._unlink_file_info(file_info)
		for hook in self.catalogue.file_hooks:
			hook.file_remove(file_info)

	def __iter__(self):
		while True:
			with self._lock.Reader():
				if not self._files_by_source_uri:
					break
				next_groups = random.sample(self._file_groups, min(len(self._file_groups), 10))
				file_infos = [ file_info for file_group in next_groups for file_info in random.sample(self._file_groups[file_group], min(len(self._file_groups[file_group]),3))]
			for file_info in file_infos:
				yield file_info.source_uri

	def __len__(self):
		return len(self._files_by_source_uri)

	def __getitem__(self, item):
		return self._files_by_source_uri[item]

	def __contains__(self, item):
		return item in self._files_by_source_uri

	def iter(self):
		"""
		Infinitely iterate over the ``source_uri``\ s of known files

		Iteration is aligned to the used :py:class:`~.MappedFileGroup` but random
		otherwise. During each iteration cycle, a number of file groups are randomly
		chossen, from each of which a set of files is drawn and yielded.
		"""
		return iter(self)

	def get_group(self, group_identifier):
		return self._file_groups.get_group(group_identifier)

	def get_files(self, cache_id=None):
		with self._lock.Reader():
			if cache_id is None:
				return self._files_by_source_uri.keys()
			cache_id = FlatList(cache_id)
			return list(file_info.source_uri for cid in cache_id for file_info in self._files_by_cache_id[cid])

	def get_file_infos(self, cache_id=None):
		with self._lock.Reader():
			if cache_id is None:
				return self._files_by_source_uri.values()
			cache_id = FlatList(cache_id)
			return list(itertools.chain(self._files_by_cache_id[cid] for cid in cache_id))


class MappedFileGroup(object):
	"""
	A generic FileGroup generator based on mapping :py:class:`~hpda.cache.fileinfo.FileInfo` to keys

	:param mapping: function generating group keys for files
	:type mapping: function

	:note: The interface guarantess that every :py:class:`~hpda.cache.fileinfo.FileInfo`
	       is statically mapped to its ``source_uri`` (the key used to identify :py:class:`~hpda.cache.fileinfo.FileInfo`).
	       The :py:class:`~hpda.cache.fileinfo.FileInfo` is not stored.
	"""
	def __init__(self, mapping=lambda file_info: file_info.source_uri):
		self.mapping = mapping
		self.groups = {}

	def insert_file(self, file_info):
		self.groups.setdefault(self.mapping(file_info),weakref.WeakSet()).add(file_info)

	def update_file(self, file_info):
		self.unlink_file(file_info)
		self.insert_file(file_info)

	def unlink_file(self, file_info):
		# current info might be insufficient, scan all
		for group in self.groups.values():
			try:
				group.remove(file_info)
				break
			except KeyError:
				pass

	# container protocols
	def keys(self):
		return self.groups.keys()

	def __getitem__(self, item):
		return self.groups[item]

	def __iter__(self):
		return iter(self.groups)

	def __len__(self):
		return len(self.groups)

	def get_group(self, group_identifier):
		try:
			group_key = self.mapping(group_identifier)
		except Exception:
			group_key = group_identifier
		return self[group_key]

	def __repr__(self):
		return "%s(%s)"%(self.__class__.__name__, self.groups)

class DirectoryFileGroup(MappedFileGroup):
	"""
	Grouping of files by containing directory
	"""
	def __init__(self):
		MappedFileGroup.__init__(self, mapping=lambda file_info: os.path.dirname(file_info.filename))

	def update_file(self, file_info):
		# mapping is stable, no action needed
		pass

	def unlink_file(self, file_info):
		# mapping is stable, we don't have to guess
		try:
			self.groups[self.mapping(file_info)].discard(file_info)
		except KeyError:
			pass