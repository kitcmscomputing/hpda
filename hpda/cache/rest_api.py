#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# base imports
import time

# library imports
import cherrypy

# custom imports
from ..utility.exceptions    import ExceptionFrame, InstallationError, FileNotFound
from ..utility.report        import log, LVL
from .allocator              import FileCandidate
from hpda.utility.validation import SchemaPrimitive, SchemaMap, SchemaArray, Schema


FileCandidateSchema = Schema(
	# List...
	SchemaArray(
		# ...of candidates
		members=(
			SchemaMap(
				members={
					"source_uri" : SchemaPrimitive(types=basestring),
					"score"      : SchemaPrimitive(types=(float, int), required = False),
				}
			)
		,)
	)
)

class ComponentAPI(object):
	"""
	API for the cache itself, i.e. setup and state of internal and
	external entities.
	"""
	exposed = True
	api_basepath = "cache/backends"
	_components = ["cache", "storage"]
	def __init__(self, catalogue):
		self._catalogue = catalogue

	@cherrypy.tools.json_out()
	def GET(self, component=None, nickname=None):
		with ExceptionFrame(ignore=[cherrypy.HTTPError], onExceptTerminate=False):
			if component is None:
				return self._components
			if component not in self._components:
				raise cherrypy.HTTPError(404, "No such component '%s'. Serving only %s" % (component, self._components))
			try:
				if component == "storage":
					if nickname is None:
						return [api.json for api in self._catalogue.backend_apis.storage_apis.values()]
					return self._catalogue.backend_apis.storage_apis[nickname].json
				if component == "cache":
					if nickname is None:
						return [api.json for api in self._catalogue.backend_apis.cache_apis.values()]
					return self._catalogue.backend_apis.cache_apis[nickname].json
			except KeyError:
				pass
			raise cherrypy.HTTPError(404, "No such component. Serving only %s" % self._components)

class ContentAPI(object):
	"""
	API for the content of the cache, i.e. staged files and their known
	meta-data.
	"""
	exposed = True
	api_basepath = "cache/content"
	def __init__(self, catalogue, allocator):
		self._catalogue = catalogue
		self._allocator = allocator

	@cherrypy.tools.json_out()
	def GET(self, source_uri=None):
		"""
		Query the content of the cache.

		Queries to the base will return a listing of files, while queries
		to subpaths will return the known attributes of the files.
		"""
		with ExceptionFrame(ignore=[cherrypy.HTTPError], onExceptTerminate=False):
			if source_uri is None:
				return {"files" : self._catalogue.get_files()}
			try:
				return self._catalogue.state[source_uri].json
			except KeyError:
				# we don't know it already - try to get it from the backends
				try:
					api = self._catalogue.backend_apis.get_storage_api(source_uri)
					return api.get_file_info(source_uri).json
				except InstallationError:
					raise cherrypy.HTTPError(404, "No backend for '%s'" % source_uri)
				except FileNotFound:
					raise cherrypy.HTTPError(404, "No such file '%s'" % source_uri)

	@cherrypy.tools.json_in()
	@cherrypy.tools.json_out()
	def POST(self):
		with ExceptionFrame(ignore=[cherrypy.HTTPError], onExceptTerminate=False):
			message = cherrypy.request.json
			if FileCandidateSchema.validate(message):
				try:
					new_file_data = [FileCandidate(source_uri=file_data["source_uri"].encode('utf-8'),score=file_data.get("score", time.time())) for file_data in message]
					failed_files = self._allocator.update_files(*new_file_data)
					return { "success" : True, "time" : time.time(), "failed" : failed_files }
				except FileNotFound as err:
					raise cherrypy.HTTPError(404, "No such file '%s'" % err)
				except InstallationError as err:
					raise cherrypy.HTTPError(404, "No backend for '%s'" % err)
			else:
				validation_report = FileCandidateSchema.report(message)
				log('server', LVL.STATUS, "Incompatible staging request:\n%s","\n".join(str(item) for item in validation_report))
				raise cherrypy.HTTPError(422, "Incorrect request content. Expected StagingSchema")

	@cherrypy.tools.json_in()
	def PUT(self, source_uri=None):
		if source_uri is not None:
			return "You PUT'd '%s' with '%s' (JSON: '%s')" % (source_uri, cherrypy.request.body, cherrypy.request.json)
		raise cherrypy.HTTPError(501, "File nomination/meta-update not implemented yet.")

	@cherrypy.tools.json_out()
	def DELETE(self, source_uri=None):
		with ExceptionFrame(ignore=[cherrypy.HTTPError], onExceptTerminate=False):
			# is the file actually here?
			try:
				file_info = self._catalogue.state[source_uri]
			except KeyError:
				return {}
			# is the file actually assigned?
			try:
				file_info.cache.release_file(file_info)
			except AttributeError:
				pass
			self._catalogue.state.unlink_file_info(source_uri)
			return {}