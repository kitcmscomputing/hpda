#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
The allocator classes handle the logical distribution of files on the caches,
compiling movement orders to best utilize the caches.
"""

# base imports
import time
from itertools import chain

# library imports
from threading          import RLock

# custom imports
from hpda.utility.exceptions import FileNotFound, InstallationError
from hpda.utility.report     import log, LVL
from hpda.utility.threads    import ThreadMaster
from hpda.cache.backend.base import AllocationFailure
from hpda.cache.fileinfo     import FileViewDiff
from hpda.utility.configuration.interface import Section, CfgSeconds

class FileCandidate(object):
	"""
	Basic container for information on a new file to add

	:param source_uri: the URI to the file
	:type source_uri: str
	:param score: importance of the file
	:type score: int or float
	:param flags:
	"""
	def __init__(self, source_uri, score=None, flags=()):
		self.source_uri, self.score, self.flags = source_uri, score, flags

	def __repr__(self):
		return "%s(uri=%s, score=%s, flags=%s)"%(self.__class__.__name__, self.source_uri, self.score, self.flags)

class Allocator(ThreadMaster):
	"""
	Trivial Allocator class that mainly forwards staging requests directly,
	only taking into account cache size.

	:param allocatorConfig: old style configuration
	:param catalogue: state of the cache
	:type catalogue: :py:class:`~.hpda.cache.journal.catalogue.Catalogue`
	"""
	def __init__(self, catalogue, interval=300, variance=10):
		ThreadMaster.__init__(self, loop_interval=interval, loop_variance=variance)
		log('allocator', LVL.STATUS, "Instantiating Allocator (%s)", self.__class__.__name__)
		self._mutex         = RLock()
		self._catalogue     = catalogue
		self.last_allocation = time.time()

	def payload(self):
		"""Perform an allocation cycle"""
		self.reallocate()

	def get_allocation_delta(self, file_info_groups=(), allocate_all_groups=False):
		"""
		Re-allocate all known files, incorporating new groups of files

		Creates a new allocation of files to caches, trying to incorporate each
		of the file groups in the process. A list of changes required to reach
		the new allocation is provided.

		The algorithm assumes that file groups must be allocated completely. If
		not all groups can be allocated and ``allocate_all_groups`` is ``False``,
		groups are attempted to be iteratively allocated in order.

		If there are no new file groups or ``allocate_all_groups`` is ``True``
		and any groups fails, only existing files are re-allocated.

		:param file_info_groups: groups of files to be allocated
		:type file_info_groups: list[list[:py:class:`~hpda.cache.fileinfo.FileInfo`]]
		:param allocate_all_groups: require all new groups to be accepted
		:type allocate_all_groups: bool
		:return: the moves and deletions required, and the indexes of allocated file groups
		"""
		try:
			return self._get_allocation_delta_single(list(chain(*file_info_groups))), range(len(file_info_groups))
		except ValueError:
			# try groups iteratively
			if allocate_all_groups:
				accepted_groups = []
				last_allocation = None
				for idx, file_info_group in enumerate(file_info_groups):
					try:
						last_allocation = self._get_allocation_delta_single(list(chain(file_info_group, *(file_info_groups[i] for i in accepted_groups))))
						accepted_groups.append(idx)
					except ValueError:
						pass
				if last_allocation:
					return last_allocation, accepted_groups
		return self._get_allocation_delta_single(), (0,)

	def _get_allocation_delta_single(self, file_info_group=()):
		"""
		The actual allocation algorithm as used by :py:mod:`~.Allocator.get_allocation_delta`

		Tries to allocate all existing files and *all* file groups. If any group
		cannot be allocated, ``ValueError`` is raised.

		:returns: moved and deleted files
		:rtype: list[:py:class:`~.FileViewDiff`, :py:class:`~.CacheAPIBase`], list[:py:class:`~.FileViewDiff`]
		"""
		# create new allocators for all caches
		cache_allocators = []
		for cache in sorted(self._catalogue.backend_apis.cache_apis.values(), key=lambda api:api.sequence, reverse=True):
			cache_allocators.append(cache.get_allocator())
		# try files successively on caches
		sorted_files = sorted(chain(self._catalogue.state.get_file_infos(), file_info_group), key=lambda file_view: file_view.score, reverse=True)
		move_diffs, del_diffs = [], []
		for file_info in sorted_files:
			for allocator in cache_allocators:
				try:
					dest = allocator.allocate(file_info)
				except AllocationFailure:
					continue
				else:
					if file_info.cache != dest:
						move_diffs.append((FileViewDiff(file_info), dest))
					break
			else: # no cache found, release it
				if file_info in file_info_group:
					raise ValueError
				del_diffs.append(FileViewDiff(file_info))
		return move_diffs, del_diffs, cache_allocators

	def reallocate(self, new_file_groups=(), allocate_all_groups=False):
		"""
		Compute the current allocation, optionally including new file groups

		Performs an allocation of existing and new files, as defined by
		:py:meth:`~.Allocator.get_allocation_delta`. The allocation is
		applied to the cache unless new files are provided and none are allocated.

		:param new_file_groups: groups of files to be allocated
		:type new_file_groups: list[list[:py:class:`~hpda.cache.fileinfo.FileInfo`]]
		:param allocate_all_groups: skip all new groups if any cannot be allocated
		:type allocate_all_groups: bool
		:return: allocated groups and cache allocation statistics
		:rtype: tuple[list[int],list[:py:class:`~hpda.cache.backend.base.CacheAllocator`]]
		"""
		log('allocator.allocate', LVL.STATUS, "Re-allocating cache content (%d/%d new groups/files)", len(new_file_groups), sum(len(file_group) for file_group in new_file_groups))
		alloc_stime = time.time()
		allocation_delta, allocated_groups = self.get_allocation_delta(new_file_groups, allocate_all_groups)
		log('allocator.allocate', LVL.STATUS,
			"Re-allocating: %d total, %d moved, %d deleted, %d groups assigned in %.1fs",
			len(self._catalogue.state),
			len(allocation_delta[0]),
			len(allocation_delta[1]),
			sum(len(new_file_groups[group_idx]) for group_idx in allocated_groups),
			time.time()-alloc_stime
		)
		if new_file_groups and not allocated_groups:
			log('allocator.allocate', LVL.WARNING, "Failed allocating any new files, not applying allocation")
			return allocated_groups, allocation_delta[2]
		with self._mutex:
			if allocated_groups:
				log('allocator.allocate', LVL.INFO, "Adding %d files to catalogue", sum(len(new_file_groups[group_idx]) for group_idx in allocated_groups))
				self._catalogue.insert_file_info(*chain(*(new_file_groups[group_idx] for group_idx in allocated_groups)))
			new_tasks = []
			for file_view in allocation_delta[1]:
				log('allocator.allocate', LVL.DEBUG, "Retiring file (%s)->( ) %s", file_view.cache_id, file_view)
				file_view.cache = None
				new_tasks.append(file_view)
			for file_view, dest_cache in reversed(allocation_delta[0]):
				log('allocator.allocate', LVL.DEBUG, "Moving file (%s)->(%s) %s", file_view.cache_id, dest_cache.nickname, file_view)
				file_view.cache = dest_cache
				new_tasks.append(file_view)
			self._catalogue.add_tasks(*new_tasks)
			for cache_allocator in allocation_delta[2]:
				cache_allocator.expose_allocation()
				log('allocator.allocate', LVL.INFO, "Allocation: %s", cache_allocator)
		return allocated_groups, allocation_delta[2]

	def update_files(self, *new_file_data):
		"""
		Update the allocation score of files

		:param file_data:
		"""
		new_file_infos = []
		old_file_infos = []
		failed_files = []
		for file_data in new_file_data:
			try:
				file_view = FileViewDiff(self._catalogue.state[file_data.source_uri])
			except KeyError:
				try:
					api = self._catalogue.backend_apis.get_storage_api(file_data.source_uri)
					file_info = api.get_file_info(file_data.source_uri)
				except InstallationError:
					log('allocator.nominate', LVL.WARNING, "File %s not supported by any storageAPI", file_data)
					failed_files.append(file_data.source_uri)
					#raise InstallationError(file_data.source_uri)
					continue
				except FileNotFound:
					log('allocator.nominate', LVL.WARNING, "File %s does not exist", file_data)
					failed_files.append(file_data.source_uri)
					#raise FileNotFound(file_data.source_uri)
					continue
				else:
					file_info.score = file_data.score
					new_file_infos.append(file_info)
			else:
				if file_data.score is not None:
					file_view.score = file_data.score
				old_file_infos.append(file_view)
				continue
		if new_file_infos:
			log('allocator.allocate', LVL.INFO, "Adding %d files to catalogue", len(new_file_infos))
			self._catalogue.insert_file_info(*new_file_infos)
		if old_file_infos:
			self._catalogue.update_file_info(*old_file_infos)
		return failed_files

	def nominate_groups(self, candidate_groups, allocate_all_groups=False):
		"""
		Try to add new groups of file candidates to the cache

		:param candidate_groups: groups of candidates for caching
		:type candidate_groups: list[list[FileCandidate]]
		:param allocate_all_groups: require all new groups to be accepted
		:type allocate_all_groups: bool
		:return: URIs of staged files and new allocation
		:rtype: tuple[list[str],list[:py:class:`~hpda.cache.backend.base.CacheAllocator` or None]]
		:raises FileNotFound: if any candidate does not exist or is unsupported

		:see: :py:meth:`~.Allocator.get_allocation_delta` for the allocation
		      scheme.
		"""
		log('allocator.nominate', LVL.STATUS, "Checking %d groups with %d candidates", len(candidate_groups), sum(len(group) for group in candidate_groups))
		new_file_groups = []
		for group in candidate_groups:
			new_file_infos = []
			update_file_views = []
			for candidate in group:
				if candidate.source_uri in self._catalogue:
					file_view = FileViewDiff(self._catalogue[candidate.source_uri])
					file_view.score = candidate.score
					update_file_views.append(file_view)
					continue
				try:
					api = self._catalogue.backend_apis.get_storage_api(candidate.source_uri)
					file_info = api.get_file_info(candidate.source_uri)
				except InstallationError:
					log('allocator.nominate', LVL.WARNING, "File %s not supported by any storageAPI", candidate)
					raise InstallationError(candidate.source_uri)
				except FileNotFound:
					log('allocator.nominate', LVL.WARNING, "File %s does not exist", candidate)
					raise FileNotFound(candidate.source_uri)
				else:
					file_info.score = candidate.score
					new_file_infos.append(file_info)
			new_file_groups.append(new_file_infos)
			if update_file_views:
				self._catalogue.update_file_info(*update_file_views)
		if not any (new_file_groups):
			log('allocator.nominate', LVL.STATUS, "All files already registered.")
			return [candidate.source_uri for candidate in chain(*candidate_groups)], [cache.allocator for cache in self._catalogue.backend_apis.cache_apis.values()]
		allocated_groups, allocation_stats = self.reallocate(new_file_groups=new_file_groups, allocate_all_groups=allocate_all_groups)
		return [candidate.source_uri for candidate in chain(*(candidate_groups[idx] for idx in allocated_groups))], allocation_stats


AllocatorConfig = Section(
	target=Allocator,
	attributes={
		"interval" : CfgSeconds(default="5m",descr="Interval between starting work cycle."),
		"variance" : CfgSeconds(default="10s",descr="Variance on work cycle interval."),
	},
	name="CacheAllocator",
	descr="Manager of the cache resources. Assigns files to backends or removes them."
)