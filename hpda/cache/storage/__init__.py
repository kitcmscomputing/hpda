#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
Provides backends for accessing different storage APIs with a common interface.

Storage APIs guarantee to strictly provide the interface as defined by
:py:class:`~hpda.cache.storage.base.StorageAPIBase` with the sole exception of
constructor arguments.

:see: :py:mod:`~hpda.cache.backend` for the corresponding API to local caches.

:note: For simplicity, storage backends are Singletons (just like cache backends).
       The idenitifer for backends is their ``nickname``; it is an error to lookup
       a backend via its singleton behaviour before it has been properly instantiated.
"""