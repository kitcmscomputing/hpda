#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# standard library imports
import logging
import threading

# third party imports

# application/library imports
from hpda.utility.report     import LVL
from hpda.utility.exceptions import AbstractError, FileNotFound, ExceptionFrame
from hpda.utility.singleton  import BaseSingleton, singleton__init

def get_source_uri(source_uri_container):
	"""
	Extract the source_uri from one of several container types

	:param source_uri_container: a plain ``str`` or an object with a ``source_uri`` attribute
	:return: extracted source_uri
	:raises TypeError: if extraction is not possible because of the type
	"""
	try:
		return source_uri_container.source_uri
	except AttributeError:
		if isinstance(source_uri_container, basestring):
			return source_uri_container
	raise TypeError("%r is an invalid filename_container"%source_uri_container)


class StorageAPIBase(BaseSingleton):
	"""
	API for accessing files on storage

	:param nickname: identifier for this API
	:type nickname: str
	:param sequence: ordering hint if several APIs match a file; higher is tried first
	:type sequence: int or float
	:param uri_prefix: the common beginning of any URI managed by this API
	:type uri_prefix: str
	"""
	_instances = {}
	@singleton__init
	def __init__(self, nickname, sequence=None, uri_prefix=None):
		assert None not in (sequence, uri_prefix), "Storage APIs must be initialized before being called"
		self.nickname = nickname
		self.sequence = sequence
		self.uri_prefix = uri_prefix
		self.logger = getattr(self, "logger", logging.getLogger("storage.%s"%self.__class__.__name__))

	@classmethod
	def _singleton_signature(cls, nickname, *args, **kwargs):
		return nickname

	def __repr__(self):
		return "%s(nickname=%r, sequence=%r, uri_prefix=%r)"%(self.__class__.__name__, self.nickname, self.sequence, self.uri_prefix)

	# file accounting mechanisms
	def supports_uri(self, source_uri):
		"""
		Test if a given file URI is compatible with this backend

		This function merely checks compatibility of the given URI and the backend's
		URI handling mechanisms. No guarantee is given that the file actually exists
		or that it can be accessed, e.g. in terms of access rights.

		:param source_uri: full URI of the file, including protocols etc.
		:type source_uri: str
		:return: whether the file is supported
		:rtype: bool
		"""
		return source_uri.startswith(self.uri_prefix)

	def guess_uri(self, filename):
		"""
		Guess a ``source_uri`` fitting for a given ``filename``

		This method attempts the reverse operation to how a ``filename`` is
		constructed for a ``source_uri``, that is create a ``source_uri`` which
		would create a :py:class:`~.FileInfo` with the given ``filename``.

		:note: There is no guarantee that the ``source_uri`` points to an existing
		       file.

		:param filename:
		:return:
		"""
		raise AbstractError

	def get_file_info(self, source_uri):
		"""
		Provides the :py:class:`~hpda.cache.fileinfo.FileInfo` for the given URI

		:param source_uri: full URI of the file, including protocols etc.
		:type source_uri: str
		:return: FileInfo to the given source URI
		:rtype: :py:class:`~hpda.cache.fileinfo.FileInfo`
		:raises: :py:class:`~hpda.utility.exceptions.FileNotFound` if the file does not exist
		"""
		raise AbstractError

	def restore_file_info(self, filename, cache=None):
		"""
		Find the source file corresponding to a ``filename``, and provide its :py:class:`~hpda.cache.fileinfo.FileInfo`

		This function attempts to find a file on the managed storage that would
		resolve to match the ``filename``. If successfull, the corresponding :py:class:`~hpda.cache.fileinfo.FileInfo`
		is provided. Otherwise, :py:exc:`~.FileNotFound` is raised.

		:note: The primary use-case for this facility is the reconstruction of the
		       cache manager's meta-data from the de-facto truth from underlying
		       cache devices. Orphaned files may be tried for reconstruction instead
		       of retired directly.

		:param filename: the ``filename`` of a file in the cache system
		:type filename: str
		:param cache: cache the file currently resides on
		:type cache: :py:class:`~.CacheAPIBase`
		:return: FileInfo to the reconstructed source_uri
		:rtype: :py:class:`~hpda.cache.fileinfo.FileInfo`
		:raises FileNotFound: if no source file can be found
		"""
		raise FileNotFound

	# file caching mechanisms
	def verify_copy(self, fileview):
		"""
		Check the validity of a cached copy against its original source

		For portability, this will only check whether source and copy are identical.
		No details of the process are provided, e.g. the response is the same if
		the source vanished, the copy vanished or source and copy differ in size.

		Implementations are free to provide debugging information via their logging
		facilities.

		:param fileview: view to the info of a file
		:type fileview: :py:class:`~hpda.cache.fileinfo.FileViewDiff`
		:return: whether copy and source are equivalent
		:rtype: bool
		"""
		raise AbstractError

	def refresh_copy(self, fileview, callback=None):
		"""
		Update a cached copy to match its original source

		This method updates the local copy and runs ``callback`` when done. Barring
		race conditions, this is a guarantee that the copy will match the source
		once ``callback`` is called.

		If ``callback`` is not ``None``, it must be a callable of the signature

		.. py:function:: callback(file_view : FileViewDiff) -> None

		and will be called after the refresh with the updated FileView.

		:param fileview: view to the info of a file
		:type fileview: :py:class:`~hpda.cache.fileinfo.FileViewDiff`
		:param callback: function to call when refresh is complete
		:type callback: callable
		:return:
		"""
		raise AbstractError

	def refresh_copy_async(self, fileview, callback=None):
		"""
		Same as :py:func:`refresh_copy`, but executing asynchronously (non-blocking)

		:param fileview: view to the info of a file
		:type fileview: :py:class:`~hpda.cache.fileinfo.FileViewDiff`
		:param callback: function to call when refresh is complete
		:type callback: callable
		"""
		def payload():
			with ExceptionFrame():
				self.refresh_copy(fileview = fileview, callback = callback)
		self.logger.log(LVL.STATUS, "Launching update thread for %s", fileview)
		threading.Thread(
			target = payload,
			).start()

	@property
	def json(self):
		"""Get information on the backend"""
		return {
			"name" : self.nickname,
			"class" : self.__class__.__name__,
			"sequence" : self.sequence,
			"uri_prefix" : self.uri_prefix,
		}