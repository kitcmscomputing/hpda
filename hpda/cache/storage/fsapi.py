#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# standard library imports
import os
import shutil
import errno

# third party imports

# application/library imports
from hpda.utility.exceptions import FileNotFound, APIError
from hpda.utility.report     import LVL
from hpda.utility.configuration.interface import Section, CfgStr, CfgFloat
from hpda.utility.utils      import ensure_dir, NotSet
from hpda.cache.fileinfo     import FileInfo
from hpda.utility.singleton  import singleton__init
from .base import StorageAPIBase, get_source_uri

class FSStorage(StorageAPIBase):
	"""
	API for accessing files with default OS filesystem tools

	:param raw_mount: mount point of the actual source, without any cache overlay mounts
	:type raw_mount: str
	:see: :py:class:`.StorageAPIBase` for additional parameters
	"""
	@singleton__init
	def __init__(self, nickname, sequence=None, uri_prefix=None, raw_mount=None):
		StorageAPIBase.__init__(self, nickname=nickname, sequence=sequence, uri_prefix=uri_prefix)
		if not os.path.exists(raw_mount):
			raise APIError("%s mount '%s' does not exist"%(self.__class__.__name__, raw_mount))
		self.raw_mount = raw_mount
		self.logger.log(LVL.STATUS, "Instantiated Storage Backend (%r)", self)

	def __repr__(self):
		return StorageAPIBase.__repr__(self)[:-1] + ", raw_mount=%r)" % self.raw_mount

	def get_file_info(self, source_uri):
		return FileInfo(
			source_uri = source_uri,
			filename   = self._raw_file_path(source_uri).lstrip(os.sep),
			storage    = self,
			size       = self._get_source_stats(source_uri).st_size,
			)

	def restore_file_info(self, filename, cache=NotSet):
		source_uri = os.path.join(self.uri_prefix, *filename.split(os.path.sep))
		return FileInfo(
			source_uri = source_uri,
			filename   = self._raw_file_path(source_uri).lstrip(os.sep),
			storage    = self,
			size       = self._get_source_stats(source_uri).st_size,
			cache      = cache,
			)

	def verify_copy(self, fileview):
		self.logger.log(LVL.INFO, "Verifying copy of %s", fileview.source_uri)
		if not self._source_exists(fileview):
			self.logger.log( LVL.WARNING, 'No source for %s', fileview)
			return False
		try:
			local_stat  = fileview.cache.get_copy_stats(fileview)
			source_stat = self._get_source_stats(fileview)
			# st_mtime is float - 1s precission should be enough for most cases
			if local_stat.st_mtime + 1 < source_stat.st_mtime:
				self.logger.log( LVL.WARNING, 'No os.stat.st_mtime match for %s (local vs source: %d vs %d)', fileview, local_stat.st_mtime, source_stat.st_mtime)
				return False
			if  local_stat.st_size != source_stat.st_size:
				self.logger.log( LVL.WARNING, 'No os.stat.st_size match for %s (local vs source: %d vs %d)', fileview, local_stat.st_size, source_stat.st_size)
				return False
		except FileNotFound:
			return False
		return True

	def refresh_copy(self, fileview, callback=None):
		self.logger.log(LVL.INFO, "Refreshing copy of %s", fileview.source_uri)
		try:
			if not self._source_exists(fileview):
				self.logger.log(LVL.WARNING, "Releasing copy of retired source %s", fileview.source_uri)
				fileview.cache.release_file(fileview)
				fileview.cache = None
			else:
				stage_path  = fileview.cache.get_staging_path(fileview)
				ensure_dir(os.path.dirname(stage_path))
				self.logger.log(LVL.INFO, "Staging '%s'->'%s'->(???)", self._raw_file_path(fileview.source_uri), stage_path)
				shutil.copy2(self._raw_file_path(fileview.source_uri), stage_path)
				fileview.cache.digest_staged_copy(fileview, stage_path)
				fileview.file_stats = fileview.cache.get_copy_stats(fileview)
		except Exception as err:
			# TODO: be a bit more finegrained here in the future
			self.logger.log(LVL.WARNING, "Unstaging after failure %s: %s", fileview, err)
			fileview.cache.release_file(fileview)
			fileview.cache = NotSet
		if callback is not None:
			callback(fileview)
		self.logger.log(LVL.STATUS, "Refreshed copy of '%s'", fileview)

	# file acces methods with inbuilt path resolutions
	@staticmethod
	def file_stats(filepath):
		return os.stat(filepath)

	def guess_uri(self, filename):
		return os.path.join(self.uri_prefix, *filename[len(self.raw_mount):].split(os.sep))

	def _raw_file_path(self, source_uri):
		"""Return the filepath on the raw mount point"""
		return os.path.join(self.raw_mount, *source_uri[len(self.uri_prefix):].split(os.sep))

	def _get_source_stats(self, source_uri):
		try:
			real_uri = self._raw_file_path(get_source_uri(source_uri))
			return self.file_stats(real_uri)
		except (IOError, OSError) as err:
			if err.errno == errno.ENOENT:
				raise FileNotFound
			raise

	def _source_exists(self, source_uri):
		real_uri = self._raw_file_path(get_source_uri(source_uri))
		return os.path.exists(real_uri)

ConfigSection = Section(
	target=FSStorage,
	attributes={
		"sequence"  : CfgFloat(default=0, descr="Order in which the backend is tried."),
		"uri_prefix": CfgStr(descr="Start of all URIs of files available from this backend. This should be the mount point as seen without cache infrastructure."),
		"raw_mount" : CfgStr(descr="Mount point of the source, without any caches overlayed."),
	},
	descr="Data source accessible via POSIX commands. Any kind of readable mount point or subfolder is adequate."
)