#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# standard library imports
import os
import shutil
import errno

# third party imports

# application/library imports
from hpda.utility.exceptions import FileNotFound, PermissionDenied
from hpda.utility.report     import LVL
from hpda.utility.configuration.interface import Section, CfgStr, CfgFloat, CfgBytes, CfgPercent, ConfigMultiAttribute
from hpda.utility.utils      import ensure_rm, ensure_dir, niceBytes
from hpda.utility.volumes    import VolumeStats
from hpda.utility.singleton  import singleton__init
from .base import CacheAPIBase, get_filename


class FSCache(CacheAPIBase):
	"""
	API for caching files with default OS level tools

	:param nickname: identifier for this API
	:type nickname: str
	:param sequence: ordering hint if several APIs match a file; higher is tried first
	:type sequence: int or float
	:param uri_prefix: the common beginning of any URI managed by this API
	:type uri_prefix: str
	:param size_limit: limit to the size of the cache
	:type size_limit: int or float

	The parameter ``uri_prefix`` should be chosen to match the root level of user
	application access.
	For example, when using a union cache mount and ``/cache/store`` is mounted
	over ``/source/store`` at ``/store``, ``uri_prefix`` for the cache is ``/cache/store``
	and for the storage it is ``/source/store``.

	The parameter ``size_limit`` is interpreted:
	A number smaller than ``1`` is applied as a factor to the hosting device capacity.
	Numbers bigger than ``1`` are interpreted as a size in Bytes.
	Any other value raises :py:class:`TypeError`.
	"""
	@singleton__init
	def __init__(self, nickname, sequence=None, uri_prefix=None, size_limit=0.9):
		CacheAPIBase.__init__(self, nickname=nickname, sequence=sequence, uri_prefix=uri_prefix)
		ensure_dir(uri_prefix)
		self._volume = VolumeStats(uri_prefix)
		if size_limit > 1:
			self.size_limit = size_limit
		elif 0 < size_limit < 1:
			self.size_limit = size_limit * self._volume.total
		else:
			raise ValueError("size_limit must be a number 0 < x < 1 or 1 < x, got %r"%size_limit)
		self.logger.log(LVL.STATUS, "Instantiated Cache Backend (%r)", self)

	def __repr__(self):
		return CacheAPIBase.__repr__(self)[:-1] + ", size_limit=%s)" % niceBytes(self.size_limit)

	# cache file copy methods
	def _get_copy_path(self, filename):
		"""Return the path to the cached copy"""
		return os.path.join(self.uri_prefix, *get_filename(filename).split(os.sep))

	def get_staging_path(self, filename):
		file_parts = get_filename(filename).split(os.path.sep)
		file_parts[-1] = self._staging_basename(file_parts[-1])
		return os.path.join(self.uri_prefix, *file_parts)

	def _staging_basename(self, basename):
		"""Return the basename of a temporary staging copy"""
		return "." + basename + ".hpda"

	def _staging_targetname(self, tmp_basename):
		"""Return the basename of a final copy, given its temporary staging copy name"""
		if not tmp_basename.startswith(".") or not tmp_basename.endswith(".hpda"):
			raise ValueError
		return tmp_basename[1:-5]

	def get_filename(self, copy_path):
		file_parts = get_filename(copy_path).split(os.path.sep)[len(self.uri_prefix.split(os.sep)):]
		return os.path.join(*file_parts)

	def digest_staged_copy(self, file_info, stage_path=None):
		stage_path = stage_path or self.get_staging_path(file_info)
		cache_path = self._get_copy_path(file_info)
		self.logger.log(LVL.INFO, "Caching ('%s')->'%s'->'%s'", file_info.source_uri, stage_path, cache_path)
		ensure_dir(os.path.dirname(cache_path))
		try:
			shutil.move(
				stage_path,
				cache_path,
				)
		except (IOError, OSError) as err:
			self.logger.log(LVL.WARNING, "Caching '%s' failed: %s", file_info, err)
			if err.errno == errno.ENOENT:
				raise FileNotFound
			if err.errno in (errno.EACCES, errno.EPERM):
				raise PermissionDenied
			raise
		file_info.cache = self
		return file_info

	def migrate_copy(self, file_info):
		# tell the other where we expect to find a copy for digestion
		stage_path = self.get_staging_path(file_info)
		file_info.cache.stage_copy(file_info, stage_path)
		# we have it, the other can kill the "original"
		file_info.cache.release_file(file_info)
		return self.digest_staged_copy(file_info, stage_path)

	def stage_copy(self, file_info, stage_path):
		source_path = self._get_copy_path(file_info)
		ensure_dir(os.path.dirname(stage_path))
		self.logger.log(LVL.INFO, "Staging '%s'->'%s'->(???)", source_path, stage_path)
		try:
			shutil.copy2(source_path, stage_path)
		except (IOError, OSError) as err:
			self.logger.log(LVL.WARNING, "Staging '%s' failed: %s", file_info, err)
			if err.errno == errno.ENOENT:
				raise FileNotFound
			if err.errno in (errno.EACCES, errno.EPERM):
				raise PermissionDenied
			raise

	def release_file(self, filename):
		filename = get_filename(filename)
		try:
			ensure_rm(self._get_copy_path(filename))
			return True
		except:
			return False

	def get_copy_stats(self, filename):
		try:
			return os.stat(self._get_copy_path(filename))
		except (IOError, OSError) as err:
			if err.errno == errno.ENOENT:
				raise FileNotFound
			raise

	# cache volume methods
	def volume_stats(self):
		self._volume.refresh()
		return {
			"total" : min(self._volume.total,self.size_limit),
			"avail" : self._volume.avail,
			"used"  : self._volume.used,
		}

	def consistency_walk(self, root_paths=()):
		"""
		:see: :py:meth:`~.CacheAPIBase.consistency_walk`

		Temporary files and empty folders are removed automatically.
		"""
		root_paths = [self._get_copy_path(root_path) for root_path in root_paths]
		for current_dir, sub_dirs, files in os.walk(self.uri_prefix, topdown=False):
			# clean up content only up to the root path level
			for root_path in root_paths:
				if current_dir in root_path and current_dir.rstrip(os.sep) != root_path.rstrip(os.sep):
					break
			else:
				for file_base in files:
					try:
						# is it a temporary stage file?
						copy_base = self._staging_targetname(file_base)
					except ValueError:
						stage_path = None
						copy_path = os.path.join(current_dir, file_base)
					else:
						stage_path = os.path.join(current_dir, file_base)
						copy_path = os.path.join(current_dir, copy_base)
						# if the real file exists, just discard the stage file...
						if os.path.exists(copy_base):
							self.release_file(os.path.join(current_dir, file_base))
						# ...else recover the real file
						else:
							try:
								shutil.move(
									stage_path,
									copy_path,
									)
							except (IOError, OSError) as err:
								# somebody else moved it already, or its not one of ours
								if err.errno in (errno.ENOENT, errno.EACCES):
									self.logger.log(LVL.WARNING, "%s while trying to recover temporary file: %s->%s", err, stage_path, copy_path)
								else:
									raise
					yield self, self.get_filename(copy_path)
			# attempt cleanup after directory is handled
			# never clean up root paths and any of their parents
			for root_path in root_paths:
				if current_dir in root_path:
					break
			else:
				if not os.listdir(current_dir) and current_dir != self.uri_prefix:
					try:
						os.rmdir(current_dir)
					except Exception as err:
						self.logger.log(LVL.STATUS, "Failed removing empty directory %s: %s", current_dir, err)


ConfigSection = Section(
	target=FSCache,
	attributes={
		"sequence"  : CfgFloat(default=0, descr="Order in which the backend is tried. Higher sequences are tried first, negative ones are ignored."),
		"uri_prefix": CfgStr(descr="Start of all URIs of files managed with this backend. This is commonly the path to a device mount point or subfolder."),
		"size_limit": ConfigMultiAttribute(descr="Limit of the space to use for caching files.",
			variants=(
				CfgPercent(default="0.75", descr="Fraction of the total device volume to use."),
				CfgBytes(descr="Absolute size to use on the device."),
			)),
	},
	descr="Cache device accessible via POSIX commands. Any kind of r/w mount point or subfolder is adequate."
)