#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# standard library imports
import logging

# third party imports

# application/library imports
from hpda.utility.utils import niceBytes
from hpda.utility.exceptions import AbstractError, BasicException
from hpda.utility.singleton  import BaseSingleton, singleton__init


class AllocationFailure(BasicException):
	"""Raised when a file cannot be allocated to a cache"""
	pass


class CacheAllocator(object):
	"""
	Allocator of files inside a cache

	This line of classes is intended to abstract the allocation performed by the
	:py:class:`~hpda.cache.allocator.Allocator`, hiding implementation details.
	The primary use are complex backends such as the multiapi.

	:param cache_api: API for which to provide the allocation
	:type cache_api: :py:class:`~.CacheAPIBase`
	:param size: size available on the cache for allocation, in bytes
	:type size: int or float
	"""
	def __init__(self, cache_api, size=None):
		self.cache_api = cache_api
		self.size = size or cache_api.volume_stats()["total"]
		self.allocated = 0
		# score keeping
		self.score_total = 0
		self.files_total = 0
		self.score_max = 0
		self.score_min = float("inf")

	@property
	def cache_id(self):
		return self.cache_api.nickname

	@property
	def score_average(self):
		if not self.files_total:
			return 0
		return self.score_total/self.files_total

	@property
	def score_per_byte(self):
		if not self.allocated:
			return 0
		return self.score_total/self.allocated

	def allocate(self, file_info):
		"""
		Make an allocation of a file to the managed cache API

		:param file_info: meta data of the file to allocate
		:type file_info: :py:class:`~hpda.cache.fileinfo.FileInfo` or :py:class:`~hpda.cache.fileinfo.FileViewDiff`
		:raises AllocationFailure:
		"""
		if file_info.size + self.allocated <= self.size:
			self.allocated += file_info.size
			self.files_total += 1
			self.score_total += file_info.score
			self.score_max = max(file_info.score, self.score_max)
			self.score_min = min(file_info.score, self.score_min)
			return self.cache_api
		raise AllocationFailure

	def __repr__(self):
		return "%s(cache_api=%s, max_size=%s, allocated=%s, files_total=%d, score_tot=%s, score_min=%s, score_max=%s, score_avg=%s, score_bytes=%s)"%(
		self.__class__.__name__, self.cache_api.nickname, niceBytes(self.size), niceBytes(self.allocated),
		self.files_total, self.score_total, self.score_min, self.score_max, self.score_average, self.score_per_byte,
		)

	@property
	def json(self):
		"""Get information in JSON compatible structure"""
		return {
			'max_size' : self.size,
			'allocated' : self.allocated,
			'files_total' : self.files_total,
			'score_total' : self.score_total,
			'score_min' : self.score_min,
			'score_max' : self.score_max,
			'score_average' : self.score_average,
			'score_per_byte' : self.score_per_byte,
		}

	def expose_allocation(self):
		"""
		Provide the allocation currently used for later query
		"""
		self.cache_api._expose_allocation(self)

def get_filename(filename_container):
	"""
	Extract the filename from one of several container types

	:param filename_container: a plain ``str`` or an object with a ``filename`` attribute
	:return: extracted filename
	:raises TypeError: if extraction is not possible because of the type
	"""
	try:
		return filename_container.filename
	except AttributeError:
		if isinstance(filename_container, basestring):
			return filename_container
	raise TypeError("%r is an invalid filename_container"%filename_container)


class CacheAPIBase(BaseSingleton):
	"""
	API for caching files

	:param nickname: identifier for this API
	:type nickname: str
	:param sequence: ordering hint if several APIs match a file; higher is tried first
	:type sequence: int or float
	:param uri_prefix: the common beginning of any URI managed by this API
	:type uri_prefix: str
	"""
	_instances = {}
	@singleton__init
	def __init__(self, nickname, sequence=None, uri_prefix=None):
		assert None not in (sequence, uri_prefix), "Cache APIs must be initialized before being called"
		self.nickname = nickname
		self.sequence = sequence
		self.uri_prefix = uri_prefix
		self.logger = getattr(self, "logger", logging.getLogger("cache.%s"%self.__class__.__name__))
		self.allocator = None

	@classmethod
	def _singleton_signature(cls, nickname, *args, **kwargs):
		return nickname

	def __repr__(self):
		return "%s(nickname=%r, sequence=%r, uri_prefix=%r)"%(self.__class__.__name__, self.nickname, self.sequence, self.uri_prefix)

	# cache file copy methods
	def get_staging_path(self, filename):
		"""
		Return a path for storage APIs to stage new copies

		:param filename: the internal name of the file
		:type filename: str or :py:class:`~hpda.cache.fileinfo.FileInfo`
		:return: staging path
		:rtype: str
		"""
		raise AbstractError

	def digest_staged_copy(self, file_info, stage_path=None):
		"""
		Move a staged file to the cache

		:param file_info: the information catalogued for the file
		:type file_info: :py:class:`~hpda.cache.fileinfo.FileInfo` or :py:class:`~hpda.cache.fileinfo.FileViewDiff`
		:param stage_path: optional path where the file has been staged
		:type stage_path: str
		:return:
		"""
		raise AbstractError

	def migrate_copy(self, file_info):
		"""
		Move a copy from another cache to this one

		:param file_info:
		:return: :py:class:`~hpda.cache.fileinfo.FileInfo` or :py:class:`~hpda.cache.fileinfo.FileViewDiff`

		:note: this function works in conjuction with :py:meth:`~.CacheAPIBase.stage_copy`.
		"""
		raise AbstractError

	def stage_copy(self, file_info, stage_path):
		"""
		Move a copy from this cache for staging to another one

		:param file_info:
		:return: :py:class:`~hpda.cache.fileinfo.FileInfo` or :py:class:`~hpda.cache.fileinfo.FileViewDiff`

		:note: this function works in conjuction with :py:meth:`~.CacheAPIBase.migrate_copy`.
		"""
		raise AbstractError

	def release_file(self, filename):
		"""
		Release a file from the cache

		:param filename: the internal name of the file
		:type filename: str or :py:class:`~hpda.cache.fileinfo.FileInfo`
		:return: whether operation succeeded
		:rtype: bool
		"""
		raise AbstractError

	def get_copy_stats(self, filename):
		"""
		Return the ``os.stat`` equivalent of the copy

		:param filename: filename or the FileInfo of the file
		:type filename: str or :py:class:`~hpda.cache.fileinfo.FileInfo`
		:return: stat of the copy
		:raises FileNotFound: if no copy exists on the cache
		"""
		raise AbstractError

	def consistency_walk(self, root_paths=()):
		"""
		Walk through all files on the cache

		For each file found on the cache, yield the api and the file's file path
		(the path relative to its source).

		This method is intended for consistency checking. The yielded data should
		allow checking if meta-data is available, i.e. whether the file should
		still be cached.

		:param root_paths: list of file path roots at which to stop cleaning up
		:type root_paths: list[str]
		:yield: filename and stage path (if applicable)
		:rtype: :py:class:`~.CacheAPIBase`, str
		"""
		raise AbstractError

	def _expose_allocation(self, allocator):
		"""
		Provide the allocation currently used for later query

		:param allocator: new allocation scheme to expose
		:type allocator: :py:class:`~.CacheAllocator`
		"""
		self.allocator = allocator

	# cache volume methods
	def volume_stats(self):
		"""
		Get the stats of the underlying volume

		This function returns a mapping containing information about ``total``,
		``used`` and ``avail`` space, in Bytes.

		:return: stats of the containing volume
		"""
		return {
			"total" : 0,
			"avail" : 0,
			"used"  : 0,
		}

	@property
	def json(self):
		"""Get information in JSON compatible structure"""
		return {
			"name" : self.nickname,
			"class" : self.__class__.__name__,
			"sequence" : self.sequence,
			"uri_prefix" : self.uri_prefix,
			"volume" : self.volume_stats(),
			"allocation" : self.allocator.json if self.allocator is not None else None,
		}

	def get_allocator(self):
		return CacheAllocator(cache_api=self)
