#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
Provides backends for accessing different cache setups with a common interface.

The existence of this module is mainly for extensibility, e.g. for multi-device
caches and cross-host shared caches. At the moment, :py:class:`~hpda.cache.backend.osapi.OSCache`
both defines and implements the reference interface for simplicity.

:see: :py:mod:`~hpda.cache.storage` for the corresponding API to (remote) storage.

:note: For simplicity, cache backends are Singletons (just like storage backends).
       The idenitifer for backends is their ``nickname``; it is an error to lookup
       a backend via its singleton behaviour before it has been properly instantiated.
"""