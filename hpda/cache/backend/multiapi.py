#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# standard library imports
import random
import itertools

# third party imports

# application/library imports
from hpda.utility.report import LVL
from hpda.utility.exceptions import APIError
from hpda.utility.singleton  import singleton__init
from .base import CacheAPIBase, CacheAllocator, AllocationFailure
from hpda.utility.configuration.interface import Section, CfgStr, CfgFloat, CfgStrArray


class RandomSelector(object):
	static=False
	def __init__(self, caches):
		self.caches = caches
	def select(self, fileview):
		return random.choice(self.caches)


class SequenceSelector(object):
	static=False
	def __init__(self, caches):
		self.caches = caches
		self._last = 0
	def select(self, fileview):
		self._last = (self._last + 1 ) % len(self.caches)
		return self.caches[self._last]


class HashSelector(RandomSelector):
	static=True
	def select(self, fileview):
		return self.caches[((hash(fileview.source_uri)&0xffffffff) % len(self.caches))]


class MultiCacheAllocator(CacheAllocator):
	"""
	CacheAllocator handling multiple caches

	Allocation is performed according to the ``selector_type``. If it is not a
	static selector, then files which cannot be allocated at first are tried on
	**all** slaved cache apis.

	:param
	"""
	def __init__(self, cache_api):
		CacheAllocator.__init__(self, cache_api, sum(api.volume_stats()["total"] for api in cache_api.slaves))
		self.slave_allocators = dict((api, api.get_allocator()) for api in cache_api.slaves)
		self.selector = cache_api.selector(self.slave_allocators.values())

	@property
	def allocated(self):
		return sum(slave.allocated for slave in self.slave_allocators.values())
	@allocated.setter
	def allocated(self, value):
		pass

	@property
	def score_total(self):
		return sum(slave.score_total for slave in self.slave_allocators.values())
	@score_total.setter
	def score_total(self, value):
		pass

	@property
	def files_total(self):
		return sum(slave.files_total for slave in self.slave_allocators.values())
	@files_total.setter
	def files_total(self, value):
		pass

	@property
	def score_max(self):
		return max(slave.score_max for slave in self.slave_allocators.values())
	@score_max.setter
	def score_max(self, value):
		pass

	@property
	def score_min(self):
		return min(slave.score_min for slave in self.slave_allocators.values())
	@score_min.setter
	def score_min(self, value):
		pass

	@property
	def score_average(self):
		if not self.slave_allocators or not self.files_total:
			return 0
		return 1.0*self.score_total/self.files_total
	@score_average.setter
	def score_average(self, value):
		pass

	@property
	def score_per_byte(self):
		if not self.slave_allocators or not self.allocated:
			return 0
		return 1.0*self.score_total/self.allocated
	@score_per_byte.setter
	def score_per_byte(self, value):
		pass

	def allocate(self, file_info):
		if file_info.cache in self.slave_allocators:
			return self.slave_allocators[file_info.cache].allocate(file_info)
		try:
			return self.selector.select(file_info).allocate(file_info)
		except AllocationFailure:
			if self.selector.static:
				raise
		for allocator in self.slave_allocators.itervalues():
			try:
				return allocator.allocate(file_info)
			except AllocationFailure:
				continue
		raise AllocationFailure


class MultiCache(CacheAPIBase):
	"""
	API for combining several cache devices into one logical cache

	:param nickname: identifier for this API
	:type nickname: str
	:param slaves: nicknames of the joined caches
	:type slave: list[str]
	:param selector: method to use for selection
	:type selector: str

	The Multicache works as a master interface to its slave APIs, making them
	accessible as a single cache API. The API is only serving as a frontend for
	selecting the underlying caches and their statistics. Internal workflows for
	file maintenance are still provided via the underlying APIs.

	:note: Once slaved to the MultiCache, :py:class:`~.CacheAPIBase`\ s are not
	       intended for direct assignment. To enforce this, the MultiCache is
	       free to make arbitrary modifications (usually, this simply means
	       demoting their sequence).
	"""
	selectors = {
		"random" : RandomSelector,
		"sequence" : SequenceSelector,
		"hash" : HashSelector,
	}
	@singleton__init
	def __init__(self, nickname, sequence=None, slaves=(), selector="random"):
		# explicit check as we delay the BaseClass Check
		assert slaves and ( selector is not None ), "Cache APIs must be initialized before being called"
		self.slaves = [CacheAPIBase(slave_name) for slave_name in slaves]
		CacheAPIBase.__init__(self, nickname=nickname, sequence=sequence, uri_prefix="<%s>"%(":".join(api.uri_prefix for api in self.slaves)))
		self.selector = self.selectors[selector]
		self.logger.log(LVL.STATUS, "Instantiated Cache Backend (%r)", self)

	def get_allocator(self):
		return MultiCacheAllocator(cache_api=self)

	def __repr__(self):
		return CacheAPIBase.__repr__(self)[:-1] + ", slaves=<%s>, selector=%r)"% (
			":".join(api.nickname for api in self.slaves),
			self.selector.__name__
		)

	def get_staging_path(self, filename):
		raise APIError("%s exposes this API only via slaves"%self.__class__.__name__)
	def digest_staged_copy(self, file_info, stage_path=None):
		raise APIError("%s exposes this API only via slaves"%self.__class__.__name__)
	def migrate_copy(self, file_info):
		raise APIError("%s exposes this API only via slaves"%self.__class__.__name__)
	def stage_copy(self, file_info, stage_path):
		raise APIError("%s exposes this API only via slaves"%self.__class__.__name__)
	def release_file(self, filename):
		raise APIError("%s exposes this API only via slaves"%self.__class__.__name__)
	def get_copy_stats(self, filename):
		raise APIError("%s exposes this API only via slaves"%self.__class__.__name__)

	def consistency_walk(self, root_paths=()):
		"""
		:see: :py:meth:`~.CacheAPIBase.consistency_walk`

		This merely forwards the actions from the slaved APIs. It is guaranteed
		that the :py:class:`~.MultiCache` does not introduce any side-effects
		to the underlying APIs.
		"""
		for slave in self.slaves:
			for item in slave.consistency_walk(root_paths=root_paths):
				yield item

	def volume_stats(self):
		total_volume = {}
		for slave in self.slaves:
			slaved_volume = slave.volume_stats()
			for key in slaved_volume.keys():
				total_volume[key] = total_volume.setdefault(key, 0) + slaved_volume[key]
		return total_volume


ConfigSection = Section(
	target=MultiCache,
	attributes={
		"sequence" : CfgFloat(default=0, descr="Order in which the backend is tried. Higher sequences are tried first, negative ones are ignored."),
		"slaves"   : CfgStrArray(default=(), descr="Nickname of slaves to be handled by this API."),
		"selector" : CfgStr(default="random", descr="Placement strategy to use for assigning files to slaves. ``%r``"%MultiCache.selectors.keys()),
	},
	descr="Pseudo cache combining several APIs."
)