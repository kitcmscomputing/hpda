#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# standard library imports

# third party imports

# application/library imports
from hpda.utility.configuration.interface import Group, Choice
from .fsapi import ConfigSection as FsApiCfgSection
from .multiapi import ConfigSection as MultiApiCfgSection

AnyCacheSection = Choice(
	sections={
		"posix" : FsApiCfgSection,
		"fs"    : FsApiCfgSection,
		"fsapi" : FsApiCfgSection,
		"filesystem" : FsApiCfgSection,
		"multi" : MultiApiCfgSection,
		"multiapi" : MultiApiCfgSection,
		"master": MultiApiCfgSection,
	},
	type_default="posix",
)

CacheConfig = Group(
	target=AnyCacheSection,
	name="cache",
	section_kw="nickname",
	descr="Resources available for caching files. The nickname is arbitrary but should not change between configuration modifications.",
	order=["posix","fs","fsapi","filesystem","multi","multiapi","master"]
)