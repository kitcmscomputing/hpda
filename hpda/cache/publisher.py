#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# standard library imports
import collections
import threading
import logging
import time

# third party imports

# application/library imports
from hpda.utility.threads    import ThreadMaster
from hpda.utility.report     import LVL
from hpda.common.pool_mapper import NodeRole
from hpda.utility.utils      import NotSet
from hpda.client.locator     import Locator
from hpda.utility.configuration.interface import Section, CfgSeconds

# flags
# hook type
_INSERT = 0
_UPDATE = 1
_REMOVE = 2
# file state
_NEWFILE = 4 # file is (probably) new
_DELFILE = 8 # file has been deleted

class Publisher(ThreadMaster):
	"""
	Regularly publishes changes of the cache state

	:param pool_mapper: map of nodes registered in the node
	:type pool_mapper: :py:class:`~hpda.common.pool_mapper.HeartbeatMapper`
	:param catalogue: state of the cache
	:type catalogue: :py:class:`~.hpda.cache.journal.catalogue.Catalogue`
	:param interval: interval between starting updates
	:type interval: float
	:param variance: variance on update interval
	:type variance: float
	"""
	def __init__(self, pool_mapper, catalogue, interval=20, variance=5):
		self.log = logging.getLogger("publisher.%s"%self.__class__.__name__).log
		ThreadMaster.__init__(self, loop_interval=interval, loop_variance=variance)
		self.log(LVL.STATUS, "Instantiating Publisher (%s)", self.__class__.__name__)
		self.pool_mapper = pool_mapper
		self.catalogue = catalogue
		self.queue = collections.deque()
		self.catalogue.add_file_hook(self)

	def file_insert(self, *file_infos):
		self.queue.append((_INSERT,file_infos))
	def file_update(self, *file_diffs):
		self.queue.append((_UPDATE,file_diffs))
	def file_remove(self, *file_infos):
		self.queue.append((_REMOVE,file_infos))

	def _update_locator(self, locator_node, new_files, del_files):
		proxy = Locator(locator_node)
		proxy.add_file_locations(self.pool_mapper.my_node, *new_files)
		proxy.remove_file_locations(self.pool_mapper.my_node, *del_files)

	def payload(self):
		# copy/clear queue first so we don't need to synchronize any further
		change_blobs = []
		while True:
			try:
				change_blobs.append(self.queue.popleft())
			except IndexError:
				break
		if not change_blobs:
			self.log(LVL.INFO, "No changes to publish")
			return
		new_files, del_files = [], []
		# resolve operations on same file
		file_states = {}
		life_times = {}
		now = time.time()
		for blob in change_blobs:
			# inserted file infos: relevant only if actually assigned to backends
			if blob[0] == _INSERT:
				for file_info in blob[1]:
					if file_info.cache_id not in (NotSet, None):
						file_states[file_info.source_uri] = _NEWFILE
			# updated file infos: relevant only if not previously assigned to backends
			elif blob[0] == _UPDATE:
				for file_diff in blob[1]:
					if "cache_id" in file_diff.updated:
						if file_diff.cache_id not in (NotSet, None) and file_diff.prev_cache_id in (None, NotSet):
							file_states[file_diff.source_uri] = _NEWFILE
			# removed file infos: relevant only if previously assigned to backends
			elif blob[0] == _REMOVE:
				for file_info in blob[1]:
					if file_info.cache_id not in (NotSet, None):
						life_times[file_info.source_uri] = now - file_info.cache_time
						file_states[file_info.source_uri] = _DELFILE
		for source_uri in file_states:
			if file_states[source_uri] == _NEWFILE:
				new_files.append(source_uri)
			else:
				del_files.append({"source_uri": source_uri, "life_time": life_times[source_uri]})
		target_locators = self.pool_mapper.get_nodes_by_role(NodeRole.LOCATOR)
		if not target_locators:
			self.log(LVL.WARNING, "No locators available to publish to")
			return
		self.log(LVL.STATUS, "Updating %d locators on %d additions, %d deletions", len(target_locators), len(new_files), len(del_files))
		# update each locator in separate thread
		update_threads = []
		for locator in target_locators[1:]:
			update_thread = threading.Thread(target=self._update_locator, kwargs={"locator_node":locator, "new_files":new_files, "del_files":del_files})
			update_thread.daemon = True
			update_thread.start()
			update_threads.append(update_thread)
		self._update_locator(locator_node=target_locators[0], new_files=new_files, del_files=del_files)
		# wait for outstanding updates to finish
		for thread in update_threads:
			thread.join()
		self.log(LVL.STATUS, "Finished update of %d locators", len(target_locators))


PublisherConfig = Section(
	target=Publisher,
	attributes={
		"interval" : CfgSeconds(default="20s",descr="Interval between starting updates."),
		"variance" : CfgSeconds(default="5s",descr="Variance on update interval."),
	},
	name="CachePublisher",
	descr="Publishes changes of the cache state to other nodes in regular intervals."
)