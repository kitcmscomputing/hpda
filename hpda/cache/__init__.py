#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.

# standard library imports

# third party imports

# application/library imports

def initialize(pool_mapper, configuration):
	"""
	Initialize the components of this sub-component

	Configures the resources and instantiates the objects this sub-component
	requires for normal operation, given the supplied configuration. Returns the
	threads to be managed by the NodeMaster and the APIs to be published.

	:param pool_mapper: mapper for the pool and its nodes
	:type pool_mapper: :py:class:`~common.pool_mapper.HeartbeatMapper`
	:param configuration: Configuration for this node
	:type configuration: :py:class:`~hpda.utility.configuration.Configuration`
	:return: (list[utility.threads.ThreadMaster],list[object])
	"""
	from hpda.cache.publisher       import PublisherConfig
	from hpda.cache.operator.master import MasterConfig
	from hpda.cache.allocator    import AllocatorConfig
	from hpda.cache.rest_api     import ContentAPI, ComponentAPI
	from hpda.cache.journal.catalogue     import CatalogueConfig
	from hpda.cache.backend.configuration import CacheConfig
	from hpda.cache.storage.configuration import StorageConfig
	thread_masters = []
	server_apis = []
	backend_apis = []
	# State container...
	cache_catalogue = CatalogueConfig.apply(configuration)
	cacheCatalogue = cache_catalogue
	# Backends...
	cache_catalogue.backend_apis.add_cache_apis(
		*CacheConfig.apply(configuration)
	)
	cache_catalogue.backend_apis.add_storage_apis(
		*StorageConfig.apply(configuration)
	)
	# Threads...
	cache_publisher = PublisherConfig.apply(configuration, target_kwargs={"catalogue":cache_catalogue, "pool_mapper":pool_mapper})
	cacheWorker = MasterConfig.apply(configuration, target_kwargs={"catalogue":cache_catalogue})
	cacheAllocator = AllocatorConfig.apply(configuration, target_kwargs={"catalogue":cache_catalogue})

	thread_masters.extend((cacheWorker, cacheAllocator, cache_publisher))
	server_apis.append(ContentAPI(cacheCatalogue, cacheAllocator))
	server_apis.append(ComponentAPI(cacheCatalogue))
	return thread_masters, server_apis
