#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# standard library imports
from threading import Lock
import itertools
import random

# third party imports

# application/library imports
from .base_item import WorkerItem, WorkPaused, WorkExhausted

class CleanCacheState(object):
	"""
	Master state of the :py:class:`~.MaintainFiles`
	"""
	def __init__(self, master):
		self._mutex = Lock()
		self._active = set() # caches currently being checked
		self._caches = master.catalogue.backend_apis.cache_apis.values()

	def get_task(self):
		with self._mutex:
			startd_idx = random.randint(0,len(self._caches)-1)
			for idx in itertools.chain(range(startd_idx, len(self._caches)),range(0,startd_idx)):
				if self._caches[idx] not in self._active:
					self._active.add(self._caches[idx])
					return self._caches[idx]
		raise WorkPaused

	def end_task(self, cache):
		with self._mutex:
			self._active.discard(cache)


class CleanCache(WorkerItem):
	"""
	Cleans up cache backends to avoid unhandled files

	Checks cache backends for external, orphaned or left-over files. Temporary
	files from aborted actions are attempted to be recovered.
	"""
	def __init__(self, master, worker):
		WorkerItem.__init__(self, master=master, worker=worker)
		self.master_state = master.work_state.setdefault(self.__class__.__name__, CleanCacheState(master))
		self._cache = self.master_state.get_task()
		storage_root_paths = [storage_api.raw_mount for storage_api in master.catalogue.backend_apis.storage_apis.values() if hasattr(storage_api, "raw_mount")]
		self._cache_iter = self._cache.consistency_walk(root_paths=storage_root_paths)

	def __repr__(self):
		return "%s(cache=%s, stats=%s)"%(self.__class__.__name__, self._cache.nickname, self._stats)

	def perform(self):
		try:
			cache_api, filename = self._cache_iter.next()
		except StopIteration:
			raise WorkExhausted
		# try guessed source_uris
		for source_uri in self._guess_uris(filename):
			try:
				file_info = self.master.catalogue.state[source_uri]
			except KeyError:
				pass
			else:
				if file_info.cache == self._cache:
					self._add_report("consistent",True)
					return True
		# file should not be on cache - release it
		if cache_api.release_file(filename):
			self._add_report("cleaned",True)
			return True
		self._add_report("cleaned",False)
		return False

	def _guess_uris(self, filename):
		for api in self.master.catalogue.backend_apis.storage_apis.values():
			yield api.guess_uri(filename)
