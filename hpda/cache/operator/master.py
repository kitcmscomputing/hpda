#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.

# standard library imports
import logging
import time

# third party imports

# application/library imports
from hpda.utility.report  import LVL
from hpda.utility.threads import ThreadMaster
from hpda.utility.configuration.interface import Section, CfgInt, CfgSeconds

from .base_item           import WorkExhausted, WorkPaused
from .maintain_files_item import MaintainFiles
from .task_item           import Tasks
from .clean_cache_item    import CleanCache


class MultiThreadWorker(ThreadMaster):
	"""
	Multi-threaded Manager for verifications and updates on the caches

	Master to a pool of worker threads, each handling a single work item at a
	time. By default, threads will iteratively maintain staged files. In
	addition, the master may order individual threads to look for additional
	work items.

	**cleanup**
	  To ensure integrity and consistency, backends are periodically checked for
	  orphaned or left-over files. A **cleanup** worker will inspect backends
	  and either recover or remove any anomalies it finds.

	:param catalogue: state of the cache
	:type catalogue: :py:class:`~.hpda.cache.journal.catalogue.Catalogue`
	:param pool_size: maximum number of worker threads to use for work items of any kind
	:type pool_size: int
	:param cleaner_interval: interval between attempting to spawn a cleaner for a single cache
	:type cleaner_interval: float
	:param cleaner_max_threads: maximum number of worker threads tending to cache cleaning duties
	:type cleaner_max_threads: int
	:param cooldown: delay between work items
	:type cooldown: float
	:param interval: interval between starting work cycle
	:type interval: float
	:param variance: variance on work cycle interval
	:type variance: float
	:param duration: minimum duration of a work cycle
	:type duration: float

	:todo: implement plugable, ``kwargs`` based WorkItem configuration
	"""
	def __init__(self, catalogue, pool_size=2, cleaner_interval=60, cleaner_max_threads=1, cooldown=0.01, interval=60, variance=0, duration=20):
		assert pool_size >=2, "'pool_size' must be at least 2"
		assert pool_size - cleaner_max_threads >= 1, "'cleaner_max_count' must allow for at least one free worker"
		self.log = logging.getLogger("operator.%s"%self.__class__.__name__).log
		ThreadMaster.__init__(self, loop_interval=interval, loop_variance=variance)
		self.log(LVL.STATUS, "Instantiating Operator (%s)", self.__class__.__name__)
		self.catalogue = catalogue
		self.cooldown = cooldown
		self.duration = duration
		self.cleaner_interval, self.cleaner_max_count = cleaner_interval, cleaner_max_threads
		# worker initialisation
		self.work_state = {}
		self._workers = [MultiThreadWorkerThread(master=self) for _ in xrange(pool_size)]
		for worker in self._workers:
			worker.add_work_item(MaintainFiles)
		self._workers[0].add_work_item(Tasks)
		self._last_cleaner_start = 0

	def payload(self):
		stime = time.time()
		# configure new worker tasks
		if time.time() - self._last_cleaner_start > self.cleaner_interval:
			cleaner_count = 0
			for worker in self._workers:
				if worker.has_work_item(CleanCache):
					cleaner_count += 1
					if cleaner_count > self.cleaner_max_count:
						break
			for worker in reversed(self._workers):
				if not worker.has_work_item(CleanCache):
					try:
						new_work = CleanCache(master=self, worker=worker)
					except WorkPaused:
						break
					else:
						worker.add_work_item(new_work)
						self.log(LVL.STATUS, "Adding work item %s", new_work)
						break
		self.log(LVL.STATUS, "Starting %d workers for %d", len(self._workers), self.duration)
		# start all workers
		for worker in self._workers:
			worker.cycle_start()
			worker.start()
		self._should_terminate.wait(self.duration)
		self.log(LVL.STATUS, "Reaping %d workers after %.1f", len(self._workers), time.time()-stime)
		# gracefully reap workers after desired duration
		for worker in self._workers:
			worker.stop()
		# wait for workers to finish their current action
		for worker in self._workers:
			# forcefully terminate blocking workers
			if not worker.join(10 - time.time() + (stime + self.duration)):
				self.log(LVL.WARNING, "Forcefully terminating blocking worker %s", worker)
				while worker.is_alive():
					worker.terminate()
					time.sleep(1)
			worker.cycle_end()
		etime = time.time()
		# collect reports
		reports = {}
		for worker in self._workers:
			worker_report = worker.report
			for key in worker_report:
				reports[key] = [old+new for old, new in zip(worker_report[key], reports.get(key, [0,0]))]
		self.log(LVL.STATUS, "Operator summary: threads %d, duration %.1f, %s", len(self._workers), etime-stime, ", ".join("%s %d/%d"%(key, reports[key][0], reports[key][1]) for key in reports))

	def stop(self):
		# first shut ourselves down so we don't resurrect anything
		ThreadMaster.stop(self)
		# ask workers to gracefully shut down as well
		for worker in self._workers:
			worker.stop()

	def terminate(self):
		# we exit with workers - no need to kill ourselves messily
		ThreadMaster.stop(self)
		# kill workers
		for worker in self._workers:
			worker.terminate()

class MultiThreadWorkerThread(ThreadMaster):
	"""
	A single worker thread of the :py:class:`~.MultiThreadWorker` pool
	"""
	def __init__(self, master):
		ThreadMaster.__init__(self)
		self.log = master.log
		self._master = master
		self._work_items = []

	def __repr__(self):
		return "%s(work=%s)"%(self.__class__.__name__,self._work_items)

	@property
	def loop_interval(self):
		return self._master.cooldown

	@property
	def report(self):
		reports = {}
		for item in self._work_items:
			item_report = item.report
			for key in item_report:
				reports[key] = [old+new for old, new in zip(item_report[key], reports.get(key, [0,0]))]
		return reports

	def clear_work_items(self):
		self._work_items = []

	def has_work_item(self, work_item):
		if type(work_item) != type:
			work_item = type(work_item)
		for item in self._work_items:
			if isinstance(item, work_item):
				return True
		return False

	def add_work_item(self, work_item):
		if type(work_item) == type:
			self.log(LVL.INFO, "Adding work item %s", work_item.__name__)
			self._work_items.insert(0,work_item(master=self._master, worker=self))
		else:
			self.log(LVL.INFO, "Adding work item %s", work_item)
			self._work_items.insert(0,work_item)

	def remove_work_item(self, work_item):
		# existing instance already assigned
		if type(work_item) != type:
			self.log(LVL.INFO, "Removing work item %s", work_item)
			return self._work_items.remove(work_item)
		# generic type for cleanup
		for item in self._work_items[:]:
			if isinstance(item, work_item):
				self._work_items.remove(item)
				self.log(LVL.INFO, "Removing work item %s", item)

	def cycle_start(self):
		for item in self._work_items:
			item.cycle_start()

	def cycle_end(self):
		for item in self._work_items:
			item.cycle_end()

	def payload(self, *args, **kwargs):
		for work_item in self._work_items:
			try:
				work_item.perform()
				break
			except WorkPaused:
				continue
			except WorkExhausted:
				self.remove_work_item(work_item)
				break


MasterConfig = Section(
	target=MultiThreadWorker,
	attributes={
		"pool_size" : CfgInt(default=3, descr="Maximum number of worker threads to use for work items of any kind. Must be ``>2`` and should by ``>1+cleaner_max_threads``."),
		"cleaner_interval"  : CfgSeconds(default="12h",descr="Interval between attempting to spawn a cleaner for a single cache."),
		"cleaner_max_threads" : CfgInt(default=1, descr="Maximum number of worker threads tending to cache cleaning duties."),
		"cooldown" : CfgSeconds(default=0.01,descr="Delay for each worker between a work action."),
		"interval" : CfgSeconds(default="5m",descr="Interval between starting work cycle."),
		"variance" : CfgSeconds(default="10s",descr="Variance on work cycle interval."),
		"duration" : CfgSeconds(default="1m",descr="Minimum duration of a work cycle."),
	},
	name="CacheOperator",
	descr="Maintainer and janitor of the cache. Fetches, validates and unlinks files and performs other maintenance duties."
)