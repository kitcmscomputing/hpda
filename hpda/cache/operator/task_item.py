#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# standard library imports
from threading import Lock

# third party imports

# application/library imports
from hpda.utility.utils   import NotSet
from hpda.utility.report  import LVL
from hpda.cache.fileinfo  import FileViewDiff
from hpda.utility.exceptions    import BackendUnavailable, APIError, BasicException
from .base_item import WorkerItem, WorkPaused


class InvalidTask(BasicException):
	"""A task was found to be invalid/impossible during handling"""
	pass


class TasksState(object):
	"""
	Master state of the :py:class:`~.Tasks`
	"""
	def __init__(self, master):
		self._master = master
		self._mutex = Lock()
		self._files_iter = iter(self._master.catalogue.tasks)

	def get_task(self):
		with self._mutex:
			try:
				return self._files_iter.next()
			except StopIteration:
				# recreate the iterator once it is exhausted
				self._files_iter = iter(self._master.catalogue.tasks)
				raise StopIteration


class Tasks(WorkerItem):
	"""
	Fetches, moves and deletes files to reach the state desired by the Allocator

	The allocation of files to backends is implemented and changed via
	**tasks** defined by the :py:class:`~hpda.cache.allocator.Allocator`.
	A **task** worker will check for and potentially execute queued tasks.
	"""
	def __init__(self, master, worker):
		WorkerItem.__init__(self, master=master, worker=worker)
		self.master_state = master.work_state.setdefault(self.__class__.__name__, TasksState(master))
		self._paused = 0 # delay when work is paused

	def perform(self):
		if self._paused:
			self._paused -= 1
			raise WorkPaused
		try:
			current_task = self.master_state.get_task()
		except StopIteration:
			self._paused = 100
			raise WorkPaused
		self.log(LVL.INFO, "Handling task '%s'", current_task)
		try:
			file_view = FileViewDiff(self.master.catalogue.state[current_task.state.source_uri])
		except KeyError:
			self.master.catalogue.tasks.skip_task(current_task, 'File not known')
			self._add_report("tasks", False)
			return False
		try:
			if self._handle_task(file_view, current_task.state):
				self.apply_file_view(file_view)
				self.master.catalogue.tasks.end_task(current_task)
				self._add_report("tasks", True)
				return True
			self._add_report("tasks", False)
			return False
		except BackendUnavailable:
			self._add_report("tasks", False)
			return False
		except InvalidTask as reason:
			self.log(LVL.WARNING, "Unable to perform task %r", current_task)
			self.master.catalogue.tasks.skip_task(current_task, reason)
			self._add_report("tasks", False)
			return False

	def _handle_task(self, file_view, target_view):
		if target_view.cache_id is None:
			return self.remove_file(file_view)
		if file_view.cache_id == target_view.cache_id:
			return True
		if file_view.cache_id:
			return self.move_file(file_view, target_view)
		if file_view.cache_id in (None,NotSet):
			return self.fetch_file(file_view, target_view)
		raise APIError

