#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# standard library imports
import logging

# third party imports

# application/library imports
from hpda.utility.report  import LVL
from hpda.utility.exceptions    import FileNotFound, BasicException

class WorkExhausted(BasicException):
	"""A :py:class:`~.WorkItem` is completely done"""
	pass
class WorkPaused(BasicException):
	"""A :py:class:`~.WorkItem` is done"""
	pass

class WorkerItem(object):
	"""
	Baseclass for work items to be performed by the Worker

	The :py:class:`~.WorkItem` API relies on three exposed methods:

	:py:meth:`~WorkItem.perform`
	  Periodically called by the Worker to perform a work step.

	:py:meth:`~WorkItem.cycle_start` and :py:meth:`~WorkItem.cycle_end`
	  Called at the start and end of a cycle to set up and tear down resources.

	In addition, the :py:meth:`~.WorkItem.report` provides statistics on the
	current work cycle.

	:note: Items are free to instantiate worker-global resources on the master
	       during ``__init__``, e.g. locks or shared queues. This should be
	       considered **not** thread safe.
	"""
	def __init__(self, master, worker):
		self.master = master
		self.worker = worker
		self.log = logging.getLogger("operator.%s"%self.__class__.__name__).log
		self._stats = {"total":[0,0]}

	def __repr__(self):
		return "%s(stats=%s)"%(self.__class__.__name__, self._stats)

	@property
	def report(self):
		"""
		Provide statistics of the work performed since the last cycle start

		:return: success and total actions per category
		:rtype: dict[str, tuple[int, int]]
		"""
		return self._stats

	def _add_report(self, category, success):
		if not category in self._stats:
			self._stats[category] = [0,0]
		self._stats["total"][1]  += 1
		self._stats[category][1] += 1
		if success:
			self._stats["total"][0]  += 1
			self._stats[category][0] += 1

	def cycle_start(self):
		"""Mark the beginning of a work cycle"""
		self._stats = {"total":[0,0]}

	def cycle_end(self):
		"""Mark the end of a work cycle"""
		pass

	def perform(self):
		"""
		Perform a single step of work

		:raises :py:class:`~.WorkPaused`: if no work is available at the moment
		:raises :py:class:`~.WorkExhausted`: if no more work is available
		"""
		raise WorkExhausted

	def fetch_file(self, file_view, target_view):
		"""
		Fetch a file from storage, put it on cache

		:param file_view: current state of the copy
		:type file_view: :py:class:`~.FileViewDiff`
		:param target_view: desired state of the copy
		:type target_view: :py:class:`~.FileViewDiff`
		"""
		self.log(LVL.INFO, "Fetching file '%s': ( )=>'%s'", file_view.source_uri, target_view.cache_id)
		file_view.cache = target_view.cache
		self.maintain_file(file_view)
		self._add_report("fetched", True)
		return True

	def move_file(self, file_view, target_view):
		"""
		Move a file between caches

		:param file_view: current state of the copy
		:type file_view: :py:class:`~.FileViewDiff`
		:param target_view: desired state of the copy
		:type target_view: :py:class:`~.FileViewDiff`
		"""
		if file_view.cache_id == target_view.cache_id:
			self.log(LVL.WARNING, "Cannot move file '%s': Inplace move order ('%s'=>'%s')", file_view.source_uri, file_view.cache_id, target_view.cache_id)
			return False
		self.log(LVL.INFO, "Moving file '%s': '%s'=>'%s'", file_view.source_uri, file_view.cache_id, target_view.cache_id)
		destination_cache = target_view.cache
		try:
			file_view = destination_cache.migrate_copy(file_view)
			self.master.catalogue.state.update_file_info(file_view)
			self._add_report("moved", True)
			return True
		except FileNotFound:
			self._add_report("moved", False)
			self.log(LVL.WARNING, "Staged file '%s' no longer present on '%s'.", file_view.source_uri, file_view.cache_id)
			return self.fetch_file(file_view, target_view)

	def remove_file(self, file_view):
		"""
		Remove a file from its cache

		:param file_view: current state of the copy
		:type file_view: :py:class:`~.FileViewDiff`
		"""
		self.log(LVL.DEBUG, "Removing file '%s': '%s'=>( )", file_view.source_uri, file_view.cache_id)
		if file_view.cache_id:
			file_view.cache.release_file(file_view)
		file_view.cache = None
		self._add_report("removed", True)
		return True

	def maintain_file(self, file_view):
		"""
		Maintain a file on a cache

		:param file_view: current state of the copy
		:type file_view: :py:class:`~.FileViewDiff`
		"""
		self._add_report("checked", True)
		self.log(LVL.DEBUG, "Maintaining copy '%s'", file_view)
		if file_view.storage.verify_copy(file_view):
			return True
		self.log(LVL.INFO, "Updating outdated copy '%s'", file_view)
		file_view.storage.refresh_copy(file_view)
		self._add_report("updated", True)
		return True

	def apply_file_view(self, file_view):
		"""
		Application of meta data to persistent state

		:param file_view: current state of the copy
		:type file_view: :py:class:`~.FileViewDiff`
		"""
		if file_view.cache is None:
			self.master.catalogue.state.unlink_file_info(file_view)
		else:
			self.master.catalogue.state.update_file_info(file_view)
		return True
