#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# standard library imports
from threading import Lock

# third party imports

# application/library imports
from hpda.utility.utils   import NotSet
from hpda.cache.fileinfo  import FileViewDiff
from .base_item import WorkerItem, WorkPaused

class MaintainFilesState(object):
	"""
	Master state of the :py:class:`~.MaintainFiles`
	"""
	def __init__(self, master):
		self._master = master
		self._mutex = Lock()
		self._active = set() # files currently being serviced
		self._files_iter = iter(self._master.catalogue.state)

	def get_file(self):
		with self._mutex:
			try:
				ret_file = self._files_iter.next()
				while ret_file in self._active:
					ret_file = self._files_iter.next()
				self._active.add(ret_file)
				return ret_file
			except StopIteration:
				# recreate the iterator once it is exhausted
				self._files_iter = iter(self._master.catalogue.state)
				raise StopIteration

	def release_file(self, ret_file):
		with self._mutex:
			self._active.discard(ret_file)


class MaintainFiles(WorkerItem):
	"""
	Validate and, if necessary, update files
	"""
	def __init__(self, master, worker):
		WorkerItem.__init__(self, master=master, worker=worker)
		self.master_state = master.work_state.setdefault(self.__class__.__name__, MaintainFilesState(master))
		self._paused = 0 # delay when work is paused

	def perform(self):
		if self._paused:
			self._paused -= 1
			raise WorkPaused
		try:
			current_uri = self.master_state.get_file()
		except StopIteration:
			self._paused = 10
			raise WorkPaused
		try:
			file_view=FileViewDiff(self.master.catalogue[current_uri])
		except KeyError:
			# race condition - skip this silently, don't report
			return False
		else:
			# nothing to maintain
			if file_view.cache_id is NotSet:
				self._add_report("ignored",True)
				return True
			# file not thoroughly deleted
			if file_view.cache_id is None:
				self.remove_file(file_view)
				self.apply_file_view(file_view)
				return True
			if self.maintain_file(file_view):
				self.apply_file_view(file_view)
				return True
			return False
		finally:
			self.master_state.release_file(current_uri)

