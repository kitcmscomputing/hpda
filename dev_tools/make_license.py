#!/usr/bin/python
# -*- coding: utf-8 -*-
#-# Copyright 2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
"""
Add license information to repository
"""
import subprocess
import argparse
import collections
import datetime
import re
import os
import hashlib
import urllib2

license_header = """
Copyright %s Karlsruhe Institute of Technology

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
""".strip()

notice_fields = [
"""This product includes backported python library code (http://www.python.org)
Copyright (C) 2001-2015 Python Software Foundation.
Licensed under the Python Software Foundation License.""",
]

# symbol sequence at the start of each license line, per file extension
license_symbols = {
	None: "#-# ", # default
	"py": "#-# ",
}
# re for encoding line
encoding_re = "coding[:=]\s*([-\w.]+)"

CLI = argparse.ArgumentParser()
CLI.add_argument(
	"-u",
	"--update-source-files",
	nargs="*",
	help="List of files to update with headers",
	default=(),
)
CLI.add_argument(
	"-l",
	"--fetch-license",
	action="store_true",
	help="List of files to update with headers",
)
CLI.add_argument(
	"-n",
	"--update-notice",
	action="store_true",
	help="Add license notice",
)
CLI.add_argument(
	"--force",
	action="store_true",
	help="Update all licensing information. Overwrites all other settings.",
)

def filehash(filepath, blocksize=65536, maxbytes=float("inf")):
	"""
	Create the sha512 hash of a file's content or portion thereof

	:param filepath: path to file
	:type filepath: str
	:param blocksize: size of blocks read from file
	:type blocksize: int
	:param maxbytes: maximum number of bytes to compare
	:type maxbytes: int or float
	:return:
	"""
	hash = hashlib.sha512()
	with open(filepath, "rb") as filedata:
		bytesread, databuffer = 0, filedata.read(blocksize)
		while databuffer and bytesread < maxbytes:
			hash.update(databuffer)
			bytesread += len(databuffer)
			databuffer = filedata.read(blocksize)
	return hash.hexdigest()


def get_license(repo_base=os.path.join(os.path.dirname(os.path.realpath(__file__)),"..")):
	license = urllib2.urlopen("http://www.apache.org/licenses/LICENSE-2.0")
	with open(os.path.join(repo_base, "LICENSE"),'wb') as output_file:
		output_file.write(license.read())


def get_license_target_files(subject_re=".*.py$|.*.sh$", repo_base=os.path.join(os.path.dirname(os.path.realpath(__file__)),"..")):
	"""
	Compiles a list of all files that require licenses
	"""
	# folders which include files subject to licensing
	base_folders = ["dev_tools", "bin", "hpda", "tools", "unit_tests"]
	for folder in base_folders:
		for dirpath, dirnames, filenames in os.walk(os.path.join(repo_base, folder)):
			for filename in filenames:
				if re.search(subject_re, filename):
					yield os.path.join(dirpath, filename)
	for filename in os.listdir(repo_base):
		filepath = os.path.join(repo_base, filename)
		if os.path.isfile(filepath) and re.search(subject_re, filename):
			yield filepath


def update_header(filepath):
	"""
	Update the license header for a specific file

	:param filepath: file to update with header
	:type filepath: str
	:return: whether the file has changed
	"""
	# exract modification dates
	try:
		file_change_dates = subprocess.check_output(['git', 'log', '--follow', '--format=%ai', filepath]).splitlines()
	except subprocess.CalledProcessError as err:
		if err.returncode != 128:
			raise
		file_change_dates = []
	if file_change_dates:
		first_year = file_change_dates[-1].split('-')[0]
		last_year = file_change_dates[0].split('-')[0]
		if first_year == last_year:
			year = first_year
		else:
			year = first_year + '-' + last_year
	else:
		year = datetime.date.today().year
	# insert license if applicable
	_, file_type = os.path.splitext(filepath)
	license_seq = license_symbols.get(file_type, license_symbols[None])
	done_insert = False
	filepath_tmp = filepath + ".lsc.tmp"
	with open(filepath_tmp, 'w') as output_file, open(filepath) as input_file:
		# iterate lines, adding header directly AFTER shebang and encoding
		for source_line in input_file:
			if not done_insert:
				# always skip existing header, add a new one
				if source_line.startswith(license_seq.strip()):
					continue
				# shebang
				elif source_line.startswith('#!'):
					output_file.write(source_line)
					continue
				# encoding
				elif re.search(encoding_re, source_line):
					output_file.write(source_line)
					continue
				# write header, continue with source
				else:
					header = license_header % year
					for header_line in header.splitlines():
						if header_line:
							output_file.write((license_seq + header_line) + "\n")
						else:
							output_file.write(license_seq.strip() + "\n")
					done_insert = True
			output_file.write(source_line)
	# check content change
	if filehash(filepath, maxbytes=len(license_header)*2) != filehash(filepath_tmp, maxbytes=len(license_header)*2):
		stat = os.stat(filepath)
		os.chmod(filepath_tmp, stat.st_mode)
		os.chown(filepath_tmp, stat.st_uid, stat.st_gid)
		os.rename(filepath_tmp, filepath)
		return True
	os.unlink(filepath_tmp)
	return False


class Contributor(object):
	"""
	Contributor to a git repository

	:param name_str: raw name of the contributor
	:type name_str: str
	"""
	contributors = {}
	def __new__(cls, name_str):
		unified_name = cls.unify_name(name_str)
		try:
			return cls.contributors[unified_name]
		except KeyError:
			contributor = object.__new__(cls, unified_name)
			cls.contributors[unified_name] = contributor
		return contributor

	def __init__(self, name_str):
		if hasattr(self, 'name'):
			return
		self.name = self.unify_name(name_str)
		self.emails = collections.Counter()
		self.commit_count = 0
		self.additions = 0
		self.deletions = 0
		self.files = set()

	@property
	def email(self):
		"""Most used email address"""
		if not self.emails:
			return None
		return sorted(self.emails.iteritems(), key=lambda item: item[1])[-1][0]

	def add_commit(self, email=None):
		"""
		Add general data of a commit
		"""
		self.commit_count += 1
		self.emails[email] += 1

	def add_diff(self, filepath, additions, deletions):
		self.additions += additions
		self.deletions += deletions
		self.files.add(filepath)

	def __str__(self):
		return "%20s, %5d commits, %5d files, %6d additions, %6d deletions, <%20s>" % (
			self.name,
			self.commit_count,
			len(self.files),
			self.additions,
			self.deletions,
			",".join(mail for mail, count in sorted(self.emails.iteritems(), key=lambda item: item[1], reverse=True))
			)

	@staticmethod
	def unify_name(name_str):
		return " ".join([sub_name.capitalize() for sub_name in name_str.split()])


def get_contributors(source_blacklist=(), aliases={}):
	"""
	Get list of contributors weighted by lines of code

	:return:
	"""
	changes = subprocess.check_output(['git', 'log', '--pretty="%H|%aN|%aE"', '--no-merges', '-w', '--numstat']).splitlines()
	total = contributor = Contributor("Total")
	for change_line in changes:
		change_line = change_line.strip().strip('"')
		if not change_line:
			continue
		if '|' in change_line and '@' in change_line:
			commit, author, email = change_line.split('|')
			author = aliases.get(author, author)
			contributor = Contributor(author)
			contributor.add_commit(email)
			total.add_commit(email)
		else:
			additions, deletions, filepath = change_line.split(None, 2)
			additions = 0 if additions == '-' else int(additions)
			deletions = 0 if deletions == '-' else int(deletions)
			contributor.add_diff(filepath, additions, deletions)
			total.add_diff(filepath, additions, deletions)
	for contributor in sorted(Contributor.contributors.values(), key=lambda con: con.commit_count):
		if contributor == total:
			continue
		yield contributor.name, contributor.email


def make_notice(repo_base=os.path.join(os.path.dirname(os.path.realpath(__file__)),"..")):
	notice = ["hpda (https://bitbucket.org/kitcmscomputing/hpda)", ""]
	notice += ["Copyright %s Karlsruhe Institute of Technology" % datetime.date.today().year, ""]
	notice += ["---"]
	notice.extend(notice_fields)
	with open(os.path.join(repo_base, "NOTICE"),'wb') as output_file:
		output_file.write("\n".join(notice))


if __name__ == "__main__":
	options = CLI.parse_args()

	if options.force:
		options.update_source_files = get_license_target_files()
	if options.fetch_license or options.force:
		get_license()
		print "Added LICENSE"
	if options.update_notice or options.force:
		make_notice()
		print "Added NOTICE"
	for file_path in options.update_source_files:
		if update_header(file_path):
			print "Added Header <%s>" % file_path