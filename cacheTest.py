#!/usr/bin/python
#-# Copyright 2014-2015 Karlsruhe Institute of Technology
#-#
#-# Licensed under the Apache License, Version 2.0 (the "License");
#-# you may not use this file except in compliance with the License.
#-# You may obtain a copy of the License at
#-#
#-#     http://www.apache.org/licenses/LICENSE-2.0
#-#
#-# Unless required by applicable law or agreed to in writing, software
#-# distributed under the License is distributed on an "AS IS" BASIS,
#-# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#-# See the License for the specific language governing permissions and
#-# limitations under the License.
# base imports
import os
import socket
import time

# library imports
import argparse
import random

# custom imports
from hpda.utility.utils      import ensureDir, niceBytes
from hpda.client.cacheproxy import CacheProxy
from hpda.utility.addressing import SocketURL

command_parser = argparse.ArgumentParser(
	description="Perform tests on running cache; currently supports only adding random files"
)
command_parser.add_argument(
	'--address',
	help = 'base URL address for cache API',
	default='http://%s:8080'%socket.getfqdn(),
)
command_parser.add_argument(
	'--dir',
	help = 'basedir for randomly generated files',
	default = '/tmp/hpda/external/'
)
command_parser.add_argument(
	'--count',
	help = 'number of randomly generated files',
	default = 12,
	type = int
)
command_parser.add_argument(
	'--size',
	help = 'size in MB of randomly generated files',
	default = 128,
	type = float
)
command_parser.add_argument(
	'--size-range',
	help = 'size range in MB in which to sample file sizes',
	default = 0,
	type = float
)
command_parser.add_argument(
	'--demote-count',
	help = 'number of files to demote score',
	default = 0,
	type = float
)

def randfile(filename, sizeMB = 64):
	if os.path.exists(filename):
		return
	ensureDir(os.path.dirname(filename))
	sizeMB = int(sizeMB)
	print "Writing temporary %s file '%s'" %( niceBytes(sizeMB*1e6), filename),
	with open(filename, 'wb') as newfile:
		for _ in xrange(sizeMB):
			newfile.write(os.urandom(1000)*1000)

def addRandTest(basedir, size, size_range):
	newfile = os.path.join(basedir, '%X' % random.getrandbits(4*16))
	randfile(newfile, sizeMB = ( size - size_range + 2 * random.random() * size_range))
	return newfile

if __name__ == "__main__":
	command_args = command_parser.parse_args()
	chunk_size = 5
	demote_size = 100
	print "Adressing '%s'"%command_args.address
	cache = CacheProxy(SocketURL.from_str(command_args.address))
	cache._bulk_size = 100
	print "Caches:"
	for cache_api in cache.get_cache_features():
		print "=>", cache_api["name"]
		for key in cache_api:
			print "", key, ":", cache_api[key]
	now_content = cache.get_file_list()
	#print now_content
	if command_args.demote_count:
		print "Modifying %d file scores"%command_args.demote_count
		demote_files = [{"source_uri":filename, "score":0} for filename in random.sample(now_content, int(command_args.demote_count))]
		chunks = [demote_files[idx:idx+demote_size] for idx in xrange(0,len(demote_files),demote_size)]
		added = cache.add_files(*chunks)
	print "Creating %d files (%d +/- %d MB)..." % (command_args.count, command_args.size, command_args.size_range)
	new_files_list = []
	for _ in xrange(command_args.count):
		print "\r\t%6d/%d "%(len(new_files_list)+1, command_args.count),
		new_files_list.append(
			{"source_uri":addRandTest(command_args.dir, command_args.size, command_args.size_range)}
		)
	print "\r\tDone", " "*80
	print "Staging..."
	stime = time.time()
	failed, rejected = cache.insert_files(new_files=new_files_list)
	etime = time.time()
	print "Reply: rejected %d, failed %d, total %d, elapsed %fs"%(len(rejected), len(failed), len(new_files_list), etime-stime)
	print "Cache content: %d"%len(cache.get_file_list())