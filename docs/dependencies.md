
# Dependencies #

* `Python` for the middleware
    * `cherrypy` for incoming communiction
    * `crontab` for task scheduling configuration
    * `pycurl` for outgoing communication
    * `sqlalchmey` for database access

* `UnionFS` (or comparable) for caches
* `sqlaclchemy compatible database` for coordinator
    * `mysql` highly recommended

## Python ##

The entire HPDA middleware is written in **Python**, specifically for the `python2`
LTS version `2.7`. Communication between nodes is implemented via the `pycurl`
and `pycharm` packages, which are common but not part of the standard library.
Client (submission) nodes do not require `pycharm`.

For compatibility, all components may be run with **Python** version `2.6` also.

### packages ###

All python packages are available from the **Python Package Index** via
`pip` as well as `easy_install`. Please see the respective tool and package
documentations.

### RHEL6/SL6 ###

`python2.7` is available from the `scl_python27` repository.

`pycurl` depends on the `libcurl` package and must be set to use the correct SSL
library, which is `nss` for a default installation.

```
yum install libcurl-devel
export PYCURL_SSL_LIBRARY=nss
pip2.7 install pycurl
```

## Union File System ##

While the HPDA service itself does not require it, the user experience in POSIX 
environments is vastly simplified by the use of union filesystems (also known as
union mounts). Using a union filesystem, several cache device mounts can easily
be overlayed onto a local or NFS-mounted source storage volume. 
Since these filesystem are tightly interwoven with the VFS layer, they require
kernel support/modification. Availability may vary depending on the distribution. 

**Note:** A union filesystem is only useful on nodes acting as caches.

**Note:** The tool `./tools/mount_unioncache.py` allows to setup a union FS for
          use on a cache node by only specifying available devices.

**Note:** Since caches perform automatic cleanup, they may delete the root of a
          union-mounted branch. To prevent this, make sure the directory
          containing the root is not writeable.

### Ubuntu ###

Ubuntu kernels support the unionFS by default.

### RHEL5/SL5 ###

The RHEL5 line of kernels supports AUFS by default.

### RHEL6/SL6 ###

The RHEL6 kernels (including CentOS and Scientific Linux) do not support AUFS by
default. You may either patch the kernel manually or use a pre-existing upgraded
kernel.

#### CVMFS ####

The CVMFS server repositories offer an up-to-date kernel with AUFS2 support. See
the CVMFS or OASIS documentations for details.

```
rpm -i http://repo.grid.iu.edu/osg/3.2/osg-3.2-el6-release-latest.rpm
rpm -i http://cvmrepo.web.cern.ch/cvmrepo/yum/cvmfs/EL/6/x86_64/cvmfs-release-2-4.el6.noarch.rpm
yum install --enablerepo=cernvm-kernel kernel aufs2-util
```

If the kernel is pulled from a wrong repo, try and be explicit!

`yum install --enablerepo=cernvm-kernel kernel-2.6.32-504.16.2.el6.aufs21.x86_64`

**Note:** SELinux must be set to permissive (recommended) or disabled for AUFS
          to provide write access!
