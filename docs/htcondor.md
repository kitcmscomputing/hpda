# README -  HTCondor #

An HPDA cache cluster can be linked to an HTCondor batch system by using **job_router** hooks on HTC **schedd** machines.
Properly configured, the **job_router** will schedule user jobs to hosts with input files and update the HPDA cluster with job statistics.

## Quick Guide ##

## Requirements ##

* HTCondor cluster, local **schedd** of version 8+
* `curl`, version 7.19 (newer recomended)
* python, version 2.6 to 2.7
	* `pycurl` module
