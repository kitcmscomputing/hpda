# README - sphinx #

An extended documentation can be generated using **sphinx**.
This currently creates a full package code documentation and `man` pages.
 
## Quick Guide ##

* Install **sphinx**, e.g. via `pip install sphinx`
* From the repository folder `./sphinx`, run `./apidoc.sh && make html`
* Open the `./sphinx/_build/html/index.html` in a browser